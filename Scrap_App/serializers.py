from rest_framework import serializers

class YourSerializer(serializers.Serializer):
   """Your data serializer, define your fields here."""
   CaseDetails = serializers.DictField()
   CaseStatus = serializers.DictField()
   PetitionerAdvocate = serializers.CharField(max_length=50000)
   RespondentAdvocate = serializers.CharField(max_length=50000)
   Acts = serializers.ListField()
   History = serializers.ListField()
   Orders = serializers.ListField()
   CaseTransferDetails = serializers.ListField()


class ErrorSerializer(serializers.Serializer):
   CaseDetailsNotFound = serializers.CharField(max_length=50000)
