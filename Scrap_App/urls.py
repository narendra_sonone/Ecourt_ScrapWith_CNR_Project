from django.urls import path, include
from . import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # --test
    path('home/<slug:state>/<slug:dist>/<slug:complex>/<slug:casetype>/<slug:caseno>/<slug:year>/', views.home), # <-- for test only

    # --{ DISTRICT COURT VICE }--
    path('caseno/<slug:state>/<slug:dist>/<slug:complex>/<slug:casetype>/<slug:caseno>/<slug:year>/', views.CaseNoView),
    # path('caseno/state:<slug:state>/dist:<slug:dist>/complex:<slug:complex>/casetype:<slug:casetype>/caseno:<slug:caseno>/year:<slug:year>/', views.CaseNoView),
    path('advocate/<slug:state>/<slug:dist>/<slug:complex>/<slug:advocate>/', views.AdvocateView),
    path('partyname/<slug:state>/<slug:dist>/<slug:complex>/<slug:petitioner>/<slug:year>/', views.PartyNameView),

    # --{ HIGH COURT VICE }--
    # path('highcourt/caseno/state:<slug:state>/dist:<slug:dist>/courtcode:<slug:courtcode>/statename:<slug:statename>/casetype:<slug:casetype>/caseno:<slug:caseno>/year:<slug:year>/', views.HighCourtCaseNoView),
    path('highcourt/caseno/<slug:state>/<slug:dist>/<slug:courtcode>/<slug:statename>/<slug:casetype>/<slug:caseno>/<slug:year>/', views.HighCourtCaseNoView),
    path('highcourt/partyname/<slug:state>/<slug:dist>/<slug:courtcode>/<slug:statename>/<slug:petitioner>/<slug:year>/', views.HighCourtPartyNameView),
    path('highcourt/advocate/<slug:state>/<slug:dist>/<slug:courtcode>/<slug:statename>/<slug:advocate>/', views.HighCourtAdvocateNameView),

    # --{ NCLT VICE }--
    path('nclt/caseno/<slug:zonal_bench>/<slug:case_type>/<slug:case_number>/<slug:case_year>/', views.NcltCaseNoView),
    path('nclt/partyname/<slug:zonal_bench>/<slug:party_type>/<slug:party_name>/<slug:case_year>/<slug:case_status>/', views.NcltPartyNameView),
    path('nclt/advocate/<slug:bench>/<slug:advocate_name>/<slug:year>/', views.NcltAdvocateNameView),


    path('confonet/<slug:state_commission_district_forum>/<slug:state>/<slug:dist>/<slug:from_date>/<slug:to_date>/<slug:select_any_field>/<slug:inputs>/', views.ConfonetView),
    path('confonet/caseno/<slug:state_commission_district_forum>/<slug:state>/<slug:dist>/<slug:case_no>/', views.ConfonetCaseView),

    # --{ INCOME TAX APPELLATE TRIBUNAL VICE }--
    path('itat/appeal_number/<slug:bench>/<slug:appeal_type>/<slug:number>/<slug:year>/', views.ITATView),
    path('itat/assesee_name/<slug:bench>/<slug:appeal_type>/<slug:assesee_name>/', views.ITATAsseseeNameView),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
