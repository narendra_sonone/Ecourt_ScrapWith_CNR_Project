import os
import json
from functools import reduce

from django.conf import settings
from azcaptchaapi import AZCaptchaApi

from Ecourt_ScrapWith_CNR_Project.config import AMAZON_CREDENTIAL

api = AZCaptchaApi('hjvwfkm7xtbp6gc9bcvmtjyqryfzlw8d')

#------------- AWS Credential's ------------------
# AWS Configurations S3
from boto3.session import Session
AWS_ACCESS_KEY_ID = AMAZON_CREDENTIAL['ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = AMAZON_CREDENTIAL['SECRET_ACCESS_KEY']
AWS_STORAGE_BUCKET_NAME = AMAZON_CREDENTIAL['STORAGE_BUCKET_NAME']

session = Session(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
s3 = session.resource('s3')

#-----------------------
import threading
from django.http import HttpResponse, JsonResponse
from bs4 import BeautifulSoup
import cv2
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ChromeOptions
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import random
import time
from PIL import Image, ImageCms, ImageOps, ImageFilter
import asyncio

from django.utils.html import strip_tags
import pytesseract as tess
#----------------------------------------------------------------------
async def home(request, state, dist, complex, casetype, caseno, year):
    start = time.perf_counter()
    #print('st:-')
    #-----------Code start---Firefox--
    '''
    options = FirefoxOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    global driver, res_dct_3
    driver = webdriver.Firefox(executable_path="/home/legtech_deploy/env/selenium_Firefox/driver/geckodriver", service_log_path='/home/legtech_deploy/env/var/log/geckodriver.log', options=options)
    global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found, data, res_dct

    driver.get('https://services.ecourts.gov.in/ecourtindia_v4_bilingual/cases/case_no.php?state=D&state_cd={s}&dist_cd={d}'.format(s=state, d=dist))
    #print('Case_f1--')
    '''
    #-------------------------------------

    #-----------Code start---Chrome--
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    global driver, res_dct_3, f2
    driver_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
    global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found, data, res_dct

    driver_.get('https://services.ecourts.gov.in/ecourtindia_v4_bilingual/cases/case_no.php?state=D&state_cd={s}&dist_cd={d}'.format(s=state, d=dist))
    #print('Case_f1--')


    WebDriverWait(driver_, 900).until(EC.presence_of_element_located((By.ID, "caseNoDet")))  # wait
    WebDriverWait(driver_, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="captcha_image_audio_controls"]/a')))  # wait audio btn
    WebDriverWait(driver_, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')))  # wait refresh
    WebDriverWait(driver_, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="caseNoDet"]/div[8]/span[3]/input[1]')))  # wait Go btn
    
    await asyncio.sleep(5)
    f = driver_.session_id
    
    element = driver_.find_element_by_xpath("//*[@id='captcha_image']")
    location = element.location
    size = element.size
    
    await asyncio.sleep(2)

    driver_.save_screenshot(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCASE.png')   # str(threading.get_native_id())
                            
    x = location['x']
    y = location['y']
    width = location['x'] + size['width']
    height = location['y'] + size['height']
    im = Image.open(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCASE.png')
    im = im.crop((int(x), int(y), int(width), int(height)))
    im.save(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCASE.png')

    await asyncio.sleep(2)

    driver_.quit()
    try:
        f2 = driver_.session_id
    except:
        pass

    #time.sleep(5)
    s = state
    #await asyncio.sleep(2)
    d = dist
    #await asyncio.sleep(2)
    c1 = complex
    #await asyncio.sleep(2)
    c2 = casetype
    #await asyncio.sleep(2)
    c3 = caseno
    #await asyncio.sleep(2)
    y = year
    #await asyncio.sleep(2)

    th_id = str(threading.get_native_id())
    #print("home:" + str(threading.get_native_id()))

    finish3 = time.perf_counter()
    #print(f'{str(threading.get_native_id())} home Finished in {round(finish3 - start, 2)} second(s)')
    return HttpResponse(f'Thread Id:- {th_id}, Time:- {round(finish3 - start, 2)}, driver ID:- {f}, home:- {s, d, c1, c2, c3, y}, driver ID_2:- {f2}')
#----------------------------------------------------------------------

#===========================================================================================
                                # { DISTRICT COURT VICE } #
#===========================================================================================

#==================---{ CASE NO SEARTCH }-----=============
#@sync_to_async(thread_sensitive=True)
async def CaseNoView(request, state, dist, complex, casetype, caseno, year):
    path = '/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/Scrap_App/files/'
    start = time.perf_counter()
    #print(f'Case1 input data:--- , {str(threading.get_native_id())}, {state, dist, complex, casetype, caseno, year}')

    #-----------Code start-----
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    global driver, res_dct_3
    driver_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
    global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found, data, res_dct

    driver_.get('https://services.ecourts.gov.in/ecourtindia_v4_bilingual/cases/case_no.php?state=D&state_cd={s}&dist_cd={d}'.format(s=state, d=dist))
    #print('Case_f1--')

    driver_Id = driver_.session_id

    WebDriverWait(driver_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "body")))  # wait

    # <--- all repeated Functions ----/////---
    def dynamicdelay(id_name):
        #print('dynamicdelay:-', id_name)
        global all_options
        loop_1 = 1
        while True:
            L = 0
            if loop_1 == 1:
                #print('D while:-')

                WebDriverWait(driver_, 900).until(EC.presence_of_element_located((By.ID, "caseNoDet")))  # wait
                WebDriverWait(driver_, 900).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="captcha_image_audio_controls"]/a')))  # wait audio btn
                WebDriverWait(driver_, 900).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')))  # wait refresh
                WebDriverWait(driver_, 900).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="caseNoDet"]/div[8]/span[3]/input[1]')))  # wait Go btn
                time.sleep(1)

                html = driver_.page_source
                soup = BeautifulSoup(html, "html.parser")
                all_divs = soup.find('select', {'id': str(id_name)})
                #print('all_divs:-', all_divs)

                all_options = all_divs.find_all('option')
                # print('all_options:-', all_options)
                loop_1 = len(all_options)
                # print('--', loop_1)
                L = 0
            else:
                L = 1

            if L == 1:
                # print('D break:-')
                break

        return all_options

    def delay1():
        time.sleep(random.randint(1, 2))
        #await asyncio.sleep(1)

    def delay2():
        time.sleep(random.randint(2, 2))
        #await asyncio.sleep(2)

    #<--Main (try)---
    try:
        html = driver_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        print('h1_data:-', h1_data)

        # <--Main (if)---
        if h1_data == None:
            #print('h1:-', h1_data)

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    WebDriverWait(driver_, 900).until(EC.presence_of_element_located((By.ID, "caseNoDet")))  # wait
                    WebDriverWait(driver_, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="captcha_image_audio_controls"]/a')))  # wait audio btn
                    WebDriverWait(driver_, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')))  # wait refresh
                    WebDriverWait(driver_, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="caseNoDet"]/div[8]/span[3]/input[1]')))  # wait Go btn
                    #delay2()
                    #delay1()
                    await asyncio.sleep(3)
                    # <------COURT COMPLEX--///---start---///-->
                    
                    html = driver_.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    all_options = all_divs.find_all('option')
                    #print('all_options:-', all_options)
                    
                    
                    #dynamicdelay(id_name='court_complex_code')

                    k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['0', '28', '2', '6',..]
                    # print('K3:-', k3)

                    # <---- take input complex_ value from fun()
                    com = complex.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver_.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()

                    # <------ Select 'COMPLEX' Options ---->
                    complex = \
                    driver_.find_elements_by_xpath('//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    #print('selected complex: ', complex.text)
                    complex.click()
                    #delay2()  # required
                    #delay1()
                    await asyncio.sleep(3)

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver_, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="case_type"]')))  # <---- wait --->

                    html = driver_.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    all_options = all_divs.find_all('option')
                    #print('all_options:-', all_options)

                    #dynamicdelay(id_name='case_type')

                    k4 = [str(n1).split('"')[1] for n1 in all_options]
                    # print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(all_options:-)', len(all_options))

                    # <---- take input state value from fun() & find index of that input value ----
                    index4 = k4.index(str(casetype))
                    case_option = index4 + 1  # op-- 20th actual is 21th

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver_.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    #delay1()  # required
                    await asyncio.sleep(1)

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver_.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[
                        0]  # e.x. option 1
                    #print('selected case: ', case.text)
                    case.click()
                    #delay1()
                    await asyncio.sleep(1)
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    WebDriverWait(driver_, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait

                    case_elem = driver_.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(str(caseno))  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver_, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver_.find_element(By.ID, "rgyear")
                    year_elem.send_keys(str(year))  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver_.find_element_by_xpath('//*[@id="captcha_container_2"]/div[1]/div/span[3]/a').click() # refresh btn
                        delay1()
                        driver_.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        delay1()
                        WebDriverWait(driver_, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="caseNoDet"]/div[8]/span[3]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        '''
                        driver.execute_script("document.body.style.zoom='190%'")
                        driver.execute_script("window.scrollTo(1, 600);")
                        delay1()
                        '''
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        start_i = time.perf_counter()
                        element = driver_.find_element_by_xpath("//*[@id='captcha_image']")
                        location = element.location
                        size = element.size
                        # --------------(for SHOW chrome)--------
                        '''
                        location = {'x': 39, 'y': 265}
                        size = {'height': 86, 'width': 310}

                        # # --------------(for hide windo)--------
                        # location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        # size = {'height': 59, 'width': 310}
                        # # ----------------------------------------
                        '''

                        driver_.save_screenshot(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCASE.png')   # str(threading.get_native_id())
                        
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCASE.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCASE.png')
                        #im.show()  # <--- comment it ----
                        #print("1'st while image:------")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        # -----------------------------------------------------------------------------
                        # # --------API CODE--------------
                        start_A = time.perf_counter()
                        with open(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCASE.png', 'rb') as captcha_file:
                            captcha_op = api.solve(captcha_file)
                        captcha_txt2 = captcha_op.await_result()
                        #print('API code captcha text:--', captcha_txt2)
                        #print('len(captcha_txt2):--', len(str(captcha_txt2))) #<-- len 6
                        #print('type(captcha_txt2):--', type(captcha_txt2))     #<-- type (str)

                        finish_A = time.perf_counter()
                        #print(f'Captcha reading Finished in {round(finish_A - start_A, 2)} second(s)')
                        # #----------------------

                        # <------- Find_data--------/// END ///---------------------------
                        ##captcha_txt2 = 'abcdefff'     #<--- for test only
                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 6:
                                #print('!=6 -----')

                                # ------------------------------
                                driver_.find_element_by_xpath(
                                    '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a').click()  # refresh btn
                                #delay1()
                                await asyncio.sleep(1)
                                driver_.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                #delay1()
                                await asyncio.sleep(1)

                                WebDriverWait(driver_, 600).until(EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="caseNoDet"]/div[8]/span[3]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                '''
                                driver.execute_script("document.body.style.zoom='190%'")
                                driver.execute_script("window.scrollTo(1, 600);")
                                delay1()
                                '''
                                # ------------------------------------------

                                # <--- to find captcha image location ---->
                                start_i = time.perf_counter()
                                element = driver_.find_element_by_xpath("//*[@id='captcha_image']")
                                location = element.location
                                size = element.size
                                # --------------(for SHOW chrome)--------
                                '''
                                location = {'x': 39, 'y': 265}
                                size = {'height': 86, 'width': 310}

                                # # --------------(for hide windo)--------
                                # location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                # size = {'height': 59, 'width': 310}
                                # # ----------------------------------------
                                '''

                                driver_.save_screenshot(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCASE.png')
                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCASE.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCASE.png')
                                #im.show()  # <--- comment it ----
                                #print("2'nd while image:------")
                                # <--------// end //----

                                # <---- for Captcha image to text convert---------------comment it------------
                                # # --------API CODE--------------
                                startC = time.perf_counter()
                                with open(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCASE.png', 'rb') as captcha_file:
                                    captcha_op = api.solve(captcha_file)
                                captcha_txt2 = captcha_op.await_result()
                                #print('API code captcha text:--', captcha_txt2)
                                #print('len(captcha_txt2):--', len(str(captcha_txt2)))
                                #print('type(captcha_txt2):--', type(captcha_txt2))

                                finishC = time.perf_counter()
                                #print(f'Captcha re-reading Finished in {round(finishC - startC, 2)} second(s)')
                                # # ----------------------

                                #captcha_txt2 = 'abcddd'
                                if len(captcha_txt2) != 6:
                                    #print('if len != 6--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('I', 'l').replace('i', 'l')
                                    #print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('I', 'l').replace('i', 'l')
                                #print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                #time.sleep(3)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                #print('finaly break done in <6 or >6-- (if length_ch2 == 1:)----- ')
                                break





                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver_.find_element_by_xpath('//*[@id="captcha"]')
                        input_element.send_keys(str(re_captcha_txt_final))  ##### <---- Pass text ---->
                        #print('send_keys done------')
                        #time.sleep(3) #-- remove_it
                        #delay1()
                        await asyncio.sleep(1)
                        # <----- for Click Search btn---------------------
                        driver_.find_element_by_xpath('//*[@id="caseNoDet"]/div[8]/span[3]/input[1]').click()  # <--- sertch btn --->
                        #print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        #delay2()
                        #delay1()
                        await asyncio.sleep(2)

                        html = driver_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display:none;")/("display: block;")

                        #print('-------------------------------')
                        #print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        #print('-------------------------------')
                        # -------------------------------------------------------------------------------
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '' or op_page_errSpanDiv == "display:block;":
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            #print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            #print('check2:-first---', check2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                #delay1()
                                #print('check2---second---', check2)
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                #print('check2-----third---', check2)
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver_.quit()
                                refresh = 1
                            
                        else:
                            #print('if-else-op_page_errSpanDiv == ')
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            #print('if refresh == 1:---')
                            break
                    if Record_not_found == 1:
                        #print('if Record_not_found == 1:---')
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        #print('elif Record_not_found == 0:---')
                        WebDriverWait(driver_, 900).until(EC.presence_of_element_located((By.ID, "showList")))  # < wait
                        WebDriverWait(driver_, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="showList1"]/tr[2]/td[4]/a')))  # < wait

                        driver_.execute_script("window.scrollTo(0,370);")
                        time.sleep(1)
                        WebDriverWait(driver_, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="showList1"]/tr[2]/td[4]/a')))
                        driver_.find_elements_by_xpath('//*[@id="showList1"]/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # -------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver_, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="back_top"]/center/a')))
                        #delay2()
                        await asyncio.sleep(2)
                        html = driver_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            cnr_text = driver_.find_elements_by_xpath('//*[@id="secondpage"]/div[2]/div[1]/b/span')[0].text
                            #print('cnr_text:--', {'CnrNumber': str(cnr_text).replace('CNR Number\n:', '')})
                            res_dct = {'ThreadId': str(threading.get_native_id()), 'CnrNumber': str(cnr_text).replace('CNR Number\n:', '')}  # final o/p


                        except Exception as e:
                            # print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}



                        if res_dct != {}:
                            case_final = res_dct
                            try:
                                driver_.quit()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {'ThreadId': str(threading.get_native_id()), "CaseDetailsNotFound": 'This Case Code does not exists/Data not available.'}
                            try:
                                driver_.quit()
                                break
                            except:
                                pass

                        else:
                            try:
                                driver_.quit()
                                break
                            except:
                                pass

                        main_ch2 = 1

                except Exception as e:
                    try:
                        driver_.quit()
                        #print('except Exception-----', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
                    except:
                        pass
                if main_ch2 == 1:
                    break

            # if res_dct != {}:
            #     yourdata = case_final
            #
            # elif res_dct == {}:
            #     yourdata = case_final_2

            # data = yourdata                 # Final response
            # print('data:--', data)

        else:
            #print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driver_.quit()
    except Exception as e:
        try:
            #print('except crser here--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driver_.quit()
        except:
            pass
    finally:
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2



        data = yourdata                 # Final response
        # print('data:--', data)
        # ----------------

    #finish3 = time.perf_counter()
    #print(f'{str(threading.get_native_id())} home Finished in {round(finish3 - start, 2)} second(s)')

    #return HttpResponse(f'Thread id:- {str(threading.get_native_id())}, driver_Id:- {driver_Id}, JSON_data:-{data}, Input data:- {state, dist, str(complex), casetype, caseno, year}, Request Finished in:- {round(finish3 - start, 2)} second(s)'.replace("'", '"'))
    #return HttpResponse(str(data).replace("'", '"').replace("-", ''))
    return JsonResponse(data)

#==================---{ ADVOCATE SEARCH }-----=============
async def AdvocateView(request, state, dist, complex, advocate):
    global data, res_dct, case_final_2, yourdata, re_captcha_txt_final, Record_not_found, case_final, list_res_dct
    #start = time.perf_counter()

    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    driverA_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)

    driverA_.get(
        'https://services.ecourts.gov.in/ecourtindia_v4_bilingual/cases/qs_civil_advocate.php?state=D&state_cd={s}&dist_cd={d}'.format(
            s=state, d=dist))
    #print('input data:--', state,',', dist,',', complex,',', advocate)
    #print('Advocate_func--')

    WebDriverWait(driverA_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "body")))  # wait


    #=====--{ all repeated Functions }--======
    def delay1_sec():
        time.sleep(random.randint(1, 2))

    def delay2_sec():
        time.sleep(random.randint(2, 2))

    #====---{ main try }----===============
    try:
        #===--{ check parrent site working or note }--===
        html = driverA_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        #print('h1_data:-', h1_data)

        # ====---{ main (if) }----===============
        if h1_data == None:
            #print('h1:-', h1_data)
            while True:
                main_ch2 = 0
                # ===-{inner Try}--==
                try:
                    WebDriverWait(driverA_, 900).until(EC.presence_of_element_located((By.ID, "caseNoDet")))  # wait
                    await asyncio.sleep(3)

                    #===-{select court complex}--==
                    html = driverA_.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    all_options = all_divs.find_all('option')

                    k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['0', '28', '2', '6',..]
                    #print('K3:-', k3)

                    #===----{ take input complex_ value from fun() }----====
                    com = complex.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    #===---{ Click on 'COMPLEX' DROP DOWN }---====
                    driverA_.find_elements(by=By.ID, value="court_complex_code")[0].click()

                    #===---{ Select 'COMPLEX' Options }---===
                    complex = \
                        driverA_.find_elements(by=By.XPATH, value=
                            '//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    #print('selected complex: ', complex.text)
                    complex.click()
                    # delay2()  # required
                    # delay1()
                    await asyncio.sleep(3)

                    #===---{ ADVOCATE NAME input field }====------
                    WebDriverWait(driverA_, 600).until(
                        EC.presence_of_element_located((By.ID, "advocate_name")))  # wait

                    advocate_elem = driverA_.find_element(By.ID, "advocate_name")
                    advo = str(advocate).replace('_', ' ').replace('-', ' ')
                    advocate_elem.send_keys(advo)  ##### <---- Pass advocate name ---->
                    # <----------------->

                    #=================-----{ CAPTCHA CODE }------==============
                    while True:
                        refresh = 0
                        driverA_.find_elements(by=By.XPATH, value=
                            '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')[0].click()  # refresh btn
                        delay1_sec()
                        driverA_.find_elements(by=By.ID, value="captcha")[0].click()  # for reduce black sqr
                        delay1_sec()

                        #===---{ to find captcha image LOCATION }====------
                        start_i = time.perf_counter()
                        element = driverA_.find_elements(by=By.ID, value="captcha_image")[0]
                        location = element.location
                        size = element.size
                        #print('location:-', location, 'size:-', size)
                        driverA_.save_screenshot(
                            settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}imageADVOCATE.png')

                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(
                            settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}imageADVOCATE.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}cropimageADVOCATE.png')
                        #im.show()  # <--- comment it ----

                        #=====---{ CAPTCHA IMAGE TO TEXT }---====
                        #--------API CODE--------------
                        with open(settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}cropimageADVOCATE.png', 'rb') as captcha_file:
                            captcha_op = api.solve(captcha_file)
                        captcha_txt2 = captcha_op.await_result()
                        # print('API code captcha text:--', captcha_txt2)
                        # print('len(captcha_txt2):--', len(str(captcha_txt2))) #<-- len 6
                        # print('type(captcha_txt2):--', type(captcha_txt2))     #<-- type (str)
                        # #----------------------

                        #=====---{ FIND DATA }---====
                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 6:
                                #print('!=6 -----')

                                driverA_.find_elements(by=By.XPATH, value=
                                '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')[0].click()  # refresh btn
                                delay1_sec()
                                driverA_.find_elements(by=By.XPATH, value='//*[@id="captcha"]')[
                                    0].click()  # for reduce black sqr
                                delay1_sec()

                                # ===---{ to find captcha image LOCATION }====------
                                start_i = time.perf_counter()
                                element = driverA_.find_elements(by=By.XPATH, value="//*[@id='captcha_image']")
                                location = element.location
                                size = element.size

                                driverA_.save_screenshot(
                                    settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}imageADVOCATE.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(
                                    settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}imageADVOCATE.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(
                                    settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}cropimageADVOCATE.png')
                                #im.show()  # <--- comment it ----
                                #print("2'nd while image:------")

                                # =====---{ CAPTCHA IMAGE TO TEXT }---====
                                # --------API CODE--------------
                                with open(
                                        settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}cropimageADVOCATE.png',
                                        'rb') as captcha_file:
                                    captcha_op = api.solve(captcha_file)
                                captcha_txt2 = captcha_op.await_result()
                                # print('API code captcha text:--', captcha_txt2)
                                # print('len(captcha_txt2):--', len(str(captcha_txt2)))  # <-- len 6
                                # print('type(captcha_txt2):--', type(captcha_txt2))  # <-- type (str)
                                # #----------------------

                                # captcha_txt2 = 'abcddd'
                                if len(captcha_txt2) != 6:
                                    #print('if len != 6--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('I', 'l').replace('i', 'l')
                                    #print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1

                            else:
                                re_captcha_txt_final = captcha_txt2.replace('I', 'l').replace('i', 'l')
                                #print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                #time.sleep(3)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                #print('finaly break done in <6 or >6-- (if length_ch2 == 1:)----- ')
                                break

                        #======-----{ ENTER CAPTCHA IMG TEXT to INPUT FIELD }-------=====
                        input_element = driverA_.find_element_by_xpath('//*[@id="captcha"]')
                        input_element.send_keys(str(re_captcha_txt_final))  ##### <---- Pass text ---->
                        #print('send_keys done------')

                        await asyncio.sleep(1)
                        # <----- for Click Search btn---------------------
                        driverA_.find_element_by_name("submit1").click()  # <--- sertch btn --->
                        #print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        # delay2()
                        # delay1()
                        await asyncio.sleep(2)

                        html = driverA_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display:none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------
                        # -------------------------------------------------------------------------------
                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '' or op_page_errSpanDiv == "display:block;":
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            #print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            #print('check2:-first---', check2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                #delay1()
                                #print('check2---second---', check2)
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                #print('check2-----third---', check2)
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driverA_.quit()
                                refresh = 1

                        else:
                            #print('if-else-op_page_errSpanDiv == ')
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            #print('if refresh == 1:---')
                            break
                    if Record_not_found == 1:
                        #print('if Record_not_found == 1:---')
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        #print('elif Record_not_found == 0:---')
                        WebDriverWait(driverA_, 900).until(EC.presence_of_element_located((By.ID, "showList")))  # < wait
                        time.sleep(1)

                        #<-------/// code for extract only CASE NO. & CNR NO. ///----------
                        #<-------/// get <a> tag data ///----------
                        a_tag_data = driverA_.find_elements(by=By.TAG_NAME, value="a")
                        try:
                            a_list=[]
                            for lnk in a_tag_data:
                                a_list.append(str(lnk.get_attribute('onclick')))
                            #print('a_list:-', a_list)   #viewHistory('201500003992018','MHNS010066912018','1');return false;

                            list_res_dct = {}
                            for l in a_list:
                                if 'viewHistory' in l:
                                    k3 = str(l).split("'")
                                          # ['viewHistory(', '201500003992018', ',', 'MHNS010066912018', ',', '1', ');return false;']
                                    #print([k3[1], k3[3]])  #..get this..['201500003992018', 'MHNS010066912018']
                                    lst = [k3[1], k3[3]]

                                    # ---- list into dict convert ----[] --> {}---
                                    res_dct = {lst[i]: lst[i + 1] for i in range(0, len(lst), 2)}
                                    # print(res_dct)      #.. {'201500003992018': 'MHNS010066912018'}
                                    list_res_dct.update(res_dct)
                            #print('list_res_dct:-', list_res_dct)
                        except:
                            pass
                        #---------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        html = driverA_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        #-----------------------
                        try:
                            table_data = soup.find('table', attrs={'id': 'showList3'})
                            #print('---------1-----------')
                            h, [_, *d] = [i.text for i in table_data.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_data.find_all('tr')]
                            res_dct_data = [dict(zip(h, i)) for i in d]
                            #print('res_dct_data:--', res_dct_data)

                            k5 = []
                            for d in res_dct_data:
                                if d['Sr No'].isdigit():
                                    k5.append({k.replace(' ', ''): v for k, v in d.items()})
                            # print(k5)

                            #--------------------------------------
                            final_list = []
                            for f_dict in k5:
                                # print(f_dict)
                                string = f_dict['CaseStage']
                                res = ''.join(filter(lambda i: i.isdigit(), string))    #..extract all 'digits' from str
                                # print(res)
                                f_dict['CaseStage'] = res  #..update dict with digit '203400040542019'
                                # print(f_dict)
                                final_list.append(f_dict)  # .. append in list

                            #print(final_list)
                            #--------------------------------------

                            for k, v in list_res_dct.items():
                                for dic in final_list:
                                    if dic['CaseStage'] == k:
                                        dic['cnr'] = v
                                        # print('k')

                            #print(final_list)
                            # --------------------------------------
                            res_dct = {"TableData": final_list}
                            # --------------------------------------

                        except Exception as e:
                            #print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}

                        # ---------------------------------------------------------------------------------------------
                        if res_dct != {}:
                            case_final = res_dct
                            try:
                                driverA_.quit()
                                #print('--driverA_.quit()')
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {'ThreadId': str(threading.get_native_id()), "CaseDetailsNotFound": 'This Case Code does not exists/Data not available.'}
                            try:
                                driverA_.quit()
                                #print('--driverA_.quit(2)')
                                break
                            except:
                                pass

                        else:
                            try:
                                driverA_.quit()
                                #print('--driverA_.quit(3)')
                                break
                            except:
                                pass

                        main_ch2 = 1


                # ===-{inner Try Exception}--==
                except Exception as e:
                    try:
                        #print('inner Try Exception:---', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
                        driverA_.quit()
                    except:
                        pass
                if main_ch2 == 1:
                    break


        else:
            #print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driverA_.quit()
    #===---{main try Exception }--===
    except Exception as e:
        try:
            #print('main try Exception:--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driverA_.quit()
        except:
            pass
    finally:
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2



        data = yourdata                 # Final response
        # print('data:--', data)
        # ----------------

    # finish3 = time.perf_counter()
    # print(f'{str(threading.get_native_id())} Advocate Finished in {round(finish3 - start, 2)} second(s)')

    #return HttpResponse(f'Thread id:- {str(threading.get_native_id())}, driver_Id:- {driver_Id}, JSON_data:-{data}, Input data:- {state, dist, str(complex), casetype, caseno, year}, Request Finished in:- {round(finish3 - start, 2)} second(s)'.replace("'", '"'))
    #return HttpResponse(str(data).replace("'", '"').replace("-", ''))
    return JsonResponse(data)
    #-------{ main try end}----------------

#==================---{ PARTY NAME SEARCH }-----=============
async def PartyNameView(request, state, dist, complex, petitioner, year):
    global data, res_dct, case_final_2, yourdata, re_captcha_txt_final, Record_not_found, case_final, list_res_dct, dict_res_dct
    #start = time.perf_counter()

    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    driverP_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)

    driverP_.get(
        'https://services.ecourts.gov.in/ecourtindia_v4_bilingual/cases/ki_petres.php?state=D&state_cd={s}&dist_cd={d}'.format(
            s=state, d=dist))
    # print('input data:-Case Party-', state,',', dist,',', complex,',', petitioner)
    #print('Petitioner_func--')

    WebDriverWait(driverP_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "body")))  # wait

    #=====--{ all repeated Functions }--======
    def delay1_sec():
        time.sleep(random.randint(1, 2))

    def delay2_sec():
        time.sleep(random.randint(2, 2))

    #====---{ main try }----===============
    try:
        #===--{ check parrent site working or note }--===
        html = driverP_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        #print('h1_data:-', h1_data)

        # ====---{ main (if) }----===============
        if h1_data == None:
            #print('h1:-', h1_data)
            while True:
                main_ch2 = 0
                # ===-{inner Try}--==
                try:
                    WebDriverWait(driverP_, 900).until(EC.presence_of_element_located((By.ID, "caseNoDet")))  # wait
                    await asyncio.sleep(3)

                    #===-{select court complex}--==
                    html = driverP_.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    all_options = all_divs.find_all('option')

                    k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['0', '28', '2', '6',..]
                    #print('K3:-', k3)

                    #===----{ take input complex_ value from fun() }----====
                    com = complex.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    #===---{ Click on 'COMPLEX' DROP DOWN }---====
                    driverP_.find_elements(by=By.ID, value="court_complex_code")[0].click()

                    #===---{ Select 'COMPLEX' Options }---===
                    complex = \
                        driverP_.find_elements(by=By.XPATH, value=
                            '//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    #print('selected complex: ', complex.text)
                    complex.click()
                    # delay2()  # required
                    # delay1()
                    await asyncio.sleep(3)

                    #===---{ ADVOCATE NAME input field }====------
                    WebDriverWait(driverP_, 600).until(
                        EC.presence_of_element_located((By.ID, "petres_name")))  # wait

                    advocate_elem = driverP_.find_element(By.ID, "petres_name")
                    pet = str(petitioner).replace('_', ' ').replace('-', ' ')
                    advocate_elem.send_keys(pet)  ##### <---- Pass advocate name ---->
                    # <----------------->

                    # ===---{ YEAR input field }====------
                    WebDriverWait(driverP_, 600).until(
                        EC.presence_of_element_located((By.ID, "rgyear")))  # wait

                    advocate_elem = driverP_.find_element(By.ID, "rgyear")
                    advocate_elem.send_keys(str(year))  ##### <---- Pass advocate name ---->
                    # <----------------->

                    #=================-----{ CAPTCHA CODE }------==============
                    while True:
                        refresh = 0
                        driverP_.find_elements(by=By.XPATH, value=
                            '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')[0].click()  # refresh btn
                        delay1_sec()
                        driverP_.find_elements(by=By.ID, value="captcha")[0].click()  # for reduce black sqr
                        delay1_sec()

                        #===---{ to find captcha image LOCATION }====------
                        start_i = time.perf_counter()
                        element = driverP_.find_elements(by=By.ID, value="captcha_image")[0]
                        location = element.location
                        size = element.size
                        #print('location:-', location, 'size:-', size)
                        driverP_.save_screenshot(
                            settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}imagePARTYNAME.png')

                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(
                            settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}imagePARTYNAME.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}cropimagePARTYNAME.png')
                        #im.show()  # <--- comment it ----

                        #=====---{ CAPTCHA IMAGE TO TEXT }---====
                        #--------API CODE--------------
                        with open(settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}cropimagePARTYNAME.png', 'rb') as captcha_file:
                            captcha_op = api.solve(captcha_file)
                        captcha_txt2 = captcha_op.await_result()
                        # print('API code captcha text:--', captcha_txt2)
                        # print('len(captcha_txt2):--', len(str(captcha_txt2))) #<-- len 6
                        # print('type(captcha_txt2):--', type(captcha_txt2))     #<-- type (str)
                        # #----------------------

                        #=====---{ FIND DATA }---====
                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 6:
                                #print('!=6 -----')

                                driverP_.find_elements(by=By.XPATH, value=
                                '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')[0].click()  # refresh btn
                                delay1_sec()
                                driverP_.find_elements(by=By.XPATH, value='//*[@id="captcha"]')[
                                    0].click()  # for reduce black sqr
                                delay1_sec()

                                # ===---{ to find captcha image LOCATION }====------
                                start_i = time.perf_counter()
                                element = driverP_.find_elements(by=By.XPATH, value="//*[@id='captcha_image']")
                                location = element.location
                                size = element.size

                                driverP_.save_screenshot(
                                    settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}imagePARTYNAME.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(
                                    settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}imagePARTYNAME.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(
                                    settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}cropimagePARTYNAME.png')
                                #im.show()  # <--- comment it ----
                                #print("2'nd while image:------")

                                # =====---{ CAPTCHA IMAGE TO TEXT }---====
                                # --------API CODE--------------
                                with open(
                                        settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}cropimagePARTYNAME.png',
                                        'rb') as captcha_file:
                                    captcha_op = api.solve(captcha_file)
                                captcha_txt2 = captcha_op.await_result()
                                # print('API code captcha text:--', captcha_txt2)
                                # print('len(captcha_txt2):--', len(str(captcha_txt2)))  # <-- len 6
                                # print('type(captcha_txt2):--', type(captcha_txt2))  # <-- type (str)
                                # #----------------------

                                # captcha_txt2 = 'abcddd'
                                if len(captcha_txt2) != 6:
                                    #print('if len != 6--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('I', 'l').replace('i', 'l')
                                    #print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1

                            else:
                                re_captcha_txt_final = captcha_txt2.replace('I', 'l').replace('i', 'l')
                                #print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                #time.sleep(3)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                #print('finaly break done in <6 or >6-- (if length_ch2 == 1:)----- ')
                                break

                        #======-----{ ENTER CAPTCHA IMG TEXT to INPUT FIELD }-------=====
                        input_element = driverP_.find_element_by_xpath('//*[@id="captcha"]')
                        input_element.send_keys(str(re_captcha_txt_final))  ##### <---- Pass text ---->
                        #print('send_keys done------')

                        await asyncio.sleep(1)
                        # <----- for Click Search btn---------------------
                        driverP_.find_element_by_name("submit1").click()  # <--- sertch btn --->
                        #print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        # delay2()
                        # delay1()
                        await asyncio.sleep(2)

                        html = driverP_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display:none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------
                        # -------------------------------------------------------------------------------
                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '' or op_page_errSpanDiv == "display:block;":
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            #print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            #print('check2:-first---', check2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                #delay1()
                                #print('check2---second---', check2)
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                #print('check2-----third---', check2)
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driverP_.quit()
                                refresh = 1

                        else:
                            #print('if-else-op_page_errSpanDiv == ')
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            #print('if refresh == 1:---')
                            break
                    if Record_not_found == 1:
                        #print('if Record_not_found == 1:---')
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        #print('elif Record_not_found == 0:---')
                        WebDriverWait(driverP_, 900).until(EC.presence_of_element_located((By.ID, "showList")))  # < wait
                        time.sleep(1)

                        #<-------/// code for extract only CASE NO. & CNR NO. ///----------
                        #<-------/// get <a> tag data ///----------
                        a_tag_data = driverP_.find_elements(by=By.TAG_NAME, value="a")
                        try:
                            a_list=[]
                            for lnk in a_tag_data:
                                a_list.append(str(lnk.get_attribute('onclick')))
                            #print('a_list:-', a_list)   #viewHistory('201500003992018','MHNS010066912018','1');return false;

                            dict_res_dct = {}
                            count = 0
                            for l in a_list:
                                if 'viewHistory' in l:
                                    count = count + 1
                                    k3 = str(l).split("'")
                                    # ['viewHistory(', '201500003992018', ',', 'MHNS010066912018', ',', '1', ');return false;']
                                    # print('[k3[1], k3[3]]:-', [k3[1], k3[3]])  #..get this..['201500003992018', 'MHNS010066912018']
                                    lst = [k3[1], k3[3]]

                                    # ---- list into dict convert ----[] --> {}---
                                    res_dct = {lst[i]: lst[i + 1] for i in range(0, len(lst), 2)}
                                    # print(res_dct)  # .. {'201500003992018': 'MHNS010066912018'}
                                    # list_res_dct.update(res_dct)
                                    dict_res_dct[count] = res_dct
                            # print('dict_res_dct:-', dict_res_dct)
                        except:
                            pass
                        #---------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        html = driverP_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        #-----------------------
                        try:
                            table_data = soup.find('table', attrs={'id': 'showList3'})
                            #print('---------1-----------')
                            h, [_, *d] = [i.text for i in table_data.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_data.find_all('tr')]
                            res_dct_data = [dict(zip(h, i)) for i in d]
                            #print('res_dct_data:--', res_dct_data)
                            #print('---------2-----------')

                            k5 = []
                            for d in res_dct_data:
                                if d['Sr No'].isdigit():
                                    k5.append({k.replace(' ', ''): v.replace('\n', '') for k, v in d.items()})
                            #print('k5:-', k5)
                            #print('---------3-----------')

                            # ----------------------------------
                            final_list = []
                            count = 0
                            for f_dict in k5:
                                string = f_dict['View']  # for case number SCA/5400/2020
                                res = ''.join(
                                    filter(lambda i: i.isdigit(),
                                           string))  # ..extract all 'digits' from str "54662019"
                                # print('res:- ', res)
                                # -update -
                                f_dict['View'] = res

                                for k, v in dict_res_dct.items():
                                    # print(k,v)
                                    # -- for match two kye's --
                                    if str(f_dict['SrNo']) == str(k):
                                        # print(f_dict['SrNo'], ',', k, ',', f_dict['View']) #1 , 1

                                        # -- for check res data in [view] --
                                        if res in str(f_dict['View']):
                                            # print(res, ',',  f_dict['View'])
                                            for k, v in v.items():
                                                f_dict['View'] = k
                                                f_dict['CNR'] = v
                                                final_list.append(f_dict)  # .. append in list

                            # print('final_list:-', final_list)
                            # ----------------------------------
                            #print('---------4-----------')
                            res_dct = {"TableData": final_list}
                            # --------------------------------------

                        except Exception as e:
                            #print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}

                        # ---------------------------------------------------------------------------------------------
                        if res_dct != {}:
                            case_final = res_dct
                            try:
                                driverP_.quit()
                                #print('--driverP_.quit()')
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {'ThreadId': str(threading.get_native_id()), "CaseDetailsNotFound": 'This Case Code does not exists/Data not available.'}
                            try:
                                driverP_.quit()
                                #print('--driverP_.quit(2)')
                                break
                            except:
                                pass

                        else:
                            try:
                                driverP_.quit()
                                #print('--driverP_.quit(3)')
                                break
                            except:
                                pass

                        main_ch2 = 1


                # ===-{inner Try Exception}--==
                except Exception as e:
                    try:
                        #print('inner Try Exception:---', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
                        driverP_.quit()
                    except:
                        pass
                if main_ch2 == 1:
                    break


        else:
            #print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driverP_.quit()
    #===---{main try Exception }--===
    except Exception as e:
        try:
            #print('main try Exception:--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driverP_.quit()
        except:
            pass
    finally:
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2



        data = yourdata                 # Final response
        # print('data:--', data)
        # ----------------

    # finish3 = time.perf_counter()
    # print(f'{str(threading.get_native_id())} Advocate Finished in {round(finish3 - start, 2)} second(s)')

    #return HttpResponse(f'Thread id:- {str(threading.get_native_id())}, driver_Id:- {driver_Id}, JSON_data:-{data}, Input data:- {state, dist, str(complex), casetype, caseno, year}, Request Finished in:- {round(finish3 - start, 2)} second(s)'.replace("'", '"'))
    #return HttpResponse(str(data).replace("'", '"').replace("-", ''))
    return JsonResponse(data)
    #-------{ main try end}----------------


#===========================================================================================
                                  # { HIGH COURT VICE } #
#===========================================================================================

#==================---{ CASE NO SEARTCH }-----================
async def HighCourtCaseNoView(request, state, dist, courtcode, statename, casetype, caseno, year):
    # start = time.perf_counter()
    global res_dct, case_final_2, yourdata, re_captcha_txt_final, Record_not_found, captcha_txt2, table_list3, Petitioner_Respondent_res_dct, IADetails_k5, act_k5, History_res_dct, res_dc, case_final
    # print(f'Case input data:--- , {str(threading.get_native_id())}, {state, dist, courtcode, statename, casetype, caseno, year}')

    # -----------Code start-----
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    driverHCc_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)

    driverHCc_.get(
        'https://services.ecourts.gov.in/ecourtindiaHC/cases/case_no.php?state_cd={s}&dist_cd={d}&court_code={cc}&stateNm={sn}'.format(
            s=state, d=dist, cc=courtcode, sn=statename))
    # print('Case_f1--')

    WebDriverWait(driverHCc_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "h1")))  # wait


    # <--//- all repeated Functions ----//---
    def delay1():
        time.sleep(random.randint(1, 2))
        #await asyncio.sleep(1)

    def delay2():
        time.sleep(random.randint(2, 2))
        #await asyncio.sleep(2)

    # <--Main (try)---
    try:
        html = driverHCc_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        # print('h1_data:-', h1_data)

        # <--Main (if)---
        if 'High Court' in str(h1_data):
            # #============================/ test /=========================================
            # # For Select Case Status
            # WebDriverWait(driverHCc_, 900).until(EC.presence_of_element_located((By.ID, "caseNoDet")))  # wait
            # WebDriverWait(driverHCc_, 900).until(
            #     EC.element_to_be_clickable((By.XPATH, '//*[@id="captcha_image_audio_controls"]/a')))  # wait audio btn
            # WebDriverWait(driverHCc_, 900).until(EC.element_to_be_clickable(
            #     (By.XPATH, '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')))  # wait refresh
            #
            # await asyncio.sleep(3)
            # # <------COURT COMPLEX--///---start---///-->
            #
            # html = driverHCc_.page_source
            # soup = BeautifulSoup(html, "html.parser")
            # all_divs = soup.find('select', {'id': 'case_type'})
            # all_options = all_divs.find_all('option')
            # # print('all_options:-', all_options)
            #
            # k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['0', '28', '2', '6',..]
            # # print('K3:-', k3)
            #
            # # <---- take input casetype value from fun() --
            # com = casetype
            # index3 = k3.index(com)
            # casetype_option = index3 + 1
            #
            # # <------ Click on 'CASE TYPE' DROP DOWN ---->
            # driverHCc_.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
            #
            # # <------ Select 'COMPLEX' Options ---->
            # casetype_drop = \
            #     driverHCc_.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(casetype_option))[0]
            # print('selected complex:- ', casetype_drop.text)
            # casetype_drop.click()
            #
            # await asyncio.sleep(3) # required
            #
            # # <--------CASE NO input field---///--Start----///--------->
            # WebDriverWait(driverHCc_, 600).until(
            #     EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
            #
            # case_elem = driverHCc_.find_element(By.ID, "search_case_no")
            # case_elem.send_keys(str(caseno))  ##### <---- Pass case no ---->
            # # <--------CASE No input field---///--End----///--------->
            #
            # # <--------YEAR input field---///--Start----///--------->
            # WebDriverWait(driverHCc_, 600).until(
            #     EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
            # year_elem = driverHCc_.find_element(By.ID, "rgyear")
            # year_elem.send_keys(str(year))  ##### <---- Pass year ---->
            # # <--------YEAR input field---///--End----///--------->
            # #============================/ test end /=====================================

            while True:
                main_ch2 = 0
                try:
                    #  ----
                    WebDriverWait(driverHCc_, 900).until(EC.presence_of_element_located((By.ID, "caseNoDet")))  # wait
                    WebDriverWait(driverHCc_, 900).until(
                        EC.element_to_be_clickable(
                            (By.XPATH, '//*[@id="captcha_image_audio_controls"]/a')))  # wait audio btn
                    WebDriverWait(driverHCc_, 900).until(EC.element_to_be_clickable(
                        (By.XPATH, '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')))  # wait refresh

                    await asyncio.sleep(3)
                    # <------For Select CASE TYPE--///---start---///-->

                    html = driverHCc_.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    all_options = all_divs.find_all('option')
                    # print('all_options:-', all_options)

                    k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['0', '28', '2', '6',..]
                    # print('K3:-', k3)

                    # <---- take input casetype value from fun() --
                    com = casetype
                    index3 = k3.index(com)
                    casetype_option = index3 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driverHCc_.find_elements_by_xpath('//*[@id="case_type"]')[0].click()

                    # <------ Select 'COMPLEX' Options ---->
                    casetype_drop = \
                        driverHCc_.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(casetype_option))[0]
                    print('selected complex:- ', casetype_drop.text)
                    casetype_drop.click()

                    await asyncio.sleep(3)  # required

                    # <--------CASE NO input field---///--Start----///--------->
                    WebDriverWait(driverHCc_, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait

                    case_elem = driverHCc_.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(str(caseno))  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driverHCc_, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driverHCc_.find_element(By.ID, "rgyear")
                    year_elem.send_keys(str(year))  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # ------------------------------
                    while True:
                        refresh = 0
                        driverHCc_.find_element_by_xpath(
                            '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a').click()  # refresh btn
                        delay1()
                        driverHCc_.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        delay1()
                        WebDriverWait(driverHCc_, 600).until(
                            EC.element_to_be_clickable(
                                (By.XPATH, '//*[@id="caseNoDet"]/div[6]/span[3]/input[1]')))  # <--GO-- wait --->

                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        element = driverHCc_.find_element_by_xpath("//*[@id='captcha_image']")
                        location = element.location
                        size = element.size

                        driverHCc_.save_screenshot(
                            settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageHC_CASE.png')

                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(
                            settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageHC_CASE.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageHC_CASE.png')
                        #im.show()  # <--- comment it ----
                        # print("1'st while image:------")
                        # <--------// end //----

                        # <---- for Captcha image to text convert-------------
                        # -----------------------------------------------------------------------------
                        # # --------API CODE--------------

                        with open(
                                settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageHC_CASE.png',
                                'rb') as captcha_file:
                            captcha_op = api.solve(captcha_file)
                        captcha_txt2 = captcha_op.await_result()
                        # print('API code captcha text:--', captcha_txt2)
                        # print('len(captcha_txt2):--', len(str(captcha_txt2))) #<-- len 5/6
                        # print('type(captcha_txt2):--', type(captcha_txt2))     #<-- type (str)
                        # <------- Find_data--------/// END ///---------------------------

                        # <--OLD---- for Captcha image to text convert---------------comment it------------
                        '''
                        img = Image.open(f"{settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageHC_CASE.png'}")
                        captcha_txt = tess.image_to_string(img, config='')'''
                        #punc = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
                        '''for ele in captcha_txt:
                            if ele in punc:
                                captcha_txt = captcha_txt.replace(ele, "")
                        # print('captcha_txt2--:', captcha_txt)
                        captcha_txt2 = captcha_txt'''
                        # -------------------------------------------------------------------------

                        # captcha_txt2 = 'abcde'     #<--- for test only

                        #-------------------------------------------------------------------------
                        re_captcha_txt_final = captcha_txt2.replace('I', 'l').replace('i', 'l')
                        #-------------------------------------------------------------------------

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driverHCc_.find_element_by_xpath('//*[@id="captcha"]')
                        input_element.send_keys(str(re_captcha_txt_final))  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        await asyncio.sleep(1)

                        # <----- for Click Search btn---------------------
                        driverHCc_.find_element_by_xpath(
                            '//*[@id="caseNoDet"]/div[6]/span[3]/input[1]').click()  # <--- sertch btn --->
                        # print('seartch btn clicked ------')
                        await asyncio.sleep(2)
                        # <--------------------------------------------------------------------------------------------------------

                        html = driverHCc_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display:none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        op_page_showList2Div = soup.find('div', {'id': 'showList2'})
                        # print('-------------------------------')
                        # print('op_page_showList2Div:-- ', op_page_showList2Div)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '' or op_page_errSpanDiv == "display:block;":
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('input')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            # print('check2:-first---', check2)
                            if 'Invalid Captcha' in check2:
                                # delay1()
                                # print('check2----Invalid Captcha')
                                refresh = 0

                            elif 'Record Not Found' in check2:
                                # print('check2-----Record Not Found')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driverHCc_.quit()
                                refresh = 1

                        elif 'Record Not Found' in str(op_page_showList2Div):
                            res_dct = {}
                            case_final_2 = {'RecordNotFound': 'Record not found'}
                            Record_not_found = 1
                            driverHCc_.quit()
                            refresh = 1

                        else:
                            # print('if-else-op_page_errSpanDiv == ')
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            # print('if refresh == 1:---')
                            break

                    if Record_not_found == 1:
                        # print('if Record_not_found == 1:---')
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        # print('elif Record_not_found == 0:---')
                        WebDriverWait(driverHCc_, 900).until(EC.presence_of_element_located((By.ID, "showList")))  # < wait


                        driverHCc_.execute_script("window.scrollTo(0,370);")
                        time.sleep(1)
                        WebDriverWait(driverHCc_, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="showList1"]/tr/td[4]/a')))
                        driverHCc_.find_elements_by_xpath('//*[@id="showList1"]/tr/td[4]/a')[0].click()
                        time.sleep(1)
                        # -------------------------------------------------------------------------------------------------

                        # <======---/// Final page table conversion start ///---======>
                        WebDriverWait(driverHCc_, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="back_top"]/center/a')))
                        # delay2()
                        await asyncio.sleep(2)
                        html = driverHCc_.page_source
                        soup = BeautifulSoup(html, "html.parser")

                        # <---1st "Case Details" Table, { build with SPAN tag } -----
                        #-------------------------------------------------------------
                        try:
                            # print('table try:--')
                            all_span = soup.find_all('span', {'class': 'case_details_table'}) #-o/p list of all <span>

                            table_list_row = []
                            for i in all_span:
                                # print('------------------------------')
                                # print('text:---', i.text)
                                # print('------------------------------')

                                text_i = i.text

                                ls = list(text_i)
                                # print(len(ls))
                                count = 0
                                for j in range(0, len(ls)):
                                    try:
                                        if ls[count].isdigit():
                                            # print('if:--', j, ls[j])
                                            count = count + 1

                                            if ls[count].isalpha():
                                                # print('if 2:--', j + 1, ls[j + 1])
                                                ls.insert(j + 1, ',')
                                                count = count + 1
                                        else:
                                            count = count + 1
                                    except:
                                        pass
                                f = ''.join(ls).replace(':', '*').replace('\xa0', '')
                                                        # o/p-- Filing Number: 12114/2019,Filing Date: 08-04-2019
                                # # print(f.split(","))
                                lst = f.split("*")      # convert comma sepa. string to list
                                table_list_row.append(lst)
                            # -----------------------------------------------------------
                            # print('table_list_row:-', table_list_row)
                            # print('-------------------------------')

                            #==================/ for handle  empty values in table /===============
                            table_list = []
                            for n in range(len(table_list_row)):
                                if len(table_list_row[n]) == 1:
                                    # --- For 'Case Type' List
                                    if table_list_row[n][0].replace(' ', '') == 'CaseType':
                                        table_list_row[n].insert(1, '')
                                    # --- For 'Case Type' List
                                    elif table_list_row[n][0].replace(' ', '') == 'CNRNumber':
                                        table_list_row[n].insert(1, '')

                                if len(table_list_row[n]) == 3:
                                    # --- For 'Filing Number' 'Filing Date' List
                                    if 'Filing Date' in table_list_row[n][1]:
                                        if ',' in table_list_row[n][1] and 'Filing Date' in table_list_row[n][1]:
                                            if len(table_list_row[n][1]) != 11:
                                                a = table_list_row[n][1][::-1]  # etaD noitartsigeR,7102/06513
                                                b = a[:11]  # etaD noitartsigeR
                                                Filing_finalstr = b[::-1]  # ---> Registration Date
                                                # print('hhhhhh Filing', Filing_finalstr)

                                            if len(table_list_row[n][1]) != 11:
                                                a = table_list_row[n][1][::-1]  # etaD noitartsigeR,7102/06513
                                                b = a[12:]  # etaD noitartsigeR
                                                Filing_finalstr2 = b[::-1]  # ---> Registration Date
                                                # print('hhhhhh Filing', Filing_finalstr2)

                                            table_list_row[n][1] = Filing_finalstr
                                            table_list_row[n].insert(1, Filing_finalstr2)
                                            # print('table_list_row[n]:--', table_list_row[n])

                                            if len(table_list_row[n]) == 3:
                                                if table_list_row[n][1].replace(' ', '') == 'FilingDate':
                                                    table_list_row[n].insert(1, '')
                                                elif table_list_row[n][2].replace(' ', '') == 'FilingDate':
                                                    table_list_row[n].insert(3, '')

                                    # --- For 'Filing Number' 'Filing Date' List
                                    if len(table_list_row[n]) == 3:
                                        if table_list_row[n][1].replace(' ', '') == 'FilingDate':
                                            table_list_row[n].insert(1, '')
                                        elif table_list_row[n][2].replace(' ', '') == 'FilingDate':
                                            table_list_row[n].insert(3, '')

                                    # --- For 'Registration Number' 'Registration Date' List
                                    if 'Registration Date' in table_list_row[n][1]:
                                        if ',' in table_list_row[n][1] and 'Registration Date' in table_list_row[n][1]:
                                            if len(table_list_row[n][1]) != 17:
                                                a = table_list_row[n][1][::-1]  # etaD noitartsigeR,7102/06513
                                                b = a[:17]  # etaD noitartsigeR
                                                finalstr = b[::-1]  # ---> Registration Date
                                                # print('hhhhhh', finalstr)

                                            if len(table_list_row[n][1]) != 17:
                                                a = table_list_row[n][1][::-1]  # etaD noitartsigeR,7102/06513
                                                b = a[18:]  # etaD noitartsigeR
                                                finalstr2 = b[::-1]  # ---> Registration Date
                                                # print('hhhhhh', finalstr2)

                                            table_list_row[n][1] = finalstr
                                            table_list_row[n].insert(1, finalstr2)
                                            # print('table_list_row[n]:--', table_list_row[n])

                                            if len(table_list_row[n]) == 3:
                                                if table_list_row[n][1].replace(' ', '') == 'RegistrationDate':
                                                    table_list_row[n].insert(1, '')
                                                elif table_list_row[n][2].replace(' ', '') == 'RegistrationDate':
                                                    table_list_row[n].insert(3, '')

                                    # --- for / ['Registration Number', '31560/2017', 'Registration Date'] /----
                                    if len(table_list_row[n]) == 3:
                                        if table_list_row[n][1].replace(' ', '') == 'RegistrationDate':
                                            table_list_row[n].insert(1, '')
                                        elif table_list_row[n][2].replace(' ', '') == 'RegistrationDate':
                                            table_list_row[n].insert(3, '')

                                # --==/ for len = 2  /==---
                                if len(table_list_row[n]) == 2:
                                    # =====--/  ['Filing Number', '15824/2014,Filing Date']      /---=====
                                    if 'Filing Date' in table_list_row[n][1]:
                                        if ',' in table_list_row[n][1] and 'Filing Date' in table_list_row[n][1]:
                                            if len(table_list_row[n][1]) != 11:
                                                a = table_list_row[n][1][::-1]  # etaD noitartsigeR,7102/06513
                                                b = a[:11]  # etaD noitartsigeR
                                                Filing_finalstr3 = b[::-1]  # ---> Registration Date
                                                # print('hhhhhh 2 Filing:-', Filing_finalstr3)

                                            if len(table_list_row[n][1]) != 11:
                                                a = table_list_row[n][1][::-1]  # etaD noitartsigeR,7102/06513
                                                b = a[12:]  # etaD noitartsigeR
                                                Filing_finalstr4 = b[::-1]  # ---> Registration Date
                                                # print('hhhhhh 2 Filing:-', Filing_finalstr4)

                                            table_list_row[n][1] = finalstr3
                                            table_list_row[n].insert(1, finalstr4)
                                            # print('table_list_row[n]:--', table_list_row[n])

                                            # ----
                                            if len(table_list_row[n]) == 3:
                                                if table_list_row[n][1].replace(' ', '') == 'Filing Date':
                                                    table_list_row[n].insert(1, '')
                                                elif table_list_row[n][2].replace(' ', '') == 'Filing Date':
                                                    table_list_row[n].insert(3, '')

                                    # ----=====/ for/ ['Registration Number', '31560/2017,Registration Date'] /=====----
                                    if 'Registration Date' in table_list_row[n][1]:
                                        if ',' in table_list_row[n][1] and 'Registration Date' in table_list_row[n][1]:
                                            if len(table_list_row[n][1]) != 17:
                                                a = table_list_row[n][1][::-1]  # etaD noitartsigeR,7102/06513
                                                b = a[:17]  # etaD noitartsigeR
                                                finalstr3 = b[::-1]  # ---> Registration Date
                                                # print('hhhhhh 2', finalstr3)

                                            if len(table_list_row[n][1]) != 17:
                                                a = table_list_row[n][1][::-1]  # etaD noitartsigeR,7102/06513
                                                b = a[18:]  # etaD noitartsigeR
                                                finalstr4 = b[::-1]  # ---> Registration Date
                                                # print('hhhhhh 2', finalstr4)

                                            table_list_row[n][1] = finalstr3
                                            table_list_row[n].insert(1, finalstr4)
                                            # print('table_list_row[n]:--', table_list_row[n])

                                            # ----
                                            if len(table_list_row[n]) == 3:
                                                if table_list_row[n][1].replace(' ', '') == 'RegistrationDate':
                                                    table_list_row[n].insert(1, '')
                                                elif table_list_row[n][2].replace(' ', '') == 'RegistrationDate':
                                                    table_list_row[n].insert(3, '')

                                table_list.append(table_list_row[n])

                            #-----------------------------------------------------------
                            # print('table_list:-', table_list)
                            # print('-----------------------------')

                            # =========  CONVERT / LIST OF LIST INTO single LIST /===============
                            flat_list = [item for sublist in table_list for item in sublist]
                            # print(flat_list)
                            # ======== CONVERT LIST TO DICT ==============
                            Case_Details_res_dct = {flat_list[l].replace(' ', ''): flat_list[l + 1] for l in range(0, len(flat_list), 2)}
                            # print('Case_Details_res_dct:- ', Case_Details_res_dct)

                        except Exception as e:
                            Case_Details_res_dct = {}


                        # <---2nd "Case Status" Table, { build with SPAN tag } -----
                        # -------------------------------------------------------------
                        try:
                            # print('table try:--')
                            div_html_data = soup.select_one('#secondpage > div:nth-child(13) > div:nth-child(8)')
                            # BY css selector
                            # print('---------')
                            # print('div_html_data:-- ', div_html_data)
                            table_list2 = []
                            for d in div_html_data:
                                table_list2.append(d.text)
                            # print('---------')
                            # print('table_list2:-- ', table_list2)
                            # ---------------------------------
                            res = [ele for ele in table_list2 if ele.strip()]

                            k = []
                            for t in res:
                                f = t.replace(':', '*').replace('\xa0', '').replace('\n', '')
                                k.append(f)
                            # print('---------')
                            # print('k:-- ', k)

                            lst2 = []
                            for j in k:
                                lst2.append(j.split('*'))
                            # print('---------')
                            # print('lst2:-- ', lst2)

                            # ======== CONVERT LIST TO DICT ==============
                            Case_Status_res_dct = {}
                            for r in lst2:
                                res_dct = {r[i].replace(' ', ''): r[i + 1] for i in range(0, len(r), 2)}
                                Case_Status_res_dct.update(res_dct)
                            # print('---------')
                            # print('Case_Status_res_dct:- ', Case_Status_res_dct)
                            # ---------------------------------


                        except Exception as e:
                            # print('eroor:--', e)
                            Case_Status_res_dct = {}


                        # <---3rd "Case Status" Table, { build with SPAN tag } -----
                        # -------------------------------------------------------------
                        try:
                            # print('table try:--')
                            div_html_data = soup.select_one('#secondpage > div:nth-child(13) > div:nth-child(10)')  # BY css selector

                            #<-- for { "Petitioner and Advocate" }---
                            if 'Petitioner and Advocate' in str(div_html_data) or 'Respondent  and Advocate' in str(div_html_data) or 'Acts' in str(div_html_data):
                                table_list3 = []
                                for p in div_html_data:
                                    # <-- for { "Petitioner and Advocate" }---
                                    try:
                                        petiti = p.find_next('span', {'class': 'Petitioner_Advocate_table'})
                                        petiti_list = []
                                        if petiti:
                                            petiti_list.append(petiti.text)
                                            for i in petiti_list:
                                                P = i.replace('\xa0', ' ')
                                                # print('P:-', [P])
                                                Petitioner_list = [P] # o/p ['1) PRIYANKA W/O NILESHKUMAR JODHANI D/O LAXMAN KHAVRE    Advocate- MR.HEMANG H PARIKH(2628) ']
                                                # print('Petitioner_list:--', Petitioner_list)
                                                table_list3.append('Petitioner and Advocate')
                                                table_list3.append(P)
                                            break
                                        else:
                                            table_list3.append('Petitioner and Advocate')
                                            table_list3.append('')
                                    except:
                                        table_list3.append('Petitioner and Advocate')
                                        table_list3.append('')

                                # <-- for { "Respondent and Advocate" }---
                                for r in div_html_data:
                                    try:
                                        Respo = r.find_next('span', {'class': 'Respondent_Advocate_table'})
                                        respo_list = []
                                        if Respo:
                                            respo_list.append(Respo.text)
                                            for i in respo_list:
                                                R = i.replace('\xa0', ' ')
                                                # print('R:-', [R])
                                                Respondent_list = [R] # o/p ['1) PRIYANKA W/O NILESHKUMAR JODHANI D/O LAXMAN KHAVRE    Advocate- MR.HEMANG H PARIKH(2628) ']
                                                # print('Respondent_list:--', Respondent_list)
                                                table_list3.append('Respondent and Advocate')
                                                table_list3.append(R)
                                            break
                                        else:
                                            table_list3.append('Respondent and Advocate')
                                            table_list3.append('')
                                    except:
                                        table_list3.append('Respondent and Advocate')
                                        table_list3.append('')

                                # ---------------------------------
                                # print('table_list3:--', table_list3)
                                # ======== CONVERT LIST TO DICT ==============
                                Petitioner_Respondent_res_dct = {table_list3[l]: table_list3[l + 1] for l in
                                                                         range(0, len(table_list3), 2)}
                                # print('Petitioner_Respondent_res_dct:- ', Petitioner_Respondent_res_dct)
                                # ---------------------------------

                                # <-- for { "Acts" }---
                                try:
                                    # <--- Table, with <th> ---------->
                                    table_a = soup.find('table', attrs={'id': 'act_table'})
                                    h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                        [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                                    act_res_dct = [dict(zip(h, i)) for i in d]
                                    # print('act_res_dct: ', act_res_dct)

                                    act_k5 = []
                                    for d in act_res_dct:
                                        act_k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                            'UnderSection(s)', 'Section'): v for k, v in d.items()})
                                    #print('act_k5:-- ', act_k5)
                                except:
                                    act_k5 = []


                            else:
                                Petitioner_Respondent_res_dct = {}
                                act_k5 = []

                        except Exception as e:
                            # print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}

                        # <---4th "IA Details" Table, { <th> tag } -----
                        # -------------------------------------------------------------
                        try:
                            # <--- Table, with <th> ---------->
                            table_b = soup.find('table', attrs={'class': 'IAheading'})
                            h, [_, *d] = [i.text for i in table_b.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_b.find_all('tr')]
                            IADetails_res_dct = [dict(zip(h, i)) for i in d]
                            # print('IADetails_res_dct:-- ', IADetails_res_dct)

                            IADetails_k5 = []
                            for d in IADetails_res_dct:
                                IADetails_k5.append({k.replace(' ', ''): v for k, v in d.items()})
                            # print('IADetails_k5:-- ', IADetails_k5)
                        except:
                            IADetails_k5 = []

                        # <---5th "History of Case Hearing" Table, { <th> tag } -----
                        # -------------------------------------------------------------
                        try:
                            # <--- Table, with <th> ---------->
                            table_c = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_c.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_c.find_all('tr')]
                            History_res_dct = [dict(zip(h, i)) for i in d]
                            # print('History_res_dct:-- ', History_res_dct)

                            History_k5 = []
                            for d in History_res_dct:
                                History_k5.append({k.replace(' ', ''): v for k, v in d.items()})
                            # print('IADetails_k5:-- ', IADetails_k5)
                        except:
                            History_k5 = []

                        # <---6th "Orders" Table, { head with <td> tag } -----
                        # -------------------------------------------------------------
                        try:
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]
                            # print('7th Table: ', res_dct7)

                            Orders_k7 = []
                            for d7 in res_dct7:
                                Orders_k7.append(
                                    {k.replace('  ', '').replace(' ', '').replace('\xa0', ''): v.replace('\xa0', '') for k, v in d7.items()})

                            # print('Orders_k7: ', Orders_k7)

                            # -------------------/ for view URL /------------------------------
                            all_a_url = table_order.find_all('a', href=True)
                            # print('all_a_url:-- ', all_a_url)
                            for n in range(len(Orders_k7)):
                                dict_n = Orders_k7[n]
                                # print('---------------------')
                                # print('dict_n:-- ', dict_n)
                                # print('---------------------')

                                # print('url:-- ', all_a_url[n]['href'])
                                # print('---------------------')
                                dict_n['OrderDetails'] = 'https://services.ecourts.gov.in/ecourtindiaHC/cases/' + \
                                                            all_a_url[n]['href']
                            # -------------------------------------------------
                        except:
                            Orders_k7 = []


                        # --------------------------------------------------------------------
                        #===--{ FINAL OUTPUT DATA}--====
                        res_dct = {'CaseDetails': Case_Details_res_dct,
                                   'CaseStatus': Case_Status_res_dct,
                                   'PetitionerRespondent': Petitioner_Respondent_res_dct,
                                   'Acts': act_k5,
                                   'IADetails': IADetails_k5,
                                   'HistoryofCaseHearing': History_k5,
                                   'Orders': Orders_k7}
                        #-------------------------------
                        if res_dct != {}:
                            case_final = res_dct
                            try:
                                driverHCc_.quit()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {'ThreadId': str(threading.get_native_id()), "CaseDetailsNotFound": 'This Case Code does not exists/Data not available.'}
                            try:
                                driverHCc_.quit()
                                break
                            except:
                                pass

                        else:
                            try:
                                driverHCc_.quit()
                                break
                            except:
                                pass

                        main_ch2 = 1
#-------------------------------------------------
                except Exception as e:
                    try:
                        driverHCc_.quit()
                        # print('except Exception-----', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
                    except:
                        pass
                if main_ch2 == 1:
                    break

        else:
            #print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driverHCc_.quit()

    except Exception as e:
        try:
            print('except crser here--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driverHCc_.quit()
        except:
            pass
    finally:
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2



        data = yourdata                 # Final response
        # print('data:--', data)
        # ----------------


    #return HttpResponse(f'Thread id:- {str(threading.get_native_id())}, driver_Id:- {driver_Id}, JSON_data:-{data}, Input data:- {state, dist, str(complex), casetype, caseno, year}, Request Finished in:- {round(finish3 - start, 2)} second(s)'.replace("'", '"'))
    # return HttpResponse(data)
    return JsonResponse(data)

#==================---{ PARTY NAME SEARTCH }-----=============
async def HighCourtPartyNameView(request, state, dist, courtcode, statename, petitioner, year):
    global res_dct, case_final_2, yourdata, Record_not_found, list_res_dct, case_final, dict_res_dct
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    driverHCP_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)

    driverHCP_.get(
        'https://services.ecourts.gov.in/ecourtindiaHC/cases/ki_petres.php?state_cd={s}&dist_cd={d}&court_code={cc}&stateNm={sn}'.format(
            s=state, d=dist, cc=courtcode, sn=statename))
    # print('input data:--', state, ',', dist, ',', courtcode, ',', statename, ',', petitioner, ',', year)
    # print('Petitioner_func--')

    WebDriverWait(driverHCP_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "h1")))  # wait

    # =====--{ all repeated Functions }--======
    def delay1():
        time.sleep(random.randint(1, 2))

    def delay2():
        time.sleep(random.randint(2, 2))

    # <--Main (try)---
    try:
        html = driverHCP_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        # print('h1_data:-', h1_data)

        # <--Main (if)---
        if 'High Court' in str(h1_data):
            while True:
                main_ch2 = 0
                # ===-{inner Try}--==
                try:
                    WebDriverWait(driverHCP_, 900).until(EC.presence_of_element_located((By.ID, "mainDiv")))  # wait
                    await asyncio.sleep(3)
                    # ============================/ MAIN CODE /=========================================
                    WebDriverWait(driverHCP_, 900).until(EC.presence_of_element_located((By.ID, "mainDiv")))  # wait
                    WebDriverWait(driverHCP_, 900).until(
                        EC.element_to_be_clickable(
                            (By.XPATH, '//*[@id="captcha_image_audio_controls"]/a')))  # wait audio btn
                    WebDriverWait(driverHCP_, 900).until(
                        EC.element_to_be_clickable(
                            (By.XPATH, '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')))  # wait refresh

                    await asyncio.sleep(3)  # required

                    # <--------Petitioner/Responden INPUT FIELD---///--Start----///--------->
                    WebDriverWait(driverHCP_, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="petres_name"]')))  # wait

                    case_elem = driverHCP_.find_element(By.ID, "petres_name")
                    pet = str(petitioner).replace('_', ' ').replace('-', ' ')
                    case_elem.send_keys(pet)  ##### <---- Pass petitioner ---->
                    # <-----------///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driverHCP_, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driverHCP_.find_element(By.ID, "rgyear")
                    year_elem.send_keys(str(year))  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->
                    # ============================/ MAIN CODE END /=====================================

                    # <---------------------------/ Captcha code /---------------------------------
                    # -----------------------------------------------------------------------------
                    while True:
                        refresh = 0
                        driverHCP_.find_element_by_xpath(
                            '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a').click()  # refresh btn
                        delay1()
                        driverHCP_.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        delay1()
                        WebDriverWait(driverHCP_, 600).until(
                            EC.element_to_be_clickable(
                                (By.XPATH, '//*[@id="mainDiv"]/div[5]/span[3]/input[1]')))  # <--GO-- wait --->

                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        element = driverHCP_.find_element_by_xpath("//*[@id='captcha_image']")
                        location = element.location
                        size = element.size

                        driverHCP_.save_screenshot(
                            settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}imageHCPARTY_CASE.png')

                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(
                            settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}imageHCPARTY_CASE.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(
                            settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}cropimageHCPARTY_CASE.png')
                        #im.show()  # <--- comment it ----
                        # print("1'st while image:------")
                        # <--------// end //----

                        # <---- for Captcha image to text convert-------------
                        # -----------------------------------------------------------------------------
                        # # --------API CODE--------------

                        with open(
                                settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}cropimageHCPARTY_CASE.png',
                                'rb') as captcha_file:
                            captcha_op = api.solve(captcha_file)
                        captcha_txt2 = captcha_op.await_result()
                        # print('API code captcha text:--', captcha_txt2)
                        # print('len(captcha_txt2):--', len(str(captcha_txt2))) #<-- len 5/6
                        # print('type(captcha_txt2):--', type(captcha_txt2))     #<-- type (str)
                        # <------- Find_data--------/// END ///---------------------------

                        # <--OLD---- for Captcha image to text convert---------------comment it------------
                        '''
                        img = Image.open(f"{settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}cropimageHCPARTY_CASE.png'}")
                        captcha_txt = tess.image_to_string(img, config='')'''
                        #punc = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
                        '''for ele in captcha_txt:
                            if ele in punc:
                                captcha_txt = captcha_txt.replace(ele, "")
                        print('captcha_txt2-OLD CODE-:', captcha_txt)
                        captcha_txt2 = captcha_txt'''
                        # -------------------------------------------------------------------------

                        # captcha_txt2 = 'abcde'     #<--- for test only

                        # -------------------------------------------------------------------------
                        re_captcha_txt_final = captcha_txt2.replace('I', 'l').replace('i', 'l')
                        # -------------------------------------------------------------------------

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driverHCP_.find_element_by_xpath('//*[@id="captcha"]')
                        input_element.send_keys(str(re_captcha_txt_final))  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        await asyncio.sleep(1)

                        # <----- for Click Search btn---------------------
                        driverHCP_.find_element_by_xpath(
                            '//*[@id="mainDiv"]/div[5]/span[3]/input[1]').click()  # <--- sertch btn --->
                        # print('seartch btn clicked ------')
                        await asyncio.sleep(2)
                        # <--------------------------------------------------------------------------------------------------------

                        html = driverHCP_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display:none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        op_page_showList2Div = soup.find('div', {'id': 'showList2'})
                        # print('-------------------------------')
                        # print('op_page_showList2Div:-- ', op_page_showList2Div)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '' or op_page_errSpanDiv == "display:block;":
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('input')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            # print('check2:-first---', check2)
                            if 'Invalid Captcha' in check2:
                                # delay1()
                                # print('check2----Invalid Captcha')
                                os.remove(f"{settings.MEDIA_ROOT + f'/PartyNameImages/{str(threading.get_native_id())}cropimageHCPARTY_CASE.png'}")
                                refresh = 0

                            elif 'Record Not Found' in check2:
                                # print('check2-----Record Not Found')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driverHCP_.quit()
                                refresh = 1

                        elif 'Record Not Found' in str(op_page_showList2Div):
                            res_dct = {}
                            case_final_2 = {'RecordNotFound': 'Record not found'}
                            Record_not_found = 1
                            driverHCP_.quit()
                            refresh = 1

                        else:
                            # print('if-else-op_page_errSpanDiv == ')
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            # print('if refresh == 1:---')
                            break

                    if Record_not_found == 1:
                        # print('if Record_not_found == 1:---')
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        # print('elif Record_not_found == 0:---')
                        WebDriverWait(driverHCP_, 900).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # < wait
                        time.sleep(1)

                        # <-------/// code for extract only CASE NO. & CNR NO. ///----------
                        # <-------/// get <a> tag data ///----------
                        a_tag_data = driverHCP_.find_elements(by=By.TAG_NAME, value="a")
                        try:
                            a_list = []
                            for lnk in a_tag_data:
                                a_list.append(str(lnk.get_attribute('onclick')))
                            # print('a_list:-', a_list)   #viewHistory('201500003992018','MHNS010066912018','1');return false;


                            dict_res_dct = {}
                            count = 0
                            for l in a_list:
                                if 'viewHistory' in l:
                                    count = count + 1
                                    k3 = str(l).split("'")
                                    # ['viewHistory(', '201500003992018', ',', 'MHNS010066912018', ',', '1', ');return false;']
                                    # print('[k3[1], k3[3]]:-', [k3[1], k3[3]])  #..get this..['201500003992018', 'MHNS010066912018']
                                    lst = [k3[1], k3[3]]

                                    # ---- list into dict convert ----[] --> {}---
                                    res_dct = {lst[i]: lst[i + 1] for i in range(0, len(lst), 2)}
                                    print(res_dct)  # .. {'201500003992018': 'MHNS010066912018'}
                                    # list_res_dct.update(res_dct)
                                    dict_res_dct[count] = res_dct
                            # print('dict_res_dct:-', dict_res_dct)
                        except:
                            pass
                        # ------------------------------------------------------------------------------------------

                        # <======---/// Final all List conversion start ///---======>
                        WebDriverWait(driverHCP_, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="showList1"]/tr[1]/td[4]/a')))
                        # delay2()
                        await asyncio.sleep(2)
                        html = driverHCP_.page_source
                        soup = BeautifulSoup(html, "html.parser")

                        # <---List Table, { build with <th> tag } -----
                        # -------------------------------------------------------------
                        try:
                            table_data = soup.find('table', attrs={'id': 'showList3'})
                            #print('---------1-----------')
                            h, [_, *d] = [i.text for i in table_data.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_data.find_all('tr')]
                            res_dct_data = [dict(zip(h, i)) for i in d]
                            # print('res_dct_data:--', res_dct_data)
                            # print('---------2-----------')

                            k5 = [] # for remove kye space's
                            for d in res_dct_data:
                                if d['Sr No'].isdigit():
                                    k5.append({k.replace(' ', ''): v for k, v in d.items()})
                            # print('k5:-', k5)
                            # print('---------3-----------')

                            #----------------------------------
                            final_list = []
                            count = 0
                            for f_dict in k5:
                                string = f_dict['View']  # for case number SCA/5400/2020
                                res = ''.join(
                                    filter(lambda i: i.isdigit(), string))  # ..extract all 'digits' from str "54662019"
                                # print('res:- ', res)
                                # -update -
                                f_dict['View'] = res

                                for k, v in dict_res_dct.items():
                                    # print(k,v)
                                    # -- for match two kye's --
                                    if str(f_dict['SrNo']) == str(k):
                                        # print(f_dict['SrNo'], ',', k, ',', f_dict['View']) #1 , 1

                                        # -- for check res data in [view] --
                                        if res in str(f_dict['View']):
                                            # print(res, ',',  f_dict['View'])
                                            for k, v in v.items():
                                                f_dict['View'] = k
                                                f_dict['CNR'] = v
                                                final_list.append(f_dict)  # .. append in list

                            # print('final_list:-', final_list)
                            # ----------------------------------
                            # print('---------4-----------')
                            res_dct = {"TableData": final_list}
                            # --------------------------------------

                        except Exception as e:
                            #print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}


                        # --------------------------------------------------------------------

                        if res_dct != {}:
                            case_final = res_dct
                            try:
                                driverHCP_.quit()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {'ThreadId': str(threading.get_native_id()),
                                            "CaseDetailsNotFound": 'This Case Code does not exists/Data not available.'}
                            try:
                                driverHCP_.quit()
                                break
                            except:
                                pass

                        else:
                            try:
                                driverHCP_.quit()
                                break
                            except:
                                pass

                        main_ch2 = 1
#--------------------------------------------------
                # ===-{inner Try Exception}--==
                except Exception as e:
                    try:
                        # print('inner Try Exception:---', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
                        driverHCP_.quit()
                    except:
                        pass
                if main_ch2 == 1:
                    break
        else:
            # print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driverHCP_.quit()

    # ===---{main try Exception }--===
    except Exception as e:
        try:
            # print('main try Exception:--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driverHCP_.quit()
        except:
            pass
    finally:
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2

        data = yourdata  # Final response
        # print('data:--', data)
        # ----------------

    return JsonResponse(data)
    # -------{ main try end}----------------

#==================---{ ADVOCATE SEARTCH }-----===============
async def HighCourtAdvocateNameView(request, state, dist, courtcode, statename, advocate):
    global Record_not_found, dict_res_dct, res_dct, case_final, case_final_2, yourdata, captcha_op
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    driverHCADVO_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)

    driverHCADVO_.get(
        'https://services.ecourts.gov.in/ecourtindiaHC/cases/qs_civil_advocate.php?state_cd={s}&dist_cd={d}&court_code={cc}&stateNm={sn}'.format(
            s=state, d=dist, cc=courtcode, sn=statename))
    # print('input data:-Advocate-', state, ',', dist, ',', courtcode, ',', statename, ',', advocate)
    # print('Petitioner_func--')

    WebDriverWait(driverHCADVO_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "h1")))  # wait

    # =====--{ all repeated Functions }--======
    def delay1():
        time.sleep(random.randint(1, 2))

    def delay2():
        time.sleep(random.randint(2, 2))

    # <--Main (try)---
    try:
        html = driverHCADVO_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        # print('h1_data:-', h1_data)

        # <--Main (if)---
        if 'High Court' in str(h1_data):
            while True:
                main_ch2 = 0
                # ===-{inner Try}--==
                try:
                    WebDriverWait(driverHCADVO_, 900).until(EC.presence_of_element_located((By.ID, "mainDiv")))  # wait
                    await asyncio.sleep(3)
                    # ============================/ MAIN CODE /=========================================
                    WebDriverWait(driverHCADVO_, 900).until(EC.presence_of_element_located((By.ID, "mainDiv")))  # wait
                    WebDriverWait(driverHCADVO_, 900).until(
                        EC.element_to_be_clickable(
                            (By.XPATH, '//*[@id="captcha_image_audio_controls"]/a')))  # wait audio btn
                    WebDriverWait(driverHCADVO_, 900).until(
                        EC.element_to_be_clickable(
                            (By.XPATH, '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')))  # wait refresh

                    await asyncio.sleep(3)  # required

                    # <--------Advocate INPUT FIELD---///--Start----///--------->
                    WebDriverWait(driverHCADVO_, 600).until(
                        EC.presence_of_element_located((By.ID, 'advocate_name')))  # wait

                    case_elem = driverHCADVO_.find_element(By.ID, "advocate_name")
                    advo = str(advocate).replace('_', ' ').replace('-', ' ')
                    case_elem.send_keys(advo)  ##### <---- Pass advocate ---->
                    # <-----------///--End----///--------->

                    # ============================/ MAIN CODE END /=====================================

                    # <---------------------------/ Captcha code /---------------------------------
                    # -----------------------------------------------------------------------------
                    while True:
                        refresh = 0
                        driverHCADVO_.find_element_by_xpath(
                            '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a').click()  # refresh btn
                        delay1()
                        driverHCADVO_.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        delay1()
                        WebDriverWait(driverHCADVO_, 600).until(
                            EC.element_to_be_clickable(
                                (By.XPATH, '//*[@id="mainDiv"]/div[7]/span[3]/input[1]')))  # <--GO-- wait --->

                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        element = driverHCADVO_.find_element_by_id("captcha_image")
                        location = element.location
                        size = element.size

                        driverHCADVO_.save_screenshot(
                            settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}imageHCADVOCATE_CASE.png')

                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(
                            settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}imageHCADVOCATE_CASE.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(
                            settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}cropimageHCADVOCATE_CASE.png')
                        #im.show()  # <--- comment it ----
                        # print("1'st while image:------")
                        # <--------// end //----

                        # <---- for Captcha image to text convert-------------
                        # -----------------------------------------------------------------------------
                        # # --------API CODE--------------

                        with open(
                                settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}cropimageHCADVOCATE_CASE.png',
                                'rb') as captcha_file:
                            captcha_op = api.solve(captcha_file)
                        captcha_txt2 = captcha_op.await_result()
                        # print('API code captcha text:--', captcha_txt2)
                        # print('len(captcha_txt2):--', len(str(captcha_txt2))) #<-- len 5/6
                        # print('type(captcha_txt2):--', type(captcha_txt2))     #<-- type (str)
                        # <------- Find_data--------/// END ///---------------------------

                        # <--OLD---- for Captcha image to text convert---------------comment it------------
                        '''
                        img = Image.open(f"{settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}cropimageHCADVOCATE_CASE.png'}")
                        captcha_txt = tess.image_to_string(img, config='')'''
                        #punc = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
                        '''for ele in captcha_txt:
                            if ele in punc:
                                captcha_txt = captcha_txt.replace(ele, "")
                        # print('captcha_txt2-OLD CODE-:', captcha_txt)
                        captcha_txt2 = captcha_txt'''
                        # -------------------------------------------------------------------------

                        # captcha_txt2 = 'abcde'     #<--- for test only

                        # -------------------------------------------------------------------------
                        re_captcha_txt_final = captcha_txt2.replace('I', 'l').replace('i', 'l')
                        # -------------------------------------------------------------------------

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driverHCADVO_.find_element_by_id('captcha')
                        input_element.send_keys(str(re_captcha_txt_final))  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        await asyncio.sleep(1)

                        # <----- for Click Search btn---------------------
                        driverHCADVO_.find_element_by_xpath(
                            '//*[@id="mainDiv"]/div[7]/span[3]/input[1]').click()  # <--- sertch btn --->
                        # print('seartch btn clicked ------')
                        await asyncio.sleep(2)
                        # <--------------------------------------------------------------------------------------------------------

                        html = driverHCADVO_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display:none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        op_page_showList2Div = soup.find('div', {'id': 'showList2'})
                        # print('-------------------------------')
                        # print('op_page_showList2Div:-- ', op_page_showList2Div)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '' or op_page_errSpanDiv == "display:block;":
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('input')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            # print('check2:-first---', check2)
                            if 'Invalid Captcha' in check2:
                                # delay1()
                                # print('check2----Invalid Captcha')
                                os.remove(f"{settings.MEDIA_ROOT + f'/AdvocateImages/{str(threading.get_native_id())}cropimageHCADVOCATE_CASE.png'}")
                                refresh = 0

                            elif 'Record Not Found' in check2:
                                # print('check2-----Record Not Found')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driverHCADVO_.quit()
                                refresh = 1

                        elif 'Record Not Found' in str(op_page_showList2Div):
                            res_dct = {}
                            case_final_2 = {'RecordNotFound': 'Record not found'}
                            Record_not_found = 1
                            driverHCADVO_.quit()
                            refresh = 1

                        else:
                            # print('if-else-op_page_errSpanDiv == ')
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            # print('if refresh == 1:---')
                            break

                    if Record_not_found == 1:
                        # print('if Record_not_found == 1:---')
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        # print('elif Record_not_found == 0:---')
                        WebDriverWait(driverHCADVO_, 900).until(
                            EC.presence_of_element_located((By.ID, "showList3")))  # < wait
                        time.sleep(1)

                        # <-------/// code for extract only CASE NO. & CNR NO. ///----------
                        # <-------/// get <a> tag data ///----------
                        a_tag_data = driverHCADVO_.find_elements(by=By.TAG_NAME, value="a")
                        try:
                            a_list = []
                            for lnk in a_tag_data:
                                a_list.append(str(lnk.get_attribute('onclick')))
                            # print('a_list:-', a_list)   #viewHistory('201500003992018','MHNS010066912018','1');return false;


                            dict_res_dct = {}
                            count = 0
                            for l in a_list:
                                if 'viewHistory' in l:
                                    count = count + 1
                                    k3 = str(l).split("'")
                                    # ['viewHistory(', '201500003992018', ',', 'MHNS010066912018', ',', '1', ');return false;']
                                    # print('[k3[1], k3[3]]:-', [k3[1], k3[3]])  #..get this..['201500003992018', 'MHNS010066912018']
                                    lst = [k3[1], k3[3]]

                                    # ---- list into dict convert ----[] --> {}---
                                    res_dct = {lst[i]: lst[i + 1] for i in range(0, len(lst), 2)}
                                    # print(res_dct)  # .. {'201500003992018': 'MHNS010066912018'}
                                    # list_res_dct.update(res_dct)
                                    dict_res_dct[count] = res_dct
                            # print('----------------------------')
                            # print('dict_res_dct:-', dict_res_dct)
                            # print('----------------------------')
                        except:
                            pass
                        # ------------------------------------------------------------------------------------------

                        # <======---/// Final all List conversion start ///---======>
                        WebDriverWait(driverHCADVO_, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="showList1"]/tr[1]/td[5]/a')))
                        # delay2()
                        await asyncio.sleep(2)
                        html = driverHCADVO_.page_source
                        soup = BeautifulSoup(html, "html.parser")

                        # <---List Table, { build with <th> tag } -----
                        # -------------------------------------------------------------
                        try:
                            table_data = soup.find('table', attrs={'id': 'showList3'})
                            #print('---------1-----------')
                            h, [_, *d] = [i.text for i in table_data.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_data.find_all('tr')]
                            res_dct_data = [dict(zip(h, i)) for i in d]
                            # print('res_dct_data:--', res_dct_data)
                            # print('---------2-----------')

                            k5 = [] # for remove kye space's
                            for d in res_dct_data:
                                if d['Sr No'].isdigit():
                                    k5.append({k.replace(' ', ''): v.replace('\n', '') for k, v in d.items()})
                            # print('k5:-', k5)
                            # print('---------3-----------')

                            #----------------------------------
                            final_list = []
                            count = 0
                            for f_dict in k5:
                                string = f_dict['View']  # for case number SCA/5400/2020
                                res = ''.join(
                                    filter(lambda i: i.isdigit(), string))  # ..extract all 'digits' from str "54662019"
                                # print('res:- ', res)
                                # -update -
                                f_dict['View'] = res # update dict with "54662019"

                                for k, v in dict_res_dct.items():
                                    # print(k,v)
                                    # -- for match two kye's --
                                    if str(f_dict['SrNo']) == str(k):
                                        # print(f_dict['SrNo'], ',', k, ',', f_dict['View']) #1 , 1

                                        # -- for check res data in [view] --
                                        if res in str(f_dict['View']):
                                            # print(res, ',',  f_dict['View'])
                                            for k, v in v.items():
                                                f_dict['View'] = k
                                                f_dict['CNR'] = v
                                                final_list.append(f_dict)  # .. append in list

                            # print('final_list:-', final_list)
                            # ----------------------------------
                            # print('---------4-----------')
                            res_dct = {"TableData": final_list}
                            # --------------------------------------

                        except Exception as e:
                            #print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}


                        # --------------------------------------------------------------------

                        if res_dct != {}:
                            case_final = res_dct
                            try:
                                driverHCADVO_.quit()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {'ThreadId': str(threading.get_native_id()),
                                            "CaseDetailsNotFound": 'This Case Code does not exists/Data not available.'}
                            try:
                                driverHCADVO_.quit()
                                break
                            except:
                                pass

                        else:
                            try:
                                driverHCADVO_.quit()
                                break
                            except:
                                pass

                        main_ch2 = 1
#--------------------------------------------------
                # ===-{inner Try Exception}--==
                except Exception as e:
                    try:
                        # print('inner Try Exception:---', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
                        driverHCADVO_.quit()
                    except:
                        pass
                if main_ch2 == 1:
                    break
        else:
            # print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driverHCADVO_.quit()

    # ===---{main try Exception }--===
    except Exception as e:
        try:
            # print('main try Exception:--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driverHCADVO_.quit()
        except:
            pass
    finally:
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2

        data = yourdata  # Final response
        # print('data:--', data)
        # ----------------

    return JsonResponse(data)
    # -------{ main try end}----------------


#===========================================================================================
                                  # { NCLT SEARTCH } #
#===========================================================================================
#==================---{ CASE NO SEARTCH }-----================
async def NcltCaseNoView(request, zonal_bench, case_type, case_number, case_year):
    #https://nclt.gov.in/case-number-wise
    # -----------Code start-----
    global res_dct, case_final_2, yourdata, Table_1_res_dct_data, case_final
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    driverNCLTc_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)

    driverNCLTc_.get('https://nclt.gov.in/case-number-wise')

    WebDriverWait(driverNCLTc_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "body")))  # wait

    # <--//- all repeated Functions ----//---
    def delay1():
        time.sleep(random.randint(1, 2))
        # await asyncio.sleep(1)

    def delay2():
        time.sleep(random.randint(2, 2))
        # await asyncio.sleep(2)

    # <--==== Main (try) =====---
    try:
        html = driverNCLTc_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        # print('h1_data:-', h1_data)

        # <--Main (if)---
        if h1_data == None:
            # #============================/ Main Code /=========================================
            # -- wait ---
            WebDriverWait(driverNCLTc_, 600).until(EC.presence_of_element_located((By.CLASS_NAME, "menu-sec-wraper")))
            WebDriverWait(driverNCLTc_, 900).until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-default')))

            # <-------/////* Zonal Bench *//////-----------------
            WebDriverWait(driverNCLTc_, 900).until(EC.presence_of_element_located((By.ID, 'bench')))

            html = driverNCLTc_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'bench'})
            all_options = all_divs.find_all('option')
            # print('all_options:-', all_options)

            k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', 'ahmedabad', 'allahabad',..]
            # print('K3:-', k3)

            # # <---- take input zonal_bench value from fun() --
            com = str(zonal_bench).lower()
            index3 = k3.index(com)
            zonal_bench_option = index3 + 1

            # # <------ Click on 'ZONAL BENCH' DROP DOWN ---->
            driverNCLTc_.find_elements_by_id('bench')[0].click()

            # # <------ Select 'ZONAL' Options ---->
            zonal_drop = \
                driverNCLTc_.find_elements_by_xpath('//*[@id="bench"]/option[{}]'.format(zonal_bench_option))[0]
            # print('selected zonal bench:- ', zonal_drop.text)
            zonal_drop.click()

            await asyncio.sleep(1) # required

            # <-----///////* Case Type *///////-----------------
            WebDriverWait(driverNCLTc_, 900).until(EC.presence_of_element_located((By.ID, 'case_type')))

            html = driverNCLTc_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'case_type'})
            all_options = all_divs.find_all('option')
            # print('case_type all_options:-', all_options)

            k4 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
            # print('K4:-', k4)

            # # <---- take input case_type value from fun() --
            com = case_type
            index3 = k4.index(com)
            case_type_option = index3 + 1

            # # <------ Click on 'CASE TYPE' DROP DOWN ---->
            driverNCLTc_.find_elements_by_id('case_type')[0].click()

            # # <------ Select 'CASE' Options ---->
            case_type_drop = \
                driverNCLTc_.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_type_option))[0]
            # print('selected case_type:- ', case_type_drop.text)
            case_type_drop.click()
            await asyncio.sleep(1)  # required

            # <-----//////* Case Number input field*--///----------
            WebDriverWait(driverNCLTc_, 600).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="case_number"]')))  # wait
            case_elem = driverNCLTc_.find_element(By.ID, "case_number")
            case_elem.send_keys(case_number)  ##### <---- Pass case no ---->

            # <-----///////* Case Year*--///----------
            html = driverNCLTc_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'case_year'})
            all_options = all_divs.find_all('option')
            # print('case_year all_options:-', all_options)

            k5 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
            # print('K5:-', k5)

            # # <---- take input case_year value from fun() --
            com = case_year
            index3 = k5.index(com)
            case_year_option = index3 + 1

            # # <------ Click on 'CASE TYPE' DROP DOWN ---->
            driverNCLTc_.find_elements_by_id('case_year')[0].click()

            # # <------ Select 'CASE' Options ---->
            case_year_drop = \
                driverNCLTc_.find_elements_by_xpath('//*[@id="case_year"]/option[{}]'.format(case_year_option))[0]
            # print('selected case_year:- ', case_year_drop.text)
            case_year_drop.click()
            await asyncio.sleep(1)  # required

            # <-----///* for Click Search btn *///-------------------
            driverNCLTc_.find_element_by_class_name('btn-default').click()  # <--- sertch btn --->
            # print('seartch btn clicked ------')
            await asyncio.sleep(1)      # required

            # <--------///* for Next page *///-------
            WebDriverWait(driverNCLTc_, 600).until(EC.presence_of_element_located((By.ID, 'block-nclt-content')))
            html = driverNCLTc_.page_source
            soup = BeautifulSoup(html, "html.parser")
            op_table_data = soup.find('table', {'class': 'table'})

            # print('-------------------------------')
            # print('op_table_data: ', op_table_data)
            # print('-------------------------------')

            # <-------//* if data not found *///-------------
            try:
                if 'for data prior' in str(op_table_data):
                    # print('check 1-----Record Not Found')
                    res_dct = {}
                    case_final_2 = {'RecordNotFound': 'Record not found'}
                    Record_not_found = 1
                    driverNCLTc_.quit()

                elif 'for data prior' not in str(op_table_data):
                    # print('data---------')
                    # <-----// click 'Disposed' btn ///----
                    driverNCLTc_.find_element_by_xpath(
                        '//*[@id="block-nclt-content"]/div/div/div[2]/table/tbody/tr/td[6]/a').click()
                    # print('Click Disposed btn---')

                    # <-------//* For New Tab Data *///-------------
                    # get current window handle
                    p = driverNCLTc_.current_window_handle
                    # get first child window
                    chwd = driverNCLTc_.window_handles

                    for w in chwd:
                        # switch focus to child window
                        if (w != p):
                            driverNCLTc_.switch_to.window(w)
                            break
                    # <--------------------------------------

                    WebDriverWait(driverNCLTc_, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="block-nclt-content"]')))

                    html = driverNCLTc_.page_source
                    soup = BeautifulSoup(html, "html.parser")

                    # <---1st Table, { head with <tr> tag } -----
                    # -------------------------------------------------------------
                    try:
                        l = []
                        table_case = soup.find('table', attrs={'class': 'table'})
                        table_case_body = table_case.find('tbody')
                        # print('----------------------------------------------')
                        # print('table_case_body--- ', table_case_body)
                        # print('----------------------------------------------')

                        rows = table_case_body.find_all('tr')
                        for row in rows:
                            cols = row.find_all('td')
                            cols = [ele.text.strip() for ele in cols]
                            l.append([ele for ele in cols if ele])  # Get rid of empty values

                        if len(l[2]) == 2:
                            l[2].insert(1, ''), l[2].insert(3, '')
                            # print('l[2]:---', l[2])#['Petitioner Advocate(s)', '', 'Respondent Advocate(s)', '']
                            # print('----------------------------------------------')

                        if len(l[2]) == 3:
                            if l[2][1] == 'Respondent Advocate(s)':
                                l[2].insert(1, '')
                                # print('l[2]:--yes- ', l[2])
                                # print('----------------------')

                            else:
                                l[2].insert(3, '')
                                # print('l[2]:---no-', l[2])
                                # print('-------------------')

                        lst = reduce(lambda x, y: x + y, l)
                        # lst.append('cnrnumber')  # < ---- add 'CNR' name
                        # lst.append(b1)  # < ---- add 'CNR number
                        Table_1_res_dct_data = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                        # print('Table_1_res_dct_data:-- ', Table_1_res_dct_data)
                        # print('----------------------------------------------')


                    except Exception as e:
                        # print('Exception Table_1:-- ', e)
                        # print('-------------------------------')
                        Table_1_res_dct_data = {}

                    try:
                        # <---2nd Table ---All Parties--------->
                        table = soup.find('div', {'id': 'collapseOne'})
                        table_a = table.find('table', attrs={'class': 'table-bordered'})
                        h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                            [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                        res_dct2 = [dict(zip(h, i)) for i in d]

                        k2 = []
                        for d in res_dct2:
                            k2.append(
                                {k.replace(' ', ''): v.replace('\n', '').replace('\t', '').replace(',', '') for
                                 k, v in d.items()})


                    except:
                        k2 = []

                    try:
                        # <---3nd Table -----Listing History Orders------->
                        table3 = soup.find('div', {'id': 'collapseTwo'})
                        table_a = table3.find('table', attrs={'class': 'table-bordered'})
                        h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                            [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                        res_dct3 = [dict(zip(h, i)) for i in d]
                        # print('res_dct3:-- ', res_dct3)
                        # print('----------------------------------------------')

                        k3 = []
                        for d in res_dct3:
                            k3.append(
                                {k.replace(' ', ''): v.replace('\n', '').replace('\t', '').replace(',', '') for
                                 k, v in d.items()})

                        # print('k3:-- ', k3)
                        # print('----------------------------------------------')

                        # -------------------/ for view URL /------------------------------
                        all_a_url = table3.find_all('a', href=True)
                        # print('all_a_url:-- ', all_a_url)
                        for n in range(len(k3)):
                            dict_n = k3[n]
                            # print('---------------------')
                            # print('dict_n:-- ', dict_n)
                            # print('---------------------')

                            # print('url:-- ', all_a_url[n]['href'])
                            # print('---------------------')
                            dict_n['Order/Judgement'] = 'https://nclt.gov.in/'+all_a_url[n]['href']

                        # -------------------------------------------------
                    except:
                        k3 = []

                    try:
                        # <---4nd Table ----IA/MA-------->
                        table4 = soup.find('div', {'id': 'collapseThree'})
                        table_a = table4.find('table', attrs={'class': 'table-bordered'})
                        h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                            [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                        res_dct4 = [dict(zip(h, i)) for i in d]
                        # print('res_dct4:-- ', res_dct4)
                        # print('----------------------------------------------')

                        k4 = []
                        for d in res_dct4:
                            k4.append(
                                {k.replace(' ', ''): v.replace('\n', '').replace('\t', '').replace(',', '') for
                                 k, v in d.items()})

                        # print('k4:-- ', k4)
                        # print('----------------------------------------------')
                    except:
                        k4 = []

                    try:
                        # <---5nd Table -----Connected Matters------->
                        table5 = soup.find('div', {'id': 'collapsefourth'})
                        table_a = table5.find('table', attrs={'class': 'table-bordered'})
                        h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                            [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                        res_dct5 = [dict(zip(h, i)) for i in d]
                        # print('res_dct5:-- ', res_dct5)
                        # print('----------------------------------------------')

                        k5 = []
                        for d in res_dct5:
                            k5.append(
                                {k.replace(' ', ''): v.replace('\n', '').replace('\t', '').replace(',', '') for
                                 k, v in d.items()})

                        # print('k5:-- ', k5)
                        # print('----------------------------------------------')
                    except:
                        k5 = []

                    # ===--{ FINAL OUTPUT DATA}--====
                    res_dct = {'CaseDetails': Table_1_res_dct_data,
                                   'AllParties': k2,
                                   'ListingHistoryOrders': k3,
                                   'IaMa': k4,
                                   'ConnectedMatters': k5,}

                    # -------------------------------

                    if res_dct != {}:
                        case_final = res_dct
                        try:
                            driverNCLTc_.quit()
                        except:
                            pass

                    else:
                        try:
                            driverNCLTc_.quit()
                        except:
                            pass
            # -------------------------------------------------
            except Exception as e:
                res_dct = {}
                case_final_2 = {'ThreadId': str(threading.get_native_id()),
                                        "CaseDetailsNotFound": 'This Case Code does not exists/Data not available.'}
                driverNCLTc_.quit()

            # #============================/ Main Code/=========================================

        else:
            #print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driverNCLTc_.quit()
    #----------------------------------------------------------
    except Exception as e:
        try:
            # print('except Main Try--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driverNCLTc_.quit()
        except:
            pass
    finally:
        # print('finally:--')
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2

        data = yourdata                 # Final response
        # print('data:--', data)
        # ----------------

    return JsonResponse(data)

#==================---{ PARTY NAME SEARTCH }-----================
async def NcltPartyNameView(request, zonal_bench, party_type, party_name, case_year, case_status):
    #https://nclt.gov.in/case-number-wise
    # -----------Code start-----
    global res_dct, case_final_2, yourdata, Table_1_res_dct_data, case_final, final_dict
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    driverNCLTp_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)

    driverNCLTp_.get('https://nclt.gov.in/party-name-wise')

    WebDriverWait(driverNCLTp_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "body")))  # wait

    # <--//- all repeated Functions ----//---
    def delay1():
        time.sleep(random.randint(1, 2))
        # await asyncio.sleep(1)

    def delay2():
        time.sleep(random.randint(2, 2))
        # await asyncio.sleep(2)

    # <--==== Main (try) =====---
    try:
        html = driverNCLTp_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        # print('h1_data:-', h1_data)

        # <--Main (if)---
        if h1_data == None:
            # #============================/ Main Code /=========================================
            # -- wait ---
            WebDriverWait(driverNCLTp_, 600).until(EC.presence_of_element_located((By.ID, "mid-section")))
            WebDriverWait(driverNCLTp_, 900).until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-default')))

            # <-------/////* Zonal Bench *//////-----------------
            WebDriverWait(driverNCLTp_, 900).until(EC.presence_of_element_located((By.ID, 'bench')))

            html = driverNCLTp_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'bench'})
            all_options = all_divs.find_all('option')
            # print('all_options:-', all_options)

            k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', 'ahmedabad', 'allahabad',..]
            # print('K3:-', k3)

            # # <---- take input zonal_bench value from fun() --
            com = str(zonal_bench).lower()
            index3 = k3.index(com)
            zonal_bench_option = index3 + 1

            # # <------ Click on 'ZONAL BENCH' DROP DOWN ---->
            driverNCLTp_.find_elements_by_id('bench')[0].click()

            # # <------ Select 'ZONAL' Options ---->
            zonal_drop = \
                driverNCLTp_.find_elements_by_xpath('//*[@id="bench"]/option[{}]'.format(zonal_bench_option))[0]
            # print('selected zonal bench:- ', zonal_drop.text)
            zonal_drop.click()

            await asyncio.sleep(1) # required

            # <-----///////* Party Type *///////-----------------
            WebDriverWait(driverNCLTp_, 900).until(EC.presence_of_element_located((By.ID, 'party_type')))

            html = driverNCLTp_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'party_type'})
            all_options = all_divs.find_all('option')
            # print('case_type all_options:-', all_options)

            k4 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
            # print('K4:-', k4)

            # # <---- take input party_type value from fun() --
            com = party_type
            index3 = k4.index(com)
            party_type_option = index3 + 1

            # # <------ Click on 'PARTY TYPE' DROP DOWN ---->
            driverNCLTp_.find_elements_by_id('party_type')[0].click()

            # # <------ Select 'PARTY' Options ---->
            party_type_drop = \
                driverNCLTp_.find_elements_by_xpath('//*[@id="party_type"]/option[{}]'.format(party_type_option))[0]
            # print('selected party_type:- ', party_type_drop.text)
            party_type_drop.click()
            await asyncio.sleep(1)  # required


            # <-----//////* Party Name input field*--///----------
            WebDriverWait(driverNCLTp_, 600).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="party_name"]')))  # wait
            party_elem = driverNCLTp_.find_element(By.ID, "party_name")
            p_name = str(party_name).replace('_', ' ').replace('-', ' ')
            party_elem.send_keys(p_name)  ##### <---- Pass party_name ---->

            # <-----///////* Case Year*--///----------
            html = driverNCLTp_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'case_year'})
            all_options = all_divs.find_all('option')
            # print('case_year all_options:-', all_options)

            k5 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
            # print('K5:-', k5)

            # # <---- take input case_year value from fun() --
            com = case_year
            index3 = k5.index(com)
            case_year_option = index3 + 1

            # # <------ Click on 'CASE YEAR' DROP DOWN ---->
            driverNCLTp_.find_elements_by_id('case_year')[0].click()

            # # <------ Select 'CASE' Options ---->
            case_year_drop = \
                driverNCLTp_.find_elements_by_xpath('//*[@id="case_year"]/option[{}]'.format(case_year_option))[0]
            # print('selected case_year:- ', case_year_drop.text)
            case_year_drop.click()
            await asyncio.sleep(1)  # required


            # <-----///////* Case STATUS *--///----------
            html = driverNCLTp_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'case_status'})
            all_options = all_divs.find_all('option')
            # print('case_status all_options:-', all_options)

            k5_status = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
            # print('k5_status:-', k5_status)

            # # <---- take input case_year value from fun() --
            com = case_status
            index3 = k5_status.index(com)
            case_year_option = index3 + 1

            # # <------ Click on 'CASE STATUS' DROP DOWN ---->
            driverNCLTp_.find_elements_by_id('case_status')[0].click()

            # # <------ Select 'case_status' Options ---->
            case_status_drop = \
                driverNCLTp_.find_elements_by_xpath('//*[@id="case_status"]/option[{}]'.format(case_year_option))[0]
            # print('selected case_status:- ', case_status_drop.text)
            case_status_drop.click()
            await asyncio.sleep(1)  # required


            # <-----///* for Click Search btn *///-------------------
            driverNCLTp_.find_element_by_class_name('btn-default').click()  # <--- sertch btn --->
            # print('seartch btn clicked ------')
            await asyncio.sleep(1)      # required


            # <--------///* for Next page *///-------
            WebDriverWait(driverNCLTp_, 600).until(EC.presence_of_element_located((By.ID, 'block-nclt-content')))
            html = driverNCLTp_.page_source
            soup = BeautifulSoup(html, "html.parser")
            op_table_data = soup.find('table', {'class': 'table'})

            # print('-------------------------------')
            # print('op_table_data: ', op_table_data)
            # print('-------------------------------')

            # <-------//* if data not found *///-------------
            try:
                if 'for data prior' in str(op_table_data):
                    # print('check 1-----Record Not Found')
                    res_dct = {}
                    case_final_2 = {'RecordNotFound': 'Record not found'}
                    Record_not_found = 1
                    driverNCLTp_.quit()

                elif 'for data prior' not in str(op_table_data):
                    # print('data---------')

                    WebDriverWait(driverNCLTp_, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="block-nclt-content"]')))

                    html = driverNCLTp_.page_source
                    soup = BeautifulSoup(html, "html.parser")

                    # <-----///* Check pagination *///--------
                    pagination_data = soup.find('div', attrs={'class': 'table-responsive'})
                    all_divs = soup.find('div', {'class': 'table-responsive'})
                    pagination_data = all_divs.find_all('li')
                    # print('------------------------------')
                    # print('pagination_data:-', pagination_data)
                    # print('------------------------------')

                    k5_pagination = [str(x).split('"')[1] for x in pagination_data]
                    # print('------------------------------')
                    # print('k5_pagination:-', k5_pagination) #['disabled', 'page-item', 'page-item', 'page-item'
                    # print('------------------------------')                 #, 'page-item']

                    if len(k5_pagination) <= 3:
                        # print('pages:-', len(k5_pagination))
                        # <---Table, { head with <th> tag } -----
                        # -------------------------------------------------------------
                        try:
                            table_data = soup.find('table', attrs={'class': 'table'})
                            h, [_, *d] = [i.text for i in table_data.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_data.find_all('tr')]
                            res_dct_data = [dict(zip(h, i)) for i in d]
                            # print('res_dct_data:--', res_dct_data)
                            # print('--------------------')

                            k2 = []
                            for d in res_dct_data:
                                k2.append(
                                    {k.replace(' ', '').replace('.', ''): v for k, v in d.items()})
                            final_dict = {'page1': k2}

                        except Exception as e:
                            # print('Exception:--', e)
                            # print('--------------------')
                            final_dict = {}

                    else:   #--- / for page greater than 1 page /---
                        final_dict = {}
                        for n in range(2, len(k5_pagination)-1):
                            driverNCLTp_.find_elements_by_xpath('//*[@id="block-nclt-content"]/div/div/div[2]/div/ul/li[{}]/a'.format(n))[0].click()

                            WebDriverWait(driverNCLTp_, 600).until(
                                EC.presence_of_element_located((By.XPATH, '//*[@id="block-nclt-content"]'))) #-- wait --

                            WebDriverWait(driverNCLTp_, 600).until(
                                EC.element_to_be_clickable((By.XPATH, '//*[@id="block-nclt-content"]/div/div/div[2]/div/ul/li[1]/a')))
                                      # -- wait --

                            html = driverNCLTp_.page_source
                            soup = BeautifulSoup(html, "html.parser")

                            # <---Table, { head with <th> tag } -----
                            # -------------------------------------------------------------
                            try:
                                table_data = soup.find('table', attrs={'class': 'table'})
                                h, [_, *d] = [i.text for i in table_data.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_data.find_all('tr')]
                                res_dct_data = [dict(zip(h, i)) for i in d]
                                # print('res_dct_data:--', res_dct_data)
                                # print('--------------------')


                                k2 = []
                                for d in res_dct_data:
                                    k2.append(
                                        {k.replace(' ', '').replace('.', ''): v for k, v in d.items()})

                                final_dict[f'page{n-1}'] = k2

                            except Exception as e:
                                # print('Exception:--', e)
                                # print('--------------------')
                                final_dict = {}


                    # ===--{ FINAL OUTPUT DATA}--====
                    res_dct = {"TableData": final_dict}

                    # -------------------------------

                    if res_dct != {}:
                        case_final = res_dct
                        try:
                            driverNCLTp_.quit()
                        except:
                            pass

                    else:
                        try:
                            driverNCLTp_.quit()
                        except:
                            pass
            # -------------------------------------------------
            except Exception as e:
                res_dct = {}
                case_final_2 = {'ThreadId': str(threading.get_native_id()),
                                        "CaseDetailsNotFound": 'This Case Code does not exists/Data not available.'}
                driverNCLTp_.quit()

            # #============================/ Main Code/=========================================

        else:
            #print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driverNCLTp_.quit()
    #----------------------------------------------------------
    except Exception as e:
        try:
            # print('except Main Try--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driverNCLTp_.quit()
        except:
            pass
    finally:
        # print('finally:--')
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2

        data = yourdata                 # Final response
        # print('data:--', data)
        # ----------------

    return JsonResponse(data)

#==================---{ ADVOCATE NAME SEARTCH }-----================
async def NcltAdvocateNameView(request, bench, advocate_name, year):
    #https://nclt.gov.in/case-number-wise
    # -----------Code start-----
    global res_dct, case_final_2, yourdata, Table_1_res_dct_data, case_final, final_dict
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    driverNCLTa_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)

    driverNCLTa_.get('https://nclt.gov.in/advocate-name-wise')

    WebDriverWait(driverNCLTa_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "body")))  # wait

    # <--//- all repeated Functions ----//---
    def delay1():
        time.sleep(random.randint(1, 2))
        # await asyncio.sleep(1)

    def delay2():
        time.sleep(random.randint(2, 2))
        # await asyncio.sleep(2)

    # <--==== Main (try) =====---
    try:
        html = driverNCLTa_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        # print('h1_data:-', h1_data)

        # <--Main (if)---
        if h1_data == None:
            # #============================/ Main Code /=========================================
            # -- wait ---
            WebDriverWait(driverNCLTa_, 600).until(EC.presence_of_element_located((By.ID, "mid-section")))
            WebDriverWait(driverNCLTa_, 900).until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-default')))

            # <-------/////* Zonal Bench *//////-----------------
            WebDriverWait(driverNCLTa_, 900).until(EC.presence_of_element_located((By.ID, 'bench')))

            html = driverNCLTa_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'bench'})
            all_options = all_divs.find_all('option')
            # print('all_options:-', all_options)

            k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', 'ahmedabad', 'allahabad',..]
            # print('K3:-', k3)

            # # <---- take input zonal_bench value from fun() --
            com = str(bench).lower()
            index3 = k3.index(com)
            zonal_bench_option = index3 + 1

            # # <------ Click on 'BENCH' DROP DOWN ---->
            driverNCLTa_.find_elements_by_id('bench')[0].click()

            # # <------ Select 'BENCH' Options ---->
            zonal_drop = \
                driverNCLTa_.find_elements_by_xpath('//*[@id="bench"]/option[{}]'.format(zonal_bench_option))[0]
            # print('selected zonal bench:- ', zonal_drop.text)
            zonal_drop.click()

            await asyncio.sleep(1) # required

            # <-----//////* Advocate Name input field*--///----------
            WebDriverWait(driverNCLTa_, 600).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="cpno"]')))  # wait
            party_elem = driverNCLTa_.find_element(By.ID, "cpno")
            p_name = str(advocate_name).replace('_', ' ').replace('-', ' ')
            party_elem.send_keys(p_name)  ##### <---- Pass party_name ---->

            # <-----///////* Year *--///----------
            html = driverNCLTa_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'year'})
            all_options = all_divs.find_all('option')
            # print('case_year all_options:-', all_options)

            k5 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
            # print('K5:-', k5)

            # # <---- take input case_year value from fun() --
            com = year
            index3 = k5.index(com)
            case_year_option = index3 + 1

            # # <------ Click on 'YEAR' DROP DOWN ---->
            driverNCLTa_.find_elements_by_id('year')[0].click()

            # # <------ Select 'YEAR' Options ---->
            case_year_drop = \
                driverNCLTa_.find_elements_by_xpath('//*[@id="year"]/option[{}]'.format(case_year_option))[0]
            # print('selected case_year:- ', case_year_drop.text)
            case_year_drop.click()
            await asyncio.sleep(1)  # required

            # <-----///* for Click Search btn *///-------------------
            driverNCLTa_.find_element_by_class_name('btn-default').click()  # <--- sertch btn --->
            # print('seartch btn clicked ------')
            await asyncio.sleep(1)      # required


            # <--------///* for Next page *///-------
            WebDriverWait(driverNCLTa_, 600).until(EC.presence_of_element_located((By.ID, 'block-nclt-content')))
            html = driverNCLTa_.page_source
            soup = BeautifulSoup(html, "html.parser")
            op_table_data = soup.find('table', {'class': 'table'})

            # print('-------------------------------')
            # print('op_table_data: ', op_table_data)
            # print('-------------------------------')

            # <-------//* if data not found *///-------------
            try:
                if 'for data prior' in str(op_table_data):
                    # print('check 1-----Record Not Found')
                    res_dct = {}
                    case_final_2 = {'RecordNotFound': 'Record not found'}
                    Record_not_found = 1
                    driverNCLTa_.quit()

                elif 'for data prior' not in str(op_table_data):
                    # print('data---------')

                    WebDriverWait(driverNCLTa_, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="block-nclt-content"]')))

                    html = driverNCLTa_.page_source
                    soup = BeautifulSoup(html, "html.parser")

                    # <-----///* Check pagination *///--------
                    pagination_data = soup.find('div', attrs={'class': 'table-responsive'})
                    all_divs = soup.find('div', {'class': 'table-responsive'})
                    pagination_data = all_divs.find_all('li')
                    # print('------------------------------')
                    # print('pagination_data:-', pagination_data)
                    # print('------------------------------')

                    k5_pagination = [str(x).split('"')[1] for x in pagination_data]
                    # print('------------------------------')
                    # print('k5_pagination:-', k5_pagination) #['disabled', 'page-item', 'page-item', 'page-item'
                    # print('------------------------------')                 #, 'page-item']

                    if len(k5_pagination) <= 3:
                        # print('pages:-', len(k5_pagination))
                        # <---Table, { head with <th> tag } -----
                        # -------------------------------------------------------------
                        try:
                            table_data = soup.find('table', attrs={'class': 'table'})
                            h, [_, *d] = [i.text for i in table_data.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_data.find_all('tr')]
                            res_dct_data = [dict(zip(h, i)) for i in d]
                            # print('res_dct_data:--', res_dct_data)
                            # print('--------------------')

                            k2 = []
                            for d in res_dct_data:
                                k2.append(
                                    {k.replace(' ', '').replace('.', ''): v for k, v in d.items()})
                            final_dict = {'page1': k2}

                        except Exception as e:
                            # print('Exception:--', e)
                            # print('--------------------')
                            final_dict = {}

                    else:   #--- / for page greater than 1 page /---
                        final_dict = {}
                        for n in range(2, len(k5_pagination)-1):
                            driverNCLTa_.find_elements_by_xpath('//*[@id="block-nclt-content"]/div/div/div[2]/div/ul/li[{}]/a'.format(n))[0].click()
                                # <-- for select page
                            WebDriverWait(driverNCLTa_, 600).until(
                                EC.presence_of_element_located((By.XPATH, '//*[@id="block-nclt-content"]'))) #-- wait --

                            WebDriverWait(driverNCLTa_, 600).until(
                                EC.element_to_be_clickable((By.XPATH, '//*[@id="block-nclt-content"]/div/div/div[2]/div/ul/li[2]/a')))
                                      # -- wait --

                            html = driverNCLTa_.page_source
                            soup = BeautifulSoup(html, "html.parser")

                            # <---Table, { head with <th> tag } -----
                            # -------------------------------------------------------------
                            try:
                                table_data = soup.find('table', attrs={'class': 'table'})
                                h, [_, *d] = [i.text for i in table_data.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_data.find_all('tr')]
                                res_dct_data = [dict(zip(h, i)) for i in d]
                                # print('res_dct_data:--', res_dct_data)
                                # print('--------------------')


                                k2 = []
                                for d in res_dct_data:
                                    k2.append(
                                        {k.replace(' ', '').replace('.', ''): v for k, v in d.items()})

                                final_dict[f'page{n-1}'] = k2

                            except Exception as e:
                                # print('Exception:--', e)
                                # print('--------------------')
                                final_dict = {}


                    # ===--{ FINAL OUTPUT DATA}--====
                    res_dct = {"TableData": final_dict}

                    # -------------------------------

                    if res_dct != {}:
                        case_final = res_dct
                        try:
                            driverNCLTa_.quit()
                        except:
                            pass

                    else:
                        try:
                            driverNCLTa_.quit()
                        except:
                            pass
            # -------------------------------------------------
            except Exception as e:
                res_dct = {}
                case_final_2 = {'ThreadId': str(threading.get_native_id()),
                                        "CaseDetailsNotFound": 'This Case Code does not exists/Data not available.'}
                driverNCLTa_.quit()

            # #============================/ Main Code/=========================================

        else:
            #print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driverNCLTa_.quit()
    #----------------------------------------------------------
    except Exception as e:
        try:
            # print('except Main Try--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driverNCLTa_.quit()
        except:
            pass
    finally:
        # print('finally:--')
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2

        data = yourdata                 # Final response
        # print('data:--', data)
        # ----------------

    return JsonResponse(data)


#===========================================================================================
#                                  # { CONFONET SEARTCH } #
#===========================================================================================
async def ConfonetView(request, state_commission_district_forum, state, dist, from_date, to_date, select_any_field, inputs):
    global res_dct, case_final_2, yourdata, Record_not_found, case_final, Orders_k7
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    driverCONFONET_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)

    driverCONFONET_.get('http://cms.nic.in/ncdrcusersWeb/login.do?method=caseStatus')

    WebDriverWait(driverCONFONET_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "body")))  # wait

    # <--==== Main (try) =====---
    try:
        html = driverCONFONET_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        # print('h1_data:-', h1_data)

        # <--Main (if)---
        if h1_data == None:
            while True:
                main_ch2 = 0
                try:
                    # #============================/ Main Code /=========================================
                    # -- wait ---
                    WebDriverWait(driverCONFONET_, 600).until(
                        EC.presence_of_element_located((By.CLASS_NAME, "baseContaner")))
                    WebDriverWait(driverCONFONET_, 900).until(
                        EC.element_to_be_clickable((By.CLASS_NAME, 'formfieldsbutton')))

                    # --/ check "State Commission" or "District Forum" /---
                    if "state_commission" in str(state_commission_district_forum).lower():
                        # print('if:-- ', state_commission_district_forum)
                        # --/ select "State Commission" /---
                        driverCONFONET_.find_elements_by_xpath(
                            '//*[@id="content"]/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[2]/td[1]/input')[
                            0].click()

                        # <-----///////* State *///////-----------------
                        html = driverCONFONET_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        all_divs = soup.find('select', {'name': 'state_id'})
                        all_options = all_divs.find_all('option')
                        # print('state all_options:-', all_options)

                        k4 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
                        # print('K4:-', k4)

                        # # <---- take input state value from fun() --
                        com = state
                        index3 = k4.index(com)
                        select_option = index3 + 1
                        # # <------ Click on 'select' DROP DOWN ---->
                        driverCONFONET_.find_elements_by_name('state_id')[0].click()
                        # # <------ Select 'select' Options ---->
                        select_drop = \
                            driverCONFONET_.find_elements_by_xpath(
                                '//*[@id="content"]/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[2]/td[2]/select/option[{}]'.format(
                                    select_option))[0]
                        # print('state:- ', select_drop.text)
                        select_drop.click()
                        await asyncio.sleep(1)  # required

                        # print('click:--')
                        # print('--------------')

                    elif "district_forum" in str(state_commission_district_forum).lower():
                        # print('elif:--', state_commission_district_forum)
                        # --/ select "District Forum" /---
                        driverCONFONET_.find_elements_by_xpath(
                            '//*[@id="content"]/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[3]/td[1]/input')[
                            0].click()

                        # <-----///////* State *///////-----------------
                        html = driverCONFONET_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        all_divs = soup.find('select', {'name': 'state_idD'})
                        all_options = all_divs.find_all('option')
                        # print('state all_options:-', all_options)

                        k4 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
                        # print('K4:-', k4)

                        # # <---- take input state value from fun() --
                        com = state
                        index3 = k4.index(com)
                        select_option = index3 + 1
                        # # <------ Click on 'select' DROP DOWN ---->
                        driverCONFONET_.find_elements_by_name('state_idD')[0].click()
                        # # <------ Select 'select' Options ---->
                        select_drop = \
                            driverCONFONET_.find_elements_by_xpath(
                                '//*[@id="content"]/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[3]/td[2]/select/option[{}]'.format(
                                    select_option))[0]
                        # print('state:- ', select_drop.text)
                        select_drop.click()
                        await asyncio.sleep(1)  # required

                        # <-----///////* Dist *///////-----------------
                        html = driverCONFONET_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        all_divs = soup.find('select', {'name': 'dist_id'})
                        all_options = all_divs.find_all('option')
                        # print('dist all_options:-', all_options)

                        k4_dist = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
                        # print('K4_dist:-', k4_dist)

                        # # <---- take input state value from fun() --
                        com = dist
                        index3 = k4_dist.index(com)
                        select_option = index3 + 1
                        # # <------ Click on 'dist' DROP DOWN ---->
                        driverCONFONET_.find_elements_by_name('dist_id')[0].click()
                        # # <------ Select 'select' Options ---->
                        select_drop = \
                            driverCONFONET_.find_elements_by_xpath(
                                '//*[@id="content"]/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[3]/td[2]/select[2]/option[{}]'.format(
                                    select_option))[0]
                        # print('dist:- ', select_drop.text)
                        select_drop.click()
                        await asyncio.sleep(1)  # required

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # ------------------------------
                    while True:
                        refresh = 0
                        driverCONFONET_.find_element_by_xpath(
                            '//*[@id="content"]/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[4]/td[1]/img[2]').click()  # refresh btn
                        await asyncio.sleep(1)  # required

                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        element = driverCONFONET_.find_element_by_xpath("//*[@id='captchaId']")
                        location = element.location
                        size = element.size

                        driverCONFONET_.save_screenshot(
                            settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCONFONET_CASE.png')

                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(
                            settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCONFONET_CASE.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(
                            settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCONFONET_CASE.png')
                        # im.show()  # <--- comment it ----
                        # print("1'st while image:------")
                        # <--------// end //----

                        # <---- for Captcha image to text convert-------------
                        # -----------------------------------------------------------------------------
                        # # --------API CODE--------------
                        with open(
                                settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCONFONET_CASE.png',
                                'rb') as captcha_file:
                            captcha_op = api.solve(captcha_file)
                        captcha_txt2 = captcha_op.await_result()
                        # print('API code captcha text:--', captcha_txt2)
                        # print('len(captcha_txt2):--', len(str(captcha_txt2))) #<-- len 5/6
                        # print('type(captcha_txt2):--', type(captcha_txt2))     #<-- type (str)
                        # <------- Find_data--------/// END ///---------------------------

                        # captcha_txt2 = 'abcde'  # <--- for test only

                        # -------------------------------------------------------------------------
                        re_captcha_txt_final = captcha_txt2.replace('I', 'l').replace('i', 'l')
                        # -------------------------------------------------------------------------

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driverCONFONET_.find_elements_by_name('captchaText')[0]
                        input_element.send_keys(str(re_captcha_txt_final))  ##### <---- Pass text ---->
                        # print('send_keys done--Captcha----')
                        # await asyncio.sleep(1)

                        # <----- for Enter "from date" to // input field //---------------------
                        input_element = driverCONFONET_.find_elements_by_name('dtFrom')[0]
                        input_element.clear()
                        input_element.send_keys(str(from_date).replace('_', '/'))  ##### <---- Pass text ---->

                        # <----- for Enter "to date" to // input field //---------------------
                        input_element = driverCONFONET_.find_elements_by_name('dtTo')[0]
                        input_element.clear()
                        input_element.send_keys(str(to_date).replace('_', '/'))  ##### <---- Pass text ---->

                        # <----- for selct // " Minimum Any One Option" //---------------------
                        driverCONFONET_.find_element_by_xpath(
                            '//*[@id="advser"]/tbody/tr[6]/td[1]/select/option[{n}]'.format(
                                n=int(select_any_field) + 1)).click()  # <--- 1st selct --->
                        # NOTE:- "select_any_field" take only 'integer'
                        # print('ck 3---')
                        # <----------------------------------------------------------------------------------

                        # ----==/ Select Case Type option /===---
                        if int(select_any_field) == 6:
                            # print('elif int(select_any_field) > 5:-====')
                            # <------For Select CASE TYPE--///---start---///-->
                            html = driverCONFONET_.page_source
                            soup = BeautifulSoup(html, "html.parser")
                            all_divs = soup.find('select', {'name': 'ctId'})
                            all_options = all_divs.find_all('option')
                            # print('all_options:-CASE TYPE', all_options)
                            # print('----------------------------')

                            k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['0', '28', '2', '6',..]
                            # print('K3:-', k3)

                            # <---- take input casetype value from fun() --
                            com = inputs
                            index3 = k3.index(com)
                            casetype_option = index3 + 1

                            # <------ Click on 'CASE TYPE' DROP DOWN ---->
                            driverCONFONET_.find_elements_by_name('ctId')[0].click()

                            # <------ Select 'CASE TYPE' Options ---->
                            casetype_drop = \
                                driverCONFONET_.find_elements_by_xpath(
                                    '//*[@id="ctcol1"]/select/option[{}]'.format(casetype_option))[0]
                            # print('selected CASE TYPE:- ', casetype_drop.text)
                            casetype_drop.click()
                            await asyncio.sleep(2)  # required

                        # ----==/ Select Category option /===---
                        elif int(select_any_field) == 7:
                            # print('elif int(select_any_field) > 5:-====')
                            # <------For Select CASE TYPE--///---start---///-->
                            html = driverCONFONET_.page_source
                            soup = BeautifulSoup(html, "html.parser")
                            all_divs = soup.find('select', {'name': 'catId'})
                            all_options = all_divs.find_all('option')
                            # print('all_options:-Category', all_options)
                            # print('----------------------------')

                            k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['0', '28', '2', '6',..]
                            # print('K3:-', k3)

                            # <---- take input casetype value from fun() --
                            com = inputs
                            index3 = k3.index(com)
                            casetype_option = index3 + 1

                            # <------ Click on 'CASE TYPE' DROP DOWN ---->
                            driverCONFONET_.find_elements_by_name('catId')[0].click()

                            # <------ Select 'CASE TYPE' Options ---->
                            casetype_drop = \
                                driverCONFONET_.find_elements_by_xpath(
                                    '//*[@id="catcol1"]/select/option[{}]'.format(casetype_option))[0]
                            print('selected Category:- ', casetype_drop.text)
                            casetype_drop.click()
                            await asyncio.sleep(2)  # required
                            
                        # ----==/ Select non Case Type option /===---
                        elif int(select_any_field) < 5:
                            # print('elif int(select_any_field) < 5:----')
                            # <----- for Enter "Input's" to // input field //---------------------
                            input_element = driverCONFONET_.find_elements_by_name('searchTxt')[0]
                            input_element.send_keys(str(inputs).replace('_', ' ').replace('-', ' ').lower())  ##### <---- Pass text ---->

                        # <------ Click on 'Search' btn ---->
                        driverCONFONET_.find_elements_by_name('advs')[0].click()
                        # print('Search:--')
                        time.sleep(2)

                        # ----==/ Check any error's /===---
                        try:
                            if 'Incorret Captcha' in str(
                                    driverCONFONET_.find_elements_by_xpath('//*[@id="advser"]/tbody/tr[3]/td')[0].text):
                                # print('Incorret Captcha:---')
                                # <------ Click on 'Case Status' btn  for refresh all page ---->
                                driverCONFONET_.find_elements_by_xpath('//*[@id="confomenu"]/ul/li[4]/a')[0].click()
                                refresh = 0

                            elif 'You have exceeded maximum number of searches' in str(
                                    driverCONFONET_.find_elements_by_xpath('//*[@id="advser"]/tbody/tr[3]/td')[0].text):
                                # print('You have exceeded maximum number of searches:---')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driverCONFONET_.quit()
                                refresh = 1
                        except:
                            # print('if-else-op_page_errSpanDiv ==--- ')
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            # print('if refresh == 1:---')
                            break

                    if Record_not_found == 1:
                        # print('if Record_not_found == 1:---')
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        # print('elif Record_not_found == 0:---')
                        WebDriverWait(driverCONFONET_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "body")))
                                                                                                        # < wait
                        time.sleep(1)
                        WebDriverWait(driverCONFONET_, 600).until(
                            EC.element_to_be_clickable((By.CLASS_NAME, 'topHdMsg'))) # < wait
                        # -------------------------------------------------------------------------------------------------
                        # <======---/// Final page table conversion start ///---======>
                        await asyncio.sleep(2)
                        html = driverCONFONET_.page_source
                        soup = BeautifulSoup(html, "html.parser")

                        # <--- Table, { head with <td> tag } -----
                        # -------------------------------------------------------------
                        table_order = soup.find('table', attrs={'class': 'topGroup'})
                        if table_order != None:
                            try:
                                table_order = soup.find('table', attrs={'class': 'topGroup'})
                                # print('------------------------------')
                                # print('table_order:--', table_order)
                                # print('------------------------------')
                                h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                    [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                                res_dct7 = [dict(zip(h7, x)) for x in q]
                                # print('------------------------------')
                                # print('Table:--- ', res_dct7)
                                # print('------------------------------')

                                Orders_k7 = []
                                for d7 in res_dct7:
                                    Orders_k7.append(
                                        {k.replace('-', '').replace('\n', '').replace('\t', '').replace(' ', '').replace('\xa0', ''): v.replace('\xa0', '').replace('\n', '').replace('\t', '').replace('          ', '').replace('        ', '') for
                                         k, v in d7.items()})

                                # print('Orders_k7: ', Orders_k7)
                                # print('------------------------------')


                            except:
                                Orders_k7 = []

                            # ===--{ FINAL OUTPUT DATA}--====
                            res_dct = {'TableData': Orders_k7}
                            # -------------------------------
                        elif table_order == None:
                            res_dct = {}

                        # --------------------------------------------------------------------
                        if res_dct != {}:
                            case_final = res_dct
                            try:
                                driverCONFONET_.quit()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {'ThreadId': str(threading.get_native_id()),
                                            "CaseDetailsNotFound": 'No record is available fo the given Parameter.'}
                            try:
                                driverCONFONET_.quit()
                                break
                            except:
                                pass

                        else:
                            try:
                                driverCONFONET_.quit()
                                break
                            except:
                                pass

                        main_ch2 = 1
# ---------------------------------------------
                except Exception as e:
                    try:
                        driverCONFONET_.quit()
                        # print('except Exception-----', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
                    except:
                        pass
                if main_ch2 == 1:
                    break

        else:
            #print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driverCONFONET_.quit()
    except Exception as e:
        try:
            # print('except Main Try--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driverCONFONET_.quit()
        except:
            pass
    finally:
        # print('finally:--')
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2

        data = yourdata                 # Final response
        # print('data:--', data)
        # ----------------

    return JsonResponse(data)

#==================---{ CONFONET CASE SEARTCH }-----================
async def ConfonetCaseView(request, state_commission_district_forum, state, dist, case_no):
    global Record_not_found, case_final, case_final_2, yourdata, res_dct
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    driverCONFONET_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)

    driverCONFONET_.get('http://cms.nic.in/ncdrcusersWeb/login.do?method=caseStatus')

    WebDriverWait(driverCONFONET_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "body")))  # wait

    # <--==== Main (try) =====---
    try:
        html = driverCONFONET_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        # print('h1_data:-', h1_data)

        # <--Main (if)---
        if h1_data == None:
            while True:
                main_ch2 = 0
                try:
                    # #============================/ Main Code /=========================================
                    # -- wait ---
                    WebDriverWait(driverCONFONET_, 600).until(
                        EC.presence_of_element_located((By.CLASS_NAME, "baseContaner")))
                    WebDriverWait(driverCONFONET_, 900).until(
                        EC.element_to_be_clickable((By.CLASS_NAME, 'formfieldsbutton')))

                    # --/ check "State Commission" or "District Forum" /---
                    if "state_commission" in str(state_commission_district_forum).lower():
                        # print('if:-- ', state_commission_district_forum)
                        # --/ select "State Commission" /---
                        driverCONFONET_.find_elements_by_xpath(
                            '//*[@id="content"]/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[2]/td[1]/input')[
                            0].click()

                        # <-----///////* State *///////-----------------
                        html = driverCONFONET_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        all_divs = soup.find('select', {'name': 'state_id'})
                        all_options = all_divs.find_all('option')
                        # print('state all_options:-', all_options)

                        k4 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
                        # print('K4:-', k4)

                        # # <---- take input state value from fun() --
                        com = state
                        index3 = k4.index(com)
                        select_option = index3 + 1
                        # # <------ Click on 'select' DROP DOWN ---->
                        driverCONFONET_.find_elements_by_name('state_id')[0].click()
                        # # <------ Select 'select' Options ---->
                        select_drop = \
                            driverCONFONET_.find_elements_by_xpath(
                                '//*[@id="content"]/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[2]/td[2]/select/option[{}]'.format(
                                    select_option))[0]
                        # print('state:- ', select_drop.text)
                        select_drop.click()
                        await asyncio.sleep(1)  # required

                        # print('click:--')
                        # print('--------------')

                    elif "district_forum" in str(state_commission_district_forum).lower():
                        # print('elif:--', state_commission_district_forum)
                        # --/ select "District Forum" /---
                        driverCONFONET_.find_elements_by_xpath(
                            '//*[@id="content"]/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[3]/td[1]/input')[
                            0].click()

                        # <-----///////* State *///////-----------------
                        html = driverCONFONET_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        all_divs = soup.find('select', {'name': 'state_idD'})
                        all_options = all_divs.find_all('option')
                        # print('state all_options:-', all_options)

                        k4 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
                        # print('K4:-', k4)

                        # # <---- take input state value from fun() --
                        com = state
                        index3 = k4.index(com)
                        select_option = index3 + 1
                        # # <------ Click on 'select' DROP DOWN ---->
                        driverCONFONET_.find_elements_by_name('state_idD')[0].click()
                        # # <------ Select 'select' Options ---->
                        select_drop = \
                            driverCONFONET_.find_elements_by_xpath(
                                '//*[@id="content"]/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[3]/td[2]/select/option[{}]'.format(
                                    select_option))[0]
                        # print('state:- ', select_drop.text)
                        select_drop.click()
                        await asyncio.sleep(1)  # required

                        # <-----///////* Dist *///////-----------------
                        html = driverCONFONET_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        all_divs = soup.find('select', {'name': 'dist_id'})
                        all_options = all_divs.find_all('option')
                        # print('dist all_options:-', all_options)

                        k4_dist = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
                        # print('K4_dist:-', k4_dist)

                        # # <---- take input state value from fun() --
                        com = dist
                        index3 = k4_dist.index(com)
                        select_option = index3 + 1
                        # # <------ Click on 'dist' DROP DOWN ---->
                        driverCONFONET_.find_elements_by_name('dist_id')[0].click()
                        # # <------ Select 'select' Options ---->
                        select_drop = \
                            driverCONFONET_.find_elements_by_xpath(
                                '//*[@id="content"]/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[3]/td[2]/select[2]/option[{}]'.format(
                                    select_option))[0]
                        # print('dist:- ', select_drop.text)
                        select_drop.click()
                        await asyncio.sleep(1)  # required

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # ------------------------------
                    while True:
                        refresh = 0
                        driverCONFONET_.find_element_by_xpath(
                            '//*[@id="content"]/tbody/tr[1]/td/table/tbody/tr/td[2]/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[4]/td[1]/img[2]').click()  # refresh btn
                        await asyncio.sleep(1)  # required

                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        element = driverCONFONET_.find_element_by_xpath("//*[@id='captchaId']")
                        location = element.location
                        size = element.size

                        driverCONFONET_.save_screenshot(
                            settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCONFONET_CASE.png')

                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(
                            settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCONFONET_CASE.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(
                            settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCONFONET_CASE.png')
                        # im.show()  # <--- comment it ----
                        # print("1'st while image:------")
                        # <--------// end //----

                        # <---- for Captcha image to text convert-------------
                        # -----------------------------------------------------------------------------
                        # # --------API CODE--------------
                        with open(
                                settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCONFONET_CASE.png',
                                'rb') as captcha_file:
                            captcha_op = api.solve(captcha_file)
                        captcha_txt2 = captcha_op.await_result()
                        # print('API code captcha text:--', captcha_txt2)
                        # print('len(captcha_txt2):--', len(str(captcha_txt2))) #<-- len 5/6
                        # print('type(captcha_txt2):--', type(captcha_txt2))     #<-- type (str)
                        # <------- Find_data--------/// END ///---------------------------

                        # captcha_txt2 = 'abcde'  # <--- for test only

                        # -------------------------------------------------------------------------
                        re_captcha_txt_final = captcha_txt2.replace('I', 'l').replace('i', 'l')
                        # -------------------------------------------------------------------------

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driverCONFONET_.find_elements_by_name('captchaText')[0]
                        input_element.send_keys(str(re_captcha_txt_final))  ##### <---- Pass text ---->
                        # print('send_keys done--Captcha----')
                        # await asyncio.sleep(1)

                        # <----- for Enter "Case no" to // input field //---------------------
                        input_element = driverCONFONET_.find_elements_by_id('attachcase')[0]
                        input_element.clear()
                        input_element.send_keys(str(case_no).replace('_', '/'))  ##### <---- Pass text ---->
                        # <----------------------------------------------------------------------------------
                        # <------ Click on 'Search' btn ---->
                        driverCONFONET_.find_elements_by_id('srcbut')[0].click()
                        # time.sleep(2)
                        style_value = driverCONFONET_.find_elements_by_id('srcbut')[0].get_attribute('style')
                        # -- wait ---
                        while True:
                            wait = 0
                            style_value = driverCONFONET_.find_elements_by_id('srcbut')[0].get_attribute('style')
                            # print('style_value:- ', style_value)
                            if style_value == 'display: none;' or style_value == 'display:none;':
                                wait = 0
                            elif style_value == 'display: inline-block;' or style_value == 'display:inline-block;':
                                wait = 1
                            if wait == 1:
                                break

                        # ----==/ Check any error's /===---

                        if 'Captcha' in str(
                                driverCONFONET_.find_elements_by_xpath('//*[@id="lblhead0"]')[0].text):
                            # print('Incorret Captcha:---')
                            # <------ Click on 'Case Status' btn  for refresh all page ---->
                            driverCONFONET_.find_elements_by_xpath('//*[@id="confomenu"]/ul/li[4]/a')[0].click()
                            refresh = 0

                        elif 'Sorry. No Details Available' in str(
                                driverCONFONET_.find_elements_by_xpath('//*[@id="lblhead0"]')[0].text):
                            # print('Sorry. No Details Available:---')
                            res_dct = {}
                            case_final_2 = {'RecordNotFound': 'Sorry. No Details Available.'}
                            Record_not_found = 1
                            driverCONFONET_.quit()
                            refresh = 1

                        elif 'You have exceeded maximum number of searches' in str(
                                driverCONFONET_.find_elements_by_xpath('//*[@id="lblhead0"]')[0].text):
                            # print('You have exceeded maximum number of searches:---')
                            res_dct = {}
                            case_final_2 = {'RecordNotFound': 'Record not found'}
                            Record_not_found = 1
                            driverCONFONET_.quit()
                            refresh = 1
                        else:
                            # print('if-else-op_page_errSpanDiv ==--- ')
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            # print('if refresh == 1:---')
                            break

                    if Record_not_found == 1:
                        # print('if Record_not_found == 1:---')
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        # print('elif Record_not_found == 0:---')
                        WebDriverWait(driverCONFONET_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "body")))
                        # < wait
                        time.sleep(1)
                        WebDriverWait(driverCONFONET_, 600).until(
                            EC.presence_of_element_located((By.CLASS_NAME, 'topHdMsg')))  # < wait
                        # -------------------------------------------------------------------------------------------------

                        # <======---/// Simple Search table conversion start ///---======>
                        await asyncio.sleep(2)
                        html = driverCONFONET_.page_source
                        soup = BeautifulSoup(html, "html.parser")

                        # -------------------------------------------------------------
                        # <---Table, { head with <tr> tag } -----
                        # -------------------------------------------------------------
                        try:
                            l = []
                            table_case = soup.find('table', attrs={'id': 'casestatus'})
                            table1_case_body = table_case.find('tbody')
                            # print('------1----------------------------------------')
                            # print('Simple Search table1_body--- ', table1_case_body)
                            # print('----------------------------------------------')

                            rows = table1_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values


                            Simple_Search_table_list = []
                            for i in l:
                                if len(i) > 1:
                                    if 'If you know' not in i[0]:
                                        Simple_Search_table_list.append(i)

                            lst = reduce(lambda x, y: x + y, Simple_Search_table_list)
                            Table_1_res_dct_data = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in
                                                    range(0, len(lst), 2)}
                            # print('----------------------------------------------')
                            # print('Table_1_res_dct_data--- ', Table_1_res_dct_data)
                            # print('----------------------------------------------')

                            # <------ Click on 'View History & Orders' btn  ---->
                            driverCONFONET_.find_elements_by_name('cho')[0].click()
                            # print('cho --')
                            WebDriverWait(driverCONFONET_, 900).until(
                                EC.presence_of_element_located((By.CLASS_NAME, "topHdMsg")))

                            # <======---/// Case Number table conversion start ///---======>
                            await asyncio.sleep(1)
                            html = driverCONFONET_.page_source
                            soup = BeautifulSoup(html, "html.parser")


                            # -------------------------------------------------------------
                            # <---Table, { head with <tr> tag } -----
                            # -------------------------------------------------------------
                            case_l = []
                            table_case = soup.find('table', attrs={'class': 'csContaner'})
                            table_case_body = table_case.find('tbody')
                            # print('----------------------------------------------')
                            # print('table_case_body--- ', table_case_body)
                            # print('----------------------------------------------')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols2 = [ele.text for ele in cols]
                                #cols = [ele.text.strip() for ele in cols]
                                # case_l.append([ele for ele in cols if ele])  # Get rid of empty values
                                case_l.append([str(ele).replace('\u00a0', '') for ele in cols2 if ele])  # for blank data
                            # print('----------------------------------------------')
                            # print('case_l--- ', case_l) # --all raw data
                            # print('----------------------------------------------')

                            case_cl = []
                            for cl in case_l:
                                if len(cl) > 1:
                                    if 'Complainant' not in cl:
                                        case_cl.append(cl)
                            # print('case_cl:--', case_cl)

                            Table_2_res_dct_data = []
                            for n in range(1, len(case_cl)):
                                res = {case_cl[0][i].replace(' ',''): case_cl[n][i] for i in range(len(case_cl[0]))}

                                # -------------------------------------------------------------
                                # <---{ For proceeding } -----
                                # -------------------------------------------------------------
                                # <------ Click on 'proceeding' btn  ---->
                                driverCONFONET_.find_elements_by_xpath('/html/body/table/tbody/tr[{tr}]/td[{col}]/a'.format(tr=n+3, col=4))[0].click()

                                time.sleep(1)
                                # <-------//* For New Tab Data *///-------------
                                # get current window handle
                                p = driverCONFONET_.current_window_handle
                                # get first child window
                                chwd = driverCONFONET_.window_handles

                                for w in chwd:
                                    # switch focus to child window
                                    if (w != p):
                                        driverCONFONET_.switch_to.window(w)
                                        break
                                # <--------------------------------------
                                # <-------//* For screen shot *///-------------
                                driverCONFONET_.save_screenshot(
                                    settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}_{case_no}_CONFOproceeding_image.png')
                                filename = settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}_{case_no}_CONFOproceeding_image.png'

                                # <---/ save file in s3 /---
                                s3.meta.client.upload_file(f'{filename}', 'sitena',
                                                           f'confo/{n}_{str(threading.get_native_id())}_{case_no}_CONFOproceeding_image.png',
                                                           ExtraArgs={'ContentType': "image/png",
                                                                      'ACL': "public-read"})
                                # print('proceeding:-- ', f'confo/{n}_{str(threading.get_native_id())}_{case_no}_CONFOproceeding_image.png')
                                # ---------------------------------------
                                os.remove(f"{filename}")
                                # ---------------------------------------
                                # Closing new_url tab
                                driverCONFONET_.close()
                                # ---------------------------------------
                                # Put focus on current window which will be the window opener
                                driverCONFONET_.switch_to.window(p)

                                # --update dict --
                                res['CourtProceeding'] = 'https://sitena.s3.amazonaws.com/'+f'confo/{n}_{str(threading.get_native_id())}_{case_no}_CONFOproceeding_image.png'

                                # <-------//* For Judgement *///------First check it is available or not-------
                                Judgement_pdf = driverCONFONET_.find_elements_by_xpath(
                                    '/html/body/table/tbody/tr[4]/td[5]/a[2]')
                                # print('Judgement_pdf:--', Judgement_pdf)
                                if Judgement_pdf == []:

                                    # -------------------------------------------------------------
                                    # <---{ For Daily Order } -----
                                    # -------------------------------------------------------------
                                    # <------ Click on 'Daily Order' btn  ---->
                                    driverCONFONET_.find_elements_by_xpath(
                                        '/html/body/table/tbody/tr[{tr}]/td[{col}]/a'.format(tr=n + 3, col=5))[
                                        0].click()

                                    time.sleep(1)
                                    # <-------//* For New Tab Data *///-------------
                                    # get current window handle
                                    p = driverCONFONET_.current_window_handle
                                    # get first child window
                                    chwd = driverCONFONET_.window_handles

                                    for w in chwd:
                                        # switch focus to child window
                                        if (w != p):
                                            driverCONFONET_.switch_to.window(w)
                                            break
                                    # <--------------------------------------
                                    # <-------//* For screen shot *///-------------
                                    driverCONFONET_.save_screenshot(
                                        settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}_{case_no}_CONFOdailyorder_image.png')
                                    filename = settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}_{case_no}_CONFOdailyorder_image.png'

                                    # <---/ save file in s3 /---
                                    s3.meta.client.upload_file(f'{filename}', 'sitena',
                                                               f'confo/{n}_{str(threading.get_native_id())}_{case_no}_CONFOdailyorder_image.png',
                                                               ExtraArgs={'ContentType': "image/png",
                                                                          'ACL': "public-read"})
                                    # print('dailyorder:-- ',
                                    #     f'confo/{n}_{str(threading.get_native_id())}_{case_no}_CONFOdailyorder_image.png')
                                    # ---------------------------------------
                                    # os.remove(f"{filename}")
                                    # ---------------------------------------
                                    # Closing new_url tab
                                    driverCONFONET_.close()
                                    # ---------------------------------------
                                    # Put focus on current window which will be the window opener
                                    driverCONFONET_.switch_to.window(p)

                                    # --update dict --
                                    res['Order'] = 'https://sitena.s3.amazonaws.com/'+f'confo/{n}_{str(threading.get_native_id())}_{case_no}_CONFOdailyorder_image.png'
                                elif Judgement_pdf != []:
                                    res['Order'] = ''

                                    href_data = Judgement_pdf[0].get_attribute('href')
                                    res['Judgement'] = str(href_data)
                                else:
                                    pass
                                # <--------------------------------------


                                # <--------------------------------------

                                Table_2_res_dct_data.append(res)
                            #---=== Final Dict ===---
                            res_dct = {'Table_1': Table_1_res_dct_data, 'Table_2': Table_2_res_dct_data}
                        #-----------------------------------
                        except Exception as e:
                            # print('Exception Table_1:-- ', e)
                            res_dct = {}


                        # --------------------------------------------------------------------
                        if res_dct != {}:
                            case_final = res_dct
                            try:
                                driverCONFONET_.quit()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {'ThreadId': str(threading.get_native_id()),
                                            "CaseDetailsNotFound": 'No record is available of the given Parameter.'}
                            try:
                                driverCONFONET_.quit()
                                break
                            except:
                                pass

                        else:
                            try:
                                driverCONFONET_.quit()
                                break
                            except:
                                pass

                        main_ch2 = 1
                # ---------------------------------------------
                except Exception as e:
                    try:
                        driverCONFONET_.quit()
                        # print('except Exception-----', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
                    except:
                        pass
                if main_ch2 == 1:
                    break

        else:
            # print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driverCONFONET_.quit()
    except Exception as e:
        try:
            # print('except Main Try--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driverCONFONET_.quit()
        except:
            pass
    finally:
        # print('finally:--')
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2

        data = yourdata  # Final response
        # print('data:--', data)
        # ----------------

    return JsonResponse(data)


# ===========================================================================================
#                                  #{ INCOME TAX APPELLATE TRIBUNAL SEARTCH } #
# ===========================================================================================
#==================---{ BY APPELLATE NUMBER SEARTCH }-----================
async def ITATView(request, bench, appeal_type, number, year):
    # print('input data:-- ', str(bench), appeal_type, number, year)
    # -----------Code start-----
    global res_dct, case_final_2, yourdata, Appellant_Respondent_res, case_final
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    driverITATn_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)

    driverITATn_.get('https://itat.gov.in/judicial/casestatus')

    WebDriverWait(driverITATn_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "body")))  # wait

    # <--//- all repeated Functions ----//---
    def delay1():
        time.sleep(random.randint(1, 2))
        # await asyncio.sleep(1)

    def delay2():
        time.sleep(random.randint(2, 2))
        # await asyncio.sleep(2)

    # <--==== Main (try) =====---
    try:
        html = driverITATn_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        # print('h1_data:-', h1_data)

        # <--Main (if)---
        if 'Service Unavailable' not in str(h1_data):
            # #============================/ Main Code /=========================================
            # -- wait ---
            WebDriverWait(driverITATn_, 600).until(EC.presence_of_element_located((By.CLASS_NAME, "current-menu-item")))

            # <-------/////* Bench *//////-----------------
            WebDriverWait(driverITATn_, 900).until(EC.presence_of_element_located((By.ID, 'bench1')))

            html = driverITATn_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'bench1'})
            all_options = all_divs.find_all('option')
            # print('--------------------')
            # print('Bench all_options:-', all_options)
            # print('--------------------')

            k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', 'ahmedabad', 'allahabad',..]
            # print('K3:-', k3)

            # # <---- take input bench value from fun() --
            com = str(bench)
            index3 = k3.index(com)
            bench_option = index3 + 1
            # # <------ Click on ' BENCH' DROP DOWN ---->
            driverITATn_.find_elements_by_id('bench1')[0].click()
            # # <------ Select 'BENCH' Options ---->
            bench_drop = \
                driverITATn_.find_elements_by_xpath('//*[@id="bench1"]/option[{}]'.format(bench_option))[0]
            # print('selected zonal bench:- ', bench_drop.text)
            bench_drop.click()
            await asyncio.sleep(1) # required

            # <-----///////* Appeal Type *///////-----------------
            WebDriverWait(driverITATn_, 900).until(EC.presence_of_element_located((By.ID, 'appeal_type1')))

            html = driverITATn_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'appeal_type1'})
            all_options = all_divs.find_all('option')
            # print('--------------------')
            # print('Appeal Type all_options:-', all_options)
            # print('--------------------')

            k4 = [str(x).split('"')[1] for x in all_options]  # <-- o/p ['Black Money Appeal|BMA', 'Cross Objection|CO',
            # print('--------------------')
            # print('K4:-', k4)
            # print('--------------------')

            k4_list = []
            for i in k4:
                k4_list.append(
                    i.replace(' ', '').replace('|', '').replace('(', '').replace(')', '').replace('&amp;', '').replace(
                        '&', '').lower())
            # print('k4_list:-', k4_list)

            # # <---- take input case_type value from fun() --
            com = str(appeal_type).replace(' ', '').replace('_', '').replace('(', '').replace(')', '').replace('&amp;', '').replace('&', '').lower()
            index3 = k4_list.index(com)
            appeal_type_option = index3 + 1

            # # <------ Click on 'Appeal' DROP DOWN ---->
            driverITATn_.find_elements_by_id('appeal_type1')[0].click()

            # # <------ Select 'Appeal' Options ---->
            appeal_type_drop = \
                driverITATn_.find_elements_by_xpath('//*[@id="appeal_type1"]/option[{}]'.format(appeal_type_option))[0]
            # print('selected appeal_type1:- ', appeal_type_drop.text)
            appeal_type_drop.click()
            await asyncio.sleep(1)  # required

            # <-----//////* Number input field*--///----------
            WebDriverWait(driverITATn_, 600).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="numbernew"]')))  # wait
            number_elem = driverITATn_.find_element(By.NAME, "numbernew")
            number_elem.send_keys(number)  ##### <---- Pass no ---->

            # <-----///////* Year *///----------
            html = driverITATn_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'yearnew'})
            all_options = all_divs.find_all('option')
            # print('-------------------')
            # print('year all_options:-', all_options)
            # print('-------------------')

            k5 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', '33', '32', '31',..]
            # print('K5:-', k5)

            # # <---- take input year value from fun() --
            com = year
            index3 = k5.index(com)
            year_option = index3 + 1

            # # <------ Click on 'YEAR' DROP DOWN ---->
            driverITATn_.find_elements_by_id('yearnew')[0].click()

            # # <------ Select 'YEAR' Options ---->
            year_drop = \
                driverITATn_.find_elements_by_xpath('//*[@id="yearnew"]/option[{}]'.format(year_option))[0]
            # print('selected year_drop:- ', year_drop.text)
            year_drop.click()
            await asyncio.sleep(1)  # required

            # <-----///* for Click Search btn *///-------------------
            driverITATn_.find_element_by_name('btnSubmit1').click()  # <--- sertch btn --->
            # print('seartch btn clicked ------')
            await asyncio.sleep(1)      # required

            # <--------///* for Next page *///-------

            WebDriverWait(driverITATn_, 600).until(EC.presence_of_element_located((By.CLASS_NAME, 'large-12')))
            html = driverITATn_.page_source
            soup = BeautifulSoup(html, "html.parser")
            op_table_data = soup.find('div', {'class': 'large-12'})

            # print('-------------------------------')
            # print('op_table_data: ', op_table_data)
            # print('-------------------------------')

            # <-------//* if data not found *///-------------
            try:
                if 'class="tableshow"' not in str(op_table_data):
                    res_dct = {}
                    case_final_2 = {'RecordNotFound': 'Record not found'}
                    Record_not_found = 1
                    driverITATn_.quit()

                elif 'class="tableshow"' in str(op_table_data):
                    WebDriverWait(driverITATn_, 600).until(EC.element_to_be_clickable((By.NAME, 'btnDetail')))
                    # <-----// click 'Detail' btn ///----
                    driverITATn_.find_element_by_name('btnDetail').click()
                    # print('Click Detail btn---')

                    # <-------//* For New Tab Data *///-------------
                    # get current window handle
                    p = driverITATn_.current_window_handle
                    # get first child window
                    chwd = driverITATn_.window_handles

                    for w in chwd:
                        # switch focus to child window
                        if (w != p):
                            driverITATn_.switch_to.window(w)
                            break
                    # <--------------------------------------

                    WebDriverWait(driverITATn_, 600).until(
                        EC.presence_of_element_located((By.CLASS_NAME, 'table')))

                    html = driverITATn_.page_source
                    soup = BeautifulSoup(html, "html.parser")

                    # <---1st Table, { head with <tr> tag } -----
                    # -------------------------------------------------------------
                    l = []
                    l_th = []
                    table_case = soup.find('table', attrs={'class': 'table'})
                    table_case_body = table_case.find('tbody')
                    # print('----------------------------------------------')
                    # print('table_case_body--- ', table_case_body)
                    # print('----------------------------------------------')

                    rows = table_case_body.find_all('tr')
                    for row in rows:
                        cols = row.find_all('td')
                        cols = [ele.text.strip() for ele in cols]
                        l.append([ele for ele in cols if ele])  # Get rid of empty values

                        cols_th = row.find_all('th')
                        cols_th = [ele_th.text.strip() for ele_th in cols_th]
                        l_th.append([ele_th for ele_th in cols_th if ele_th])  # Get rid of empty values

                    # print('l:- ', l)
                    # print('l_th:- ', l_th)

                    # print(l_th[3])
                    # print(l[4])

                    # --- CASE DETAILS -- table o/p ---
                    try:
                        Case_details_res = {
                            l_th[3][i].replace(' ', ''): l[4][i].replace('\t', '').replace('\n', '').replace(' ', '')
                            for i in range(len(l_th[3]))}
                        # print(Case_details_res)
                    except:
                        Case_details_res = {}

                    # --- Appellant Respondent -- table o/p ---
                    try:
                        k = [i for i in l[5] if i == 'Appellant']
                        if k[0] == 'Appellant':
                            print('y')
                            Appellant_Respondent_res = {
                                l[5][i].replace(' ', ''): l[6][i].replace('\t', '').replace('\n', '') for i in
                                range(len(l[5]))}
                            # print(Appellant_Respondent_res)
                    except:
                        Appellant_Respondent_res = {}

                    # <---2nd Table, SHORT SUMMARY { head with <th> tag, but same id } -----
                    # ----------------------------------------------------------------------
                    try:
                        table_html_data = soup.select_one('#panel2-3 > div > table')  # BY css selector
                        # <-- if present ---
                        summary_td = []
                        summary_th = []
                        if 'Date of Last Hearing' in str(table_html_data):

                            summary_table_tr = table_html_data.find_all('tr')
                            for row in summary_table_tr:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                summary_td.append([ele for ele in cols if ele])  # Get rid of empty values

                                cols_th = row.find_all('th')
                                cols_th = [ele_th.text.strip() for ele_th in cols_th]
                                summary_th.append([ele_th for ele_th in cols_th if ele_th])  # Get rid of empty values

                            # print('summary_th:- ', summary_th)
                            # print('summary_td:- ', summary_td)

                        short_summary_res = {summary_th[2][0].replace(' ', ''): summary_td[1],
                                             summary_th[2][1].replace(' ', ''): summary_td[2]}
                        print(short_summary_res)
                    except Exception as e:
                        # print('Exception:-', e)
                        short_summary_res = {}

                    try:
                        table3_html_data = soup.select_one('#showhide > div > main > div > div > div.tabs-content > section:nth-child(2) > div.table-responsive > table')  # BY css selector
                        # <-- if present ---
                        if 'Result' in str(table3_html_data):
                            h, [_, *d] = [i.text for i in table3_html_data.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table3_html_data.find_all('tr')]
                            res_dc3 = [dict(zip(h, i)) for i in d]
                            # print('res_dct3:-- ', res_dc3)

                            k3 = []
                            for d in res_dc3:
                                k3.append(
                                    {k.replace(' ', ''): v.replace('\n', '').replace('\t', '').lstrip().rstrip() for
                                     k, v in d.items()})

                            # -------------------/ for view URL /------------------------------
                            a_url = table3_html_data.find_all('a', href=True)
                            # print('a_url:-- ', a_url[0]['href'])

                            #-- update dict with a url --
                            k3[0]['Status/Remarks'] = a_url[0]['href']
                            # print('k3:-- ', k3)

                    except Exception as e:
                        # print('Exception:-', e)
                        k3 = {}

                    ## ===--{ FINAL OUTPUT DATA}--====
                    res_dct = {'CaseDetails': Case_details_res,
                                   'AppellantRespondent': Appellant_Respondent_res,
                                   'ShortSummary': short_summary_res,
                                   'Orders': k3,
                                   }

                    # -------------------------------

                    if res_dct != {}:
                        case_final = res_dct
                        try:
                            driverITATn_.quit()
                        except:
                            pass

                    else:
                        try:
                            driverITATn_.quit()
                        except:
                            pass
            # -------------------------------------------------
            except Exception as e:
                res_dct = {}
                case_final_2 = {'ThreadId': str(threading.get_native_id()),
                                        "CaseDetailsNotFound": 'This Case Code does not exists/Data not available.'}
                driverITATn_.quit()

            # #============================/ Main Code/=========================================

        else:
            #print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driverITATn_.quit()
    #----------------------------------------------------------
    except Exception as e:
        try:
            # print('except Main Try--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driverITATn_.quit()
        except:
            pass
    finally:
        # print('finally:--')
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2

        data = yourdata                 # Final response
        # print('data:--', data)
        # ----------------

    return JsonResponse(data)


#==================---{ BY ASSESEE NAME SEARTCH }-----================
async def ITATAsseseeNameView(request, bench, appeal_type, assesee_name):
    # -----------Code start-----
    global res_dct, case_final_2, yourdata, Appellant_Respondent_res, case_final
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    driverITATa_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)

    driverITATa_.get('https://itat.gov.in/judicial/casestatus')

    WebDriverWait(driverITATa_, 900).until(EC.presence_of_element_located((By.TAG_NAME, "body")))  # wait

    # <--//- all repeated Functions ----//---
    def delay1():
        time.sleep(random.randint(1, 2))
        # await asyncio.sleep(1)

    def delay2():
        time.sleep(random.randint(2, 2))
        # await asyncio.sleep(2)

    # <--==== Main (try) =====---
    try:
        html = driverITATa_.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        # print('h1_data:-', h1_data)

        # <--Main (if)---
        if 'Service Unavailable' not in str(h1_data):
            # #============================/ Main Code /=========================================
            # -- wait ---
            WebDriverWait(driverITATa_, 600).until(EC.presence_of_element_located((By.CLASS_NAME, "current-menu-item")))

            # # <------ Click on ' Assesee Name ' ---->
            driverITATa_.find_elements_by_xpath('//*[@id="content"]/section[3]/p/label')[0].click()

            # <-------/////* Bench *//////-----------------
            WebDriverWait(driverITATa_, 900).until(EC.presence_of_element_located((By.ID, 'bench3')))

            html = driverITATa_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'bench3'})
            all_options = all_divs.find_all('option')
            # print('Bench all_options:-', all_options)

            k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['', 'ahmedabad', 'allahabad',..]
            # print('K3:-', k3)

            # # <---- take input bench value from fun() --
            com = str(bench)
            index3 = k3.index(com)
            bench_option = index3 + 1
            # # <------ Click on ' BENCH' DROP DOWN ---->
            driverITATa_.find_elements_by_id('bench3')[0].click()
            # # <------ Select 'BENCH' Options ---->
            bench_drop = \
                driverITATa_.find_elements_by_xpath('//*[@id="bench3"]/option[{}]'.format(bench_option))[0]
            # print('selected zonal bench:- ', bench_drop.text)
            bench_drop.click()
            await asyncio.sleep(1) # required

            # <-----///////* Appeal Type *///////-----------------
            WebDriverWait(driverITATa_, 900).until(EC.presence_of_element_located((By.ID, 'appeal_type3')))

            html = driverITATa_.page_source
            soup = BeautifulSoup(html, "html.parser")
            all_divs = soup.find('select', {'id': 'appeal_type3'})
            all_options = all_divs.find_all('option')
            # print('Appeal Type all_options:-', all_options)

            k4 = [str(x).split('"')[1] for x in all_options]  # <-- o/p ['Black Money Appeal|BMA', 'Cross Objection|CO',
            # print('K4:-', k4)

            k4_list = []
            for i in k4:
                k4_list.append(
                    i.replace(' ', '').replace('|', '').replace('(', '').replace(')', '').replace('&amp;', '').replace(
                        '&', '').lower())
            # print('k4_list:-', k4_list)

            # # <---- take input case_type value from fun() --
            com = str(appeal_type).replace(' ', '').replace('_', '').replace('(', '').replace(')', '').replace('&amp;', '').replace('&', '').lower()
            index3 = k4_list.index(com)
            appeal_type_option = index3 + 1

            # # <------ Click on 'Appeal' DROP DOWN ---->
            driverITATa_.find_elements_by_id('appeal_type3')[0].click()

            # # <------ Select 'Appeal' Options ---->
            appeal_type_drop = \
                driverITATa_.find_elements_by_xpath('//*[@id="appeal_type3"]/option[{}]'.format(appeal_type_option))[0]
            # print('selected appeal_type1:- ', appeal_type_drop.text)
            appeal_type_drop.click()
            await asyncio.sleep(1)  # required

            # <-----//////* Assesee Name field*--///----------
            WebDriverWait(driverITATa_, 600).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="assesse_name"]')))  # wait
            number_elem = driverITATa_.find_element(By.ID, "assesse_name")
            number_elem.send_keys(str(assesee_name).replace('_', ' ').replace('-', ' ').lower())  ##### <---- Pass no ---->



            # <-----///* for Click Search btn *///-------------------
            driverITATa_.find_element_by_name('btnSubmit2').click()  # <--- sertch btn --->
            await asyncio.sleep(1)      # required

            # <--------///* for Next page *///-------

            WebDriverWait(driverITATa_, 600).until(EC.presence_of_element_located((By.CLASS_NAME, 'large-12')))
            html = driverITATa_.page_source
            soup = BeautifulSoup(html, "html.parser")
            op_table_data = soup.find('div', {'class': 'large-12'})

            # print('-------------------------------')
            # print('op_table_data: ', op_table_data)
            # print('-------------------------------')

            # <-------//* if data not found *///-------------
            try:
                if 'class="tableshow"' not in str(op_table_data):
                    res_dct = {}
                    case_final_2 = {'RecordNotFound': 'Record not found'}
                    Record_not_found = 1
                    driverITATa_.quit()

                elif 'class="tableshow"' in str(op_table_data):
                    WebDriverWait(driverITATa_, 600).until(EC.element_to_be_clickable((By.NAME, 'btnDetail')))
                    # <-----// click 'Detail' btn ///----

                    #--====/ final table /=====-----

                    try:
                        table_order = soup.find('table', attrs={'class': 'tableshow'})
                        h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                            [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                        res_dct7 = [dict(zip(h7, x)) for x in q]
                        # print('------------------------------')
                        # print('Table:--- ', res_dct7)
                        # print('------------------------------')

                        Orders_k7 = []
                        for d7 in res_dct7:
                            Orders_k7.append(
                                {k.replace('-', '').replace('\n', '').replace('\t', '').replace(' ', '').replace('\xa0',
                                                                                                                 ''): v.replace(
                                    '\xa0', '').replace('\n', '').replace('\t', '').replace('          ', '').replace(
                                    '        ', '') for
                                 k, v in d7.items()})

                        # print('Orders_k7: ', Orders_k7)
                        # print('------------------------------')


                    except:
                        Orders_k7 = []



#----------------------------------------

                    ## ===--{ FINAL OUTPUT DATA}--====
                    res_dct = {'TableData': Orders_k7}

                    # -------------------------------

                    if res_dct != {}:
                        case_final = res_dct
                        try:
                            driverITATa_.quit()
                        except:
                            pass

                    else:
                        try:
                            driverITATa_.quit()
                        except:
                            pass
            # -------------------------------------------------
            except Exception as e:
                res_dct = {}
                case_final_2 = {'ThreadId': str(threading.get_native_id()),
                                        "CaseDetailsNotFound": 'This Case Code does not exists/Data not available.'}
                driverITATa_.quit()

            # #============================/ Main Code/=========================================

        else:
            #print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "Service Unavailable."}
            driverITATa_.quit()
    #----------------------------------------------------------
    except Exception as e:
        try:
            # print('except Main Try--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
            driverITATa_.quit()
        except:
            pass
    finally:
        # print('finally:--')
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2

        data = yourdata                 # Final response
        # print('data:--', data)
        # ----------------

    return JsonResponse(data)
