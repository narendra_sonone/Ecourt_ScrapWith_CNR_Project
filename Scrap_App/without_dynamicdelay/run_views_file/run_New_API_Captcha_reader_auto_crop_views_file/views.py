import os
import psycopg2
import json
from django.conf import settings

from azcaptchaapi import AZCaptchaApi
api = AZCaptchaApi('tpcj9jrqfkqmbdpgm8lkdvhwxngz7ftc')

from .models import Count1, Count2, Count3, Count4, Count5, Count6, Count7, Count8, Count9, Count10,\
    Case_1, Case_2, Case_3, Case_4, Case_5, Case_6, Case_7, Case_8, Case_9, Case_10

#-----------------------
import multiprocessing
import threading
from rest_framework import views
from rest_framework.response import Response

from .serializers import YourSerializer, ErrorSerializer
from django.http import HttpResponse
from PIL import Image, ImageEnhance
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ChromeOptions
from selenium.webdriver.support import expected_conditions as EC
import random
import time
import cv2
import pytesseract as tess
from PIL import Image, ImageCms, ImageOps, ImageFilter
import numpy as np
from functools import reduce
import uuid
from asgiref.sync import sync_to_async, async_to_sync
import asyncio
#----------------------------------------------------------------------
#@sync_to_async(thread_sensitive=True)
async def home(request, state, dist, complex, casetype, caseno, year):
#def home(request):
    start = time.perf_counter()
    print('st:-')
    #time.sleep(5)
    s = state
    await asyncio.sleep(1)
    d = dist
    await asyncio.sleep(1)
    c1 = complex
    await asyncio.sleep(1)
    c2 = casetype
    await asyncio.sleep(1)
    c3 = caseno
    await asyncio.sleep(1)
    y = year
    await asyncio.sleep(1)

    th_id = str(threading.get_native_id())
    print("home:" + str(threading.get_native_id()))

    finish3 = time.perf_counter()
    print(f'{str(threading.get_native_id())} home Finished in {round(finish3 - start, 2)} second(s)')
    return HttpResponse(f'{th_id} Home, {s, d, c1, c2, c3, y}')
#----------------------------------------------------------------------

def CnrNoView(request, slug):
    pass


#@sync_to_async(thread_sensitive=True)
async def CaseNoView(request, state, dist, complex, casetype, caseno, year):
    path = '/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/Scrap_App/files/'
    start = time.perf_counter()
    print(f'Case1 input data:--- , {str(threading.get_native_id())}, {state, dist, complex, casetype, caseno, year}')

    #-----------Code start-----
    options = ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.headless = True
    global driver, res_dct_3
    driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
    global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found, data, res_dct

    driver.get('https://services.ecourts.gov.in/ecourtindia_v4_bilingual/cases/case_no.php?state=D&state_cd={s}&dist_cd={d}'.format(s=state, d=dist))
    #print('Case_f1--')



    WebDriverWait(driver, 900).until(EC.presence_of_element_located((By.ID, "caseNoDet")))  # wait
    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="captcha_image_audio_controls"]/a')))  # wait audio btn
    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')))  # wait refresh
    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="caseNoDet"]/div[8]/span[3]/input[1]')))  # wait Go btn


    # <--- all repeated Functions ----/////---
    def dynamicdelay(id_name):
        print('dynamicdelay:-', id_name)
        global all_options
        loop_1 = 1
        while True:
            L = 0
            if loop_1 == 1:
                print('D while:-')

                WebDriverWait(driver, 900).until(EC.presence_of_element_located((By.ID, "caseNoDet")))  # wait
                WebDriverWait(driver, 900).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="captcha_image_audio_controls"]/a')))  # wait audio btn
                WebDriverWait(driver, 900).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')))  # wait refresh
                WebDriverWait(driver, 900).until(EC.element_to_be_clickable(
                    (By.XPATH, '//*[@id="caseNoDet"]/div[8]/span[3]/input[1]')))  # wait Go btn
                time.sleep(1)

                html = driver.page_source
                soup = BeautifulSoup(html, "html.parser")
                all_divs = soup.find('select', {'id': str(id_name)})
                #print('all_divs:-', all_divs)

                all_options = all_divs.find_all('option')
                # print('all_options:-', all_options)
                loop_1 = len(all_options)
                # print('--', loop_1)
                L = 0
            else:
                L = 1

            if L == 1:
                # print('D break:-')
                break

        return all_options

    def delay1():
        time.sleep(random.randint(1, 2))
        #await asyncio.sleep(1)

    def delay2():
        time.sleep(random.randint(2, 2))
        #await asyncio.sleep(2)

    #<--Main (try)---
    try:
        html = driver.page_source
        soup = BeautifulSoup(html, "html.parser")
        h1_data = soup.find('h1')
        print('h1_data:-', h1_data)

        # <--Main (if)---
        if h1_data == None:
            print('h1:-', h1_data)

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    WebDriverWait(driver, 900).until(EC.presence_of_element_located((By.ID, "caseNoDet")))  # wait
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="captcha_image_audio_controls"]/a')))  # wait audio btn
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a')))  # wait refresh
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="caseNoDet"]/div[8]/span[3]/input[1]')))  # wait Go btn
                    #delay2()
                    #delay1()
                    await asyncio.sleep(3)
                    # <------COURT COMPLEX--///---start---///-->
                    
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    all_options = all_divs.find_all('option')
                    #print('all_options:-', all_options)
                    
                    
                    #dynamicdelay(id_name='court_complex_code')

                    k3 = [str(x).split('"')[1] for x in all_options]  # <-- output  ['0', '28', '2', '6',..]
                    # print('K3:-', k3)

                    # <---- take input complex_ value from fun()
                    com = complex.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()

                    # <------ Select 'COMPLEX' Options ---->
                    complex = \
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    print('selected complex: ', complex.text)
                    complex.click()
                    #delay2()  # required
                    #delay1()
                    await asyncio.sleep(3)

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="case_type"]')))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    all_options = all_divs.find_all('option')
                    #print('all_options:-', all_options)

                    #dynamicdelay(id_name='case_type')

                    k4 = [str(n1).split('"')[1] for n1 in all_options]
                    # print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(all_options:-)', len(all_options))

                    # <---- take input state value from fun() & find index of that input value ----
                    index4 = k4.index(str(casetype))
                    case_option = index4 + 1  # op-- 20th actual is 21th

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    #delay1()  # required
                    await asyncio.sleep(1)

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[
                        0]  # e.x. option 1
                    print('selected case: ', case.text)
                    case.click()
                    #delay1()
                    await asyncio.sleep(1)
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait

                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(str(caseno))  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(str(year))  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_xpath('//*[@id="captcha_container_2"]/div[1]/div/span[3]/a').click() # refresh btn
                        delay1()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="caseNoDet"]/div[8]/span[3]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        '''
                        driver.execute_script("document.body.style.zoom='190%'")
                        driver.execute_script("window.scrollTo(1, 600);")
                        delay1()
                        '''
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        start_i = time.perf_counter()
                        element = driver.find_element_by_xpath("//*[@id='captcha_image']")
                        location = element.location
                        size = element.size
                        # --------------(for SHOW chrome)--------
                        '''
                        location = {'x': 39, 'y': 265}
                        size = {'height': 86, 'width': 310}

                        # # --------------(for hide windo)--------
                        # location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        # size = {'height': 59, 'width': 310}
                        # # ----------------------------------------
                        '''

                        driver.save_screenshot(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCASE.png')   # str(threading.get_native_id())
                        
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCASE.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCASE.png')
                        #im.show()  # <--- comment it ----
                        print("1'st while image:------")
                        '''
                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/cropimageCASE.png')
                        width, height = im2.size

                        # --------------(for SHOW chrome)--------
                        
                        left = 0  # done
                        top = 0  # done
                        right = 227  # done
                        bottom = 80  # done
                        # ----------------------

                        # # --------------(for hide windo)--------
                        # left = 30  # ... run with option
                        # top = 0  # done
                        # right = 220  # done
                        # bottom = 57  # done
                        # # ----------------------
                        

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/finalimageCASE.png')  # row img
                        #im1.show()
                        # >-----------------------
                        
                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        '''
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        # -----------------------------------------------------------------------------
                        # # --------API CODE--------------
                        start_A = time.perf_counter()
                        with open(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCASE.png', 'rb') as captcha_file:
                            captcha_op = api.solve(captcha_file)
                        captcha_txt2 = captcha_op.await_result()
                        print('API code captcha text:--', captcha_txt2)
                        #print('len(captcha_txt2):--', len(str(captcha_txt2))) #<-- len 6
                        #print('type(captcha_txt2):--', type(captcha_txt2))     #<-- type (str)

                        finish_A = time.perf_counter()
                        print(f'Captcha reading Finished in {round(finish_A - start_A, 2)} second(s)')
                        # #----------------------

                        # <------- Find_data--------/// END ///---------------------------
                        ##captcha_txt2 = 'abcdefff'     #<--- for test only
                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 6:
                                print('!=6 -----')

                                # ------------------------------
                                driver.find_element_by_xpath(
                                    '//*[@id="captcha_container_2"]/div[1]/div/span[3]/a').click()  # refresh btn
                                #delay1()
                                await asyncio.sleep(1)
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                #delay1()
                                await asyncio.sleep(1)

                                WebDriverWait(driver, 600).until(EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="caseNoDet"]/div[8]/span[3]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                '''
                                driver.execute_script("document.body.style.zoom='190%'")
                                driver.execute_script("window.scrollTo(1, 600);")
                                delay1()
                                '''
                                # ------------------------------------------

                                # <--- to find captcha image location ---->
                                start_i = time.perf_counter()
                                element = driver.find_element_by_xpath("//*[@id='captcha_image']")
                                location = element.location
                                size = element.size
                                # --------------(for SHOW chrome)--------
                                '''
                                location = {'x': 39, 'y': 265}
                                size = {'height': 86, 'width': 310}

                                # # --------------(for hide windo)--------
                                # location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                # size = {'height': 59, 'width': 310}
                                # # ----------------------------------------
                                '''

                                driver.save_screenshot(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCASE.png')
                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}imageCASE.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCASE.png')
                                #im.show()  # <--- comment it ----
                                print("2'nd while image:------")
                                '''
                                # <--------// start //----
                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/cropimageCASE.png')
                                width, height = im2.size

                                # --------------(for SHOW chrome)--------
                                
                                left = 0  # done
                                top = 0  # done
                                right = 227  # done
                                bottom = 80  # done
                                # ----------------------

                                # # --------------(for hide windo)--------
                                # left = 30  # ... run with option
                                # top = 0  # done
                                # right = 220  # done
                                # bottom = 57  # done
                                
                                # # ----------------------

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/finalimageCASE.png')  # row img
                                #im1.show()
                                # >-----------------------
                                
                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                '''
                                # <--------// end //----

                                # <---- for Captcha image to text convert---------------comment it------------
                                # # --------API CODE--------------
                                startC = time.perf_counter()
                                with open(settings.MEDIA_ROOT + f'/CaseImages/{str(threading.get_native_id())}cropimageCASE.png', 'rb') as captcha_file:
                                    captcha_op = api.solve(captcha_file)
                                captcha_txt2 = captcha_op.await_result()
                                print('API code captcha text:--', captcha_txt2)
                                print('len(captcha_txt2):--', len(str(captcha_txt2)))
                                print('type(captcha_txt2):--', type(captcha_txt2))

                                finishC = time.perf_counter()
                                print(f'Captcha re-reading Finished in {round(finishC - startC, 2)} second(s)')
                                # # ----------------------

                                #captcha_txt2 = 'abcddd'
                                if len(captcha_txt2) != 6:
                                    print('if len != 6--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('I', 'l')
                                    print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('I', 'l')
                                print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                #time.sleep(3)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                print('finaly break done in <6 or >6-- (if length_ch2 == 1:)----- ')
                                break





                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath('//*[@id="captcha"]')
                        input_element.send_keys(str(re_captcha_txt_final))  ##### <---- Pass text ---->
                        print('send_keys done------')
                        #time.sleep(3) #-- remove_it
                        #delay1()
                        await asyncio.sleep(1)
                        # <----- for Click Search btn---------------------
                        driver.find_element_by_xpath('//*[@id="caseNoDet"]/div[8]/span[3]/input[1]').click()  # <--- sertch btn --->
                        print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        #delay2()
                        #delay1()
                        await asyncio.sleep(2)

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display:none;")/("display: block;")

                        print('-------------------------------')
                        print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        print('-------------------------------')
                        # -------------------------------------------------------------------------------
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '' or op_page_errSpanDiv == "display:block;":
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            print('check2:-first---', check2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                #delay1()
                                print('check2---second---', check2)
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                print('check2-----third---', check2)
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.quit()
                                refresh = 1
                            
                        else:
                            print('if-else-op_page_errSpanDiv == ')
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            print('if refresh == 1:---')
                            break
                    if Record_not_found == 1:
                        print('if Record_not_found == 1:---')
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        print('elif Record_not_found == 0:---')
                        WebDriverWait(driver, 900).until(EC.presence_of_element_located((By.ID, "showList")))  # < wait
                        WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="showList1"]/tr[2]/td[4]/a')))  # < wait

                        driver.execute_script("window.scrollTo(0,370);")
                        time.sleep(1)
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="showList1"]/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="showList1"]/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # -------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="back_top"]/center/a')))
                        #delay2()
                        await asyncio.sleep(2)
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            cnr_text = driver.find_elements_by_xpath('//*[@id="secondpage"]/div[2]/div[1]/b/span')[0].text
                            print('cnr_text:--', {'CnrNumber': str(cnr_text).replace('CNR Number\n:', '')})
                            res_dct = {'ThreadId': str(threading.get_native_id()), 'CnrNumber': str(cnr_text).replace('CNR Number\n:', '')}  # final o/p


                        except Exception as e:
                            # print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}



                        if res_dct != {}:
                            case_final = res_dct
                            try:
                                driver.quit()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {'ThreadId': str(threading.get_native_id()), "CaseDetailsNotFound": 'This Case Code does not exists/Data not available.'}
                            try:
                                driver.quit()
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.quit()
                                break
                            except:
                                pass

                        main_ch2 = 1

                except Exception as e:
                    try:
                        driver.quit()
                        print('except Exception-----', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
                    except:
                        pass
                if main_ch2 == 1:
                    break

            # if res_dct != {}:
            #     yourdata = case_final
            #
            # elif res_dct == {}:
            #     yourdata = case_final_2

            # data = yourdata                 # Final response
            # print('data:--', data)

        else:
            print('parent site server error:-', h1_data)
            res_dct = {}
            case_final_2 = {"ParentSiteServerError": "parent site server error."}
            driver.quit()
    except Exception as e:
        try:
            # driver.quit()
            print('except crser here--', e)
            res_dct = {}
            case_final_2 = {'ThreadId': str(threading.get_native_id()), "ProgramError": str(e)}
        except:
            pass
    finally:
        if res_dct != {}:
            yourdata = case_final

        elif res_dct == {}:
            yourdata = case_final_2



        data = yourdata                 # Final response
        # print('data:--', data)
        # ----------------

    #finish3 = time.perf_counter()
    #print(f'{str(threading.get_native_id())} home Finished in {round(finish3 - start, 2)} second(s)')

    #return HttpResponse(f'Thread id:- {str(threading.get_native_id())}, final_data:-{data}, data:- {state, dist, str(complex), casetype, caseno, year}, Request Finished in:- {round(finish3 - start, 2)} second(s)'.replace("'", '"'))
    return HttpResponse(str(data).replace("'", '"').replace("-", ''))

