# from Scrap_App.models import Count
import os
import psycopg2
import json
from django.conf import settings

# from azcaptchaapi import AZCaptchaApi
# api = AZCaptchaApi('wf9hryjnvvg3clnxh2xcyb7jzgqmkmk8')

from .models import Count1, Count2, Count3, Count4, Count5, Count6, Count7, Count8, Count9, Count10,\
    Case_1, Case_2, Case_3, Case_4, Case_5, Case_6, Case_7, Case_8, Case_9, Case_10

#-----------------------
import multiprocessing
import threading
from rest_framework import views
from rest_framework.response import Response

from .serializers import YourSerializer, ErrorSerializer
from django.http import HttpResponse
from PIL import Image, ImageEnhance
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ChromeOptions
from selenium.webdriver.support import expected_conditions as EC
import random
import time
import cv2
import pytesseract as tess
from PIL import Image, ImageCms, ImageOps, ImageFilter
import numpy as np
from functools import reduce

import uuid
#----------------------------------------------------------------------

#/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/Scrap_App/files/
#<-----------------/* Case No */------------------
def CaseNoView(request, state, dist, complex, casetype, caseno, year):
    path = '/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/Scrap_App/files/'
    if Case_1.objects.all().order_by('-id')[0].var == 0:
        start = time.perf_counter()

        var_dict = {"var": 1, "data": 0}
        # <-- for write --
        with open(path+'case_1.txt', 'w') as json_file:
            json.dump(var_dict, json_file)

        Case_1(var=1, time=time.time(), state=state, dist=dist, complex=complex, casetype=casetype, caseno=caseno,
               year=year).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        print('Case_1 input data:--- ', state, dist, complex, casetype, caseno, year)

        state_data = Case_1.objects.all().order_by('-id')[0].state
        dist_data = Case_1.objects.all().order_by('-id')[0].dist
        complex_data = Case_1.objects.all().order_by('-id')[0].complex
        casetype_data = Case_1.objects.all().order_by('-id')[0].casetype
        caseno_data = Case_1.objects.all().order_by('-id')[0].caseno
        year_data = Case_1.objects.all().order_by('-id')[0].year
        # ------------------(cwork1)----------------------

        def Case_f1():
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            #driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")
            global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found

            #driver.maximize_window()

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            print('Case_f1--')

            # <--- all repeated Functions ----/////---
            '''
            def dynamicdelay(id_name):
                #print('dynamicdelay:-', id_name)
                global job_profiles
                loop_1 = 1
                while True:
                    L = 0
                    if loop_1 == 1:
                        print('D while:-')
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        time.sleep(1)
                        all_divs = soup.find('select', {'id': str(id_name)})
                        job_profiles = all_divs.find_all('option')
                        #print('job_profiles:-', job_profiles)
                        loop_1 = len(job_profiles)
                        #print('--', loop_1)
                        L = 0
                    else:
                        L = 1    #include proxy_params;

                    if L == 1:
                        #print('D break:-')
                        break

                return job_profiles
                '''
            def delay1():
                time.sleep(random.randint(1, 2))    # 1sec
            def delay():
                time.sleep(random.randint(2, 2))    # 2sec

            # ---/ op / ---
            state_ = state_data
            dist_ = dist_data
            complex_ = complex_data
            casetype_ = casetype_data
            caseno_ = caseno_data
            year_ = year_data
            # ----------------// our code //-----------------

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "menuPage")))  # wait
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="menuPage"]/div/div[1]/ul/li[2]')))  # wait
                    driver.find_elements_by_xpath('//*[@id="menuPage"]/div/div[1]/ul/li[2]')[0].click()
                    delay1()
                    # for alert OK btn
                    WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "bs_alert")))  # wait
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bs_alert"]/div/div/div[2]/button')))  # wait
                    driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                    delay1()

                    # <--------STATE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                         EC.presence_of_element_located((By.ID, "divLangState")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_state_code'})
                    job_profiles = all_divs.find_all('option')
                    
                    #dynamicdelay(id_name='sess_state_code')
                    #print('job_profiles:-', job_profiles)
                    delay()
                    delay1()

                    k = [str(x).split('"')[1] for x in job_profiles]  # <-- output  ['0', '28', '2', '6',..]
                    #print('k:-', k)

                    # <---- take input state value from fun() & find index of that input value ----
                    index = k.index(state_)
                    state_option = index + 1    # op-- 20th actual is 21th

                    # <------ Click on 'STATE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_state_code"]')[0].click()
                    delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'STATE' Options ---->
                    state = driver.find_elements_by_xpath('//*[@id="sess_state_code"]/option[{}]'.format(state_option))[
                        0]  # option 1
                    print('selected state:-- ', state.text)
                    state.click()
                    delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    # <--------STATE---///--End----///--------->

                    # <--------DISTRICT--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "sess_dist_code")))  # <---- wait --->
                    
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_dist_code'})
                    dist_job_profiles = all_divs.find_all('option')
                    
                    # print('dist_job_profiles', dist_job_profiles)
                    delay()
                    #dynamicdelay(id_name='sess_dist_code')

                    k2 = [str(j).split('"')[1] for j in dist_job_profiles]
                    #print('k2:-',k2)

                    # <---- take input state value from fun() & find index of that input value ----
                    index2 = k2.index(dist_)
                    dist_option = index2 + 1        # op-- 20th actual is 21th

                    # <------ Click on 'DISTRICT' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]')[0].click()
                    #delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'DISTRICT' Options ---->
                    district=driver.find_elements_by_xpath('//*[@id="sess_dist_code"]/option[{}]'.format(dist_option))[0]
                                                                                                # option 2
                    print('selected dist:-- ', district.text)
                    district.click()
                    #delay()
                    # <--------DISTRICT---///--End----///--------->

                    # <--------COMPLEX--///---start---///-->
                    WebDriverWait(driver, 600).until(
                         EC.presence_of_element_located((By.ID, "court_complex_code")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    complex_job_profiles = all_divs.find_all('option')

                    #dynamicdelay(id_name='court_complex_code')
                    delay()
                    k3 = [str(n).split('"')[1] for n in complex_job_profiles]
                    #print('k3:- ', k3, 'len(k3:-)', len(k3), 'len(complex_job_profiles:-)', len(job_profiles))

                    # <---- take input complex_ value from fun()
                    com = complex_.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()
                    delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait

                    # <------ Select 'COMPLEX' Options ---->
                    complex=driver.find_elements_by_xpath('//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                                                                                                        # option 2
                    print('selected complex: ', complex.text)
                    complex.click()
                    delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    # <--------COMPLEX---///--End----///--------->

                    # <--------CLICK CASE NO. BTN--///------>
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CScaseNumber")))  # <---- wait --->
                    driver.find_elements_by_xpath('//*[@id="CScaseNumber"]')[0].click()
                    delay()
                    # delay()

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    case_job_profiles = all_divs.find_all('option')
                    # print('case_job_profiles', case_job_profiles)

                    delay()
                    #dynamicdelay(id_name='case_type')

                    k4 = [str(n1).split('"')[1] for n1 in case_job_profiles]
                    #print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(case_job_profiles:-)', len(case_job_profiles))

                    case = casetype_.replace('_', '^')
                    index4 = k4.index(str(case))
                    case_option = index4 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    #delay()
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[0]
                    print('selected case: ', case.text)
                    case.click()
                    delay1()
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                         EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(caseno_)  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(year_)  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # Find_data----- START --
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_class_name('refresh-btn').click()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        #delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        driver.execute_script("document.body.style.zoom='170%'")
                        driver.execute_script("window.scrollTo(0,400);")
                        delay1()
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        start2 = time.perf_counter()
                        
                        driver.find_element_by_xpath("//*[@id='captcha_image']")

                        
                        
                        # # --------------(for SHOW chrome)--------
                        location = {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}

                        '''
                        # --------------(for hide windo)--------
                        location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        # ----------------------------------------'''
                        

                        driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE.png')
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')
                        #im.show()  # <--- comment it ----

                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')
                        width, height = im2.size

                        
                        
                        # --------------(for SHOW chrome)--------
                        left = 0
                        top = 0  # done
                        right = 175
                        bottom = 57  # done
                        # ----------------------
                        '''
                        # --------------(for hide windo)--------
                        left = 30  # ... run with option
                        top = 0  # done
                        right = 220  # done
                        bottom = 57  # done
                        # ----------------------'''
                        

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png') #row img
                        #im1.show()
                        # >-----------------------

                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')

                        # <-------- improve image quality ----------->
                        # img.save("quality_abc.png", quality=200)
                        contrast = ImageEnhance.Contrast(img)
                        contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE.png')

                        # <----- for remove unwanted color from image ---------->
                        image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE.png')
                        grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                        lower_range = np.array([0, 0, 0])
                        upper_range = np.array([0, 0, 0])

                        mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE.png', mask)
                        # <-----Done remove unwanted color from image ---------->

                        # <------ image color change in black n white ------
                        Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE.png', 0)
                        ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE.png', thresh_img)
                        cv2.waitKey(0)
                        cv2.destroyAllWindows()

                        # <----- for expand image text ---------------------
                        Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE.png', 0)
                        kernel = np.ones((3, 3), np.uint8)
                        erosion = cv2.erode(Load2, kernel, iterations=1)
                        img = erosion.copy()
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE.png', img)

                        # <----- for Captcha image to text convert---------------------
                        img2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE.png')
                        captcha_txt2 = tess.image_to_string(img2, config='')
                        finish2 = time.perf_counter()
                        print(f'OLD CODE captcha reading Finished in {round(finish2 - start2, 2)} second(s)')

                        #<------- Find_data--------/// END ///---------------------------

                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 8:
                                #print('!=8 --')

                                # ------------------------------
                                driver.find_element_by_class_name('refresh-btn').click()
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                #delay1()
                                WebDriverWait(driver, 600).until(
                                    EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                driver.execute_script("document.body.style.zoom='170%'")
                                driver.execute_script("window.scrollTo(0,400);")
                                delay1()
                                # ----------------------
                                # <--- to find captcha image location ---->
                                driver.find_element_by_xpath("//*[@id='captcha_image']")
                                # start3 = time.perf_counter()

                                
                                
                                # --------------(for SHOW chrome)--------
                                location = {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                #------------------------------------
                                '''
                                # --------------(for hide windo)--------
                                location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                # --------------------------------------'''

                                driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE.png')
                                # driver.get_screenshot_as_file(settings.MEDIA_ROOT + '/CaseImages/imageCASE.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')
                                #im.show()  # <--- comment it ----
                                # <--------

                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')
                                width, height = im2.size

                                
                                
                                # --------------(for SHOW chrome)--------
                                left = 0
                                top = 0  # done
                                right = 175
                                bottom = 57  # done
                                # ----------------------
                                '''
                                # --------------(for hide windo)--------
                                left = 30  # ... run with option
                                top = 0  # done
                                right = 220  # done
                                bottom = 57  # done
                                # ----------------------'''

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')
                                im1.show()
                                # >-----------------------

                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                # delay()

                                # for Captcha image to text convert---------------comment it------------
                                img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')

                                # <-------- improve image quality ----------->
                                contrast = ImageEnhance.Contrast(img)
                                contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE.png')

                                # <----- for remove unwanted color from image ---------->
                                image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE.png')
                                grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                                grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                                lower_range = np.array([0, 0, 0])
                                upper_range = np.array([0, 0, 0])

                                mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE.png', mask)
                                # <-----Done remove unwanted color from image ---------->

                                # <------ image color change in black n white ------
                                Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE.png', 0)
                                ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE.png', thresh_img)
                                cv2.waitKey(0)
                                cv2.destroyAllWindows()

                                # <----- for expand image text ---------------------
                                Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE.png', 0)
                                kernel = np.ones((3, 3), np.uint8)
                                erosion = cv2.erode(Load2, kernel, iterations=1)
                                img = erosion.copy()
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE.png', img)

                                # <----- for Captcha image to text convert---------------------
                                img3 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE.png')
                                re_captcha_txt = tess.image_to_string(img3, config='')
                                # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)

                                # finish3 = time.perf_counter()
                                # print(f'captcha reading Finished in {round(finish3 - start3, 2)} second(s)')

                                captcha_txt2 = re_captcha_txt
                                if len(captcha_txt2) != 8:
                                    # print('if len != 8--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                    # print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                # print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                # print('finaly break done in <6 or >6--')
                                break

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                        input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                        #print('send_keys done------')
                        # <----- for Click Search btn---------------------

                        # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                        driver.find_element_by_xpath('//*[@id="goResetDiv"]/input[1]').click()  # <--- sertch btn --->

                        #print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        delay()
                        delay()

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display: none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        # html = driver.page_source
                        # soup = BeautifulSoup(html, "html.parser")
                        op_page_bs_alertDiv = soup.find('div', {'id': 'bs_alert'}).get('style')
                        # -----For Invalid captcha page("display: block;")
                        #print('op_page_bs_alertDiv--- ', op_page_bs_alertDiv)
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '':
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            #print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                delay()
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                #print('check2--r')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.close()
                                refresh = 1

                        elif op_page_bs_alertDiv == "display: block;":
                            #print('alert 1 ------')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                            refresh = 0

                        elif op_page_bs_alertDiv == "display: block; padding-right: 15px;":
                            #print('alert 2 ---Enter year from 1901 to---')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()

                            res_dct = {}
                            case_final_2 = {'EnterYearFrom1901': 'Enter year from 1901'}
                            Record_not_found = 1
                            driver.close()
                            refresh = 1

                        else:
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            break
                    if Record_not_found == 1:
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        WebDriverWait(driver, 600).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # <---- wait --->
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_showListDiv = soup.find('div', {'id': 'showList'}).get('style')
                        # -----For Invalid captcha page ("display:none;")

                        driver.execute_script("window.scrollTo(0,300);")
                        time.sleep(1)
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # ----------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "caseHistoryDiv")))
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bckbtn"]')))
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            print('try_1:-')
                            l = []
                            table_case = soup.find('table', attrs={'class': 'case_details_table'})
                            table_case_body = table_case.find('tbody')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values

                            b = l[:len(l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                            b1 = l[-1][1].replace('\xa0\xa0 (Note the CNR number for future reference)', '')
                                                    # <-- for remove last "(Note the CNR number for future reference)" text o/p-- "MHPU050000132021"

                            lst = reduce(lambda x, y: x + y, b)
                            lst.append('cnrnumber')  # < ---- add 'CNR' name
                            lst.append(b1)  # < ---- add 'CNR number
                            res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except Exception as e:
                            #print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {"ProgramError": str(e)}

                        try:
                            # <---2nd Table, take single table data, into list ------------>
                            l2 = []
                            table = soup.find('table', attrs={'class': 'table_r'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l2.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst2 = reduce(lambda x, y: x + y, l2)
                            res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in range(0, len(lst2), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct2 = {}

                        try:
                            # <---3rd Table, take single table data, into list ----------->
                            l3 = []
                            table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l3.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst3 = reduce(lambda x, y: x + y, l3)
                            listToStr = ' '.join(map(str, lst3))
                            res_dct3 = listToStr.replace('\xa0', '')  # <-- make kye 'LOWER case', remove 'space' ---
                        except:
                            res_dct3 = ''

                        try:
                            # <---4th Table, take single table data, into list ---------->
                            l4 = []
                            table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l4.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst4 = reduce(lambda x, y: x + y, l4)
                            listToStr2 = ' '.join(map(str, lst4))
                            res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct4 = ''

                        try:
                            # <---5th Table, take single table data, into list ---------->
                            table_a = soup.find('table', attrs={'class': 'acts_table'})
                            h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                            res_dct5 = [dict(zip(h, i)) for i in d]

                            k5 = []
                            for d in res_dct5:
                                k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                    'UnderSection(s)', 'Section'): v for k, v in d.items()})
                        except:
                            k5 = []

                        try:
                            # <---6th Table, take single table data, into list ---------->
                            table_t = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                            res_dct6 = [dict(zip(h, i)) for i in d]

                            k6 = []
                            for d in res_dct6:
                                k6.append(
                                    {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                     d.items()})
                        except:
                            k6 = []

                        try:
                            # <---7th Table, take single table data, into list ---------->
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]

                            k7 = []
                            for d7 in res_dct7:
                                k7.append(
                                    {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                        except:
                            k7 = []

                        try:
                            # <---8th Table, take single table data, into list ---------->
                            table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                            h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                            res_dct8 = [dict(zip(h1, x)) for x in q]

                            k8 = []
                            for d in res_dct8:
                                k8.append({k.replace(' ', ''): v for k, v in d.items()})
                        except:
                            k8 = []

                        main_ch2 = 1

                        if res_dct != {}:
                            case_final = {"CaseDetails": res_dct, "CaseStatus": res_dct2,
                                          "PetitionerAdvocate": res_dct3,
                                          "RespondentAdvocate": res_dct4, "Acts": k5,
                                          "History": k6, "Orders": k7, "CaseTransferDetails": k8}
                            try:
                                driver.close()
                                print('Exception:-1 1')
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {
                                "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}
                            try:
                                driver.close()
                                print('Exception:-1 2')
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.close()
                                print('Exception:-1 3')
                                break
                            except:
                                pass
                except Exception as e:
                    try:
                        driver.close()
                        print('Exception:-1 4')
                        #print('except crser here--', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {"ProgramError": str(e)}
                    except:
                        pass

                if main_ch2 == 1:
                    break

            if res_dct != {}:
                yourdata = case_final

            elif res_dct == {}:
                yourdata = case_final_2

            data = yourdata
            #print('data:-', data)

            try:
                print('--pg try:--')
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")

                cursor = conn.cursor()
                print('--psycopg2 connect execute--')

                var_dict = {"var": 0, "data": 1}
                # <-- for write --
                with open(path+'case_1.txt', 'w') as json_file:
                    json.dump(var_dict, json_file)

            except (Exception, psycopg2.DatabaseError) as error:
                print("--Error while creating PostgreSQL table", error)
                #pass
            else:

                cursor.execute('SELECT * FROM public."Scrap_App_case_1" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_case_1" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                print("--Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------//Case function 1 End //--------------------------------------------------

        multiprocessing.Process(target=Case_f1).start()
        #threading.Thread(target=Case_f1).start()
        print('multi-1-')

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            #wait = Case_1.objects.all().order_by('-id')[0].data

            # <-- for read ---
            with open(path+'case_1.txt', 'r') as f:
                _dict = json.load(f)
                print(_dict['var'])  # 0
            print('wait data:- ', _dict['data'])
            time.sleep(5)
            wait = _dict['data']  # 0

            #print('wait ON-')
            #print('wait OFF-')
            if wait == 0:
                w = 0
            else:
                w = 1
            if w == 1:
                print('out while..MAIN:-')
                break
        # ------// --- //----
        time.sleep(3)
        #print('test_1')    
        data = Case_1.objects.all().order_by('-id')[0].data
        #print('test_2', data)

        # -----------current id make it 0 -------------
        c_id = Case_1.objects.all().order_by('-id')[0].id
        c_id2 = Case_1.objects.get(id=c_id)
        c_id2.var = 0  # for make 1 to 0
        c_id2.save()

        finish = time.perf_counter()
        print(f'Case_1 Finished in {round(finish - start, 2)} second(s)')
# ------------------(cwork1e)----------------------

    elif Case_2.objects.all().order_by('-id')[0].var == 0:
        start = time.perf_counter()

        var_dict = {"var": 1, "data": 0}
        # <-- for write --
        with open('Scrap_App/files/case_2.txt', 'w') as json_file:
            json.dump(var_dict, json_file)

        Case_2(var=1, time=time.time(), state=state, dist=dist, complex=complex, casetype=casetype, caseno=caseno,
               year=year).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        print('Case_2 input data:--- ', state, dist, complex, casetype, caseno, year)

        state_data = Case_2.objects.all().order_by('-id')[0].state
        dist_data = Case_2.objects.all().order_by('-id')[0].dist
        complex_data = Case_2.objects.all().order_by('-id')[0].complex
        casetype_data = Case_2.objects.all().order_by('-id')[0].casetype
        caseno_data = Case_2.objects.all().order_by('-id')[0].caseno
        year_data = Case_2.objects.all().order_by('-id')[0].year

        # ------------------(cwork2)----------------------

        def Case_f2():
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = ''
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            #driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")
            global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            # driver.maximize_window()

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            print('Case_2-f-')
            '''
            # <--- all repeated Functions ----/////---
            def dynamicdelay(id_name):
                # print('dynamicdelay:-', id_name)
                global job_profiles
                loop_1 = 1
                while True:
                    L = 0
                    if loop_1 == 1:
                        print('D while Case_2:-')
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        time.sleep(1)
                        all_divs = soup.find('select', {'id': str(id_name)})
                        job_profiles = all_divs.find_all('option')
                        # print('job_profiles:-', job_profiles)
                        loop_1 = len(job_profiles)
                        # print('--', loop_1)
                        L = 0
                    else:
                        L = 1

                    if L == 1:
                        #print('D break:-')
                        break

                return job_profiles
                '''
            def delay1():
                time.sleep(random.randint(1, 2))  # 1sec

            def delay():
                time.sleep(random.randint(2, 2))  # 2sec

            # ---/ op / ---
            state_ = state_data
            dist_ = dist_data
            complex_ = complex_data
            casetype_ = casetype_data
            caseno_ = caseno_data
            year_ = year_data
            # ----------------// our code //-----------------

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "menuPage")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="menuPage"]/div/div[1]/ul/li[2]')))  # wait
                    driver.find_elements_by_xpath('//*[@id="menuPage"]/div/div[1]/ul/li[2]')[0].click()
                    delay1()
                    # for alert OK btn
                    WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "bs_alert")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="bs_alert"]/div/div/div[2]/button')))  # wait
                    driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                    delay1()

                    # <--------STATE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                         EC.presence_of_element_located((By.ID, "divLangState")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_state_code'})
                    state_job_profiles = all_divs.find_all('option')

                    #dynamicdelay(id_name='sess_state_code')
                    # print('job_profiles:-', job_profiles)
                    delay()
                    
                    k = [str(x).split('"')[1] for x in state_job_profiles]  # <-- output  ['0', '28', '2', '6',..]
                    # print('k:-', k)

                    # <---- take input state value from fun() & find index of that input value ----
                    index = k.index(state_)
                    state_option = index + 1  # op-- 20th actual is 21th

                    # <------ Click on 'STATE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_state_code"]')[0].click()
                    delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'STATE' Options ---->
                    state = driver.find_elements_by_xpath('//*[@id="sess_state_code"]/option[{}]'.format(state_option))[
                        0]  # option 1
                    print('selected state case_2:-- ', state.text)
                    state.click()
                    delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    # <--------STATE---///--End----///--------->

                    # <--------DISTRICT--///---start---///-->
                    WebDriverWait(driver, 600).until(
                         EC.presence_of_element_located((By.ID, "sess_dist_code")))  # <---- wait --->
                    
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_dist_code'})
                    dist_job_profiles = all_divs.find_all('option')
                    # print('dist_job_profiles', dist_job_profiles)

                    #dynamicdelay(id_name='sess_dist_code')
                    delay()
                    
                    k2 = [str(j).split('"')[1] for j in dist_job_profiles]
                    # print('k2:-', k2)

                    # <---- take input state value from fun() & find index of that input value ----
                    index2 = k2.index(dist_)
                    dist_option = index2 + 1  # op-- 20th actual is 21th

                    # <------ Click on 'DISTRICT' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]')[0].click()
                    delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'DISTRICT' Options ---->
                    district = \
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]/option[{}]'.format(dist_option))[0]
                    # option 2
                    print('selected dist case_2:-- ', district.text)
                    district.click()
                    # delay()
                    # <--------DISTRICT---///--End----///--------->

                    # <--------COMPLEX--///---start---///-->
                    WebDriverWait(driver, 600).until(
                         EC.presence_of_element_located((By.ID, "court_complex_code")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    complex_job_profiles = all_divs.find_all('option')

                    #dynamicdelay(id_name='court_complex_code')
                    delay()
                    
                    k3 = [str(n).split('"')[1] for n in complex_job_profiles]
                    # print('k3:- ', k3, 'len(k3:-)', len(k3), 'len(complex_job_profiles:-)', len(job_profiles))

                    # <---- take input complex_ value from fun()
                    com = complex_.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()
                    delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait

                    # <------ Select 'COMPLEX' Options ---->
                    complex = \
                        driver.find_elements_by_xpath(
                            '//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    # option 2
                    print('selected complex case_2:- ', complex.text)
                    complex.click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    # <--------COMPLEX---///--End----///--------->

                    # <--------CLICK CASE NO. BTN--///------>
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CScaseNumber")))  # <---- wait --->
                    driver.find_elements_by_xpath('//*[@id="CScaseNumber"]')[0].click()
                    delay()
                    # delay()

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    case_job_profiles = all_divs.find_all('option')
                    # print('case_job_profiles', case_job_profiles)

                    #dynamicdelay(id_name='case_type')
                    dealy()
                    
                    k4 = [str(n1).split('"')[1] for n1 in case_job_profiles]
                    # print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(case_job_profiles:-)', len(job_profiles))

                    case = casetype_.replace('_', '^')
                    index4 = k4.index(str(case))
                    case_option = index4 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    delay()
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[0]
                    print('selected case TYPE case_2:- ', case.text)
                    case.click()
                    delay1()
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                         EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(caseno_)  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(year_)  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # Find_data----- START --
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_class_name('refresh-btn').click()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        # delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        driver.execute_script("document.body.style.zoom='170%'")
                        driver.execute_script("window.scrollTo(0,400);")
                        delay1()
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        # start2 = time.perf_counter()
                        driver.find_element_by_xpath("//*[@id='captcha_image']")

                        # --------------(for SHOW chrome)--------
                        location = {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        #------------------------------------
                        '''
                        # --------------(for hide windo)--------
                        location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        # --------------------------------------'''

                        driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_2.png')
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_2.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')
                        # im.show()  # <--- comment it ----

                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')
                        width, height = im2.size

                        # --------------(for SHOW chrome)--------
                        left = 0
                        top = 0  # done
                        right = 175
                        bottom = 57  # done
                        # ----------------------
                        '''
                        # --------------(for hide windo)--------
                        left = 30  # ... run with option
                        top = 0  # done
                        right = 220  # done
                        bottom = 57  # done
                        # ----------------------'''

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')  # row img
                        #im1.show()
                        # >-----------------------

                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')

                        # <-------- improve image quality ----------->
                        # img.save("quality_abc_2.png", quality=200)
                        contrast = ImageEnhance.Contrast(img)
                        contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_2.png')

                        # <----- for remove unwanted color from image ---------->
                        image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_2.png')
                        grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                        lower_range = np.array([0, 0, 0])
                        upper_range = np.array([0, 0, 0])

                        mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_2.png', mask)
                        # <-----Done remove unwanted color from image ---------->

                        # <------ image color change in black n white ------
                        Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_2.png', 0)
                        ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_2.png', thresh_img)
                        cv2.waitKey(0)
                        cv2.destroyAllWindows()

                        # <----- for expand image text ---------------------
                        Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_2.png', 0)
                        kernel = np.ones((3, 3), np.uint8)
                        erosion = cv2.erode(Load2, kernel, iterations=1)
                        img = erosion.copy()
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_2.png', img)

                        # <----- for Captcha image to text convert---------------------
                        img2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_2.png')
                        captcha_txt2 = tess.image_to_string(img2, config='')
                        # finish2 = time.perf_counter()
                        # print(f'OLD CODE captcha reading Finished in {round(finish2 - start2, 2)} second(s)')

                        # <------- Find_data--------/// END ///---------------------------

                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 8:
                                # print('!=8 --')

                                # ------------------------------
                                driver.find_element_by_class_name('refresh-btn').click()
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                # delay1()
                                WebDriverWait(driver, 600).until(
                                    EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                driver.execute_script("document.body.style.zoom='170%'")
                                driver.execute_script("window.scrollTo(0,400);")
                                delay1()
                                # ----------------------
                                # <--- to find captcha image location ---->
                                driver.find_element_by_xpath("//*[@id='captcha_image']")
                                # start3 = time.perf_counter()

                                # --------------(for SHOW chrome)--------
                                location = {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                #------------------------------------
                                '''
                                # --------------(for hide windo)--------
                                location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                # --------------------------------------'''

                                driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_2.png')
                                # driver.get_screenshot_as_file(settings.MEDIA_ROOT + '/CaseImages/imageCASE_2.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_2.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')
                                # im.show()  # <--- comment it ----
                                # <--------

                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')
                                width, height = im2.size

                                # --------------(for SHOW chrome)--------
                                left = 0
                                top = 0  # done
                                right = 175
                                bottom = 57  # done
                                # ----------------------
                                '''
                                # --------------(for hide windo)--------
                                left = 30  # ... run with option
                                top = 0  # done
                                right = 220  # done
                                bottom = 57  # done
                                # ----------------------'''

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')
                                # im1.show()
                                # >-----------------------

                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                # delay()

                                # for Captcha image to text convert---------------comment it------------
                                img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')

                                # <-------- improve image quality ----------->
                                contrast = ImageEnhance.Contrast(img)
                                contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_2.png')

                                # <----- for remove unwanted color from image ---------->
                                image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_2.png')
                                grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                                grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                                lower_range = np.array([0, 0, 0])
                                upper_range = np.array([0, 0, 0])

                                mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_2.png', mask)
                                # <-----Done remove unwanted color from image ---------->

                                # <------ image color change in black n white ------
                                Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_2.png', 0)
                                ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_2.png', thresh_img)
                                cv2.waitKey(0)
                                cv2.destroyAllWindows()

                                # <----- for expand image text ---------------------
                                Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_2.png', 0)
                                kernel = np.ones((3, 3), np.uint8)
                                erosion = cv2.erode(Load2, kernel, iterations=1)
                                img = erosion.copy()
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_2.png', img)

                                # <----- for Captcha image to text convert---------------------
                                img3 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_2.png')
                                re_captcha_txt = tess.image_to_string(img3, config='')
                                # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)

                                # finish3 = time.perf_counter()
                                # print(f'captcha reading Finished in {round(finish3 - start3, 2)} second(s)')

                                captcha_txt2 = re_captcha_txt
                                if len(captcha_txt2) != 8:
                                    # print('if len != 8--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                    # print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                # print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                # print('finaly break done in <6 or >6--')
                                break

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                        input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        # <----- for Click Search btn---------------------

                        # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                        driver.find_element_by_xpath('//*[@id="goResetDiv"]/input[1]').click()  # <--- sertch btn --->

                        # print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        delay()
                        delay()

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display: none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        # html = driver.page_source
                        # soup = BeautifulSoup(html, "html.parser")
                        op_page_bs_alertDiv = soup.find('div', {'id': 'bs_alert'}).get('style')
                        # -----For Invalid captcha page("display: block;")
                        # print('op_page_bs_alertDiv--- ', op_page_bs_alertDiv)
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '':
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                delay()
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                # print('check2--r')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.close()
                                refresh = 1

                        elif op_page_bs_alertDiv == "display: block;":
                            # print('alert 1 ------')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                            refresh = 0

                        elif op_page_bs_alertDiv == "display: block; padding-right: 15px;":
                            # print('alert 2 ---Enter year from 1901 to---')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()

                            res_dct = {}
                            case_final_2 = {'EnterYearFrom1901': 'Enter year from 1901'}
                            Record_not_found = 1
                            driver.close()
                            refresh = 1

                        else:
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            break
                    if Record_not_found == 1:
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        WebDriverWait(driver, 600).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # <---- wait --->
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_showListDiv = soup.find('div', {'id': 'showList'}).get('style')
                        # -----For Invalid captcha page ("display:none;")

                        driver.execute_script("window.scrollTo(0,300);")
                        time.sleep(1)
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # ----------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "caseHistoryDiv")))
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bckbtn"]')))
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            l = []
                            table_case = soup.find('table', attrs={'class': 'case_details_table'})
                            table_case_body = table_case.find('tbody')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values

                            b = l[:len(l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                            b1 = l[-1][1].replace('\xa0\xa0 (Note the CNR number for future reference)', '')
                            # <-- for remove last "(Note the CNR number for future reference)" text o/p-- "MHPU050000132021"

                            lst = reduce(lambda x, y: x + y, b)
                            lst.append('cnrnumber')  # < ---- add 'CNR' name
                            lst.append(b1)  # < ---- add 'CNR number
                            res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except Exception as e:
                            print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {"ProgramError": str(e)}

                        try:
                            # <---2nd Table, take single table data, into list ------------>
                            l2 = []
                            table = soup.find('table', attrs={'class': 'table_r'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l2.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst2 = reduce(lambda x, y: x + y, l2)
                            res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in range(0, len(lst2), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct2 = {}

                        try:
                            # <---3rd Table, take single table data, into list ----------->
                            l3 = []
                            table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l3.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst3 = reduce(lambda x, y: x + y, l3)
                            listToStr = ' '.join(map(str, lst3))
                            res_dct3 = listToStr.replace('\xa0', '')  # <-- make kye 'LOWER case', remove 'space' ---
                        except:
                            res_dct3 = ''

                        try:
                            # <---4th Table, take single table data, into list ---------->
                            l4 = []
                            table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l4.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst4 = reduce(lambda x, y: x + y, l4)
                            listToStr2 = ' '.join(map(str, lst4))
                            res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct4 = ''

                        try:
                            # <---5th Table, take single table data, into list ---------->
                            table_a = soup.find('table', attrs={'class': 'acts_table'})
                            h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                            res_dct5 = [dict(zip(h, i)) for i in d]

                            k5 = []
                            for d in res_dct5:
                                k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                    'UnderSection(s)', 'Section'): v for k, v in d.items()})
                        except:
                            k5 = []

                        try:
                            # <---6th Table, take single table data, into list ---------->
                            table_t = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                            res_dct6 = [dict(zip(h, i)) for i in d]

                            k6 = []
                            for d in res_dct6:
                                k6.append(
                                    {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                     d.items()})
                        except:
                            k6 = []

                        try:
                            # <---7th Table, take single table data, into list ---------->
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]

                            k7 = []
                            for d7 in res_dct7:
                                k7.append(
                                    {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                        except:
                            k7 = []

                        try:
                            # <---8th Table, take single table data, into list ---------->
                            table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                            h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                            res_dct8 = [dict(zip(h1, x)) for x in q]

                            k8 = []
                            for d in res_dct8:
                                k8.append({k.replace(' ', ''): v for k, v in d.items()})
                        except:
                            k8 = []

                        main_ch2 = 1

                        if res_dct != {}:
                            case_final = {"CaseDetails": res_dct, "CaseStatus": res_dct2,
                                          "PetitionerAdvocate": res_dct3,
                                          "RespondentAdvocate": res_dct4, "Acts": k5,
                                          "History": k6, "Orders": k7, "CaseTransferDetails": k8}
                            try:
                                driver.close()
                                print('Exception:-C2 1')
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {"CaseDetailsNotFound": 'Please try again.'}
                            try:
                                driver.close()
                                print('Exception:-C2 2')
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.close()
                                print('Exception:-C2 3')
                                break
                            except:
                                pass
                except Exception as e:
                    try:
                        driver.close()
                        print('Exception:-C2 4', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {"ProgramError": str(e)}
                    except:
                        pass

                if main_ch2 == 1:
                    break

            if res_dct != {}:
                yourdata = case_final

            elif res_dct == {}:
                yourdata = case_final_2

            data = yourdata

            try:
                conn = psycopg2.connect(database="defaultdb", user="doadmin", password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")

                cursor = conn.cursor()
                # print('psycopg2 connect execute--')

                var_dict = {"var": 0, "data": 1}
                # <-- for write --
                with open('Scrap_App/files/case_2.txt', 'w') as json_file:
                    json.dump(var_dict, json_file)

            except (Exception, psycopg2.DatabaseError) as error:
                # print("Error while creating PostgreSQL table", error)
                pass
            else:

                cursor.execute('SELECT * FROM public."Scrap_App_case_2" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_case_2" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                # print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------//Case function 1 End //--------------------------------------------------

        multiprocessing.Process(target=Case_f2).start()
        #threading.Thread(target=Case_f2).start()
        print('multi 2--')

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            # wait = Case_2.objects.all().order_by('-id')[0].data

            # <-- for read ---
            with open('Scrap_App/files/case_2.txt', 'r') as f:
                _dict = json.load(f)
                # print(_dict['var'])  # 0
            # print('wait data:- ', _dict['data'])
            time.sleep(5)
            wait = _dict['data']  # 0

            # print('wait ON-')
            # print('wait OFF-')
            if wait == 0:
                w = 0
            else:
                w = 1
            if w == 1:
                # print('out while..')
                break
        # ------// --- //----
        time.sleep(3)

        data = Case_2.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        c_id = Case_2.objects.all().order_by('-id')[0].id
        c_id2 = Case_2.objects.get(id=c_id)
        c_id2.var = 0  # for make 1 to 0
        c_id2.save()

        finish = time.perf_counter()
        print(f'Case_2 Finished in {round(finish - start, 2)} second(s)')
# ------------------(cwork2e)----------------------

    elif Case_3.objects.all().order_by('-id')[0].var == 0:
        start = time.perf_counter()

        var_dict = {"var": 1, "data": 0}
        # <-- for write --
        with open('Scrap_App/files/case_3.txt', 'w') as json_file:
            json.dump(var_dict, json_file)

        Case_3(var=1, time=time.time(), state=state, dist=dist, complex=complex, casetype=casetype, caseno=caseno,
               year=year).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        print('Case_3 input data:--- ', state, dist, complex, casetype, caseno, year)

        state_data = Case_3.objects.all().order_by('-id')[0].state
        dist_data = Case_3.objects.all().order_by('-id')[0].dist
        complex_data = Case_3.objects.all().order_by('-id')[0].complex
        casetype_data = Case_3.objects.all().order_by('-id')[0].casetype
        caseno_data = Case_3.objects.all().order_by('-id')[0].caseno
        year_data = Case_3.objects.all().order_by('-id')[0].year

        # ------------------(cwork3)----------------------

        def Case_3_fun():
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")
            global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            # driver.maximize_window()

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')

            print('Case_3_fun')

            # <--- all repeated Functions ----/////---
            def dynamicdelay(id_name):
                # print('dynamicdelay:-', id_name)
                global job_profiles
                loop_1 = 1
                while True:
                    L = 0
                    if loop_1 == 1:
                        # print('D while:-')
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        time.sleep(1)
                        all_divs = soup.find('select', {'id': str(id_name)})
                        job_profiles = all_divs.find_all('option')
                        # print('job_profiles:-', job_profiles)
                        loop_1 = len(job_profiles)
                        # print('--', loop_1)
                        L = 0
                    else:
                        L = 1

                    if L == 1:
                        # print('D break:-')
                        break

                return job_profiles

            def delay1():
                time.sleep(random.randint(1, 2))  # 1sec

            def delay():
                time.sleep(random.randint(2, 2))  # 2sec

            # ---/ op / ---
            state_ = state_data
            dist_ = dist_data
            complex_ = complex_data
            casetype_ = casetype_data
            caseno_ = caseno_data
            year_ = year_data
            # ----------------// our code //-----------------

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "menuPage")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="menuPage"]/div/div[1]/ul/li[2]')))  # wait
                    driver.find_elements_by_xpath('//*[@id="menuPage"]/div/div[1]/ul/li[2]')[0].click()
                    # delay1()
                    # for alert OK btn
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "bs_alert")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="bs_alert"]/div/div/div[2]/button')))  # wait
                    driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                    # delay1()

                    # <--------STATE--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "divLangState")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_state_code'})
                    state_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='sess_state_code')
                    # print('job_profiles:-', job_profiles)

                    k = [str(x).split('"')[1] for x in job_profiles]  # <-- output  ['0', '28', '2', '6',..]
                    # print('k:-', k)

                    # <---- take input state value from fun() & find index of that input value ----
                    index = k.index(state_)
                    state_option = index + 1  # op-- 20th actual is 21th

                    # <------ Click on 'STATE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_state_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'STATE' Options ---->
                    state = driver.find_elements_by_xpath('//*[@id="sess_state_code"]/option[{}]'.format(state_option))[
                        0]  # option 1
                    # print('selected state:-- ', state.text)
                    state.click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    # <--------STATE---///--End----///--------->

                    # <--------DISTRICT--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "sess_dist_code")))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_dist_code'})
                    dist_job_profiles = all_divs.find_all('option')
                    # print('dist_job_profiles', dist_job_profiles)'''

                    dynamicdelay(id_name='sess_dist_code')

                    k2 = [str(j).split('"')[1] for j in job_profiles]
                    # print('k2:-', k2)

                    # <---- take input state value from fun() & find index of that input value ----
                    index2 = k2.index(dist_)
                    dist_option = index2 + 1  # op-- 20th actual is 21th

                    # <------ Click on 'DISTRICT' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'DISTRICT' Options ---->
                    district = \
                        driver.find_elements_by_xpath('//*[@id="sess_dist_code"]/option[{}]'.format(dist_option))[0]
                    # option 2
                    # print('selected dist:-- ', district.text)
                    district.click()
                    # delay()
                    # <--------DISTRICT---///--End----///--------->

                    # <--------COMPLEX--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "court_complex_code")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    complex_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='court_complex_code')

                    k3 = [str(n).split('"')[1] for n in job_profiles]
                    # print('k3:- ', k3, 'len(k3:-)', len(k3), 'len(complex_job_profiles:-)', len(job_profiles))

                    # <---- take input complex_ value from fun()
                    com = complex_.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait

                    # <------ Select 'COMPLEX' Options ---->
                    complex = \
                        driver.find_elements_by_xpath(
                            '//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    # option 2
                    # print('selected complex: ', complex.text)
                    complex.click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    # <--------COMPLEX---///--End----///--------->

                    # <--------CLICK CASE NO. BTN--///------>
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CScaseNumber")))  # <---- wait --->
                    driver.find_elements_by_xpath('//*[@id="CScaseNumber"]')[0].click()
                    # delay()
                    # delay()

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    case_job_profiles = all_divs.find_all('option')
                    # print('case_job_profiles', case_job_profiles)'''

                    dynamicdelay(id_name='case_type')

                    k4 = [str(n1).split('"')[1] for n1 in job_profiles]
                    # print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(case_job_profiles:-)', len(job_profiles))

                    case = casetype_.replace('_', '^')
                    index4 = k4.index(str(case))
                    case_option = index4 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[0]
                    # print('selected case: ', case.text)
                    case.click()
                    # delay1()
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(caseno_)  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(year_)  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # Find_data----- START --
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_class_name('refresh-btn').click()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        # delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        driver.execute_script("document.body.style.zoom='170%'")
                        driver.execute_script("window.scrollTo(0,400);")
                        delay1()
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        # start2 = time.perf_counter()
                        driver.find_element_by_xpath("//*[@id='captcha_image']")

                        # # --------------(for SHOW chrome)--------
                        # location = {'x': 219, 'y': 380}
                        # size = {'height': 59, 'width': 310}

                        # --------------(for hide windo)--------
                        location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        # ----------------------------------------

                        driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_3.png')
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_3.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_3.png')
                        # im.show()  # <--- comment it ----

                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_3.png')
                        width, height = im2.size

                        # # --------------(for SHOW chrome)--------
                        # left = 0
                        # top = 0  # done
                        # right = 175
                        # bottom = 57  # done
                        # # ----------------------

                        # --------------(for hide windo)--------
                        left = 30  # ... run with option
                        top = 0  # done
                        right = 220  # done
                        bottom = 57  # done
                        # ----------------------

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_3.png')  # row img
                        # im1.show()
                        # >-----------------------

                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_3.png')

                        # <-------- improve image quality ----------->
                        # img.save("quality_abc_2.png", quality=200)
                        contrast = ImageEnhance.Contrast(img)
                        contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_3.png')

                        # <----- for remove unwanted color from image ---------->
                        image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_3.png')
                        grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                        lower_range = np.array([0, 0, 0])
                        upper_range = np.array([0, 0, 0])

                        mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_3.png', mask)
                        # <-----Done remove unwanted color from image ---------->

                        # <------ image color change in black n white ------
                        Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_3.png', 0)
                        ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_3.png', thresh_img)
                        cv2.waitKey(0)
                        cv2.destroyAllWindows()

                        # <----- for expand image text ---------------------
                        Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_3.png', 0)
                        kernel = np.ones((3, 3), np.uint8)
                        erosion = cv2.erode(Load2, kernel, iterations=1)
                        img = erosion.copy()
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_3.png', img)

                        # <----- for Captcha image to text convert---------------------
                        img2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_3.png')
                        captcha_txt2 = tess.image_to_string(img2, config='')
                        # finish2 = time.perf_counter()
                        # print(f'OLD CODE captcha reading Finished in {round(finish2 - start2, 2)} second(s)')

                        # <------- Find_data--------/// END ///---------------------------

                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 8:
                                # print('!=8 --')

                                # ------------------------------
                                driver.find_element_by_class_name('refresh-btn').click()
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                # delay1()
                                WebDriverWait(driver, 600).until(
                                    EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                driver.execute_script("document.body.style.zoom='170%'")
                                driver.execute_script("window.scrollTo(0,400);")
                                delay1()
                                # ----------------------
                                # <--- to find captcha image location ---->
                                driver.find_element_by_xpath("//*[@id='captcha_image']")
                                # start3 = time.perf_counter()

                                # # --------------(for SHOW chrome)--------
                                # location = {'x': 219, 'y': 380}
                                # size = {'height': 59, 'width': 310}

                                # --------------(for hide windo)--------
                                location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                # ----------------------------------------

                                driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_3.png')
                                # driver.get_screenshot_as_file(settings.MEDIA_ROOT + '/CaseImages/imageCASE_3.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_3.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_3.png')
                                # im.show()  # <--- comment it ----
                                # <--------

                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_3.png')
                                width, height = im2.size

                                # # --------------(for SHOW chrome)--------
                                # left = 0
                                # top = 0  # done
                                # right = 175
                                # bottom = 57  # done
                                # # ----------------------

                                # --------------(for hide windo)--------
                                left = 30  # ... run with option
                                top = 0  # done
                                right = 220  # done
                                bottom = 57  # done
                                # ----------------------

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_3.png')
                                # im1.show()
                                # >-----------------------

                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                # delay()

                                # for Captcha image to text convert---------------comment it------------
                                img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_3.png')

                                # <-------- improve image quality ----------->
                                contrast = ImageEnhance.Contrast(img)
                                contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_3.png')

                                # <----- for remove unwanted color from image ---------->
                                image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_3.png')
                                grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                                grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                                lower_range = np.array([0, 0, 0])
                                upper_range = np.array([0, 0, 0])

                                mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_3.png', mask)
                                # <-----Done remove unwanted color from image ---------->

                                # <------ image color change in black n white ------
                                Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_3.png', 0)
                                ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_3.png', thresh_img)
                                cv2.waitKey(0)
                                cv2.destroyAllWindows()

                                # <----- for expand image text ---------------------
                                Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_3.png', 0)
                                kernel = np.ones((3, 3), np.uint8)
                                erosion = cv2.erode(Load2, kernel, iterations=1)
                                img = erosion.copy()
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_3.png', img)

                                # <----- for Captcha image to text convert---------------------
                                img3 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_3.png')
                                re_captcha_txt = tess.image_to_string(img3, config='')
                                # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)

                                # finish3 = time.perf_counter()
                                # print(f'captcha reading Finished in {round(finish3 - start3, 2)} second(s)')

                                captcha_txt2 = re_captcha_txt
                                if len(captcha_txt2) != 8:
                                    # print('if len != 8--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                    # print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                # print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                # print('finaly break done in <6 or >6--')
                                break

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                        input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        # <----- for Click Search btn---------------------

                        # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                        driver.find_element_by_xpath('//*[@id="goResetDiv"]/input[1]').click()  # <--- sertch btn --->

                        # print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        delay()
                        delay()

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display: none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        # html = driver.page_source
                        # soup = BeautifulSoup(html, "html.parser")
                        op_page_bs_alertDiv = soup.find('div', {'id': 'bs_alert'}).get('style')
                        # -----For Invalid captcha page("display: block;")
                        # print('op_page_bs_alertDiv--- ', op_page_bs_alertDiv)
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '':
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                delay()
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                # print('check2--r')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.close()
                                refresh = 1

                        elif op_page_bs_alertDiv == "display: block;":
                            # print('alert 1 ------')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                            refresh = 0

                        elif op_page_bs_alertDiv == "display: block; padding-right: 15px;":
                            # print('alert 2 ---Enter year from 1901 to---')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()

                            res_dct = {}
                            case_final_2 = {'EnterYearFrom1901': 'Enter year from 1901'}
                            Record_not_found = 1
                            driver.close()
                            refresh = 1

                        else:
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            break
                    if Record_not_found == 1:
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        WebDriverWait(driver, 600).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # <---- wait --->
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_showListDiv = soup.find('div', {'id': 'showList'}).get('style')
                        # -----For Invalid captcha page ("display:none;")

                        driver.execute_script("window.scrollTo(0,300);")
                        time.sleep(1)
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # ----------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "caseHistoryDiv")))
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bckbtn"]')))
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            l = []
                            table_case = soup.find('table', attrs={'class': 'case_details_table'})
                            table_case_body = table_case.find('tbody')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values

                            b = l[:len(l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                            b1 = l[-1][1].replace('\xa0\xa0 (Note the CNR number for future reference)', '')
                            # <-- for remove last "(Note the CNR number for future reference)" text o/p-- "MHPU050000132021"

                            lst = reduce(lambda x, y: x + y, b)
                            lst.append('cnrnumber')  # < ---- add 'CNR' name
                            lst.append(b1)  # < ---- add 'CNR number
                            res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except Exception as e:
                            print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {"ProgramError": str(e)}

                        try:
                            # <---2nd Table, take single table data, into list ------------>
                            l2 = []
                            table = soup.find('table', attrs={'class': 'table_r'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l2.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst2 = reduce(lambda x, y: x + y, l2)
                            res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in range(0, len(lst2), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct2 = {}

                        try:
                            # <---3rd Table, take single table data, into list ----------->
                            l3 = []
                            table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l3.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst3 = reduce(lambda x, y: x + y, l3)
                            listToStr = ' '.join(map(str, lst3))
                            res_dct3 = listToStr.replace('\xa0', '')  # <-- make kye 'LOWER case', remove 'space' ---
                        except:
                            res_dct3 = ''

                        try:
                            # <---4th Table, take single table data, into list ---------->
                            l4 = []
                            table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l4.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst4 = reduce(lambda x, y: x + y, l4)
                            listToStr2 = ' '.join(map(str, lst4))
                            res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct4 = ''

                        try:
                            # <---5th Table, take single table data, into list ---------->
                            table_a = soup.find('table', attrs={'class': 'acts_table'})
                            h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                            res_dct5 = [dict(zip(h, i)) for i in d]

                            k5 = []
                            for d in res_dct5:
                                k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                    'UnderSection(s)', 'Section'): v for k, v in d.items()})
                        except:
                            k5 = []

                        try:
                            # <---6th Table, take single table data, into list ---------->
                            table_t = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                            res_dct6 = [dict(zip(h, i)) for i in d]

                            k6 = []
                            for d in res_dct6:
                                k6.append(
                                    {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                     d.items()})
                        except:
                            k6 = []

                        try:
                            # <---7th Table, take single table data, into list ---------->
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]

                            k7 = []
                            for d7 in res_dct7:
                                k7.append(
                                    {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                        except:
                            k7 = []

                        try:
                            # <---8th Table, take single table data, into list ---------->
                            table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                            h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                            res_dct8 = [dict(zip(h1, x)) for x in q]

                            k8 = []
                            for d in res_dct8:
                                k8.append({k.replace(' ', ''): v for k, v in d.items()})
                        except:
                            k8 = []

                        main_ch2 = 1

                        if res_dct != {}:
                            case_final = {"CaseDetails": res_dct, "CaseStatus": res_dct2,
                                          "PetitionerAdvocate": res_dct3,
                                          "RespondentAdvocate": res_dct4, "Acts": k5,
                                          "History": k6, "Orders": k7, "CaseTransferDetails": k8}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {"CaseDetailsNotFound": 'Please try again.'}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.close()
                                break
                            except:
                                pass
                except Exception as e:
                    try:
                        driver.close()
                        # print('except crser here--', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {"ProgramError": str(e)}
                    except:
                        pass

                if main_ch2 == 1:
                    break

            if res_dct != {}:
                yourdata = case_final

            elif res_dct == {}:
                yourdata = case_final_2

            data = yourdata

            try:
                conn = psycopg2.connect(database="defaultdb", user="doadmin", password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")

                cursor = conn.cursor()
                # print('psycopg2 connect execute--')

                var_dict = {"var": 0, "data": 1}
                # <-- for write --
                with open('Scrap_App/files/case_3.txt', 'w') as json_file:
                    json.dump(var_dict, json_file)

            except (Exception, psycopg2.DatabaseError) as error:
                # print("Error while creating PostgreSQL table", error)
                pass
            else:

                cursor.execute('SELECT * FROM public."Scrap_App_case_3" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_case_3" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                # print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------//Case function 1 End //--------------------------------------------------

        # multiprocessing.Process(target=Case_f1).start()
        threading.Thread(target=Case_3_fun).start()
        # print('thred--')

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            # wait = Case_3.objects.all().order_by('-id')[0].data

            # <-- for read ---
            with open('Scrap_App/files/case_3.txt', 'r') as f:
                _dict = json.load(f)
                # print(_dict['var'])  # 0
            # print('wait data:- ', _dict['data'])
            time.sleep(5)
            wait = _dict['data']  # 0

            # print('wait ON-')
            # print('wait OFF-')
            if wait == 0:
                w = 0
            else:
                w = 1
            if w == 1:
                # print('out while..')
                break
        # ------// --- //----
        time.sleep(3)

        data = Case_3.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        c_id = Case_3.objects.all().order_by('-id')[0].id
        c_id2 = Case_3.objects.get(id=c_id)
        c_id2.var = 0  # for make 1 to 0
        c_id2.save()

        finish = time.perf_counter()
        print(f'Case_3 Finished in {round(finish - start, 2)} second(s)')
# ------------------(cwork3e)----------------------

    elif Case_4.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()

        var_dict = {"var": 1, "data": 0}
        # <-- for write --
        with open('Scrap_App/files/case_4.txt', 'w') as json_file:
            json.dump(var_dict, json_file)

        Case_4(var=1, time=time.time(), state=state, dist=dist, complex=complex, casetype=casetype, caseno=caseno,
               year=year).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        #print('Case_4 input data:--- ', state, dist, complex, casetype, caseno, year)

        state_data = Case_4.objects.all().order_by('-id')[0].state
        dist_data = Case_4.objects.all().order_by('-id')[0].dist
        complex_data = Case_4.objects.all().order_by('-id')[0].complex
        casetype_data = Case_4.objects.all().order_by('-id')[0].casetype
        caseno_data = Case_4.objects.all().order_by('-id')[0].caseno
        year_data = Case_4.objects.all().order_by('-id')[0].year

        # ------------------(cwork3)----------------------

        def Case_4_fun():
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")
            global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            # driver.maximize_window()

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')

            #print('Case_4_fun')

            # <--- all repeated Functions ----/////---
            def dynamicdelay(id_name):
                # print('dynamicdelay:-', id_name)
                global job_profiles
                loop_1 = 1
                while True:
                    L = 0
                    if loop_1 == 1:
                        # print('D while:-')
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        time.sleep(1)
                        all_divs = soup.find('select', {'id': str(id_name)})
                        job_profiles = all_divs.find_all('option')
                        # print('job_profiles:-', job_profiles)
                        loop_1 = len(job_profiles)
                        # print('--', loop_1)
                        L = 0
                    else:
                        L = 1

                    if L == 1:
                        # print('D break:-')
                        break

                return job_profiles

            def delay1():
                time.sleep(random.randint(1, 2))  # 1sec

            def delay():
                time.sleep(random.randint(2, 2))  # 2sec

            # ---/ op / ---
            state_ = state_data
            dist_ = dist_data
            complex_ = complex_data
            casetype_ = casetype_data
            caseno_ = caseno_data
            year_ = year_data
            # ----------------// our code //-----------------

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "menuPage")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="menuPage"]/div/div[1]/ul/li[2]')))  # wait
                    driver.find_elements_by_xpath('//*[@id="menuPage"]/div/div[1]/ul/li[2]')[0].click()
                    # delay1()
                    # for alert OK btn
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "bs_alert")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="bs_alert"]/div/div/div[2]/button')))  # wait
                    driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                    # delay1()

                    # <--------STATE--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "divLangState")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_state_code'})
                    state_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='sess_state_code')
                    # print('job_profiles:-', job_profiles)

                    k = [str(x).split('"')[1] for x in job_profiles]  # <-- output  ['0', '28', '2', '6',..]
                    # print('k:-', k)

                    # <---- take input state value from fun() & find index of that input value ----
                    index = k.index(state_)
                    state_option = index + 1  # op-- 20th actual is 21th

                    # <------ Click on 'STATE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_state_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'STATE' Options ---->
                    state = driver.find_elements_by_xpath('//*[@id="sess_state_code"]/option[{}]'.format(state_option))[
                        0]  # option 1
                    # print('selected state:-- ', state.text)
                    state.click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    # <--------STATE---///--End----///--------->

                    # <--------DISTRICT--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "sess_dist_code")))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_dist_code'})
                    dist_job_profiles = all_divs.find_all('option')
                    # print('dist_job_profiles', dist_job_profiles)'''

                    dynamicdelay(id_name='sess_dist_code')

                    k2 = [str(j).split('"')[1] for j in job_profiles]
                    # print('k2:-', k2)

                    # <---- take input state value from fun() & find index of that input value ----
                    index2 = k2.index(dist_)
                    dist_option = index2 + 1  # op-- 20th actual is 21th

                    # <------ Click on 'DISTRICT' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'DISTRICT' Options ---->
                    district = \
                        driver.find_elements_by_xpath('//*[@id="sess_dist_code"]/option[{}]'.format(dist_option))[0]
                    # option 2
                    # print('selected dist:-- ', district.text)
                    district.click()
                    # delay()
                    # <--------DISTRICT---///--End----///--------->

                    # <--------COMPLEX--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "court_complex_code")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    complex_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='court_complex_code')

                    k3 = [str(n).split('"')[1] for n in job_profiles]
                    # print('k3:- ', k3, 'len(k3:-)', len(k3), 'len(complex_job_profiles:-)', len(job_profiles))

                    # <---- take input complex_ value from fun()
                    com = complex_.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait

                    # <------ Select 'COMPLEX' Options ---->
                    complex = \
                        driver.find_elements_by_xpath(
                            '//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    # option 2
                    # print('selected complex: ', complex.text)
                    complex.click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    # <--------COMPLEX---///--End----///--------->

                    # <--------CLICK CASE NO. BTN--///------>
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CScaseNumber")))  # <---- wait --->
                    driver.find_elements_by_xpath('//*[@id="CScaseNumber"]')[0].click()
                    # delay()
                    # delay()

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    case_job_profiles = all_divs.find_all('option')
                    # print('case_job_profiles', case_job_profiles)'''

                    dynamicdelay(id_name='case_type')

                    k4 = [str(n1).split('"')[1] for n1 in job_profiles]
                    # print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(case_job_profiles:-)', len(job_profiles))

                    case = casetype_.replace('_', '^')
                    index4 = k4.index(str(case))
                    case_option = index4 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[0]
                    # print('selected case: ', case.text)
                    case.click()
                    # delay1()
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(caseno_)  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(year_)  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # Find_data----- START --
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_class_name('refresh-btn').click()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        # delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        driver.execute_script("document.body.style.zoom='170%'")
                        driver.execute_script("window.scrollTo(0,400);")
                        delay1()
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        # start2 = time.perf_counter()
                        driver.find_element_by_xpath("//*[@id='captcha_image']")

                        # # --------------(for SHOW chrome)--------
                        # location = {'x': 219, 'y': 380}
                        # size = {'height': 59, 'width': 310}

                        # --------------(for hide windo)--------
                        location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        # ----------------------------------------

                        driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_4.png')
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_4.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_4.png')
                        # im.show()  # <--- comment it ----

                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_4.png')
                        width, height = im2.size

                        # # --------------(for SHOW chrome)--------
                        # left = 0
                        # top = 0  # done
                        # right = 175
                        # bottom = 57  # done
                        # # ----------------------

                        # --------------(for hide windo)--------
                        left = 30  # ... run with option
                        top = 0  # done
                        right = 220  # done
                        bottom = 57  # done
                        # ----------------------

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_4.png')  # row img
                        # im1.show()
                        # >-----------------------

                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_4.png')

                        # <-------- improve image quality ----------->
                        # img.save("quality_abc_2.png", quality=200)
                        contrast = ImageEnhance.Contrast(img)
                        contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_4.png')

                        # <----- for remove unwanted color from image ---------->
                        image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_4.png')
                        grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                        lower_range = np.array([0, 0, 0])
                        upper_range = np.array([0, 0, 0])

                        mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_4.png', mask)
                        # <-----Done remove unwanted color from image ---------->

                        # <------ image color change in black n white ------
                        Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_4.png', 0)
                        ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_4.png', thresh_img)
                        cv2.waitKey(0)
                        cv2.destroyAllWindows()

                        # <----- for expand image text ---------------------
                        Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_4.png', 0)
                        kernel = np.ones((3, 3), np.uint8)
                        erosion = cv2.erode(Load2, kernel, iterations=1)
                        img = erosion.copy()
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_4.png', img)

                        # <----- for Captcha image to text convert---------------------
                        img2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_4.png')
                        captcha_txt2 = tess.image_to_string(img2, config='')
                        # finish2 = time.perf_counter()
                        # print(f'OLD CODE captcha reading Finished in {round(finish2 - start2, 2)} second(s)')

                        # <------- Find_data--------/// END ///---------------------------

                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 8:
                                # print('!=8 --')

                                # ------------------------------
                                driver.find_element_by_class_name('refresh-btn').click()
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                # delay1()
                                WebDriverWait(driver, 600).until(
                                    EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                driver.execute_script("document.body.style.zoom='170%'")
                                driver.execute_script("window.scrollTo(0,400);")
                                delay1()
                                # ----------------------
                                # <--- to find captcha image location ---->
                                driver.find_element_by_xpath("//*[@id='captcha_image']")
                                # start3 = time.perf_counter()

                                # # --------------(for SHOW chrome)--------
                                # location = {'x': 219, 'y': 380}
                                # size = {'height': 59, 'width': 310}

                                # --------------(for hide windo)--------
                                location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                # ----------------------------------------

                                driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_4.png')
                                # driver.get_screenshot_as_file(settings.MEDIA_ROOT + '/CaseImages/imageCASE_4.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_4.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_4.png')
                                # im.show()  # <--- comment it ----
                                # <--------

                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_4.png')
                                width, height = im2.size

                                # # --------------(for SHOW chrome)--------
                                # left = 0
                                # top = 0  # done
                                # right = 175
                                # bottom = 57  # done
                                # # ----------------------

                                # --------------(for hide windo)--------
                                left = 30  # ... run with option
                                top = 0  # done
                                right = 220  # done
                                bottom = 57  # done
                                # ----------------------

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/ab4.png')
                                # im1.show()
                                # >-----------------------

                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                # delay()

                                # for Captcha image to text convert---------------comment it------------
                                img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_4.png')

                                # <-------- improve image quality ----------->
                                contrast = ImageEnhance.Contrast(img)
                                contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_4.png')

                                # <----- for remove unwanted color from image ---------->
                                image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_4.png')
                                grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                                grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                                lower_range = np.array([0, 0, 0])
                                upper_range = np.array([0, 0, 0])

                                mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_4.png', mask)
                                # <-----Done remove unwanted color from image ---------->

                                # <------ image color change in black n white ------
                                Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_4.png', 0)
                                ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_4.png', thresh_img)
                                cv2.waitKey(0)
                                cv2.destroyAllWindows()

                                # <----- for expand image text ---------------------
                                Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_4.png', 0)
                                kernel = np.ones((3, 3), np.uint8)
                                erosion = cv2.erode(Load2, kernel, iterations=1)
                                img = erosion.copy()
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_4.png', img)

                                # <----- for Captcha image to text convert---------------------
                                img3 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_4.png')
                                re_captcha_txt = tess.image_to_string(img3, config='')
                                # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)

                                # finish3 = time.perf_counter()
                                # print(f'captcha reading Finished in {round(finish3 - start3, 2)} second(s)')

                                captcha_txt2 = re_captcha_txt
                                if len(captcha_txt2) != 8:
                                    # print('if len != 8--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                    # print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                # print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                # print('finaly break done in <6 or >6--')
                                break

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                        input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        # <----- for Click Search btn---------------------

                        # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                        driver.find_element_by_xpath('//*[@id="goResetDiv"]/input[1]').click()  # <--- sertch btn --->

                        # print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        delay()
                        delay()

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display: none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        # html = driver.page_source
                        # soup = BeautifulSoup(html, "html.parser")
                        op_page_bs_alertDiv = soup.find('div', {'id': 'bs_alert'}).get('style')
                        # -----For Invalid captcha page("display: block;")
                        # print('op_page_bs_alertDiv--- ', op_page_bs_alertDiv)
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '':
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                delay()
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                # print('check2--r')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.close()
                                refresh = 1

                        elif op_page_bs_alertDiv == "display: block;":
                            # print('alert 1 ------')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                            refresh = 0

                        elif op_page_bs_alertDiv == "display: block; padding-right: 15px;":
                            # print('alert 2 ---Enter year from 1901 to---')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()

                            res_dct = {}
                            case_final_2 = {'EnterYearFrom1901': 'Enter year from 1901'}
                            Record_not_found = 1
                            driver.close()
                            refresh = 1

                        else:
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            break
                    if Record_not_found == 1:
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        WebDriverWait(driver, 600).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # <---- wait --->
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_showListDiv = soup.find('div', {'id': 'showList'}).get('style')
                        # -----For Invalid captcha page ("display:none;")

                        driver.execute_script("window.scrollTo(0,300);")
                        time.sleep(1)
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # ----------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "caseHistoryDiv")))
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bckbtn"]')))
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            l = []
                            table_case = soup.find('table', attrs={'class': 'case_details_table'})
                            table_case_body = table_case.find('tbody')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values

                            b = l[:len(l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                            b1 = l[-1][1].replace('\xa0\xa0 (Note the CNR number for future reference)', '')
                            # <-- for remove last "(Note the CNR number for future reference)" text o/p-- "MHPU050000132021"

                            lst = reduce(lambda x, y: x + y, b)
                            lst.append('cnrnumber')  # < ---- add 'CNR' name
                            lst.append(b1)  # < ---- add 'CNR number
                            res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except Exception as e:
                            print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {"ProgramError": str(e)}

                        try:
                            # <---2nd Table, take single table data, into list ------------>
                            l2 = []
                            table = soup.find('table', attrs={'class': 'table_r'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l2.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst2 = reduce(lambda x, y: x + y, l2)
                            res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in range(0, len(lst2), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct2 = {}

                        try:
                            # <---3rd Table, take single table data, into list ----------->
                            l3 = []
                            table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l3.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst3 = reduce(lambda x, y: x + y, l3)
                            listToStr = ' '.join(map(str, lst3))
                            res_dct3 = listToStr.replace('\xa0', '')  # <-- make kye 'LOWER case', remove 'space' ---
                        except:
                            res_dct3 = ''

                        try:
                            # <---4th Table, take single table data, into list ---------->
                            l4 = []
                            table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l4.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst4 = reduce(lambda x, y: x + y, l4)
                            listToStr2 = ' '.join(map(str, lst4))
                            res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct4 = ''

                        try:
                            # <---5th Table, take single table data, into list ---------->
                            table_a = soup.find('table', attrs={'class': 'acts_table'})
                            h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                            res_dct5 = [dict(zip(h, i)) for i in d]

                            k5 = []
                            for d in res_dct5:
                                k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                    'UnderSection(s)', 'Section'): v for k, v in d.items()})
                        except:
                            k5 = []

                        try:
                            # <---6th Table, take single table data, into list ---------->
                            table_t = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                            res_dct6 = [dict(zip(h, i)) for i in d]

                            k6 = []
                            for d in res_dct6:
                                k6.append(
                                    {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                     d.items()})
                        except:
                            k6 = []

                        try:
                            # <---7th Table, take single table data, into list ---------->
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]

                            k7 = []
                            for d7 in res_dct7:
                                k7.append(
                                    {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                        except:
                            k7 = []

                        try:
                            # <---8th Table, take single table data, into list ---------->
                            table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                            h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                            res_dct8 = [dict(zip(h1, x)) for x in q]

                            k8 = []
                            for d in res_dct8:
                                k8.append({k.replace(' ', ''): v for k, v in d.items()})
                        except:
                            k8 = []

                        main_ch2 = 1

                        if res_dct != {}:
                            case_final = {"CaseDetails": res_dct, "CaseStatus": res_dct2,
                                          "PetitionerAdvocate": res_dct3,
                                          "RespondentAdvocate": res_dct4, "Acts": k5,
                                          "History": k6, "Orders": k7, "CaseTransferDetails": k8}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {"CaseDetailsNotFound": 'Please try again.'}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.close()
                                break
                            except:
                                pass
                except Exception as e:
                    try:
                        driver.close()
                        # print('except crser here--', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {"ProgramError": str(e)}
                    except:
                        pass

                if main_ch2 == 1:
                    break

            if res_dct != {}:
                yourdata = case_final

            elif res_dct == {}:
                yourdata = case_final_2

            data = yourdata

            try:
                conn = psycopg2.connect(database="defaultdb", user="doadmin", password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")

                cursor = conn.cursor()
                # print('psycopg2 connect execute--')

                var_dict = {"var": 0, "data": 1}
                # <-- for write --
                with open('Scrap_App/files/case_4.txt', 'w') as json_file:
                    json.dump(var_dict, json_file)

            except (Exception, psycopg2.DatabaseError) as error:
                # print("Error while creating PostgreSQL table", error)
                pass
            else:

                cursor.execute('SELECT * FROM public."Scrap_App_case_4" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_case_4" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                # print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------//Case function 1 End //--------------------------------------------------

        # multiprocessing.Process(target=Case_f1).start()
        threading.Thread(target=Case_4_fun).start()
        # print('thred--')

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            # wait = Case_4.objects.all().order_by('-id')[0].data

            # <-- for read ---
            with open('Scrap_App/files/case_4.txt', 'r') as f:
                _dict = json.load(f)
                # print(_dict['var'])  # 0
            # print('wait data:- ', _dict['data'])
            time.sleep(5)
            wait = _dict['data']  # 0

            # print('wait ON-')
            # print('wait OFF-')
            if wait == 0:
                w = 0
            else:
                w = 1
            if w == 1:
                # print('out while..')
                break
        # ------// --- //----
        time.sleep(3)

        data = Case_4.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        c_id = Case_4.objects.all().order_by('-id')[0].id
        c_id2 = Case_4.objects.get(id=c_id)
        c_id2.var = 0  # for make 1 to 0
        c_id2.save()

        # finish = time.perf_counter()
        # print(f'Case_4 Finished in {round(finish - start, 2)} second(s)')
    # ------------------(cwork4e)----------------------

    elif Case_5.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()

        var_dict = {"var": 1, "data": 0}
        # <-- for write --
        with open('Scrap_App/files/case_5.txt', 'w') as json_file:
            json.dump(var_dict, json_file)

        Case_5(var=1, time=time.time(), state=state, dist=dist, complex=complex, casetype=casetype, caseno=caseno,
               year=year).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        #print('Case_5 input data:--- ', state, dist, complex, casetype, caseno, year)

        state_data = Case_5.objects.all().order_by('-id')[0].state
        dist_data = Case_5.objects.all().order_by('-id')[0].dist
        complex_data = Case_5.objects.all().order_by('-id')[0].complex
        casetype_data = Case_5.objects.all().order_by('-id')[0].casetype
        caseno_data = Case_5.objects.all().order_by('-id')[0].caseno
        year_data = Case_5.objects.all().order_by('-id')[0].year

        # ------------------(cwork5)----------------------

        def Case_5_fun():
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")
            global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            # driver.maximize_window()

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')

            #print('Case_5_fun')

            # <--- all repeated Functions ----/////---
            def dynamicdelay(id_name):
                # print('dynamicdelay:-', id_name)
                global job_profiles
                loop_1 = 1
                while True:
                    L = 0
                    if loop_1 == 1:
                        # print('D while:-')
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        time.sleep(1)
                        all_divs = soup.find('select', {'id': str(id_name)})
                        job_profiles = all_divs.find_all('option')
                        # print('job_profiles:-', job_profiles)
                        loop_1 = len(job_profiles)
                        # print('--', loop_1)
                        L = 0
                    else:
                        L = 1

                    if L == 1:
                        # print('D break:-')
                        break

                return job_profiles

            def delay1():
                time.sleep(random.randint(1, 2))  # 1sec

            def delay():
                time.sleep(random.randint(2, 2))  # 2sec

            # ---/ op / ---
            state_ = state_data
            dist_ = dist_data
            complex_ = complex_data
            casetype_ = casetype_data
            caseno_ = caseno_data
            year_ = year_data
            # ----------------// our code //-----------------

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "menuPage")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="menuPage"]/div/div[1]/ul/li[2]')))  # wait
                    driver.find_elements_by_xpath('//*[@id="menuPage"]/div/div[1]/ul/li[2]')[0].click()
                    # delay1()
                    # for alert OK btn
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "bs_alert")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="bs_alert"]/div/div/div[2]/button')))  # wait
                    driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                    # delay1()

                    # <--------STATE--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "divLangState")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_state_code'})
                    state_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='sess_state_code')
                    # print('job_profiles:-', job_profiles)

                    k = [str(x).split('"')[1] for x in job_profiles]  # <-- output  ['0', '28', '2', '6',..]
                    # print('k:-', k)

                    # <---- take input state value from fun() & find index of that input value ----
                    index = k.index(state_)
                    state_option = index + 1  # op-- 20th actual is 21th

                    # <------ Click on 'STATE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_state_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'STATE' Options ---->
                    state = driver.find_elements_by_xpath('//*[@id="sess_state_code"]/option[{}]'.format(state_option))[
                        0]  # option 1
                    # print('selected state:-- ', state.text)
                    state.click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    # <--------STATE---///--End----///--------->

                    # <--------DISTRICT--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "sess_dist_code")))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_dist_code'})
                    dist_job_profiles = all_divs.find_all('option')
                    # print('dist_job_profiles', dist_job_profiles)'''

                    dynamicdelay(id_name='sess_dist_code')

                    k2 = [str(j).split('"')[1] for j in job_profiles]
                    # print('k2:-', k2)

                    # <---- take input state value from fun() & find index of that input value ----
                    index2 = k2.index(dist_)
                    dist_option = index2 + 1  # op-- 20th actual is 21th

                    # <------ Click on 'DISTRICT' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'DISTRICT' Options ---->
                    district = \
                        driver.find_elements_by_xpath('//*[@id="sess_dist_code"]/option[{}]'.format(dist_option))[0]
                    # option 2
                    # print('selected dist:-- ', district.text)
                    district.click()
                    # delay()
                    # <--------DISTRICT---///--End----///--------->

                    # <--------COMPLEX--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "court_complex_code")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    complex_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='court_complex_code')

                    k3 = [str(n).split('"')[1] for n in job_profiles]
                    # print('k3:- ', k3, 'len(k3:-)', len(k3), 'len(complex_job_profiles:-)', len(job_profiles))

                    # <---- take input complex_ value from fun()
                    com = complex_.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait

                    # <------ Select 'COMPLEX' Options ---->
                    complex = \
                        driver.find_elements_by_xpath(
                            '//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    # option 2
                    # print('selected complex: ', complex.text)
                    complex.click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    # <--------COMPLEX---///--End----///--------->

                    # <--------CLICK CASE NO. BTN--///------>
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CScaseNumber")))  # <---- wait --->
                    driver.find_elements_by_xpath('//*[@id="CScaseNumber"]')[0].click()
                    # delay()
                    # delay()

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    case_job_profiles = all_divs.find_all('option')
                    # print('case_job_profiles', case_job_profiles)'''

                    dynamicdelay(id_name='case_type')

                    k4 = [str(n1).split('"')[1] for n1 in job_profiles]
                    # print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(case_job_profiles:-)', len(job_profiles))

                    case = casetype_.replace('_', '^')
                    index4 = k4.index(str(case))
                    case_option = index4 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[0]
                    # print('selected case: ', case.text)
                    case.click()
                    # delay1()
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(caseno_)  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(year_)  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # Find_data----- START --
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_class_name('refresh-btn').click()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        # delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        driver.execute_script("document.body.style.zoom='170%'")
                        driver.execute_script("window.scrollTo(0,400);")
                        delay1()
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        # start2 = time.perf_counter()
                        driver.find_element_by_xpath("//*[@id='captcha_image']")

                        # # --------------(for SHOW chrome)--------
                        # location = {'x': 219, 'y': 380}
                        # size = {'height': 59, 'width': 310}

                        # --------------(for hide windo)--------
                        location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        # ----------------------------------------

                        driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_5.png')
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_5.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_5.png')
                        # im.show()  # <--- comment it ----

                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_5.png')
                        width, height = im2.size

                        # # --------------(for SHOW chrome)--------
                        # left = 0
                        # top = 0  # done
                        # right = 175
                        # bottom = 57  # done
                        # # ----------------------

                        # --------------(for hide windo)--------
                        left = 30  # ... run with option
                        top = 0  # done
                        right = 220  # done
                        bottom = 57  # done
                        # ----------------------

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_5.png')  # row img
                        # im1.show()
                        # >-----------------------

                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_5.png')

                        # <-------- improve image quality ----------->
                        # img.save("quality_abc_2.png", quality=200)
                        contrast = ImageEnhance.Contrast(img)
                        contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_5.png')

                        # <----- for remove unwanted color from image ---------->
                        image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_5.png')
                        grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                        lower_range = np.array([0, 0, 0])
                        upper_range = np.array([0, 0, 0])

                        mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_5.png', mask)
                        # <-----Done remove unwanted color from image ---------->

                        # <------ image color change in black n white ------
                        Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_5.png', 0)
                        ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_5.png', thresh_img)
                        cv2.waitKey(0)
                        cv2.destroyAllWindows()

                        # <----- for expand image text ---------------------
                        Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_5.png', 0)
                        kernel = np.ones((3, 3), np.uint8)
                        erosion = cv2.erode(Load2, kernel, iterations=1)
                        img = erosion.copy()
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_5.png', img)

                        # <----- for Captcha image to text convert---------------------
                        img2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_5.png')
                        captcha_txt2 = tess.image_to_string(img2, config='')
                        # finish2 = time.perf_counter()
                        # print(f'OLD CODE captcha reading Finished in {round(finish2 - start2, 2)} second(s)')

                        # <------- Find_data--------/// END ///---------------------------

                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 8:
                                # print('!=8 --')

                                # ------------------------------
                                driver.find_element_by_class_name('refresh-btn').click()
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                # delay1()
                                WebDriverWait(driver, 600).until(
                                    EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                driver.execute_script("document.body.style.zoom='170%'")
                                driver.execute_script("window.scrollTo(0,400);")
                                delay1()
                                # ----------------------
                                # <--- to find captcha image location ---->
                                driver.find_element_by_xpath("//*[@id='captcha_image']")
                                # start3 = time.perf_counter()

                                # # --------------(for SHOW chrome)--------
                                # location = {'x': 219, 'y': 380}
                                # size = {'height': 59, 'width': 310}

                                # --------------(for hide windo)--------
                                location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                # ----------------------------------------

                                driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_5.png')
                                # driver.get_screenshot_as_file(settings.MEDIA_ROOT + '/CaseImages/imageCASE_5.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_5.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_5.png')
                                # im.show()  # <--- comment it ----
                                # <--------

                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_5.png')
                                width, height = im2.size

                                # # --------------(for SHOW chrome)--------
                                # left = 0
                                # top = 0  # done
                                # right = 175
                                # bottom = 57  # done
                                # # ----------------------

                                # --------------(for hide windo)--------
                                left = 30  # ... run with option
                                top = 0  # done
                                right = 220  # done
                                bottom = 57  # done
                                # ----------------------

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/ab4.png')
                                # im1.show()
                                # >-----------------------

                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                # delay()

                                # for Captcha image to text convert---------------comment it------------
                                img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_5.png')

                                # <-------- improve image quality ----------->
                                contrast = ImageEnhance.Contrast(img)
                                contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_5.png')

                                # <----- for remove unwanted color from image ---------->
                                image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_5.png')
                                grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                                grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                                lower_range = np.array([0, 0, 0])
                                upper_range = np.array([0, 0, 0])

                                mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_5.png', mask)
                                # <-----Done remove unwanted color from image ---------->

                                # <------ image color change in black n white ------
                                Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_5.png', 0)
                                ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_5.png', thresh_img)
                                cv2.waitKey(0)
                                cv2.destroyAllWindows()

                                # <----- for expand image text ---------------------
                                Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_5.png', 0)
                                kernel = np.ones((3, 3), np.uint8)
                                erosion = cv2.erode(Load2, kernel, iterations=1)
                                img = erosion.copy()
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_5.png', img)

                                # <----- for Captcha image to text convert---------------------
                                img3 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_5.png')
                                re_captcha_txt = tess.image_to_string(img3, config='')
                                # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)

                                # finish3 = time.perf_counter()
                                # print(f'captcha reading Finished in {round(finish3 - start3, 2)} second(s)')

                                captcha_txt2 = re_captcha_txt
                                if len(captcha_txt2) != 8:
                                    # print('if len != 8--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                    # print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                # print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                # print('finaly break done in <6 or >6--')
                                break

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                        input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        # <----- for Click Search btn---------------------

                        # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                        driver.find_element_by_xpath('//*[@id="goResetDiv"]/input[1]').click()  # <--- sertch btn --->

                        # print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        delay()
                        delay()

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display: none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        # html = driver.page_source
                        # soup = BeautifulSoup(html, "html.parser")
                        op_page_bs_alertDiv = soup.find('div', {'id': 'bs_alert'}).get('style')
                        # -----For Invalid captcha page("display: block;")
                        # print('op_page_bs_alertDiv--- ', op_page_bs_alertDiv)
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '':
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                delay()
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                # print('check2--r')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.close()
                                refresh = 1

                        elif op_page_bs_alertDiv == "display: block;":
                            # print('alert 1 ------')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                            refresh = 0

                        elif op_page_bs_alertDiv == "display: block; padding-right: 15px;":
                            # print('alert 2 ---Enter year from 1901 to---')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()

                            res_dct = {}
                            case_final_2 = {'EnterYearFrom1901': 'Enter year from 1901'}
                            Record_not_found = 1
                            driver.close()
                            refresh = 1

                        else:
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            break
                    if Record_not_found == 1:
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        WebDriverWait(driver, 600).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # <---- wait --->
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_showListDiv = soup.find('div', {'id': 'showList'}).get('style')
                        # -----For Invalid captcha page ("display:none;")

                        driver.execute_script("window.scrollTo(0,300);")
                        time.sleep(1)
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # ----------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "caseHistoryDiv")))
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bckbtn"]')))
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            l = []
                            table_case = soup.find('table', attrs={'class': 'case_details_table'})
                            table_case_body = table_case.find('tbody')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values

                            b = l[:len(l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                            b1 = l[-1][1].replace('\xa0\xa0 (Note the CNR number for future reference)', '')
                            # <-- for remove last "(Note the CNR number for future reference)" text o/p-- "MHPU050000132021"

                            lst = reduce(lambda x, y: x + y, b)
                            lst.append('cnrnumber')  # < ---- add 'CNR' name
                            lst.append(b1)  # < ---- add 'CNR number
                            res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except Exception as e:
                            print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {"ProgramError": str(e)}

                        try:
                            # <---2nd Table, take single table data, into list ------------>
                            l2 = []
                            table = soup.find('table', attrs={'class': 'table_r'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l2.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst2 = reduce(lambda x, y: x + y, l2)
                            res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in range(0, len(lst2), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct2 = {}

                        try:
                            # <---3rd Table, take single table data, into list ----------->
                            l3 = []
                            table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l3.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst3 = reduce(lambda x, y: x + y, l3)
                            listToStr = ' '.join(map(str, lst3))
                            res_dct3 = listToStr.replace('\xa0', '')  # <-- make kye 'LOWER case', remove 'space' ---
                        except:
                            res_dct3 = ''

                        try:
                            # <---4th Table, take single table data, into list ---------->
                            l4 = []
                            table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l4.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst4 = reduce(lambda x, y: x + y, l4)
                            listToStr2 = ' '.join(map(str, lst4))
                            res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct4 = ''

                        try:
                            # <---5th Table, take single table data, into list ---------->
                            table_a = soup.find('table', attrs={'class': 'acts_table'})
                            h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                            res_dct5 = [dict(zip(h, i)) for i in d]

                            k5 = []
                            for d in res_dct5:
                                k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                    'UnderSection(s)', 'Section'): v for k, v in d.items()})
                        except:
                            k5 = []

                        try:
                            # <---6th Table, take single table data, into list ---------->
                            table_t = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                            res_dct6 = [dict(zip(h, i)) for i in d]

                            k6 = []
                            for d in res_dct6:
                                k6.append(
                                    {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                     d.items()})
                        except:
                            k6 = []

                        try:
                            # <---7th Table, take single table data, into list ---------->
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]

                            k7 = []
                            for d7 in res_dct7:
                                k7.append(
                                    {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                        except:
                            k7 = []

                        try:
                            # <---8th Table, take single table data, into list ---------->
                            table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                            h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                            res_dct8 = [dict(zip(h1, x)) for x in q]

                            k8 = []
                            for d in res_dct8:
                                k8.append({k.replace(' ', ''): v for k, v in d.items()})
                        except:
                            k8 = []

                        main_ch2 = 1

                        if res_dct != {}:
                            case_final = {"CaseDetails": res_dct, "CaseStatus": res_dct2,
                                          "PetitionerAdvocate": res_dct3,
                                          "RespondentAdvocate": res_dct4, "Acts": k5,
                                          "History": k6, "Orders": k7, "CaseTransferDetails": k8}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {"CaseDetailsNotFound": 'Please try again.'}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.close()
                                break
                            except:
                                pass
                except Exception as e:
                    try:
                        driver.close()
                        # print('except crser here--', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {"ProgramError": str(e)}
                    except:
                        pass

                if main_ch2 == 1:
                    break

            if res_dct != {}:
                yourdata = case_final

            elif res_dct == {}:
                yourdata = case_final_2

            data = yourdata

            try:
                conn = psycopg2.connect(database="defaultdb", user="doadmin", password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")

                cursor = conn.cursor()
                # print('psycopg2 connect execute--')

                var_dict = {"var": 0, "data": 1}
                # <-- for write --
                with open('Scrap_App/files/case_5.txt', 'w') as json_file:
                    json.dump(var_dict, json_file)

            except (Exception, psycopg2.DatabaseError) as error:
                # print("Error while creating PostgreSQL table", error)
                pass
            else:

                cursor.execute('SELECT * FROM public."Scrap_App_case_5" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_case_5" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                # print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------//Case function 1 End //--------------------------------------------------

        # multiprocessing.Process(target=Case_f1).start()
        threading.Thread(target=Case_5_fun).start()
        # print('thred--')

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            # wait = Case_5.objects.all().order_by('-id')[0].data

            # <-- for read ---
            with open('Scrap_App/files/case_5.txt', 'r') as f:
                _dict = json.load(f)
                # print(_dict['var'])  # 0
            # print('wait data:- ', _dict['data'])
            time.sleep(5)
            wait = _dict['data']  # 0

            # print('wait ON-')
            # print('wait OFF-')
            if wait == 0:
                w = 0
            else:
                w = 1
            if w == 1:
                # print('out while..')
                break
        # ------// --- //----
        time.sleep(3)

        data = Case_5.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        c_id = Case_5.objects.all().order_by('-id')[0].id
        c_id2 = Case_5.objects.get(id=c_id)
        c_id2.var = 0  # for make 1 to 0
        c_id2.save()

        # finish = time.perf_counter()
        # print(f'Case_5 Finished in {round(finish - start, 2)} second(s)')
# ------------------(cwork5e)----------------------

    elif Case_6.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()

        var_dict = {"var": 1, "data": 0}
        # <-- for write --
        with open('Scrap_App/files/case_6.txt', 'w') as json_file:
            json.dump(var_dict, json_file)

        Case_6(var=1, time=time.time(), state=state, dist=dist, complex=complex, casetype=casetype, caseno=caseno,
               year=year).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        #print('Case_6 input data:--- ', state, dist, complex, casetype, caseno, year)

        state_data = Case_6.objects.all().order_by('-id')[0].state
        dist_data = Case_6.objects.all().order_by('-id')[0].dist
        complex_data = Case_6.objects.all().order_by('-id')[0].complex
        casetype_data = Case_6.objects.all().order_by('-id')[0].casetype
        caseno_data = Case_6.objects.all().order_by('-id')[0].caseno
        year_data = Case_6.objects.all().order_by('-id')[0].year

        # ------------------(cwork6)----------------------

        def Case_6_fun():
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")
            global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            # driver.maximize_window()

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')

            #print('Case_6_fun')

            # <--- all repeated Functions ----/////---
            def dynamicdelay(id_name):
                # print('dynamicdelay:-', id_name)
                global job_profiles
                loop_1 = 1
                while True:
                    L = 0
                    if loop_1 == 1:
                        # print('D while:-')
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        time.sleep(1)
                        all_divs = soup.find('select', {'id': str(id_name)})
                        job_profiles = all_divs.find_all('option')
                        # print('job_profiles:-', job_profiles)
                        loop_1 = len(job_profiles)
                        # print('--', loop_1)
                        L = 0
                    else:
                        L = 1

                    if L == 1:
                        # print('D break:-')
                        break

                return job_profiles

            def delay1():
                time.sleep(random.randint(1, 2))  # 1sec

            def delay():
                time.sleep(random.randint(2, 2))  # 2sec

            # ---/ op / ---
            state_ = state_data
            dist_ = dist_data
            complex_ = complex_data
            casetype_ = casetype_data
            caseno_ = caseno_data
            year_ = year_data
            # ----------------// our code //-----------------

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "menuPage")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="menuPage"]/div/div[1]/ul/li[2]')))  # wait
                    driver.find_elements_by_xpath('//*[@id="menuPage"]/div/div[1]/ul/li[2]')[0].click()
                    # delay1()
                    # for alert OK btn
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "bs_alert")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="bs_alert"]/div/div/div[2]/button')))  # wait
                    driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                    # delay1()

                    # <--------STATE--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "divLangState")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_state_code'})
                    state_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='sess_state_code')
                    # print('job_profiles:-', job_profiles)

                    k = [str(x).split('"')[1] for x in job_profiles]  # <-- output  ['0', '28', '2', '6',..]
                    # print('k:-', k)

                    # <---- take input state value from fun() & find index of that input value ----
                    index = k.index(state_)
                    state_option = index + 1  # op-- 20th actual is 21th

                    # <------ Click on 'STATE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_state_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'STATE' Options ---->
                    state = driver.find_elements_by_xpath('//*[@id="sess_state_code"]/option[{}]'.format(state_option))[
                        0]  # option 1
                    # print('selected state:-- ', state.text)
                    state.click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    # <--------STATE---///--End----///--------->

                    # <--------DISTRICT--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "sess_dist_code")))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_dist_code'})
                    dist_job_profiles = all_divs.find_all('option')
                    # print('dist_job_profiles', dist_job_profiles)'''

                    dynamicdelay(id_name='sess_dist_code')

                    k2 = [str(j).split('"')[1] for j in job_profiles]
                    # print('k2:-', k2)

                    # <---- take input state value from fun() & find index of that input value ----
                    index2 = k2.index(dist_)
                    dist_option = index2 + 1  # op-- 20th actual is 21th

                    # <------ Click on 'DISTRICT' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'DISTRICT' Options ---->
                    district = \
                        driver.find_elements_by_xpath('//*[@id="sess_dist_code"]/option[{}]'.format(dist_option))[0]
                    # option 2
                    # print('selected dist:-- ', district.text)
                    district.click()
                    # delay()
                    # <--------DISTRICT---///--End----///--------->

                    # <--------COMPLEX--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "court_complex_code")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    complex_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='court_complex_code')

                    k3 = [str(n).split('"')[1] for n in job_profiles]
                    # print('k3:- ', k3, 'len(k3:-)', len(k3), 'len(complex_job_profiles:-)', len(job_profiles))

                    # <---- take input complex_ value from fun()
                    com = complex_.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait

                    # <------ Select 'COMPLEX' Options ---->
                    complex = \
                        driver.find_elements_by_xpath(
                            '//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    # option 2
                    # print('selected complex: ', complex.text)
                    complex.click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    # <--------COMPLEX---///--End----///--------->

                    # <--------CLICK CASE NO. BTN--///------>
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CScaseNumber")))  # <---- wait --->
                    driver.find_elements_by_xpath('//*[@id="CScaseNumber"]')[0].click()
                    # delay()
                    # delay()

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    case_job_profiles = all_divs.find_all('option')
                    # print('case_job_profiles', case_job_profiles)'''

                    dynamicdelay(id_name='case_type')

                    k4 = [str(n1).split('"')[1] for n1 in job_profiles]
                    # print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(case_job_profiles:-)', len(job_profiles))

                    case = casetype_.replace('_', '^')
                    index4 = k4.index(str(case))
                    case_option = index4 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[0]
                    # print('selected case: ', case.text)
                    case.click()
                    # delay1()
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(caseno_)  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(year_)  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # Find_data----- START --
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_class_name('refresh-btn').click()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        # delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        driver.execute_script("document.body.style.zoom='170%'")
                        driver.execute_script("window.scrollTo(0,400);")
                        delay1()
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        # start2 = time.perf_counter()
                        driver.find_element_by_xpath("//*[@id='captcha_image']")

                        # # --------------(for SHOW chrome)--------
                        # location = {'x': 219, 'y': 380}
                        # size = {'height': 59, 'width': 310}

                        # --------------(for hide windo)--------
                        location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        # ----------------------------------------

                        driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_6.png')
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_6.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_6.png')
                        # im.show()  # <--- comment it ----

                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_6.png')
                        width, height = im2.size

                        # # --------------(for SHOW chrome)--------
                        # left = 0
                        # top = 0  # done
                        # right = 175
                        # bottom = 57  # done
                        # # ----------------------

                        # --------------(for hide windo)--------
                        left = 30  # ... run with option
                        top = 0  # done
                        right = 220  # done
                        bottom = 57  # done
                        # ----------------------

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_6.png')  # row img
                        # im1.show()
                        # >-----------------------

                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_6.png')

                        # <-------- improve image quality ----------->
                        # img.save("quality_abc_2.png", quality=200)
                        contrast = ImageEnhance.Contrast(img)
                        contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_6.png')

                        # <----- for remove unwanted color from image ---------->
                        image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_6.png')
                        grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                        lower_range = np.array([0, 0, 0])
                        upper_range = np.array([0, 0, 0])

                        mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_6.png', mask)
                        # <-----Done remove unwanted color from image ---------->

                        # <------ image color change in black n white ------
                        Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_6.png', 0)
                        ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_6.png', thresh_img)
                        cv2.waitKey(0)
                        cv2.destroyAllWindows()

                        # <----- for expand image text ---------------------
                        Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_6.png', 0)
                        kernel = np.ones((3, 3), np.uint8)
                        erosion = cv2.erode(Load2, kernel, iterations=1)
                        img = erosion.copy()
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_6.png', img)

                        # <----- for Captcha image to text convert---------------------
                        img2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_6.png')
                        captcha_txt2 = tess.image_to_string(img2, config='')
                        # finish2 = time.perf_counter()
                        # print(f'OLD CODE captcha reading Finished in {round(finish2 - start2, 2)} second(s)')

                        # <------- Find_data--------/// END ///---------------------------

                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 8:
                                # print('!=8 --')

                                # ------------------------------
                                driver.find_element_by_class_name('refresh-btn').click()
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                # delay1()
                                WebDriverWait(driver, 600).until(
                                    EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                driver.execute_script("document.body.style.zoom='170%'")
                                driver.execute_script("window.scrollTo(0,400);")
                                delay1()
                                # ----------------------
                                # <--- to find captcha image location ---->
                                driver.find_element_by_xpath("//*[@id='captcha_image']")
                                # start3 = time.perf_counter()

                                # # --------------(for SHOW chrome)--------
                                # location = {'x': 219, 'y': 380}
                                # size = {'height': 59, 'width': 310}

                                # --------------(for hide windo)--------
                                location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                # ----------------------------------------

                                driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_6.png')
                                # driver.get_screenshot_as_file(settings.MEDIA_ROOT + '/CaseImages/imageCASE_6.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_6.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_6.png')
                                # im.show()  # <--- comment it ----
                                # <--------

                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_6.png')
                                width, height = im2.size

                                # # --------------(for SHOW chrome)--------
                                # left = 0
                                # top = 0  # done
                                # right = 175
                                # bottom = 57  # done
                                # # ----------------------

                                # --------------(for hide windo)--------
                                left = 30  # ... run with option
                                top = 0  # done
                                right = 220  # done
                                bottom = 57  # done
                                # ----------------------

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/ab4.png')
                                # im1.show()
                                # >-----------------------

                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                # delay()

                                # for Captcha image to text convert---------------comment it------------
                                img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_6.png')

                                # <-------- improve image quality ----------->
                                contrast = ImageEnhance.Contrast(img)
                                contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_6.png')

                                # <----- for remove unwanted color from image ---------->
                                image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_6.png')
                                grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                                grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                                lower_range = np.array([0, 0, 0])
                                upper_range = np.array([0, 0, 0])

                                mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_6.png', mask)
                                # <-----Done remove unwanted color from image ---------->

                                # <------ image color change in black n white ------
                                Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_6.png', 0)
                                ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_6.png', thresh_img)
                                cv2.waitKey(0)
                                cv2.destroyAllWindows()

                                # <----- for expand image text ---------------------
                                Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_6.png', 0)
                                kernel = np.ones((3, 3), np.uint8)
                                erosion = cv2.erode(Load2, kernel, iterations=1)
                                img = erosion.copy()
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_6.png', img)

                                # <----- for Captcha image to text convert---------------------
                                img3 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_6.png')
                                re_captcha_txt = tess.image_to_string(img3, config='')
                                # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)

                                # finish3 = time.perf_counter()
                                # print(f'captcha reading Finished in {round(finish3 - start3, 2)} second(s)')

                                captcha_txt2 = re_captcha_txt
                                if len(captcha_txt2) != 8:
                                    # print('if len != 8--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                    # print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                # print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                # print('finaly break done in <6 or >6--')
                                break

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                        input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        # <----- for Click Search btn---------------------

                        # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                        driver.find_element_by_xpath('//*[@id="goResetDiv"]/input[1]').click()  # <--- sertch btn --->

                        # print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        delay()
                        delay()

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display: none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        # html = driver.page_source
                        # soup = BeautifulSoup(html, "html.parser")
                        op_page_bs_alertDiv = soup.find('div', {'id': 'bs_alert'}).get('style')
                        # -----For Invalid captcha page("display: block;")
                        # print('op_page_bs_alertDiv--- ', op_page_bs_alertDiv)
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '':
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                delay()
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                # print('check2--r')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.close()
                                refresh = 1

                        elif op_page_bs_alertDiv == "display: block;":
                            # print('alert 1 ------')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                            refresh = 0

                        elif op_page_bs_alertDiv == "display: block; padding-right: 15px;":
                            # print('alert 2 ---Enter year from 1901 to---')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()

                            res_dct = {}
                            case_final_2 = {'EnterYearFrom1901': 'Enter year from 1901'}
                            Record_not_found = 1
                            driver.close()
                            refresh = 1

                        else:
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            break
                    if Record_not_found == 1:
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        WebDriverWait(driver, 600).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # <---- wait --->
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_showListDiv = soup.find('div', {'id': 'showList'}).get('style')
                        # -----For Invalid captcha page ("display:none;")

                        driver.execute_script("window.scrollTo(0,300);")
                        time.sleep(1)
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # ----------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "caseHistoryDiv")))
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bckbtn"]')))
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            l = []
                            table_case = soup.find('table', attrs={'class': 'case_details_table'})
                            table_case_body = table_case.find('tbody')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values

                            b = l[:len(l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                            b1 = l[-1][1].replace('\xa0\xa0 (Note the CNR number for future reference)', '')
                            # <-- for remove last "(Note the CNR number for future reference)" text o/p-- "MHPU050000132021"

                            lst = reduce(lambda x, y: x + y, b)
                            lst.append('cnrnumber')  # < ---- add 'CNR' name
                            lst.append(b1)  # < ---- add 'CNR number
                            res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except Exception as e:
                            print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {"ProgramError": str(e)}

                        try:
                            # <---2nd Table, take single table data, into list ------------>
                            l2 = []
                            table = soup.find('table', attrs={'class': 'table_r'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l2.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst2 = reduce(lambda x, y: x + y, l2)
                            res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in range(0, len(lst2), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct2 = {}

                        try:
                            # <---3rd Table, take single table data, into list ----------->
                            l3 = []
                            table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l3.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst3 = reduce(lambda x, y: x + y, l3)
                            listToStr = ' '.join(map(str, lst3))
                            res_dct3 = listToStr.replace('\xa0', '')  # <-- make kye 'LOWER case', remove 'space' ---
                        except:
                            res_dct3 = ''

                        try:
                            # <---4th Table, take single table data, into list ---------->
                            l4 = []
                            table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l4.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst4 = reduce(lambda x, y: x + y, l4)
                            listToStr2 = ' '.join(map(str, lst4))
                            res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct4 = ''

                        try:
                            # <---5th Table, take single table data, into list ---------->
                            table_a = soup.find('table', attrs={'class': 'acts_table'})
                            h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                            res_dct5 = [dict(zip(h, i)) for i in d]

                            k5 = []
                            for d in res_dct5:
                                k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                    'UnderSection(s)', 'Section'): v for k, v in d.items()})
                        except:
                            k5 = []

                        try:
                            # <---6th Table, take single table data, into list ---------->
                            table_t = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                            res_dct6 = [dict(zip(h, i)) for i in d]

                            k6 = []
                            for d in res_dct6:
                                k6.append(
                                    {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                     d.items()})
                        except:
                            k6 = []

                        try:
                            # <---7th Table, take single table data, into list ---------->
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]

                            k7 = []
                            for d7 in res_dct7:
                                k7.append(
                                    {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                        except:
                            k7 = []

                        try:
                            # <---8th Table, take single table data, into list ---------->
                            table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                            h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                            res_dct8 = [dict(zip(h1, x)) for x in q]

                            k8 = []
                            for d in res_dct8:
                                k8.append({k.replace(' ', ''): v for k, v in d.items()})
                        except:
                            k8 = []

                        main_ch2 = 1

                        if res_dct != {}:
                            case_final = {"CaseDetails": res_dct, "CaseStatus": res_dct2,
                                          "PetitionerAdvocate": res_dct3,
                                          "RespondentAdvocate": res_dct4, "Acts": k5,
                                          "History": k6, "Orders": k7, "CaseTransferDetails": k8}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {"CaseDetailsNotFound": 'Please try again.'}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.close()
                                break
                            except:
                                pass
                except Exception as e:
                    try:
                        driver.close()
                        # print('except crser here--', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {"ProgramError": str(e)}
                    except:
                        pass

                if main_ch2 == 1:
                    break

            if res_dct != {}:
                yourdata = case_final

            elif res_dct == {}:
                yourdata = case_final_2

            data = yourdata

            try:
                conn = psycopg2.connect(database="defaultdb", user="doadmin", password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")

                cursor = conn.cursor()
                # print('psycopg2 connect execute--')

                var_dict = {"var": 0, "data": 1}
                # <-- for write --
                with open('Scrap_App/files/case_6.txt', 'w') as json_file:
                    json.dump(var_dict, json_file)

            except (Exception, psycopg2.DatabaseError) as error:
                # print("Error while creating PostgreSQL table", error)
                pass
            else:

                cursor.execute('SELECT * FROM public."Scrap_App_case_6" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_case_6" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                # print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------//Case function 1 End //--------------------------------------------------

        # multiprocessing.Process(target=Case_f1).start()
        threading.Thread(target=Case_6_fun).start()
        # print('thred--')

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            # wait = Case_6.objects.all().order_by('-id')[0].data

            # <-- for read ---
            with open('Scrap_App/files/case_6.txt', 'r') as f:
                _dict = json.load(f)
                # print(_dict['var'])  # 0
            # print('wait data:- ', _dict['data'])
            time.sleep(5)
            wait = _dict['data']  # 0

            # print('wait ON-')
            # print('wait OFF-')
            if wait == 0:
                w = 0
            else:
                w = 1
            if w == 1:
                # print('out while..')
                break
        # ------// --- //----
        time.sleep(3)

        data = Case_6.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        c_id = Case_6.objects.all().order_by('-id')[0].id
        c_id2 = Case_6.objects.get(id=c_id)
        c_id2.var = 0  # for make 1 to 0
        c_id2.save()

        # finish = time.perf_counter()
        # print(f'Case_6 Finished in {round(finish - start, 2)} second(s)')
# ------------------(cwork6e)----------------------

    elif Case_7.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()

        var_dict = {"var": 1, "data": 0}
        # <-- for write --
        with open('Scrap_App/files/case_7.txt', 'w') as json_file:
            json.dump(var_dict, json_file)

        Case_7(var=1, time=time.time(), state=state, dist=dist, complex=complex, casetype=casetype, caseno=caseno,
               year=year).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        #print('Case_7 input data:--- ', state, dist, complex, casetype, caseno, year)

        state_data = Case_7.objects.all().order_by('-id')[0].state
        dist_data = Case_7.objects.all().order_by('-id')[0].dist
        complex_data = Case_7.objects.all().order_by('-id')[0].complex
        casetype_data = Case_7.objects.all().order_by('-id')[0].casetype
        caseno_data = Case_7.objects.all().order_by('-id')[0].caseno
        year_data = Case_7.objects.all().order_by('-id')[0].year

        # ------------------(cwork7)----------------------

        def Case_7_fun():
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")
            global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            # driver.maximize_window()

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')

            #print('Case_7_fun')

            # <--- all repeated Functions ----/////---
            def dynamicdelay(id_name):
                # print('dynamicdelay:-', id_name)
                global job_profiles
                loop_1 = 1
                while True:
                    L = 0
                    if loop_1 == 1:
                        # print('D while:-')
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        time.sleep(1)
                        all_divs = soup.find('select', {'id': str(id_name)})
                        job_profiles = all_divs.find_all('option')
                        # print('job_profiles:-', job_profiles)
                        loop_1 = len(job_profiles)
                        # print('--', loop_1)
                        L = 0
                    else:
                        L = 1

                    if L == 1:
                        # print('D break:-')
                        break

                return job_profiles

            def delay1():
                time.sleep(random.randint(1, 2))  # 1sec

            def delay():
                time.sleep(random.randint(2, 2))  # 2sec

            # ---/ op / ---
            state_ = state_data
            dist_ = dist_data
            complex_ = complex_data
            casetype_ = casetype_data
            caseno_ = caseno_data
            year_ = year_data
            # ----------------// our code //-----------------

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "menuPage")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="menuPage"]/div/div[1]/ul/li[2]')))  # wait
                    driver.find_elements_by_xpath('//*[@id="menuPage"]/div/div[1]/ul/li[2]')[0].click()
                    # delay1()
                    # for alert OK btn
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "bs_alert")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="bs_alert"]/div/div/div[2]/button')))  # wait
                    driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                    # delay1()

                    # <--------STATE--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "divLangState")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_state_code'})
                    state_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='sess_state_code')
                    # print('job_profiles:-', job_profiles)

                    k = [str(x).split('"')[1] for x in job_profiles]  # <-- output  ['0', '28', '2', '6',..]
                    # print('k:-', k)

                    # <---- take input state value from fun() & find index of that input value ----
                    index = k.index(state_)
                    state_option = index + 1  # op-- 20th actual is 21th

                    # <------ Click on 'STATE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_state_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'STATE' Options ---->
                    state = driver.find_elements_by_xpath('//*[@id="sess_state_code"]/option[{}]'.format(state_option))[
                        0]  # option 1
                    # print('selected state:-- ', state.text)
                    state.click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    # <--------STATE---///--End----///--------->

                    # <--------DISTRICT--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "sess_dist_code")))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_dist_code'})
                    dist_job_profiles = all_divs.find_all('option')
                    # print('dist_job_profiles', dist_job_profiles)'''

                    dynamicdelay(id_name='sess_dist_code')

                    k2 = [str(j).split('"')[1] for j in job_profiles]
                    # print('k2:-', k2)

                    # <---- take input state value from fun() & find index of that input value ----
                    index2 = k2.index(dist_)
                    dist_option = index2 + 1  # op-- 20th actual is 21th

                    # <------ Click on 'DISTRICT' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'DISTRICT' Options ---->
                    district = \
                        driver.find_elements_by_xpath('//*[@id="sess_dist_code"]/option[{}]'.format(dist_option))[0]
                    # option 2
                    # print('selected dist:-- ', district.text)
                    district.click()
                    # delay()
                    # <--------DISTRICT---///--End----///--------->

                    # <--------COMPLEX--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "court_complex_code")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    complex_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='court_complex_code')

                    k3 = [str(n).split('"')[1] for n in job_profiles]
                    # print('k3:- ', k3, 'len(k3:-)', len(k3), 'len(complex_job_profiles:-)', len(job_profiles))

                    # <---- take input complex_ value from fun()
                    com = complex_.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait

                    # <------ Select 'COMPLEX' Options ---->
                    complex = \
                        driver.find_elements_by_xpath(
                            '//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    # option 2
                    # print('selected complex: ', complex.text)
                    complex.click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    # <--------COMPLEX---///--End----///--------->

                    # <--------CLICK CASE NO. BTN--///------>
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CScaseNumber")))  # <---- wait --->
                    driver.find_elements_by_xpath('//*[@id="CScaseNumber"]')[0].click()
                    # delay()
                    # delay()

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    case_job_profiles = all_divs.find_all('option')
                    # print('case_job_profiles', case_job_profiles)'''

                    dynamicdelay(id_name='case_type')

                    k4 = [str(n1).split('"')[1] for n1 in job_profiles]
                    # print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(case_job_profiles:-)', len(job_profiles))

                    case = casetype_.replace('_', '^')
                    index4 = k4.index(str(case))
                    case_option = index4 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[0]
                    # print('selected case: ', case.text)
                    case.click()
                    # delay1()
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(caseno_)  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(year_)  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # Find_data----- START --
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_class_name('refresh-btn').click()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        # delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        driver.execute_script("document.body.style.zoom='170%'")
                        driver.execute_script("window.scrollTo(0,400);")
                        delay1()
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        # start2 = time.perf_counter()
                        driver.find_element_by_xpath("//*[@id='captcha_image']")

                        # # --------------(for SHOW chrome)--------
                        # location = {'x': 219, 'y': 380}
                        # size = {'height': 59, 'width': 310}

                        # --------------(for hide windo)--------
                        location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        # ----------------------------------------

                        driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_7.png')
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_7.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_7.png')
                        # im.show()  # <--- comment it ----

                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_7.png')
                        width, height = im2.size

                        # # --------------(for SHOW chrome)--------
                        # left = 0
                        # top = 0  # done
                        # right = 175
                        # bottom = 57  # done
                        # # ----------------------

                        # --------------(for hide windo)--------
                        left = 30  # ... run with option
                        top = 0  # done
                        right = 220  # done
                        bottom = 57  # done
                        # ----------------------

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_7.png')  # row img
                        # im1.show()
                        # >-----------------------

                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_7.png')

                        # <-------- improve image quality ----------->
                        # img.save("quality_abc_2.png", quality=200)
                        contrast = ImageEnhance.Contrast(img)
                        contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_7.png')

                        # <----- for remove unwanted color from image ---------->
                        image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_7.png')
                        grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                        lower_range = np.array([0, 0, 0])
                        upper_range = np.array([0, 0, 0])

                        mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_7.png', mask)
                        # <-----Done remove unwanted color from image ---------->

                        # <------ image color change in black n white ------
                        Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_7.png', 0)
                        ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_7.png', thresh_img)
                        cv2.waitKey(0)
                        cv2.destroyAllWindows()

                        # <----- for expand image text ---------------------
                        Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_7.png', 0)
                        kernel = np.ones((3, 3), np.uint8)
                        erosion = cv2.erode(Load2, kernel, iterations=1)
                        img = erosion.copy()
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_7.png', img)

                        # <----- for Captcha image to text convert---------------------
                        img2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_7.png')
                        captcha_txt2 = tess.image_to_string(img2, config='')
                        # finish2 = time.perf_counter()
                        # print(f'OLD CODE captcha reading Finished in {round(finish2 - start2, 2)} second(s)')

                        # <------- Find_data--------/// END ///---------------------------

                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 8:
                                # print('!=8 --')

                                # ------------------------------
                                driver.find_element_by_class_name('refresh-btn').click()
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                # delay1()
                                WebDriverWait(driver, 600).until(
                                    EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                driver.execute_script("document.body.style.zoom='170%'")
                                driver.execute_script("window.scrollTo(0,400);")
                                delay1()
                                # ----------------------
                                # <--- to find captcha image location ---->
                                driver.find_element_by_xpath("//*[@id='captcha_image']")
                                # start3 = time.perf_counter()

                                # # --------------(for SHOW chrome)--------
                                # location = {'x': 219, 'y': 380}
                                # size = {'height': 59, 'width': 310}

                                # --------------(for hide windo)--------
                                location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                # ----------------------------------------

                                driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_7.png')
                                # driver.get_screenshot_as_file(settings.MEDIA_ROOT + '/CaseImages/imageCASE_7.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_7.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_7.png')
                                # im.show()  # <--- comment it ----
                                # <--------

                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_7.png')
                                width, height = im2.size

                                # # --------------(for SHOW chrome)--------
                                # left = 0
                                # top = 0  # done
                                # right = 175
                                # bottom = 57  # done
                                # # ----------------------

                                # --------------(for hide windo)--------
                                left = 30  # ... run with option
                                top = 0  # done
                                right = 220  # done
                                bottom = 57  # done
                                # ----------------------

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/ab4.png')
                                # im1.show()
                                # >-----------------------

                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                # delay()

                                # for Captcha image to text convert---------------comment it------------
                                img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_7.png')

                                # <-------- improve image quality ----------->
                                contrast = ImageEnhance.Contrast(img)
                                contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_7.png')

                                # <----- for remove unwanted color from image ---------->
                                image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_7.png')
                                grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                                grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                                lower_range = np.array([0, 0, 0])
                                upper_range = np.array([0, 0, 0])

                                mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_7.png', mask)
                                # <-----Done remove unwanted color from image ---------->

                                # <------ image color change in black n white ------
                                Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_7.png', 0)
                                ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_7.png', thresh_img)
                                cv2.waitKey(0)
                                cv2.destroyAllWindows()

                                # <----- for expand image text ---------------------
                                Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_7.png', 0)
                                kernel = np.ones((3, 3), np.uint8)
                                erosion = cv2.erode(Load2, kernel, iterations=1)
                                img = erosion.copy()
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_7.png', img)

                                # <----- for Captcha image to text convert---------------------
                                img3 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_7.png')
                                re_captcha_txt = tess.image_to_string(img3, config='')
                                # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)

                                # finish3 = time.perf_counter()
                                # print(f'captcha reading Finished in {round(finish3 - start3, 2)} second(s)')

                                captcha_txt2 = re_captcha_txt
                                if len(captcha_txt2) != 8:
                                    # print('if len != 8--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                    # print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                # print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                # print('finaly break done in <6 or >6--')
                                break

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                        input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        # <----- for Click Search btn---------------------

                        # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                        driver.find_element_by_xpath('//*[@id="goResetDiv"]/input[1]').click()  # <--- sertch btn --->

                        # print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        delay()
                        delay()

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display: none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        # html = driver.page_source
                        # soup = BeautifulSoup(html, "html.parser")
                        op_page_bs_alertDiv = soup.find('div', {'id': 'bs_alert'}).get('style')
                        # -----For Invalid captcha page("display: block;")
                        # print('op_page_bs_alertDiv--- ', op_page_bs_alertDiv)
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '':
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                delay()
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                # print('check2--r')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.close()
                                refresh = 1

                        elif op_page_bs_alertDiv == "display: block;":
                            # print('alert 1 ------')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                            refresh = 0

                        elif op_page_bs_alertDiv == "display: block; padding-right: 15px;":
                            # print('alert 2 ---Enter year from 1901 to---')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()

                            res_dct = {}
                            case_final_2 = {'EnterYearFrom1901': 'Enter year from 1901'}
                            Record_not_found = 1
                            driver.close()
                            refresh = 1

                        else:
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            break
                    if Record_not_found == 1:
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        WebDriverWait(driver, 600).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # <---- wait --->
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_showListDiv = soup.find('div', {'id': 'showList'}).get('style')
                        # -----For Invalid captcha page ("display:none;")

                        driver.execute_script("window.scrollTo(0,300);")
                        time.sleep(1)
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # ----------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "caseHistoryDiv")))
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bckbtn"]')))
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            l = []
                            table_case = soup.find('table', attrs={'class': 'case_details_table'})
                            table_case_body = table_case.find('tbody')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values

                            b = l[:len(l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                            b1 = l[-1][1].replace('\xa0\xa0 (Note the CNR number for future reference)', '')
                            # <-- for remove last "(Note the CNR number for future reference)" text o/p-- "MHPU050000132021"

                            lst = reduce(lambda x, y: x + y, b)
                            lst.append('cnrnumber')  # < ---- add 'CNR' name
                            lst.append(b1)  # < ---- add 'CNR number
                            res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except Exception as e:
                            print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {"ProgramError": str(e)}

                        try:
                            # <---2nd Table, take single table data, into list ------------>
                            l2 = []
                            table = soup.find('table', attrs={'class': 'table_r'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l2.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst2 = reduce(lambda x, y: x + y, l2)
                            res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in range(0, len(lst2), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct2 = {}

                        try:
                            # <---3rd Table, take single table data, into list ----------->
                            l3 = []
                            table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l3.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst3 = reduce(lambda x, y: x + y, l3)
                            listToStr = ' '.join(map(str, lst3))
                            res_dct3 = listToStr.replace('\xa0', '')  # <-- make kye 'LOWER case', remove 'space' ---
                        except:
                            res_dct3 = ''

                        try:
                            # <---4th Table, take single table data, into list ---------->
                            l4 = []
                            table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l4.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst4 = reduce(lambda x, y: x + y, l4)
                            listToStr2 = ' '.join(map(str, lst4))
                            res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct4 = ''

                        try:
                            # <---5th Table, take single table data, into list ---------->
                            table_a = soup.find('table', attrs={'class': 'acts_table'})
                            h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                            res_dct5 = [dict(zip(h, i)) for i in d]

                            k5 = []
                            for d in res_dct5:
                                k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                    'UnderSection(s)', 'Section'): v for k, v in d.items()})
                        except:
                            k5 = []

                        try:
                            # <---6th Table, take single table data, into list ---------->
                            table_t = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                            res_dct6 = [dict(zip(h, i)) for i in d]

                            k6 = []
                            for d in res_dct6:
                                k6.append(
                                    {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                     d.items()})
                        except:
                            k6 = []

                        try:
                            # <---7th Table, take single table data, into list ---------->
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]

                            k7 = []
                            for d7 in res_dct7:
                                k7.append(
                                    {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                        except:
                            k7 = []

                        try:
                            # <---8th Table, take single table data, into list ---------->
                            table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                            h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                            res_dct8 = [dict(zip(h1, x)) for x in q]

                            k8 = []
                            for d in res_dct8:
                                k8.append({k.replace(' ', ''): v for k, v in d.items()})
                        except:
                            k8 = []

                        main_ch2 = 1

                        if res_dct != {}:
                            case_final = {"CaseDetails": res_dct, "CaseStatus": res_dct2,
                                          "PetitionerAdvocate": res_dct3,
                                          "RespondentAdvocate": res_dct4, "Acts": k5,
                                          "History": k6, "Orders": k7, "CaseTransferDetails": k8}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {"CaseDetailsNotFound": 'Please try again.'}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.close()
                                break
                            except:
                                pass
                except Exception as e:
                    try:
                        driver.close()
                        # print('except crser here--', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {"ProgramError": str(e)}
                    except:
                        pass

                if main_ch2 == 1:
                    break

            if res_dct != {}:
                yourdata = case_final

            elif res_dct == {}:
                yourdata = case_final_2

            data = yourdata

            try:
                conn = psycopg2.connect(database="defaultdb", user="doadmin", password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")

                cursor = conn.cursor()
                # print('psycopg2 connect execute--')

                var_dict = {"var": 0, "data": 1}
                # <-- for write --
                with open('Scrap_App/files/case_7.txt', 'w') as json_file:
                    json.dump(var_dict, json_file)

            except (Exception, psycopg2.DatabaseError) as error:
                # print("Error while creating PostgreSQL table", error)
                pass
            else:

                cursor.execute('SELECT * FROM public."Scrap_App_case_7" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_case_7" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                # print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------//Case function 1 End //--------------------------------------------------

        # multiprocessing.Process(target=Case_f1).start()
        threading.Thread(target=Case_7_fun).start()
        # print('thred--')

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            # wait = Case_7.objects.all().order_by('-id')[0].data

            # <-- for read ---
            with open('Scrap_App/files/case_7.txt', 'r') as f:
                _dict = json.load(f)
                # print(_dict['var'])  # 0
            # print('wait data:- ', _dict['data'])
            time.sleep(5)
            wait = _dict['data']  # 0

            # print('wait ON-')
            # print('wait OFF-')
            if wait == 0:
                w = 0
            else:
                w = 1
            if w == 1:
                # print('out while..')
                break
        # ------// --- //----
        time.sleep(3)

        data = Case_7.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        c_id = Case_7.objects.all().order_by('-id')[0].id
        c_id2 = Case_7.objects.get(id=c_id)
        c_id2.var = 0  # for make 1 to 0
        c_id2.save()

        # finish = time.perf_counter()
        # print(f'Case_7 Finished in {round(finish - start, 2)} second(s)')
# ------------------(cwork7e)----------------------

    elif Case_8.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()

        var_dict = {"var": 1, "data": 0}
        # <-- for write --
        with open('Scrap_App/files/case_8.txt', 'w') as json_file:
            json.dump(var_dict, json_file)

        Case_8(var=1, time=time.time(), state=state, dist=dist, complex=complex, casetype=casetype, caseno=caseno,
               year=year).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        #print('Case_8 input data:--- ', state, dist, complex, casetype, caseno, year)

        state_data = Case_8.objects.all().order_by('-id')[0].state
        dist_data = Case_8.objects.all().order_by('-id')[0].dist
        complex_data = Case_8.objects.all().order_by('-id')[0].complex
        casetype_data = Case_8.objects.all().order_by('-id')[0].casetype
        caseno_data = Case_8.objects.all().order_by('-id')[0].caseno
        year_data = Case_8.objects.all().order_by('-id')[0].year

        # ------------------(cwork8)----------------------

        def Case_8_fun():
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")
            global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            # driver.maximize_window()

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')

            #print('Case_8_fun')

            # <--- all repeated Functions ----/////---
            def dynamicdelay(id_name):
                # print('dynamicdelay:-', id_name)
                global job_profiles
                loop_1 = 1
                while True:
                    L = 0
                    if loop_1 == 1:
                        # print('D while:-')
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        time.sleep(1)
                        all_divs = soup.find('select', {'id': str(id_name)})
                        job_profiles = all_divs.find_all('option')
                        # print('job_profiles:-', job_profiles)
                        loop_1 = len(job_profiles)
                        # print('--', loop_1)
                        L = 0
                    else:
                        L = 1

                    if L == 1:
                        # print('D break:-')
                        break

                return job_profiles

            def delay1():
                time.sleep(random.randint(1, 2))  # 1sec

            def delay():
                time.sleep(random.randint(2, 2))  # 2sec

            # ---/ op / ---
            state_ = state_data
            dist_ = dist_data
            complex_ = complex_data
            casetype_ = casetype_data
            caseno_ = caseno_data
            year_ = year_data
            # ----------------// our code //-----------------

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "menuPage")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="menuPage"]/div/div[1]/ul/li[2]')))  # wait
                    driver.find_elements_by_xpath('//*[@id="menuPage"]/div/div[1]/ul/li[2]')[0].click()
                    # delay1()
                    # for alert OK btn
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "bs_alert")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="bs_alert"]/div/div/div[2]/button')))  # wait
                    driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                    # delay1()

                    # <--------STATE--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "divLangState")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_state_code'})
                    state_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='sess_state_code')
                    # print('job_profiles:-', job_profiles)

                    k = [str(x).split('"')[1] for x in job_profiles]  # <-- output  ['0', '28', '2', '6',..]
                    # print('k:-', k)

                    # <---- take input state value from fun() & find index of that input value ----
                    index = k.index(state_)
                    state_option = index + 1  # op-- 20th actual is 21th

                    # <------ Click on 'STATE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_state_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'STATE' Options ---->
                    state = driver.find_elements_by_xpath('//*[@id="sess_state_code"]/option[{}]'.format(state_option))[
                        0]  # option 1
                    # print('selected state:-- ', state.text)
                    state.click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    # <--------STATE---///--End----///--------->

                    # <--------DISTRICT--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "sess_dist_code")))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_dist_code'})
                    dist_job_profiles = all_divs.find_all('option')
                    # print('dist_job_profiles', dist_job_profiles)'''

                    dynamicdelay(id_name='sess_dist_code')

                    k2 = [str(j).split('"')[1] for j in job_profiles]
                    # print('k2:-', k2)

                    # <---- take input state value from fun() & find index of that input value ----
                    index2 = k2.index(dist_)
                    dist_option = index2 + 1  # op-- 20th actual is 21th

                    # <------ Click on 'DISTRICT' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'DISTRICT' Options ---->
                    district = \
                        driver.find_elements_by_xpath('//*[@id="sess_dist_code"]/option[{}]'.format(dist_option))[0]
                    # option 2
                    # print('selected dist:-- ', district.text)
                    district.click()
                    # delay()
                    # <--------DISTRICT---///--End----///--------->

                    # <--------COMPLEX--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "court_complex_code")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    complex_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='court_complex_code')

                    k3 = [str(n).split('"')[1] for n in job_profiles]
                    # print('k3:- ', k3, 'len(k3:-)', len(k3), 'len(complex_job_profiles:-)', len(job_profiles))

                    # <---- take input complex_ value from fun()
                    com = complex_.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait

                    # <------ Select 'COMPLEX' Options ---->
                    complex = \
                        driver.find_elements_by_xpath(
                            '//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    # option 2
                    # print('selected complex: ', complex.text)
                    complex.click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    # <--------COMPLEX---///--End----///--------->

                    # <--------CLICK CASE NO. BTN--///------>
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CScaseNumber")))  # <---- wait --->
                    driver.find_elements_by_xpath('//*[@id="CScaseNumber"]')[0].click()
                    # delay()
                    # delay()

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    case_job_profiles = all_divs.find_all('option')
                    # print('case_job_profiles', case_job_profiles)'''

                    dynamicdelay(id_name='case_type')

                    k4 = [str(n1).split('"')[1] for n1 in job_profiles]
                    # print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(case_job_profiles:-)', len(job_profiles))

                    case = casetype_.replace('_', '^')
                    index4 = k4.index(str(case))
                    case_option = index4 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[0]
                    # print('selected case: ', case.text)
                    case.click()
                    # delay1()
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(caseno_)  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(year_)  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # Find_data----- START --
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_class_name('refresh-btn').click()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        # delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        driver.execute_script("document.body.style.zoom='170%'")
                        driver.execute_script("window.scrollTo(0,400);")
                        delay1()
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        # start2 = time.perf_counter()
                        driver.find_element_by_xpath("//*[@id='captcha_image']")

                        # # --------------(for SHOW chrome)--------
                        # location = {'x': 219, 'y': 380}
                        # size = {'height': 59, 'width': 310}

                        # --------------(for hide windo)--------
                        location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        # ----------------------------------------

                        driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_8.png')
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_8.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_8.png')
                        # im.show()  # <--- comment it ----

                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_8.png')
                        width, height = im2.size

                        # # --------------(for SHOW chrome)--------
                        # left = 0
                        # top = 0  # done
                        # right = 175
                        # bottom = 57  # done
                        # # ----------------------

                        # --------------(for hide windo)--------
                        left = 30  # ... run with option
                        top = 0  # done
                        right = 220  # done
                        bottom = 57  # done
                        # ----------------------

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_8.png')  # row img
                        # im1.show()
                        # >-----------------------

                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_8.png')

                        # <-------- improve image quality ----------->
                        # img.save("quality_abc_2.png", quality=200)
                        contrast = ImageEnhance.Contrast(img)
                        contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_8.png')

                        # <----- for remove unwanted color from image ---------->
                        image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_8.png')
                        grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                        lower_range = np.array([0, 0, 0])
                        upper_range = np.array([0, 0, 0])

                        mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_8.png', mask)
                        # <-----Done remove unwanted color from image ---------->

                        # <------ image color change in black n white ------
                        Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_8.png', 0)
                        ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_8.png', thresh_img)
                        cv2.waitKey(0)
                        cv2.destroyAllWindows()

                        # <----- for expand image text ---------------------
                        Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_8.png', 0)
                        kernel = np.ones((3, 3), np.uint8)
                        erosion = cv2.erode(Load2, kernel, iterations=1)
                        img = erosion.copy()
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_8.png', img)

                        # <----- for Captcha image to text convert---------------------
                        img2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_8.png')
                        captcha_txt2 = tess.image_to_string(img2, config='')
                        # finish2 = time.perf_counter()
                        # print(f'OLD CODE captcha reading Finished in {round(finish2 - start2, 2)} second(s)')

                        # <------- Find_data--------/// END ///---------------------------

                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 8:
                                # print('!=8 --')

                                # ------------------------------
                                driver.find_element_by_class_name('refresh-btn').click()
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                # delay1()
                                WebDriverWait(driver, 600).until(
                                    EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                driver.execute_script("document.body.style.zoom='170%'")
                                driver.execute_script("window.scrollTo(0,400);")
                                delay1()
                                # ----------------------
                                # <--- to find captcha image location ---->
                                driver.find_element_by_xpath("//*[@id='captcha_image']")
                                # start3 = time.perf_counter()

                                # # --------------(for SHOW chrome)--------
                                # location = {'x': 219, 'y': 380}
                                # size = {'height': 59, 'width': 310}

                                # --------------(for hide windo)--------
                                location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                # ----------------------------------------

                                driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_8.png')
                                # driver.get_screenshot_as_file(settings.MEDIA_ROOT + '/CaseImages/imageCASE_8.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_8.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_8.png')
                                # im.show()  # <--- comment it ----
                                # <--------

                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_8.png')
                                width, height = im2.size

                                # # --------------(for SHOW chrome)--------
                                # left = 0
                                # top = 0  # done
                                # right = 175
                                # bottom = 57  # done
                                # # ----------------------

                                # --------------(for hide windo)--------
                                left = 30  # ... run with option
                                top = 0  # done
                                right = 220  # done
                                bottom = 57  # done
                                # ----------------------

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/ab4.png')
                                # im1.show()
                                # >-----------------------

                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                # delay()

                                # for Captcha image to text convert---------------comment it------------
                                img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_8.png')

                                # <-------- improve image quality ----------->
                                contrast = ImageEnhance.Contrast(img)
                                contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_8.png')

                                # <----- for remove unwanted color from image ---------->
                                image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_8.png')
                                grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                                grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                                lower_range = np.array([0, 0, 0])
                                upper_range = np.array([0, 0, 0])

                                mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_8.png', mask)
                                # <-----Done remove unwanted color from image ---------->

                                # <------ image color change in black n white ------
                                Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_8.png', 0)
                                ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_8.png', thresh_img)
                                cv2.waitKey(0)
                                cv2.destroyAllWindows()

                                # <----- for expand image text ---------------------
                                Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_8.png', 0)
                                kernel = np.ones((3, 3), np.uint8)
                                erosion = cv2.erode(Load2, kernel, iterations=1)
                                img = erosion.copy()
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_8.png', img)

                                # <----- for Captcha image to text convert---------------------
                                img3 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_8.png')
                                re_captcha_txt = tess.image_to_string(img3, config='')
                                # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)

                                # finish3 = time.perf_counter()
                                # print(f'captcha reading Finished in {round(finish3 - start3, 2)} second(s)')

                                captcha_txt2 = re_captcha_txt
                                if len(captcha_txt2) != 8:
                                    # print('if len != 8--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                    # print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                # print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                # print('finaly break done in <6 or >6--')
                                break

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                        input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        # <----- for Click Search btn---------------------

                        # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                        driver.find_element_by_xpath('//*[@id="goResetDiv"]/input[1]').click()  # <--- sertch btn --->

                        # print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        delay()
                        delay()

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display: none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        # html = driver.page_source
                        # soup = BeautifulSoup(html, "html.parser")
                        op_page_bs_alertDiv = soup.find('div', {'id': 'bs_alert'}).get('style')
                        # -----For Invalid captcha page("display: block;")
                        # print('op_page_bs_alertDiv--- ', op_page_bs_alertDiv)
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '':
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                delay()
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                # print('check2--r')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.close()
                                refresh = 1

                        elif op_page_bs_alertDiv == "display: block;":
                            # print('alert 1 ------')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                            refresh = 0

                        elif op_page_bs_alertDiv == "display: block; padding-right: 15px;":
                            # print('alert 2 ---Enter year from 1901 to---')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()

                            res_dct = {}
                            case_final_2 = {'EnterYearFrom1901': 'Enter year from 1901'}
                            Record_not_found = 1
                            driver.close()
                            refresh = 1

                        else:
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            break
                    if Record_not_found == 1:
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        WebDriverWait(driver, 600).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # <---- wait --->
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_showListDiv = soup.find('div', {'id': 'showList'}).get('style')
                        # -----For Invalid captcha page ("display:none;")

                        driver.execute_script("window.scrollTo(0,300);")
                        time.sleep(1)
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # ----------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "caseHistoryDiv")))
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bckbtn"]')))
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            l = []
                            table_case = soup.find('table', attrs={'class': 'case_details_table'})
                            table_case_body = table_case.find('tbody')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values

                            b = l[:len(l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                            b1 = l[-1][1].replace('\xa0\xa0 (Note the CNR number for future reference)', '')
                            # <-- for remove last "(Note the CNR number for future reference)" text o/p-- "MHPU050000132021"

                            lst = reduce(lambda x, y: x + y, b)
                            lst.append('cnrnumber')  # < ---- add 'CNR' name
                            lst.append(b1)  # < ---- add 'CNR number
                            res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except Exception as e:
                            print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {"ProgramError": str(e)}

                        try:
                            # <---2nd Table, take single table data, into list ------------>
                            l2 = []
                            table = soup.find('table', attrs={'class': 'table_r'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l2.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst2 = reduce(lambda x, y: x + y, l2)
                            res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in range(0, len(lst2), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct2 = {}

                        try:
                            # <---3rd Table, take single table data, into list ----------->
                            l3 = []
                            table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l3.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst3 = reduce(lambda x, y: x + y, l3)
                            listToStr = ' '.join(map(str, lst3))
                            res_dct3 = listToStr.replace('\xa0', '')  # <-- make kye 'LOWER case', remove 'space' ---
                        except:
                            res_dct3 = ''

                        try:
                            # <---4th Table, take single table data, into list ---------->
                            l4 = []
                            table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l4.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst4 = reduce(lambda x, y: x + y, l4)
                            listToStr2 = ' '.join(map(str, lst4))
                            res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct4 = ''

                        try:
                            # <---5th Table, take single table data, into list ---------->
                            table_a = soup.find('table', attrs={'class': 'acts_table'})
                            h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                            res_dct5 = [dict(zip(h, i)) for i in d]

                            k5 = []
                            for d in res_dct5:
                                k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                    'UnderSection(s)', 'Section'): v for k, v in d.items()})
                        except:
                            k5 = []

                        try:
                            # <---6th Table, take single table data, into list ---------->
                            table_t = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                            res_dct6 = [dict(zip(h, i)) for i in d]

                            k6 = []
                            for d in res_dct6:
                                k6.append(
                                    {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                     d.items()})
                        except:
                            k6 = []

                        try:
                            # <---7th Table, take single table data, into list ---------->
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]

                            k7 = []
                            for d7 in res_dct7:
                                k7.append(
                                    {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                        except:
                            k7 = []

                        try:
                            # <---8th Table, take single table data, into list ---------->
                            table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                            h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                            res_dct8 = [dict(zip(h1, x)) for x in q]

                            k8 = []
                            for d in res_dct8:
                                k8.append({k.replace(' ', ''): v for k, v in d.items()})
                        except:
                            k8 = []

                        main_ch2 = 1

                        if res_dct != {}:
                            case_final = {"CaseDetails": res_dct, "CaseStatus": res_dct2,
                                          "PetitionerAdvocate": res_dct3,
                                          "RespondentAdvocate": res_dct4, "Acts": k5,
                                          "History": k6, "Orders": k7, "CaseTransferDetails": k8}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {"CaseDetailsNotFound": 'Please try again.'}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.close()
                                break
                            except:
                                pass
                except Exception as e:
                    try:
                        driver.close()
                        # print('except crser here--', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {"ProgramError": str(e)}
                    except:
                        pass

                if main_ch2 == 1:
                    break

            if res_dct != {}:
                yourdata = case_final

            elif res_dct == {}:
                yourdata = case_final_2

            data = yourdata

            try:
                conn = psycopg2.connect(database="defaultdb", user="doadmin", password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")

                cursor = conn.cursor()
                # print('psycopg2 connect execute--')

                var_dict = {"var": 0, "data": 1}
                # <-- for write --
                with open('Scrap_App/files/case_8.txt', 'w') as json_file:
                    json.dump(var_dict, json_file)

            except (Exception, psycopg2.DatabaseError) as error:
                # print("Error while creating PostgreSQL table", error)
                pass
            else:

                cursor.execute('SELECT * FROM public."Scrap_App_case_8" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_case_8" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                # print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------//Case function 1 End //--------------------------------------------------

        # multiprocessing.Process(target=Case_f1).start()
        threading.Thread(target=Case_8_fun).start()
        # print('thred--')

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            # wait = Case_8.objects.all().order_by('-id')[0].data

            # <-- for read ---
            with open('Scrap_App/files/case_8.txt', 'r') as f:
                _dict = json.load(f)
                # print(_dict['var'])  # 0
            # print('wait data:- ', _dict['data'])
            time.sleep(5)
            wait = _dict['data']  # 0

            # print('wait ON-')
            # print('wait OFF-')
            if wait == 0:
                w = 0
            else:
                w = 1
            if w == 1:
                # print('out while..')
                break
        # ------// --- //----
        time.sleep(3)

        data = Case_8.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        c_id = Case_8.objects.all().order_by('-id')[0].id
        c_id2 = Case_8.objects.get(id=c_id)
        c_id2.var = 0  # for make 1 to 0
        c_id2.save()

        # finish = time.perf_counter()
        # print(f'Case_8 Finished in {round(finish - start, 2)} second(s)')
# ------------------(cwork8e)----------------------

    elif Case_9.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()

        var_dict = {"var": 1, "data": 0}
        # <-- for write --
        with open('Scrap_App/files/case_9.txt', 'w') as json_file:
            json.dump(var_dict, json_file)

        Case_9(var=1, time=time.time(), state=state, dist=dist, complex=complex, casetype=casetype, caseno=caseno,
               year=year).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        #print('Case_9 input data:--- ', state, dist, complex, casetype, caseno, year)

        state_data = Case_9.objects.all().order_by('-id')[0].state
        dist_data = Case_9.objects.all().order_by('-id')[0].dist
        complex_data = Case_9.objects.all().order_by('-id')[0].complex
        casetype_data = Case_9.objects.all().order_by('-id')[0].casetype
        caseno_data = Case_9.objects.all().order_by('-id')[0].caseno
        year_data = Case_9.objects.all().order_by('-id')[0].year

        # ------------------(cwork9)----------------------

        def Case_9_fun():
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")
            global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            # driver.maximize_window()

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')

            #print('Case_9_fun')

            # <--- all repeated Functions ----/////---
            def dynamicdelay(id_name):
                # print('dynamicdelay:-', id_name)
                global job_profiles
                loop_1 = 1
                while True:
                    L = 0
                    if loop_1 == 1:
                        # print('D while:-')
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        time.sleep(1)
                        all_divs = soup.find('select', {'id': str(id_name)})
                        job_profiles = all_divs.find_all('option')
                        # print('job_profiles:-', job_profiles)
                        loop_1 = len(job_profiles)
                        # print('--', loop_1)
                        L = 0
                    else:
                        L = 1

                    if L == 1:
                        # print('D break:-')
                        break

                return job_profiles

            def delay1():
                time.sleep(random.randint(1, 2))  # 1sec

            def delay():
                time.sleep(random.randint(2, 2))  # 2sec

            # ---/ op / ---
            state_ = state_data
            dist_ = dist_data
            complex_ = complex_data
            casetype_ = casetype_data
            caseno_ = caseno_data
            year_ = year_data
            # ----------------// our code //-----------------

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "menuPage")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="menuPage"]/div/div[1]/ul/li[2]')))  # wait
                    driver.find_elements_by_xpath('//*[@id="menuPage"]/div/div[1]/ul/li[2]')[0].click()
                    # delay1()
                    # for alert OK btn
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "bs_alert")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="bs_alert"]/div/div/div[2]/button')))  # wait
                    driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                    # delay1()

                    # <--------STATE--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "divLangState")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_state_code'})
                    state_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='sess_state_code')
                    # print('job_profiles:-', job_profiles)

                    k = [str(x).split('"')[1] for x in job_profiles]  # <-- output  ['0', '28', '2', '6',..]
                    # print('k:-', k)

                    # <---- take input state value from fun() & find index of that input value ----
                    index = k.index(state_)
                    state_option = index + 1  # op-- 20th actual is 21th

                    # <------ Click on 'STATE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_state_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'STATE' Options ---->
                    state = driver.find_elements_by_xpath('//*[@id="sess_state_code"]/option[{}]'.format(state_option))[
                        0]  # option 1
                    # print('selected state:-- ', state.text)
                    state.click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    # <--------STATE---///--End----///--------->

                    # <--------DISTRICT--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "sess_dist_code")))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_dist_code'})
                    dist_job_profiles = all_divs.find_all('option')
                    # print('dist_job_profiles', dist_job_profiles)'''

                    dynamicdelay(id_name='sess_dist_code')

                    k2 = [str(j).split('"')[1] for j in job_profiles]
                    # print('k2:-', k2)

                    # <---- take input state value from fun() & find index of that input value ----
                    index2 = k2.index(dist_)
                    dist_option = index2 + 1  # op-- 20th actual is 21th

                    # <------ Click on 'DISTRICT' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'DISTRICT' Options ---->
                    district = \
                        driver.find_elements_by_xpath('//*[@id="sess_dist_code"]/option[{}]'.format(dist_option))[0]
                    # option 2
                    # print('selected dist:-- ', district.text)
                    district.click()
                    # delay()
                    # <--------DISTRICT---///--End----///--------->

                    # <--------COMPLEX--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "court_complex_code")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    complex_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='court_complex_code')

                    k3 = [str(n).split('"')[1] for n in job_profiles]
                    # print('k3:- ', k3, 'len(k3:-)', len(k3), 'len(complex_job_profiles:-)', len(job_profiles))

                    # <---- take input complex_ value from fun()
                    com = complex_.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait

                    # <------ Select 'COMPLEX' Options ---->
                    complex = \
                        driver.find_elements_by_xpath(
                            '//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    # option 2
                    # print('selected complex: ', complex.text)
                    complex.click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    # <--------COMPLEX---///--End----///--------->

                    # <--------CLICK CASE NO. BTN--///------>
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CScaseNumber")))  # <---- wait --->
                    driver.find_elements_by_xpath('//*[@id="CScaseNumber"]')[0].click()
                    # delay()
                    # delay()

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    case_job_profiles = all_divs.find_all('option')
                    # print('case_job_profiles', case_job_profiles)'''

                    dynamicdelay(id_name='case_type')

                    k4 = [str(n1).split('"')[1] for n1 in job_profiles]
                    # print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(case_job_profiles:-)', len(job_profiles))

                    case = casetype_.replace('_', '^')
                    index4 = k4.index(str(case))
                    case_option = index4 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[0]
                    # print('selected case: ', case.text)
                    case.click()
                    # delay1()
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(caseno_)  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(year_)  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # Find_data----- START --
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_class_name('refresh-btn').click()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        # delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        driver.execute_script("document.body.style.zoom='170%'")
                        driver.execute_script("window.scrollTo(0,400);")
                        delay1()
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        # start2 = time.perf_counter()
                        driver.find_element_by_xpath("//*[@id='captcha_image']")

                        # # --------------(for SHOW chrome)--------
                        # location = {'x': 219, 'y': 380}
                        # size = {'height': 59, 'width': 310}

                        # --------------(for hide windo)--------
                        location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        # ----------------------------------------

                        driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_9.png')
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_9.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_9.png')
                        # im.show()  # <--- comment it ----

                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_9.png')
                        width, height = im2.size

                        # # --------------(for SHOW chrome)--------
                        # left = 0
                        # top = 0  # done
                        # right = 175
                        # bottom = 57  # done
                        # # ----------------------

                        # --------------(for hide windo)--------
                        left = 30  # ... run with option
                        top = 0  # done
                        right = 220  # done
                        bottom = 57  # done
                        # ----------------------

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_9.png')  # row img
                        # im1.show()
                        # >-----------------------

                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_9.png')

                        # <-------- improve image quality ----------->
                        # img.save("quality_abc_2.png", quality=200)
                        contrast = ImageEnhance.Contrast(img)
                        contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_9.png')

                        # <----- for remove unwanted color from image ---------->
                        image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_9.png')
                        grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                        lower_range = np.array([0, 0, 0])
                        upper_range = np.array([0, 0, 0])

                        mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_9.png', mask)
                        # <-----Done remove unwanted color from image ---------->

                        # <------ image color change in black n white ------
                        Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_9.png', 0)
                        ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_9.png', thresh_img)
                        cv2.waitKey(0)
                        cv2.destroyAllWindows()

                        # <----- for expand image text ---------------------
                        Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_9.png', 0)
                        kernel = np.ones((3, 3), np.uint8)
                        erosion = cv2.erode(Load2, kernel, iterations=1)
                        img = erosion.copy()
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_9.png', img)

                        # <----- for Captcha image to text convert---------------------
                        img2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_9.png')
                        captcha_txt2 = tess.image_to_string(img2, config='')
                        # finish2 = time.perf_counter()
                        # print(f'OLD CODE captcha reading Finished in {round(finish2 - start2, 2)} second(s)')

                        # <------- Find_data--------/// END ///---------------------------

                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 8:
                                # print('!=8 --')

                                # ------------------------------
                                driver.find_element_by_class_name('refresh-btn').click()
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                # delay1()
                                WebDriverWait(driver, 600).until(
                                    EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                driver.execute_script("document.body.style.zoom='170%'")
                                driver.execute_script("window.scrollTo(0,400);")
                                delay1()
                                # ----------------------
                                # <--- to find captcha image location ---->
                                driver.find_element_by_xpath("//*[@id='captcha_image']")
                                # start3 = time.perf_counter()

                                # # --------------(for SHOW chrome)--------
                                # location = {'x': 219, 'y': 380}
                                # size = {'height': 59, 'width': 310}

                                # --------------(for hide windo)--------
                                location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                # ----------------------------------------

                                driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_9.png')
                                # driver.get_screenshot_as_file(settings.MEDIA_ROOT + '/CaseImages/imageCASE_9.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_9.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_9.png')
                                # im.show()  # <--- comment it ----
                                # <--------

                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_9.png')
                                width, height = im2.size

                                # # --------------(for SHOW chrome)--------
                                # left = 0
                                # top = 0  # done
                                # right = 175
                                # bottom = 57  # done
                                # # ----------------------

                                # --------------(for hide windo)--------
                                left = 30  # ... run with option
                                top = 0  # done
                                right = 220  # done
                                bottom = 57  # done
                                # ----------------------

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/ab4.png')
                                # im1.show()
                                # >-----------------------

                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                # delay()

                                # for Captcha image to text convert---------------comment it------------
                                img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_9.png')

                                # <-------- improve image quality ----------->
                                contrast = ImageEnhance.Contrast(img)
                                contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_9.png')

                                # <----- for remove unwanted color from image ---------->
                                image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_9.png')
                                grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                                grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                                lower_range = np.array([0, 0, 0])
                                upper_range = np.array([0, 0, 0])

                                mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_9.png', mask)
                                # <-----Done remove unwanted color from image ---------->

                                # <------ image color change in black n white ------
                                Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_9.png', 0)
                                ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_9.png', thresh_img)
                                cv2.waitKey(0)
                                cv2.destroyAllWindows()

                                # <----- for expand image text ---------------------
                                Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_9.png', 0)
                                kernel = np.ones((3, 3), np.uint8)
                                erosion = cv2.erode(Load2, kernel, iterations=1)
                                img = erosion.copy()
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_9.png', img)

                                # <----- for Captcha image to text convert---------------------
                                img3 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_9.png')
                                re_captcha_txt = tess.image_to_string(img3, config='')
                                # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)

                                # finish3 = time.perf_counter()
                                # print(f'captcha reading Finished in {round(finish3 - start3, 2)} second(s)')

                                captcha_txt2 = re_captcha_txt
                                if len(captcha_txt2) != 8:
                                    # print('if len != 8--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                    # print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                # print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                # print('finaly break done in <6 or >6--')
                                break

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                        input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        # <----- for Click Search btn---------------------

                        # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                        driver.find_element_by_xpath('//*[@id="goResetDiv"]/input[1]').click()  # <--- sertch btn --->

                        # print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        delay()
                        delay()

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display: none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        # html = driver.page_source
                        # soup = BeautifulSoup(html, "html.parser")
                        op_page_bs_alertDiv = soup.find('div', {'id': 'bs_alert'}).get('style')
                        # -----For Invalid captcha page("display: block;")
                        # print('op_page_bs_alertDiv--- ', op_page_bs_alertDiv)
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '':
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                delay()
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                # print('check2--r')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.close()
                                refresh = 1

                        elif op_page_bs_alertDiv == "display: block;":
                            # print('alert 1 ------')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                            refresh = 0

                        elif op_page_bs_alertDiv == "display: block; padding-right: 15px;":
                            # print('alert 2 ---Enter year from 1901 to---')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()

                            res_dct = {}
                            case_final_2 = {'EnterYearFrom1901': 'Enter year from 1901'}
                            Record_not_found = 1
                            driver.close()
                            refresh = 1

                        else:
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            break
                    if Record_not_found == 1:
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        WebDriverWait(driver, 600).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # <---- wait --->
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_showListDiv = soup.find('div', {'id': 'showList'}).get('style')
                        # -----For Invalid captcha page ("display:none;")

                        driver.execute_script("window.scrollTo(0,300);")
                        time.sleep(1)
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # ----------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "caseHistoryDiv")))
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bckbtn"]')))
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            l = []
                            table_case = soup.find('table', attrs={'class': 'case_details_table'})
                            table_case_body = table_case.find('tbody')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values

                            b = l[:len(l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                            b1 = l[-1][1].replace('\xa0\xa0 (Note the CNR number for future reference)', '')
                            # <-- for remove last "(Note the CNR number for future reference)" text o/p-- "MHPU050000132021"

                            lst = reduce(lambda x, y: x + y, b)
                            lst.append('cnrnumber')  # < ---- add 'CNR' name
                            lst.append(b1)  # < ---- add 'CNR number
                            res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except Exception as e:
                            print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {"ProgramError": str(e)}

                        try:
                            # <---2nd Table, take single table data, into list ------------>
                            l2 = []
                            table = soup.find('table', attrs={'class': 'table_r'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l2.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst2 = reduce(lambda x, y: x + y, l2)
                            res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in range(0, len(lst2), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct2 = {}

                        try:
                            # <---3rd Table, take single table data, into list ----------->
                            l3 = []
                            table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l3.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst3 = reduce(lambda x, y: x + y, l3)
                            listToStr = ' '.join(map(str, lst3))
                            res_dct3 = listToStr.replace('\xa0', '')  # <-- make kye 'LOWER case', remove 'space' ---
                        except:
                            res_dct3 = ''

                        try:
                            # <---4th Table, take single table data, into list ---------->
                            l4 = []
                            table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l4.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst4 = reduce(lambda x, y: x + y, l4)
                            listToStr2 = ' '.join(map(str, lst4))
                            res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct4 = ''

                        try:
                            # <---5th Table, take single table data, into list ---------->
                            table_a = soup.find('table', attrs={'class': 'acts_table'})
                            h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                            res_dct5 = [dict(zip(h, i)) for i in d]

                            k5 = []
                            for d in res_dct5:
                                k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                    'UnderSection(s)', 'Section'): v for k, v in d.items()})
                        except:
                            k5 = []

                        try:
                            # <---6th Table, take single table data, into list ---------->
                            table_t = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                            res_dct6 = [dict(zip(h, i)) for i in d]

                            k6 = []
                            for d in res_dct6:
                                k6.append(
                                    {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                     d.items()})
                        except:
                            k6 = []

                        try:
                            # <---7th Table, take single table data, into list ---------->
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]

                            k7 = []
                            for d7 in res_dct7:
                                k7.append(
                                    {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                        except:
                            k7 = []

                        try:
                            # <---8th Table, take single table data, into list ---------->
                            table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                            h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                            res_dct8 = [dict(zip(h1, x)) for x in q]

                            k8 = []
                            for d in res_dct8:
                                k8.append({k.replace(' ', ''): v for k, v in d.items()})
                        except:
                            k8 = []

                        main_ch2 = 1

                        if res_dct != {}:
                            case_final = {"CaseDetails": res_dct, "CaseStatus": res_dct2,
                                          "PetitionerAdvocate": res_dct3,
                                          "RespondentAdvocate": res_dct4, "Acts": k5,
                                          "History": k6, "Orders": k7, "CaseTransferDetails": k8}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {"CaseDetailsNotFound": 'Please try again.'}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.close()
                                break
                            except:
                                pass
                except Exception as e:
                    try:
                        driver.close()
                        # print('except crser here--', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {"ProgramError": str(e)}
                    except:
                        pass

                if main_ch2 == 1:
                    break

            if res_dct != {}:
                yourdata = case_final

            elif res_dct == {}:
                yourdata = case_final_2

            data = yourdata

            try:
                conn = psycopg2.connect(database="defaultdb", user="doadmin", password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")

                cursor = conn.cursor()
                # print('psycopg2 connect execute--')

                var_dict = {"var": 0, "data": 1}
                # <-- for write --
                with open('Scrap_App/files/case_9.txt', 'w') as json_file:
                    json.dump(var_dict, json_file)

            except (Exception, psycopg2.DatabaseError) as error:
                # print("Error while creating PostgreSQL table", error)
                pass
            else:

                cursor.execute('SELECT * FROM public."Scrap_App_case_9" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_case_9" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                # print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------//Case function 1 End //--------------------------------------------------

        # multiprocessing.Process(target=Case_f1).start()
        threading.Thread(target=Case_9_fun).start()
        # print('thred--')

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            # wait = Case_9.objects.all().order_by('-id')[0].data

            # <-- for read ---
            with open('Scrap_App/files/case_9.txt', 'r') as f:
                _dict = json.load(f)
                # print(_dict['var'])  # 0
            # print('wait data:- ', _dict['data'])
            time.sleep(5)
            wait = _dict['data']  # 0

            # print('wait ON-')
            # print('wait OFF-')
            if wait == 0:
                w = 0
            else:
                w = 1
            if w == 1:
                # print('out while..')
                break
        # ------// --- //----
        time.sleep(3)

        data = Case_9.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        c_id = Case_9.objects.all().order_by('-id')[0].id
        c_id2 = Case_9.objects.get(id=c_id)
        c_id2.var = 0  # for make 1 to 0
        c_id2.save()

        # finish = time.perf_counter()
        # print(f'Case_9 Finished in {round(finish - start, 2)} second(s)')
# ------------------(cwork9e)----------------------

    elif Case_10.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()

        var_dict = {"var": 1, "data": 0}
        # <-- for write --
        with open('Scrap_App/files/case_10.txt', 'w') as json_file:
            json.dump(var_dict, json_file)

        Case_10(var=1, time=time.time(), state=state, dist=dist, complex=complex, casetype=casetype, caseno=caseno,
                year=year).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        #print('Case_10 input data:--- ', state, dist, complex, casetype, caseno, year)

        state_data = Case_10.objects.all().order_by('-id')[0].state
        dist_data = Case_10.objects.all().order_by('-id')[0].dist
        complex_data = Case_10.objects.all().order_by('-id')[0].complex
        casetype_data = Case_10.objects.all().order_by('-id')[0].casetype
        caseno_data = Case_10.objects.all().order_by('-id')[0].caseno
        year_data = Case_10.objects.all().order_by('-id')[0].year

        # ------------------(cwork10)----------------------

        def Case_10_fun():
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")
            global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            # driver.maximize_window()

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')

            #print('Case_10_fun')

            # <--- all repeated Functions ----/////---
            def dynamicdelay(id_name):
                # print('dynamicdelay:-', id_name)
                global job_profiles
                loop_1 = 1
                while True:
                    L = 0
                    if loop_1 == 1:
                        # print('D while:-')
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        time.sleep(1)
                        all_divs = soup.find('select', {'id': str(id_name)})
                        job_profiles = all_divs.find_all('option')
                        # print('job_profiles:-', job_profiles)
                        loop_1 = len(job_profiles)
                        # print('--', loop_1)
                        L = 0
                    else:
                        L = 1

                    if L == 1:
                        # print('D break:-')
                        break

                return job_profiles

            def delay1():
                time.sleep(random.randint(1, 2))  # 1sec

            def delay():
                time.sleep(random.randint(2, 2))  # 2sec

            # ---/ op / ---
            state_ = state_data
            dist_ = dist_data
            complex_ = complex_data
            casetype_ = casetype_data
            caseno_ = caseno_data
            year_ = year_data
            # ----------------// our code //-----------------

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "menuPage")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="menuPage"]/div/div[1]/ul/li[2]')))  # wait
                    driver.find_elements_by_xpath('//*[@id="menuPage"]/div/div[1]/ul/li[2]')[0].click()
                    # delay1()
                    # for alert OK btn
                    # WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "bs_alert")))  # wait
                    WebDriverWait(driver, 900).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="bs_alert"]/div/div/div[2]/button')))  # wait
                    driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                    # delay1()

                    # <--------STATE--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "divLangState")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_state_code'})
                    state_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='sess_state_code')
                    # print('job_profiles:-', job_profiles)

                    k = [str(x).split('"')[1] for x in job_profiles]  # <-- output  ['0', '28', '2', '6',..]
                    # print('k:-', k)

                    # <---- take input state value from fun() & find index of that input value ----
                    index = k.index(state_)
                    state_option = index + 1  # op-- 20th actual is 21th

                    # <------ Click on 'STATE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_state_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'STATE' Options ---->
                    state = driver.find_elements_by_xpath('//*[@id="sess_state_code"]/option[{}]'.format(state_option))[
                        0]  # option 1
                    # print('selected state:-- ', state.text)
                    state.click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    # <--------STATE---///--End----///--------->

                    # <--------DISTRICT--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "sess_dist_code")))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_dist_code'})
                    dist_job_profiles = all_divs.find_all('option')
                    # print('dist_job_profiles', dist_job_profiles)'''

                    dynamicdelay(id_name='sess_dist_code')

                    k2 = [str(j).split('"')[1] for j in job_profiles]
                    # print('k2:-', k2)

                    # <---- take input state value from fun() & find index of that input value ----
                    index2 = k2.index(dist_)
                    dist_option = index2 + 1  # op-- 20th actual is 21th

                    # <------ Click on 'DISTRICT' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    # <------ Select 'DISTRICT' Options ---->
                    district = \
                        driver.find_elements_by_xpath('//*[@id="sess_dist_code"]/option[{}]'.format(dist_option))[0]
                    # option 2
                    # print('selected dist:-- ', district.text)
                    district.click()
                    # delay()
                    # <--------DISTRICT---///--End----///--------->

                    # <--------COMPLEX--///---start---///-->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.ID, "court_complex_code")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    complex_job_profiles = all_divs.find_all('option')'''

                    dynamicdelay(id_name='court_complex_code')

                    k3 = [str(n).split('"')[1] for n in job_profiles]
                    # print('k3:- ', k3, 'len(k3:-)', len(k3), 'len(complex_job_profiles:-)', len(job_profiles))

                    # <---- take input complex_ value from fun()
                    com = complex_.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait

                    # <------ Select 'COMPLEX' Options ---->
                    complex = \
                        driver.find_elements_by_xpath(
                            '//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    # option 2
                    # print('selected complex: ', complex.text)
                    complex.click()
                    # delay()
                    # delay1()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    # <--------COMPLEX---///--End----///--------->

                    # <--------CLICK CASE NO. BTN--///------>
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CScaseNumber")))  # <---- wait --->
                    driver.find_elements_by_xpath('//*[@id="CScaseNumber"]')[0].click()
                    # delay()
                    # delay()

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    '''
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'case_type'})
                    case_job_profiles = all_divs.find_all('option')
                    # print('case_job_profiles', case_job_profiles)'''

                    dynamicdelay(id_name='case_type')

                    k4 = [str(n1).split('"')[1] for n1 in job_profiles]
                    # print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(case_job_profiles:-)', len(job_profiles))

                    case = casetype_.replace('_', '^')
                    index4 = k4.index(str(case))
                    case_option = index4 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    # delay()
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[0]
                    # print('selected case: ', case.text)
                    case.click()
                    # delay1()
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    # WebDriverWait(driver, 600).until(
                    #     EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->
                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(caseno_)  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(year_)  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # Find_data----- START --
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_class_name('refresh-btn').click()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        # delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        driver.execute_script("document.body.style.zoom='170%'")
                        driver.execute_script("window.scrollTo(0,400);")
                        delay1()
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        # start2 = time.perf_counter()
                        driver.find_element_by_xpath("//*[@id='captcha_image']")

                        # # --------------(for SHOW chrome)--------
                        # location = {'x': 219, 'y': 380}
                        # size = {'height': 59, 'width': 310}

                        # --------------(for hide windo)--------
                        location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        # ----------------------------------------

                        driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_10.png')
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_10.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_10.png')
                        # im.show()  # <--- comment it ----

                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_10.png')
                        width, height = im2.size

                        # # --------------(for SHOW chrome)--------
                        # left = 0
                        # top = 0  # done
                        # right = 175
                        # bottom = 57  # done
                        # # ----------------------

                        # --------------(for hide windo)--------
                        left = 30  # ... run with option
                        top = 0  # done
                        right = 220  # done
                        bottom = 57  # done
                        # ----------------------

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_10.png')  # row img
                        # im1.show()
                        # >-----------------------

                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_10.png')

                        # <-------- improve image quality ----------->
                        # img.save("quality_abc_2.png", quality=200)
                        contrast = ImageEnhance.Contrast(img)
                        contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_10.png')

                        # <----- for remove unwanted color from image ---------->
                        image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_10.png')
                        grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                        lower_range = np.array([0, 0, 0])
                        upper_range = np.array([0, 0, 0])

                        mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_10.png', mask)
                        # <-----Done remove unwanted color from image ---------->

                        # <------ image color change in black n white ------
                        Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_10.png', 0)
                        ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_10.png', thresh_img)
                        cv2.waitKey(0)
                        cv2.destroyAllWindows()

                        # <----- for expand image text ---------------------
                        Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_10.png', 0)
                        kernel = np.ones((3, 3), np.uint8)
                        erosion = cv2.erode(Load2, kernel, iterations=1)
                        img = erosion.copy()
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_10.png', img)

                        # <----- for Captcha image to text convert---------------------
                        img2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_10.png')
                        captcha_txt2 = tess.image_to_string(img2, config='')
                        # finish2 = time.perf_counter()
                        # print(f'OLD CODE captcha reading Finished in {round(finish2 - start2, 2)} second(s)')

                        # <------- Find_data--------/// END ///---------------------------

                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 8:
                                # print('!=8 --')

                                # ------------------------------
                                driver.find_element_by_class_name('refresh-btn').click()
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                # delay1()
                                WebDriverWait(driver, 600).until(
                                    EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                driver.execute_script("document.body.style.zoom='170%'")
                                driver.execute_script("window.scrollTo(0,400);")
                                delay1()
                                # ----------------------
                                # <--- to find captcha image location ---->
                                driver.find_element_by_xpath("//*[@id='captcha_image']")
                                # start3 = time.perf_counter()

                                # # --------------(for SHOW chrome)--------
                                # location = {'x': 219, 'y': 380}
                                # size = {'height': 59, 'width': 310}

                                # --------------(for hide windo)--------
                                location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                # ----------------------------------------

                                driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_10.png')
                                # driver.get_screenshot_as_file(settings.MEDIA_ROOT + '/CaseImages/imageCASE_10.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_10.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_10.png')
                                # im.show()  # <--- comment it ----
                                # <--------

                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_10.png')
                                width, height = im2.size

                                # # --------------(for SHOW chrome)--------
                                # left = 0
                                # top = 0  # done
                                # right = 175
                                # bottom = 57  # done
                                # # ----------------------

                                # --------------(for hide windo)--------
                                left = 30  # ... run with option
                                top = 0  # done
                                right = 220  # done
                                bottom = 57  # done
                                # ----------------------

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/ab4.png')
                                # im1.show()
                                # >-----------------------

                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                # delay()

                                # for Captcha image to text convert---------------comment it------------
                                img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_10.png')

                                # <-------- improve image quality ----------->
                                contrast = ImageEnhance.Contrast(img)
                                contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_10.png')

                                # <----- for remove unwanted color from image ---------->
                                image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_10.png')
                                grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                                grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                                lower_range = np.array([0, 0, 0])
                                upper_range = np.array([0, 0, 0])

                                mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_10.png', mask)
                                # <-----Done remove unwanted color from image ---------->

                                # <------ image color change in black n white ------
                                Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_10.png', 0)
                                ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_10.png', thresh_img)
                                cv2.waitKey(0)
                                cv2.destroyAllWindows()

                                # <----- for expand image text ---------------------
                                Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_10.png', 0)
                                kernel = np.ones((3, 3), np.uint8)
                                erosion = cv2.erode(Load2, kernel, iterations=1)
                                img = erosion.copy()
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_10.png', img)

                                # <----- for Captcha image to text convert---------------------
                                img3 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_10.png')
                                re_captcha_txt = tess.image_to_string(img3, config='')
                                # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)

                                # finish3 = time.perf_counter()
                                # print(f'captcha reading Finished in {round(finish3 - start3, 2)} second(s)')

                                captcha_txt2 = re_captcha_txt
                                if len(captcha_txt2) != 8:
                                    # print('if len != 8--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                    # print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                # print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                # print('finaly break done in <6 or >6--')
                                break

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                        input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        # <----- for Click Search btn---------------------

                        # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                        driver.find_element_by_xpath('//*[@id="goResetDiv"]/input[1]').click()  # <--- sertch btn --->

                        # print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        delay()
                        delay()

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display: none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        # html = driver.page_source
                        # soup = BeautifulSoup(html, "html.parser")
                        op_page_bs_alertDiv = soup.find('div', {'id': 'bs_alert'}).get('style')
                        # -----For Invalid captcha page("display: block;")
                        # print('op_page_bs_alertDiv--- ', op_page_bs_alertDiv)
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '':
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                delay()
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                # print('check2--r')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.close()
                                refresh = 1

                        elif op_page_bs_alertDiv == "display: block;":
                            # print('alert 1 ------')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                            refresh = 0

                        elif op_page_bs_alertDiv == "display: block; padding-right: 15px;":
                            # print('alert 2 ---Enter year from 1901 to---')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()

                            res_dct = {}
                            case_final_2 = {'EnterYearFrom1901': 'Enter year from 1901'}
                            Record_not_found = 1
                            driver.close()
                            refresh = 1

                        else:
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            break
                    if Record_not_found == 1:
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        WebDriverWait(driver, 600).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # <---- wait --->
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_showListDiv = soup.find('div', {'id': 'showList'}).get('style')
                        # -----For Invalid captcha page ("display:none;")

                        driver.execute_script("window.scrollTo(0,300);")
                        time.sleep(1)
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # ----------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "caseHistoryDiv")))
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bckbtn"]')))
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            l = []
                            table_case = soup.find('table', attrs={'class': 'case_details_table'})
                            table_case_body = table_case.find('tbody')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values

                            b = l[:len(l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                            b1 = l[-1][1].replace('\xa0\xa0 (Note the CNR number for future reference)', '')
                            # <-- for remove last "(Note the CNR number for future reference)" text o/p-- "MHPU050000132021"

                            lst = reduce(lambda x, y: x + y, b)
                            lst.append('cnrnumber')  # < ---- add 'CNR' name
                            lst.append(b1)  # < ---- add 'CNR number
                            res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except Exception as e:
                            print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {"ProgramError": str(e)}

                        try:
                            # <---2nd Table, take single table data, into list ------------>
                            l2 = []
                            table = soup.find('table', attrs={'class': 'table_r'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l2.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst2 = reduce(lambda x, y: x + y, l2)
                            res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in range(0, len(lst2), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct2 = {}

                        try:
                            # <---3rd Table, take single table data, into list ----------->
                            l3 = []
                            table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l3.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst3 = reduce(lambda x, y: x + y, l3)
                            listToStr = ' '.join(map(str, lst3))
                            res_dct3 = listToStr.replace('\xa0', '')  # <-- make kye 'LOWER case', remove 'space' ---
                        except:
                            res_dct3 = ''

                        try:
                            # <---4th Table, take single table data, into list ---------->
                            l4 = []
                            table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l4.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst4 = reduce(lambda x, y: x + y, l4)
                            listToStr2 = ' '.join(map(str, lst4))
                            res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct4 = ''

                        try:
                            # <---5th Table, take single table data, into list ---------->
                            table_a = soup.find('table', attrs={'class': 'acts_table'})
                            h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                            res_dct5 = [dict(zip(h, i)) for i in d]

                            k5 = []
                            for d in res_dct5:
                                k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                    'UnderSection(s)', 'Section'): v for k, v in d.items()})
                        except:
                            k5 = []

                        try:
                            # <---6th Table, take single table data, into list ---------->
                            table_t = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                            res_dct6 = [dict(zip(h, i)) for i in d]

                            k6 = []
                            for d in res_dct6:
                                k6.append(
                                    {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                     d.items()})
                        except:
                            k6 = []

                        try:
                            # <---7th Table, take single table data, into list ---------->
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]

                            k7 = []
                            for d7 in res_dct7:
                                k7.append(
                                    {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                        except:
                            k7 = []

                        try:
                            # <---8th Table, take single table data, into list ---------->
                            table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                            h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                            res_dct8 = [dict(zip(h1, x)) for x in q]

                            k8 = []
                            for d in res_dct8:
                                k8.append({k.replace(' ', ''): v for k, v in d.items()})
                        except:
                            k8 = []

                        main_ch2 = 1

                        if res_dct != {}:
                            case_final = {"CaseDetails": res_dct, "CaseStatus": res_dct2,
                                          "PetitionerAdvocate": res_dct3,
                                          "RespondentAdvocate": res_dct4, "Acts": k5,
                                          "History": k6, "Orders": k7, "CaseTransferDetails": k8}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {"CaseDetailsNotFound": 'Please try again.'}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.close()
                                break
                            except:
                                pass
                except Exception as e:
                    try:
                        driver.close()
                        # print('except crser here--', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {"ProgramError": str(e)}
                    except:
                        pass

                if main_ch2 == 1:
                    break

            if res_dct != {}:
                yourdata = case_final

            elif res_dct == {}:
                yourdata = case_final_2

            data = yourdata

            try:
                conn = psycopg2.connect(database="defaultdb", user="doadmin", password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")

                cursor = conn.cursor()
                # print('psycopg2 connect execute--')

                var_dict = {"var": 0, "data": 1}
                # <-- for write --
                with open('Scrap_App/files/case_10.txt', 'w') as json_file:
                    json.dump(var_dict, json_file)

            except (Exception, psycopg2.DatabaseError) as error:
                # print("Error while creating PostgreSQL table", error)
                pass
            else:

                cursor.execute('SELECT * FROM public."Scrap_App_case_10" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_case_10" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                # print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------//Case function 1 End //--------------------------------------------------

        # multiprocessing.Process(target=Case_f1).start()
        threading.Thread(target=Case_10_fun).start()
        # print('thred--')

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            # wait = Case_10.objects.all().order_by('-id')[0].data

            # <-- for read ---
            with open('Scrap_App/files/case_10.txt', 'r') as f:
                _dict = json.load(f)
                # print(_dict['var'])  # 0
            # print('wait data:- ', _dict['data'])
            time.sleep(5)
            wait = _dict['data']  # 0

            # print('wait ON-')
            # print('wait OFF-')
            if wait == 0:
                w = 0
            else:
                w = 1
            if w == 1:
                # print('out while..')
                break
        # ------// --- //----
        time.sleep(3)

        data = Case_10.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        c_id = Case_10.objects.all().order_by('-id')[0].id
        c_id2 = Case_10.objects.get(id=c_id)
        c_id2.var = 0  # for make 1 to 0
        c_id2.save()

        # finish = time.perf_counter()
        # print(f'Case_10 Finished in {round(finish - start, 2)} second(s)')


# ------------------(cwork10e)----------------------


    return HttpResponse(str(data).replace("'", '"'))



#<-----------------/* CNR No */------------------
def CnrNoView(request, slug):
    if Count1.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()
        Count1(var=1, time=time.time(), slug=slug).save()                               # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        #print('slug_1: ', slug)

        slug_data = Count1.objects.all().order_by('-id')[0].slug
        # ------------------(cnr_work1)----------------------
        def f1():
            global res_dct, final, final_2, re_captcha_txt_final, re_captcha_txt_final
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            # print('f1--')

            # ---/ op / ---
            slug_ = slug_data
            # -------------------------// our code //---------------
            # <--- all repeated Functions ----/////---
            def delay1():
                time.sleep(random.randint(1, 2))
            def delay():
                time.sleep(random.randint(2, 2))

            while True:
                main_ch = 0
                try:
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    div_style_value = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # ----- get value "display: none;"

                    # Find_data----- START --
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.XPATH, "//*[@id='cino']")))  # wait
                    elem = driver.find_element(By.ID, "cino")
                    elem.send_keys(slug_)  ##### <---- Pass CNR no ---->

                    # <----- for maximise windo upto 170% & scroll down --->
                    driver.execute_script("document.body.style.zoom='170%'")
                    driver.execute_script("window.scrollTo(0,400);")
                    delay1()

                    # <--- to find captcha image location ---->
                    element = driver.find_element_by_xpath("//*[@id='captcha_image']")
                    location = {'x': 240, 'y': 260}  # -for without hide chrome {'x': 240, 'y': 280}
                    size = {'height': 61, 'width': 310}  # -for without hide chrome {'height': 59, 'width': 310}

                    driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR.png')

                    x = location['x']
                    y = location['y']
                    width = location['x'] + size['width']
                    height = location['y'] + size['height']

                    im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR.png')
                    im = im.crop((int(x), int(y), int(width), int(height)))
                    im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR.png')
                    delay1()

                    # <-------- for crop save captcha image in propper size ----// START //--
                    im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR.png')
                    width, height = im2.size
                    #print('w: ', width, 'h: ', height)
                    # Setting the points for cropped image
                    left = 59  # -for without hide chrome "left = 146"
                    top = 0  # done
                    right = 250  # done                      # -for without hide chrome "right = 310"
                    bottom = 57  # done

                    # Cropped image of above dimension
                    im1 = im2.crop((left, top, right, bottom))
                    im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR.png')
                    #im1.show()  # <-- comment it ---
                    # <-------- for crop save captcha image in propper size ---// END //---

                    # <---- for Captcha image to text convert---------------comment it------------
                    img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR.png')

                    # <-------- improve image quality ----------->
                    contrast = ImageEnhance.Contrast(img)
                    contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR.png')
                    # <------------------->

                    # <----- for remove unwanted color from image ---------->
                    image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR.png')
                    grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                    grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                    lower_range = np.array([0, 0, 0])
                    upper_range = np.array([0, 0, 0])

                    mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR.png', mask)
                    # <-----Done remove unwanted color from image ---------->

                    # <------ image color change in black n white ------
                    Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR.png', 0)
                    ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR.png', thresh_img)
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                    # <----- for expand image text ---------------------
                    Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR.png', 0)
                    kernel = np.ones((3, 3), np.uint8)
                    erosion = cv2.erode(Load2, kernel, iterations=1)
                    img = erosion.copy()
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR.png', img)

                    # <----- for Captcha image to text convert---------------------
                    img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR.png')
                    captcha_txt2 = tess.image_to_string(img2, config='')
                    delay1()

                    # Find_data-------- END ----------------//////-----------

                    # <---- driver back to original position ----
                    driver.execute_script("document.body.style.zoom='100%'")
                    driver.execute_script("window.scrollTo(0,0);")
                    delay1()

                    # --------------------------------
                    # ---------------------------------
                    while True:
                        length_ch = 0
                        if len(captcha_txt2) != 8:
                            # Re_ScreenShot_captcha()
                            # ------------------------------
                            driver.find_element_by_class_name('refresh-btn').click()
                            driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr

                            # <----- for maximise windo upto 170% & scroll down --->
                            driver.execute_script("document.body.style.zoom='170%'")
                            driver.execute_script("window.scrollTo(0,400);")
                            delay1()

                            element = driver.find_element_by_xpath("//*[@id='captcha_image']")

                            location = {'x': 240, 'y': 260}
                            size = {'height': 61, 'width': 310}

                            driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR.png')

                            x = location['x']
                            y = location['y']
                            width = location['x'] + size['width']
                            height = location['y'] + size['height']

                            im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR.png')
                            im = im.crop((int(x), int(y), int(width), int(height)))

                            im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR.png')
                            delay1()
                            # <-------- for crop save captcha image in propper size ----// START //--
                            im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR.png')
                            width, height = im2.size

                            left = 59
                            top = 0  # done
                            right = 250  # done
                            bottom = 57  # done

                            # Cropped image of above dimension
                            im1 = im2.crop((left, top, right, bottom))
                            im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR.png')
                            # <-------- for crop save captcha image in propper size ---// END //---

                            # for Captcha image to text convert---------------comment it------------
                            img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR.png')
                            captcha_txt = tess.image_to_string(img, config='')

                            # <-------- improve image quality ----------->
                            contrast = ImageEnhance.Contrast(img)
                            contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR.png')
                            # <------------------->

                            # <----- for remove unwanted color from image ---------->
                            image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR.png')
                            grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                            grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                            lower_range = np.array([0, 0, 0])
                            upper_range = np.array([0, 0, 0])

                            mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR.png', mask)
                            # <-----Done remove unwanted color from image ---------->

                            # <------ image color change in black n white ------
                            Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR.png', 0)
                            ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR.png', thresh_img)
                            cv2.waitKey(0)
                            cv2.destroyAllWindows()

                            # <----- for expand image text ---------------------
                            Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR.png', 0)
                            kernel = np.ones((3, 3), np.uint8)  # <---- (2, 2)  also work

                            erosion = cv2.erode(Load2, kernel, iterations=1)
                            img = erosion.copy()
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR.png', img)

                            # <----- for Captcha image to text convert---------------------
                            img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR.png')
                            # global re_captcha_txt
                            re_captcha_txt = tess.image_to_string(img2, config='')
                            # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)
                            captcha_txt2 = re_captcha_txt

                            # ---------------end---------------------
                            # <---- driver back to original position ----
                            driver.execute_script("document.body.style.zoom='100%'")
                            driver.execute_script("window.scrollTo(0,0);")
                            delay1()

                            if len(captcha_txt2) != 8:
                                length_ch = 0
                                delay1()
                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                #print('re_captcha_txt_final:-', re_captcha_txt_final)

                        else:
                            re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                            #print('re_captcha_txt_final:-', re_captcha_txt_final)
                            length_ch = 1

                        if length_ch == 1:
                            break

                    # <----- for Enter Captcha image text to input field---------------------
                    input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                    input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                    # <----- for Click Search btn---------------------

                    # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                    driver.find_element_by_xpath("//*[@id='searchbtn']").click()  # <--- sertch btn --->
                    # Enter_Captcha_image_text_to_input_field ---- END --------------//////-----------


                except:
                    delay()
                    delay()
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    op_page_caseHistoryDiv = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # -----For Invalid captcha page ("display: block;")

                    if op_page_caseHistoryDiv == 'display: block;':
                        all_divs = soup.find('div', {'id': 'caseHistoryDiv'})
                        all_tables = all_divs.find_all('p')

                        check = str(all_tables)
                        if check == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                            driver.find_elements_by_xpath('//*[@id="bckbtn"]')[0].click()
                            delay()

                            main_ch = 0

                        else:
                            # <-----/// Final page table conversion start ///----->
                            try:
                                # <---1st Table, take single table data, into list ------------>
                                l = []
                                table = soup.find('table', attrs={'class': 'case_details_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l)

                                b = l[:len(
                                    l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                                lst = reduce(lambda x, y: x + y, b)
                                lst.append('cnrnumber')  # < ---- add 'CNR' name
                                lst.append(slug_)  # < ---- add 'CNR number
                                # print(lst)
                                res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in
                                           range(0, len(lst), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('1st Table:-- ', res_dct)
                            except:
                                res_dct = {}

                            try:
                                # <---2nd Table, take single table data, into list ------------>
                                l2 = []
                                table = soup.find('table', attrs={'class': 'table_r'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l2.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l2)

                                lst2 = reduce(lambda x, y: x + y, l2)
                                # print(lst2)
                                res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in
                                            range(0, len(lst2), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('2nd Table: ', res_dct2)
                            except:
                                res_dct2 = {}

                            try:
                                # <---3rd Table, take single table data, into list ----------->
                                l3 = []
                                table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l3.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('Petitioner_Advocate_table: ', l3)

                                lst3 = reduce(lambda x, y: x + y, l3)
                                listToStr = ' '.join(map(str, lst3))
                                # print(lst3)
                                res_dct3 = listToStr.replace('\xa0',
                                                             '')  # <-- make kye 'LOWER case', remove 'space' ---
                                # print('PetitionerAdvocate Table: ', res_dct3)
                            except:
                                res_dct3 = ''

                            try:
                                # <---4th Table, take single table data, into list ---------->
                                l4 = []
                                table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l4.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ', l4)

                                lst4 = reduce(lambda x, y: x + y, l4)
                                listToStr2 = ' '.join(map(str, lst4))
                                # print(lst4)
                                res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                                # print('4th Table: ', res_dct4)
                            except:
                                res_dct4 = ''

                            try:
                                # <---5th Table, take single table data, into list ---------->
                                table_a = soup.find('table', attrs={'class': 'acts_table'})
                                h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                                res_dct5 = [dict(zip(h, i)) for i in d]
                                # print('5th Table: ', res_dct5)

                                # k5 = res_dct5
                                # print(k5)

                                k5 = []
                                for d in res_dct5:
                                    k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                        'UnderSection(s)', 'Section'): v for k, v in d.items()})
                                # print('res_dct5: ', res_dct5)
                            except:
                                k5 = []

                            try:
                                # <---6th Table, take single table data, into list ---------->
                                table_t = soup.find('table', attrs={'class': 'history_table'})
                                h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                                res_dct6 = [dict(zip(h, i)) for i in d]


                                k6 = []
                                for d in res_dct6:
                                    k6.append(
                                        {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                         d.items()})
                                # print('k6---:', k6)
                            except:
                                k6 = []

                            try:
                                # <---7th Table, take single table data, into list ---------->
                                table_order = soup.find('table', attrs={'class': 'order_table'})
                                h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                    [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                                res_dct7 = [dict(zip(h7, x)) for x in q]
                                # print('7th Table: ', res_dct7)

                                k7 = []
                                for d7 in res_dct7:
                                    k7.append(
                                        {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                            except:
                                k7 = []

                            try:
                                # <---8th Table, take single table data, into list ---------->
                                table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                                h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                    [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                                res_dct8 = [dict(zip(h1, x)) for x in q]
                                # print('8th Table: ', res_dct8)

                                k8 = []
                                for d in res_dct8:
                                    k8.append({k.replace(' ', ''): v.replace('\xa0', '') for k, v in d.items()})
                                # # print(k8)
                            except:
                                k8 = []

                            main_ch = 1

                            if res_dct != {}:
                                # print("if res_dct != '{}':---- ", res_dct)
                                final = {"CaseDetails": res_dct, "CaseStatus": res_dct2, "PetitionerAdvocate": res_dct3,
                                         "RespondentAdvocate": res_dct4, "Acts": k5,
                                         "History": k6, "Orders": k7, "CaseTransferDetails": k8}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            elif res_dct == {}:
                                final_2 = {
                                    "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            else:
                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                    elif op_page_caseHistoryDiv == 'display: none;':
                        # <- if any captcha text miss, then 'Invalid Captcha' ALERT BOX ---
                        # alert_page = soup.find('div', {'id': 'bs_alert'}).get('style')  # ----- get value "true/false"
                        ok_btn = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok = ok_btn.is_enabled()  # value in True n false

                        if ok == True:
                            ok_btn.click()

                        driver.refresh()
                        main_ch = 0
                        delay()

                    elif op_page_caseHistoryDiv == 'display:none;':  # <-- submit empty catcha --- then alert popup
                        ok_btn2 = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok2 = ok_btn2.is_enabled()  # value in True n false

                        if ok2 == True:
                            ok_btn2.click()
                        driver.refresh()
                        main_ch = 0
                        delay()
                        #delay()
                if main_ch == 1:
                    break

            if res_dct != {}:
                yourdata = final

            elif res_dct == {}:
                yourdata = final_2

            data = yourdata
            try:
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")
                cursor = conn.cursor()
                #print('psycopg2 connect execute--')
            except (Exception, psycopg2.DatabaseError) as error:
                #print("Error while creating PostgreSQL table", error)
                pass
            else:
                cursor.execute('SELECT * FROM public."Scrap_App_count1" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_count1" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                #print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------// function 1 End //--------------------------------------------------

        # t = multiprocessing.Process(target=f1).start()
        t = threading.Thread(target=f1).start()

        #------// wait antil final data is coming //----
        while True:
            w=0
            wait = Count1.objects.all().order_by('-id')[0].data
            if wait=='':
                w=0
            else:
                w=1
            if w==1:
                break
        #------// --- //----
        time.sleep(3)
        data = Count1.objects.all().order_by('-id')[0].data

        #-----------current id make it 0 -------------
        aa_id2 = Count1.objects.all().order_by('-id')[0].id
        aa_id3=Count1.objects.get(id=aa_id2)
        aa_id3.var = 0                              # make 1 to 0 for available for next request.
        aa_id3.save()


# ------------------(cnr_work1e)----------------------

    elif Count2.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()
        Count2(var=1, time=time.time(),
                slug=slug).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        #print('slug_2: ', slug)

        slug_data = Count2.objects.all().order_by('-id')[0].slug

        # ------------------(cnr_work2)----------------------
        def f2():
            global res_dct, final, final_2, re_captcha_txt_final, re_captcha_txt_final, yourdata
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            # print('f2--')

            # ---/ op / ---
            slug_ = slug_data

            # -------------------------// our code //---------------
            # <--- all repeated Functions ----/////---
            def delay1():
                time.sleep(random.randint(1, 2))

            def delay():
                time.sleep(random.randint(2, 2))

            while True:
                main_ch = 0
                try:
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    div_style_value = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # ----- get value "display: none;"

                    # Find_data----- START --
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.XPATH, "//*[@id='cino']")))  # wait
                    elem = driver.find_element(By.ID, "cino")
                    elem.send_keys(slug_)  ##### <---- Pass CNR no ---->

                    # <----- for maximise windo upto 170% & scroll down --->
                    driver.execute_script("document.body.style.zoom='170%'")
                    driver.execute_script("window.scrollTo(0,400);")
                    delay1()

                    # <--- to find captcha image location ---->
                    element = driver.find_element_by_xpath("//*[@id='captcha_image']")
                    location = {'x': 240, 'y': 260}  # -for without hide chrome {'x': 240, 'y': 280}
                    size = {'height': 61, 'width': 310}  # -for without hide chrome {'height': 59, 'width': 310}

                    driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR2.png')

                    x = location['x']
                    y = location['y']
                    width = location['x'] + size['width']
                    height = location['y'] + size['height']

                    im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR2.png')
                    im = im.crop((int(x), int(y), int(width), int(height)))
                    im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR2.png')
                    delay1()

                    # <-------- for crop save captcha image in propper size ----// START //--
                    im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR2.png')
                    width, height = im2.size
                    #print('w: ', width, 'h: ', height)
                    # Setting the points for cropped image
                    left = 59  # -for without hide chrome "left = 146"
                    top = 0  # done
                    right = 250  # done                      # -for without hide chrome "right = 310"
                    bottom = 57  # done

                    # Cropped image of above dimension
                    im1 = im2.crop((left, top, right, bottom))
                    im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR2.png')
                    #im1.show()  # <-- comment it ---
                    # <-------- for crop save captcha image in propper size ---// END //---

                    # <---- for Captcha image to text convert---------------comment it------------
                    img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR2.png')

                    # <-------- improve image quality ----------->
                    contrast = ImageEnhance.Contrast(img)
                    contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR2.png')
                    # <------------------->

                    # <----- for remove unwanted color from image ---------->
                    image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR2.png')
                    grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                    grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                    lower_range = np.array([0, 0, 0])
                    upper_range = np.array([0, 0, 0])

                    mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR2.png', mask)
                    # <-----Done remove unwanted color from image ---------->

                    # <------ image color change in black n white ------
                    Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR2.png', 0)
                    ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR2.png', thresh_img)
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                    # <----- for expand image text ---------------------
                    Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR2.png', 0)
                    kernel = np.ones((3, 3), np.uint8)
                    erosion = cv2.erode(Load2, kernel, iterations=1)
                    img = erosion.copy()
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR2.png', img)

                    # <----- for Captcha image to text convert---------------------
                    img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR2.png')
                    captcha_txt2 = tess.image_to_string(img2, config='')
                    delay1()

                    # Find_data-------- END ----------------//////-----------

                    # <---- driver back to original position ----
                    driver.execute_script("document.body.style.zoom='100%'")
                    driver.execute_script("window.scrollTo(0,0);")
                    delay1()

                    # --------------------------------
                    # ---------------------------------
                    while True:
                        length_ch = 0
                        if len(captcha_txt2) != 8:
                            # Re_ScreenShot_captcha()
                            # ------------------------------
                            driver.find_element_by_class_name('refresh-btn').click()
                            driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr

                            # <----- for maximise windo upto 170% & scroll down --->
                            driver.execute_script("document.body.style.zoom='170%'")
                            driver.execute_script("window.scrollTo(0,400);")
                            delay1()

                            element = driver.find_element_by_xpath("//*[@id='captcha_image']")

                            location = {'x': 240, 'y': 260}
                            size = {'height': 61, 'width': 310}

                            driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR2.png')

                            x = location['x']
                            y = location['y']
                            width = location['x'] + size['width']
                            height = location['y'] + size['height']

                            im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR2.png')
                            im = im.crop((int(x), int(y), int(width), int(height)))

                            im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR2.png')
                            delay1()
                            # <-------- for crop save captcha image in propper size ----// START //--
                            im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR2.png')
                            width, height = im2.size

                            left = 59
                            top = 0  # done
                            right = 250  # done
                            bottom = 57  # done

                            # Cropped image of above dimension
                            im1 = im2.crop((left, top, right, bottom))
                            im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR2.png')
                            # <-------- for crop save captcha image in propper size ---// END //---

                            # for Captcha image to text convert---------------comment it------------
                            img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR2.png')
                            captcha_txt = tess.image_to_string(img, config='')

                            # <-------- improve image quality ----------->
                            contrast = ImageEnhance.Contrast(img)
                            contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR2.png')
                            # <------------------->

                            # <----- for remove unwanted color from image ---------->
                            image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR2.png')
                            grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                            grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                            lower_range = np.array([0, 0, 0])
                            upper_range = np.array([0, 0, 0])

                            mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR2.png', mask)
                            # <-----Done remove unwanted color from image ---------->

                            # <------ image color change in black n white ------
                            Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR2.png', 0)
                            ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR2.png', thresh_img)
                            cv2.waitKey(0)
                            cv2.destroyAllWindows()

                            # <----- for expand image text ---------------------
                            Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR2.png', 0)
                            kernel = np.ones((3, 3), np.uint8)  # <---- (2, 2)  also work

                            erosion = cv2.erode(Load2, kernel, iterations=1)
                            img = erosion.copy()
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR2.png', img)

                            # <----- for Captcha image to text convert---------------------
                            img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR2.png')
                            # global re_captcha_txt
                            re_captcha_txt = tess.image_to_string(img2, config='')
                            # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)
                            captcha_txt2 = re_captcha_txt

                            # ---------------end---------------------
                            # <---- driver back to original position ----
                            driver.execute_script("document.body.style.zoom='100%'")
                            driver.execute_script("window.scrollTo(0,0);")
                            delay1()

                            if len(captcha_txt2) != 8:
                                length_ch = 0
                                delay1()
                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')

                        else:
                            re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                            length_ch = 1

                        if length_ch == 1:
                            break

                    # <----- for Enter Captcha image text to input field---------------------
                    input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                    input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                    # <----- for Click Search btn---------------------

                    # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                    driver.find_element_by_xpath("//*[@id='searchbtn']").click()  # <--- sertch btn --->
                    # Enter_Captcha_image_text_to_input_field ---- END --------------//////-----------


                except:
                    delay()
                    delay()
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    op_page_caseHistoryDiv = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # -----For Invalid captcha page ("display: block;")

                    if op_page_caseHistoryDiv == 'display: block;':
                        all_divs = soup.find('div', {'id': 'caseHistoryDiv'})
                        all_tables = all_divs.find_all('p')

                        check = str(all_tables)
                        if check == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                            driver.find_elements_by_xpath('//*[@id="bckbtn"]')[0].click()
                            delay()

                            main_ch = 0

                        else:
                            # <-----/// Final page table conversion start ///----->
                            try:
                                # <---1st Table, take single table data, into list ------------>
                                l = []
                                table = soup.find('table', attrs={'class': 'case_details_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l)

                                b = l[:len(
                                    l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                                lst = reduce(lambda x, y: x + y, b)
                                lst.append('cnrnumber')  # < ---- add 'CNR' name
                                lst.append(slug_)  # < ---- add 'CNR number
                                # print(lst)
                                res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in
                                           range(0, len(lst), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('1st Table:-- ', res_dct)
                            except:
                                res_dct = {}

                            try:
                                # <---2nd Table, take single table data, into list ------------>
                                l2 = []
                                table = soup.find('table', attrs={'class': 'table_r'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l2.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l2)

                                lst2 = reduce(lambda x, y: x + y, l2)
                                # print(lst2)
                                res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in
                                            range(0, len(lst2), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('2nd Table: ', res_dct2)
                            except:
                                res_dct2 = {}

                            try:
                                # <---3rd Table, take single table data, into list ----------->
                                l3 = []
                                table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l3.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('Petitioner_Advocate_table: ', l3)

                                lst3 = reduce(lambda x, y: x + y, l3)
                                listToStr = ' '.join(map(str, lst3))
                                # print(lst3)
                                res_dct3 = listToStr.replace('\xa0',
                                                             '')  # <-- make kye 'LOWER case', remove 'space' ---
                                # print('PetitionerAdvocate Table: ', res_dct3)
                            except:
                                res_dct3 = ''

                            try:
                                # <---4th Table, take single table data, into list ---------->
                                l4 = []
                                table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l4.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ', l4)

                                lst4 = reduce(lambda x, y: x + y, l4)
                                listToStr2 = ' '.join(map(str, lst4))
                                # print(lst4)
                                res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                                # print('4th Table: ', res_dct4)
                            except:
                                res_dct4 = ''

                            try:
                                # <---5th Table, take single table data, into list ---------->
                                table_a = soup.find('table', attrs={'class': 'acts_table'})
                                h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                                res_dct5 = [dict(zip(h, i)) for i in d]
                                # print('5th Table: ', res_dct5)

                                # k5 = res_dct5
                                # print(k5)

                                k5 = []
                                for d in res_dct5:
                                    k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                        'UnderSection(s)', 'Section'): v for k, v in d.items()})
                                # print('res_dct5: ', res_dct5)
                            except:
                                k5 = []

                            try:
                                # <---6th Table, take single table data, into list ---------->
                                table_t = soup.find('table', attrs={'class': 'history_table'})
                                h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                                res_dct6 = [dict(zip(h, i)) for i in d]

                                k6 = []
                                for d in res_dct6:
                                    k6.append(
                                        {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                         d.items()})
                                # print('k6---:', k6)
                            except:
                                k6 = []

                            try:
                                # <---7th Table, take single table data, into list ---------->
                                table_order = soup.find('table', attrs={'class': 'order_table'})
                                h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                    [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                                res_dct7 = [dict(zip(h7, x)) for x in q]
                                # print('7th Table: ', res_dct7)

                                k7 = []
                                for d7 in res_dct7:
                                    k7.append(
                                        {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                            except:
                                k7 = []

                            try:
                                # <---8th Table, take single table data, into list ---------->
                                table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                                h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                    [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                                res_dct8 = [dict(zip(h1, x)) for x in q]
                                # print('8th Table: ', res_dct8)

                                k8 = []
                                for d in res_dct8:
                                    k8.append({k.replace(' ', ''): v.replace('\xa0', '') for k, v in d.items()})
                                # # print(k8)
                            except:
                                k8 = []

                            main_ch = 1

                            if res_dct != {}:
                                # print("if res_dct != '{}':---- ", res_dct)
                                final = {"CaseDetails": res_dct, "CaseStatus": res_dct2, "PetitionerAdvocate": res_dct3,
                                         "RespondentAdvocate": res_dct4, "Acts": k5,
                                         "History": k6, "Orders": k7, "CaseTransferDetails": k8}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            elif res_dct == {}:
                                final_2 = {
                                    "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            else:
                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                    elif op_page_caseHistoryDiv == 'display: none;':
                        # <- if any captcha text miss, then 'Invalid Captcha' ALERT BOX ---
                        # alert_page = soup.find('div', {'id': 'bs_alert'}).get('style')  # ----- get value "true/false"
                        ok_btn = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok = ok_btn.is_enabled()  # value in True n false

                        if ok == True:
                            ok_btn.click()

                        driver.refresh()
                        main_ch = 0
                        delay()

                    elif op_page_caseHistoryDiv == 'display:none;':  # <-- submit empty catcha --- then alert popup
                        ok_btn2 = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok2 = ok_btn2.is_enabled()  # value in True n false

                        if ok2 == True:
                            ok_btn2.click()
                        driver.refresh()
                        main_ch = 0
                        delay()
                        # delay()
                if main_ch == 1:
                    break

            if res_dct != {}:
                yourdata = final

            elif res_dct == {}:
                yourdata = final_2

            data = yourdata
            try:
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")
                cursor = conn.cursor()
                #print('psycopg2 connect execute--')
            except (Exception, psycopg2.DatabaseError) as error:
                #print("Error while creating PostgreSQL table", error)
                pass
            else:
                cursor.execute('SELECT * FROM public."Scrap_App_count2" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_count2" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                #print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------// function 1 End //--------------------------------------------------

        # t = multiprocessing.Process(target=f2).start()
        t = threading.Thread(target=f2).start()

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            wait = Count2.objects.all().order_by('-id')[0].data
            if wait == '':
                w = 0
            else:
                w = 1
            if w == 1:
                break
        # ------// --- //----
        time.sleep(3)
        data = Count2.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        aa_id2 = Count2.objects.all().order_by('-id')[0].id
        aa_id3 = Count2.objects.get(id=aa_id2)
        aa_id3.var = 0  # make 1 to 0 for available for next request.
        aa_id3.save()


# ------------------(cnr_work2e)----------------------

    elif Count3.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()
        Count3(var=1, time=time.time(),
                slug=slug).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        # print('slug_3: ', slug)

        slug_data = Count3.objects.all().order_by('-id')[0].slug

        # ------------------(cnr_work3)----------------------
        def f3():
            global res_dct, final, final_2, re_captcha_txt_final, re_captcha_txt_final
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            # print('f3--')

            # ---/ op / ---
            slug_ = slug_data

            # -------------------------// our code //---------------
            # <--- all repeated Functions ----/////---
            def delay1():
                time.sleep(random.randint(1, 2))

            def delay():
                time.sleep(random.randint(2, 2))

            while True:
                main_ch = 0
                try:
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    div_style_value = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # ----- get value "display: none;"

                    # Find_data----- START --
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.XPATH, "//*[@id='cino']")))  # wait
                    elem = driver.find_element(By.ID, "cino")
                    elem.send_keys(slug_)  ##### <---- Pass CNR no ---->

                    # <----- for maximise windo upto 170% & scroll down --->
                    driver.execute_script("document.body.style.zoom='170%'")
                    driver.execute_script("window.scrollTo(0,400);")
                    delay1()

                    # <--- to find captcha image location ---->
                    element = driver.find_element_by_xpath("//*[@id='captcha_image']")
                    location = {'x': 240, 'y': 260}  # -for without hide chrome {'x': 240, 'y': 280}
                    size = {'height': 61, 'width': 310}  # -for without hide chrome {'height': 59, 'width': 310}

                    driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR3.png')

                    x = location['x']
                    y = location['y']
                    width = location['x'] + size['width']
                    height = location['y'] + size['height']

                    im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR3.png')
                    im = im.crop((int(x), int(y), int(width), int(height)))
                    im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR3.png')
                    delay1()

                    # <-------- for crop save captcha image in propper size ----// START //--
                    im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR3.png')
                    width, height = im2.size
                    #print('w: ', width, 'h: ', height)
                    # Setting the points for cropped image
                    left = 59  # -for without hide chrome "left = 146"
                    top = 0  # done
                    right = 250  # done                      # -for without hide chrome "right = 310"
                    bottom = 57  # done

                    # Cropped image of above dimension
                    im1 = im2.crop((left, top, right, bottom))
                    im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR3.png')
                    #im1.show()  # <-- comment it ---
                    # <-------- for crop save captcha image in propper size ---// END //---

                    # <---- for Captcha image to text convert---------------comment it------------
                    img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR3.png')

                    # <-------- improve image quality ----------->
                    contrast = ImageEnhance.Contrast(img)
                    contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR3.png')
                    # <------------------->

                    # <----- for remove unwanted color from image ---------->
                    image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR3.png')
                    grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                    grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                    lower_range = np.array([0, 0, 0])
                    upper_range = np.array([0, 0, 0])

                    mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR3.png', mask)
                    # <-----Done remove unwanted color from image ---------->

                    # <------ image color change in black n white ------
                    Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR3.png', 0)
                    ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR3.png', thresh_img)
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                    # <----- for expand image text ---------------------
                    Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR3.png', 0)
                    kernel = np.ones((3, 3), np.uint8)
                    erosion = cv2.erode(Load2, kernel, iterations=1)
                    img = erosion.copy()
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR3.png', img)

                    # <----- for Captcha image to text convert---------------------
                    img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR3.png')
                    captcha_txt2 = tess.image_to_string(img2, config='')
                    delay1()

                    # Find_data-------- END ----------------//////-----------

                    # <---- driver back to original position ----
                    driver.execute_script("document.body.style.zoom='100%'")
                    driver.execute_script("window.scrollTo(0,0);")
                    delay1()

                    # --------------------------------
                    # ---------------------------------
                    while True:
                        length_ch = 0
                        if len(captcha_txt2) != 8:
                            # Re_ScreenShot_captcha()
                            # ------------------------------
                            driver.find_element_by_class_name('refresh-btn').click()
                            driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr

                            # <----- for maximise windo upto 170% & scroll down --->
                            driver.execute_script("document.body.style.zoom='170%'")
                            driver.execute_script("window.scrollTo(0,400);")
                            delay1()

                            element = driver.find_element_by_xpath("//*[@id='captcha_image']")

                            location = {'x': 240, 'y': 260}
                            size = {'height': 61, 'width': 310}

                            driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR3.png')

                            x = location['x']
                            y = location['y']
                            width = location['x'] + size['width']
                            height = location['y'] + size['height']

                            im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR3.png')
                            im = im.crop((int(x), int(y), int(width), int(height)))

                            im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR3.png')
                            delay1()
                            # <-------- for crop save captcha image in propper size ----// START //--
                            im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR3.png')
                            width, height = im2.size

                            left = 59
                            top = 0  # done
                            right = 250  # done
                            bottom = 57  # done

                            # Cropped image of above dimension
                            im1 = im2.crop((left, top, right, bottom))
                            im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR3.png')
                            # <-------- for crop save captcha image in propper size ---// END //---

                            # for Captcha image to text convert---------------comment it------------
                            img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR3.png')
                            captcha_txt = tess.image_to_string(img, config='')

                            # <-------- improve image quality ----------->
                            contrast = ImageEnhance.Contrast(img)
                            contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR3.png')
                            # <------------------->

                            # <----- for remove unwanted color from image ---------->
                            image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR3.png')
                            grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                            grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                            lower_range = np.array([0, 0, 0])
                            upper_range = np.array([0, 0, 0])

                            mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR3.png', mask)
                            # <-----Done remove unwanted color from image ---------->

                            # <------ image color change in black n white ------
                            Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR3.png', 0)
                            ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR3.png', thresh_img)
                            cv2.waitKey(0)
                            cv2.destroyAllWindows()

                            # <----- for expand image text ---------------------
                            Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR3.png', 0)
                            kernel = np.ones((3, 3), np.uint8)  # <---- (2, 2)  also work

                            erosion = cv2.erode(Load2, kernel, iterations=1)
                            img = erosion.copy()
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR3.png', img)

                            # <----- for Captcha image to text convert---------------------
                            img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR3.png')
                            # global re_captcha_txt
                            re_captcha_txt = tess.image_to_string(img2, config='')
                            # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)
                            captcha_txt2 = re_captcha_txt

                            # ---------------end---------------------
                            # <---- driver back to original position ----
                            driver.execute_script("document.body.style.zoom='100%'")
                            driver.execute_script("window.scrollTo(0,0);")
                            delay1()

                            if len(captcha_txt2) != 8:
                                length_ch = 0
                                delay1()
                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')

                        else:
                            re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                            length_ch = 1

                        if length_ch == 1:
                            break

                    # <----- for Enter Captcha image text to input field---------------------
                    input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                    input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                    # <----- for Click Search btn---------------------

                    # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                    driver.find_element_by_xpath("//*[@id='searchbtn']").click()  # <--- sertch btn --->
                    # Enter_Captcha_image_text_to_input_field ---- END --------------//////-----------


                except:
                    delay()
                    delay()
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    op_page_caseHistoryDiv = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # -----For Invalid captcha page ("display: block;")

                    if op_page_caseHistoryDiv == 'display: block;':
                        all_divs = soup.find('div', {'id': 'caseHistoryDiv'})
                        all_tables = all_divs.find_all('p')

                        check = str(all_tables)
                        if check == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                            driver.find_elements_by_xpath('//*[@id="bckbtn"]')[0].click()
                            delay()

                            main_ch = 0

                        else:
                            # <-----/// Final page table conversion start ///----->
                            try:
                                # <---1st Table, take single table data, into list ------------>
                                l = []
                                table = soup.find('table', attrs={'class': 'case_details_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l)

                                b = l[:len(
                                    l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                                lst = reduce(lambda x, y: x + y, b)
                                lst.append('cnrnumber')  # < ---- add 'CNR' name
                                lst.append(slug_)  # < ---- add 'CNR number
                                # print(lst)
                                res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in
                                           range(0, len(lst), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('1st Table:-- ', res_dct)
                            except:
                                res_dct = {}

                            try:
                                # <---2nd Table, take single table data, into list ------------>
                                l2 = []
                                table = soup.find('table', attrs={'class': 'table_r'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l2.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l2)

                                lst2 = reduce(lambda x, y: x + y, l2)
                                # print(lst2)
                                res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in
                                            range(0, len(lst2), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('2nd Table: ', res_dct2)
                            except:
                                res_dct2 = {}

                            try:
                                # <---3rd Table, take single table data, into list ----------->
                                l3 = []
                                table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l3.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('Petitioner_Advocate_table: ', l3)

                                lst3 = reduce(lambda x, y: x + y, l3)
                                listToStr = ' '.join(map(str, lst3))
                                # print(lst3)
                                res_dct3 = listToStr.replace('\xa0',
                                                             '')  # <-- make kye 'LOWER case', remove 'space' ---
                                # print('PetitionerAdvocate Table: ', res_dct3)
                            except:
                                res_dct3 = ''

                            try:
                                # <---4th Table, take single table data, into list ---------->
                                l4 = []
                                table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l4.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ', l4)

                                lst4 = reduce(lambda x, y: x + y, l4)
                                listToStr2 = ' '.join(map(str, lst4))
                                # print(lst4)
                                res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                                # print('4th Table: ', res_dct4)
                            except:
                                res_dct4 = ''

                            try:
                                # <---5th Table, take single table data, into list ---------->
                                table_a = soup.find('table', attrs={'class': 'acts_table'})
                                h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                                res_dct5 = [dict(zip(h, i)) for i in d]
                                # print('5th Table: ', res_dct5)

                                # k5 = res_dct5
                                # print(k5)

                                k5 = []
                                for d in res_dct5:
                                    k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                        'UnderSection(s)', 'Section'): v for k, v in d.items()})
                                # print('res_dct5: ', res_dct5)
                            except:
                                k5 = []

                            try:
                                # <---6th Table, take single table data, into list ---------->
                                table_t = soup.find('table', attrs={'class': 'history_table'})
                                h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                                res_dct6 = [dict(zip(h, i)) for i in d]

                                k6 = []
                                for d in res_dct6:
                                    k6.append(
                                        {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                         d.items()})
                                # print('k6---:', k6)
                            except:
                                k6 = []

                            try:
                                # <---7th Table, take single table data, into list ---------->
                                table_order = soup.find('table', attrs={'class': 'order_table'})
                                h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                    [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                                res_dct7 = [dict(zip(h7, x)) for x in q]
                                # print('7th Table: ', res_dct7)

                                k7 = []
                                for d7 in res_dct7:
                                    k7.append(
                                        {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                            except:
                                k7 = []

                            try:
                                # <---8th Table, take single table data, into list ---------->
                                table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                                h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                    [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                                res_dct8 = [dict(zip(h1, x)) for x in q]
                                # print('8th Table: ', res_dct8)

                                k8 = []
                                for d in res_dct8:
                                    k8.append({k.replace(' ', ''): v.replace('\xa0', '') for k, v in d.items()})
                                # # print(k8)
                            except:
                                k8 = []

                            main_ch = 1

                            if res_dct != {}:
                                # print("if res_dct != '{}':---- ", res_dct)
                                final = {"CaseDetails": res_dct, "CaseStatus": res_dct2, "PetitionerAdvocate": res_dct3,
                                         "RespondentAdvocate": res_dct4, "Acts": k5,
                                         "History": k6, "Orders": k7, "CaseTransferDetails": k8}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            elif res_dct == {}:
                                final_2 = {
                                    "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            else:
                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                    elif op_page_caseHistoryDiv == 'display: none;':
                        # <- if any captcha text miss, then 'Invalid Captcha' ALERT BOX ---
                        # alert_page = soup.find('div', {'id': 'bs_alert'}).get('style')  # ----- get value "true/false"
                        ok_btn = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok = ok_btn.is_enabled()  # value in True n false

                        if ok == True:
                            ok_btn.click()

                        driver.refresh()
                        main_ch = 0
                        delay()

                    elif op_page_caseHistoryDiv == 'display:none;':  # <-- submit empty catcha --- then alert popup
                        ok_btn2 = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok2 = ok_btn2.is_enabled()  # value in True n false

                        if ok2 == True:
                            ok_btn2.click()
                        driver.refresh()
                        main_ch = 0
                        delay()
                        # delay()
                if main_ch == 1:
                    break

            if res_dct != {}:
                yourdata = final

            elif res_dct == {}:
                yourdata = final_2

            data = yourdata
            try:
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")
                cursor = conn.cursor()
                #print('psycopg2 connect execute--')
            except (Exception, psycopg2.DatabaseError) as error:
                #print("Error while creating PostgreSQL table", error)
                pass
            else:
                cursor.execute('SELECT * FROM public."Scrap_App_count3" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_count3" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                #print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------// function 3 End //--------------------------------------------------

        # t = multiprocessing.Process(target=f3).start()
        t = threading.Thread(target=f3).start()

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            wait = Count3.objects.all().order_by('-id')[0].data
            if wait == '':
                w = 0
            else:
                w = 1
            if w == 1:
                break
        # ------// --- //----
        time.sleep(3)
        data = Count3.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        aa_id2 = Count3.objects.all().order_by('-id')[0].id
        aa_id3 = Count3.objects.get(id=aa_id2)
        aa_id3.var = 0  # make 1 to 0 for available for next request.
        aa_id3.save()


# ------------------(cnr_work3e)----------------------

    elif Count4.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()
        Count4(var=1, time=time.time(),
                slug=slug).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        # print('slug_4: ', slug)

        slug_data = Count4.objects.all().order_by('-id')[0].slug

        # ------------------(cnr_work1)----------------------
        def f4():
            global res_dct, final, final_2, re_captcha_txt_final, re_captcha_txt_final
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            # print('f4--')

            # ---/ op / ---
            slug_ = slug_data

            # -------------------------// our code //---------------
            # <--- all repeated Functions ----/////---
            def delay1():
                time.sleep(random.randint(1, 2))

            def delay():
                time.sleep(random.randint(2, 2))

            while True:
                main_ch = 0
                try:
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    div_style_value = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # ----- get value "display: none;"

                    # Find_data----- START --
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.XPATH, "//*[@id='cino']")))  # wait
                    elem = driver.find_element(By.ID, "cino")
                    elem.send_keys(slug_)  ##### <---- Pass CNR no ---->

                    # <----- for maximise windo upto 170% & scroll down --->
                    driver.execute_script("document.body.style.zoom='170%'")
                    driver.execute_script("window.scrollTo(0,400);")
                    delay1()

                    # <--- to find captcha image location ---->
                    element = driver.find_element_by_xpath("//*[@id='captcha_image']")
                    location = {'x': 240, 'y': 260}  # -for without hide chrome {'x': 240, 'y': 280}
                    size = {'height': 61, 'width': 310}  # -for without hide chrome {'height': 59, 'width': 310}

                    driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR4.png')

                    x = location['x']
                    y = location['y']
                    width = location['x'] + size['width']
                    height = location['y'] + size['height']

                    im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR4.png')
                    im = im.crop((int(x), int(y), int(width), int(height)))
                    im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR4.png')
                    delay1()

                    # <-------- for crop save captcha image in propper size ----// START //--
                    im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR4.png')
                    width, height = im2.size
                    #print('w: ', width, 'h: ', height)
                    # Setting the points for cropped image
                    left = 59  # -for without hide chrome "left = 146"
                    top = 0  # done
                    right = 250  # done                      # -for without hide chrome "right = 310"
                    bottom = 57  # done

                    # Cropped image of above dimension
                    im1 = im2.crop((left, top, right, bottom))
                    im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR4.png')
                    #im1.show()  # <-- comment it ---
                    # <-------- for crop save captcha image in propper size ---// END //---

                    # <---- for Captcha image to text convert---------------comment it------------
                    img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR4.png')

                    # <-------- improve image quality ----------->
                    contrast = ImageEnhance.Contrast(img)
                    contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR4.png')
                    # <------------------->

                    # <----- for remove unwanted color from image ---------->
                    image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR4.png')
                    grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                    grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                    lower_range = np.array([0, 0, 0])
                    upper_range = np.array([0, 0, 0])

                    mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR4.png', mask)
                    # <-----Done remove unwanted color from image ---------->

                    # <------ image color change in black n white ------
                    Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR4.png', 0)
                    ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR4.png', thresh_img)
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                    # <----- for expand image text ---------------------
                    Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR4.png', 0)
                    kernel = np.ones((3, 3), np.uint8)
                    erosion = cv2.erode(Load2, kernel, iterations=1)
                    img = erosion.copy()
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR4.png', img)

                    # <----- for Captcha image to text convert---------------------
                    img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR4.png')
                    captcha_txt2 = tess.image_to_string(img2, config='')
                    delay1()

                    # Find_data-------- END ----------------//////-----------

                    # <---- driver back to original position ----
                    driver.execute_script("document.body.style.zoom='100%'")
                    driver.execute_script("window.scrollTo(0,0);")
                    delay1()

                    # --------------------------------
                    # ---------------------------------
                    while True:
                        length_ch = 0
                        if len(captcha_txt2) != 8:
                            # Re_ScreenShot_captcha()
                            # ------------------------------
                            driver.find_element_by_class_name('refresh-btn').click()
                            driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr

                            # <----- for maximise windo upto 170% & scroll down --->
                            driver.execute_script("document.body.style.zoom='170%'")
                            driver.execute_script("window.scrollTo(0,400);")
                            delay1()

                            element = driver.find_element_by_xpath("//*[@id='captcha_image']")

                            location = {'x': 240, 'y': 260}
                            size = {'height': 61, 'width': 310}

                            driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR4.png')

                            x = location['x']
                            y = location['y']
                            width = location['x'] + size['width']
                            height = location['y'] + size['height']

                            im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR4.png')
                            im = im.crop((int(x), int(y), int(width), int(height)))

                            im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR4.png')
                            delay1()
                            # <-------- for crop save captcha image in propper size ----// START //--
                            im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR4.png')
                            width, height = im2.size

                            left = 59
                            top = 0  # done
                            right = 250  # done
                            bottom = 57  # done

                            # Cropped image of above dimension
                            im1 = im2.crop((left, top, right, bottom))
                            im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR4.png')
                            # <-------- for crop save captcha image in propper size ---// END //---

                            # for Captcha image to text convert---------------comment it------------
                            img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR4.png')
                            captcha_txt = tess.image_to_string(img, config='')

                            # <-------- improve image quality ----------->
                            contrast = ImageEnhance.Contrast(img)
                            contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR4.png')
                            # <------------------->

                            # <----- for remove unwanted color from image ---------->
                            image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR4.png')
                            grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                            grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                            lower_range = np.array([0, 0, 0])
                            upper_range = np.array([0, 0, 0])

                            mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR4.png', mask)
                            # <-----Done remove unwanted color from image ---------->

                            # <------ image color change in black n white ------
                            Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR4.png', 0)
                            ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR4.png', thresh_img)
                            cv2.waitKey(0)
                            cv2.destroyAllWindows()

                            # <----- for expand image text ---------------------
                            Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR4.png', 0)
                            kernel = np.ones((3, 3), np.uint8)  # <---- (2, 2)  also work

                            erosion = cv2.erode(Load2, kernel, iterations=1)
                            img = erosion.copy()
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR4.png', img)

                            # <----- for Captcha image to text convert---------------------
                            img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR4.png')
                            # global re_captcha_txt
                            re_captcha_txt = tess.image_to_string(img2, config='')
                            # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)
                            captcha_txt2 = re_captcha_txt

                            # ---------------end---------------------
                            # <---- driver back to original position ----
                            driver.execute_script("document.body.style.zoom='100%'")
                            driver.execute_script("window.scrollTo(0,0);")
                            delay1()

                            if len(captcha_txt2) != 8:
                                length_ch = 0
                                delay1()
                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                #print('re_captcha_txt_final:-', re_captcha_txt_final)

                        else:
                            re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                            #print('re_captcha_txt_final:-', re_captcha_txt_final)
                            length_ch = 1

                        if length_ch == 1:
                            break

                    # <----- for Enter Captcha image text to input field---------------------
                    input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                    input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                    # <----- for Click Search btn---------------------

                    # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                    driver.find_element_by_xpath("//*[@id='searchbtn']").click()  # <--- sertch btn --->
                    # Enter_Captcha_image_text_to_input_field ---- END --------------//////-----------


                except:
                    delay()
                    delay()
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    op_page_caseHistoryDiv = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # -----For Invalid captcha page ("display: block;")

                    if op_page_caseHistoryDiv == 'display: block;':
                        all_divs = soup.find('div', {'id': 'caseHistoryDiv'})
                        all_tables = all_divs.find_all('p')

                        check = str(all_tables)
                        if check == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                            driver.find_elements_by_xpath('//*[@id="bckbtn"]')[0].click()
                            delay()

                            main_ch = 0

                        else:
                            # <-----/// Final page table conversion start ///----->
                            try:
                                # <---1st Table, take single table data, into list ------------>
                                l = []
                                table = soup.find('table', attrs={'class': 'case_details_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l)

                                b = l[:len(
                                    l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                                lst = reduce(lambda x, y: x + y, b)
                                lst.append('cnrnumber')  # < ---- add 'CNR' name
                                lst.append(slug_)  # < ---- add 'CNR number
                                # print(lst)
                                res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in
                                           range(0, len(lst), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('1st Table:-- ', res_dct)
                            except:
                                res_dct = {}

                            try:
                                # <---2nd Table, take single table data, into list ------------>
                                l2 = []
                                table = soup.find('table', attrs={'class': 'table_r'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l2.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l2)

                                lst2 = reduce(lambda x, y: x + y, l2)
                                # print(lst2)
                                res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in
                                            range(0, len(lst2), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('2nd Table: ', res_dct2)
                            except:
                                res_dct2 = {}

                            try:
                                # <---3rd Table, take single table data, into list ----------->
                                l3 = []
                                table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l3.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('Petitioner_Advocate_table: ', l3)

                                lst3 = reduce(lambda x, y: x + y, l3)
                                listToStr = ' '.join(map(str, lst3))
                                # print(lst3)
                                res_dct3 = listToStr.replace('\xa0',
                                                             '')  # <-- make kye 'LOWER case', remove 'space' ---
                                # print('PetitionerAdvocate Table: ', res_dct3)
                            except:
                                res_dct3 = ''

                            try:
                                # <---4th Table, take single table data, into list ---------->
                                l4 = []
                                table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l4.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ', l4)

                                lst4 = reduce(lambda x, y: x + y, l4)
                                listToStr2 = ' '.join(map(str, lst4))
                                # print(lst4)
                                res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                                # print('4th Table: ', res_dct4)
                            except:
                                res_dct4 = ''

                            try:
                                # <---5th Table, take single table data, into list ---------->
                                table_a = soup.find('table', attrs={'class': 'acts_table'})
                                h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                                res_dct5 = [dict(zip(h, i)) for i in d]
                                # print('5th Table: ', res_dct5)

                                # k5 = res_dct5
                                # print(k5)

                                k5 = []
                                for d in res_dct5:
                                    k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                        'UnderSection(s)', 'Section'): v for k, v in d.items()})
                                # print('res_dct5: ', res_dct5)
                            except:
                                k5 = []

                            try:
                                # <---6th Table, take single table data, into list ---------->
                                table_t = soup.find('table', attrs={'class': 'history_table'})
                                h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                                res_dct6 = [dict(zip(h, i)) for i in d]

                                k6 = []
                                for d in res_dct6:
                                    k6.append(
                                        {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                         d.items()})
                                # print('k6---:', k6)
                            except:
                                k6 = []

                            try:
                                # <---7th Table, take single table data, into list ---------->
                                table_order = soup.find('table', attrs={'class': 'order_table'})
                                h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                    [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                                res_dct7 = [dict(zip(h7, x)) for x in q]
                                # print('7th Table: ', res_dct7)

                                k7 = []
                                for d7 in res_dct7:
                                    k7.append(
                                        {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                            except:
                                k7 = []

                            try:
                                # <---8th Table, take single table data, into list ---------->
                                table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                                h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                    [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                                res_dct8 = [dict(zip(h1, x)) for x in q]
                                # print('8th Table: ', res_dct8)

                                k8 = []
                                for d in res_dct8:
                                    k8.append({k.replace(' ', ''): v.replace('\xa0', '') for k, v in d.items()})
                                # # print(k8)
                            except:
                                k8 = []

                            main_ch = 1

                            if res_dct != {}:
                                # print("if res_dct != '{}':---- ", res_dct)
                                final = {"CaseDetails": res_dct, "CaseStatus": res_dct2, "PetitionerAdvocate": res_dct3,
                                         "RespondentAdvocate": res_dct4, "Acts": k5,
                                         "History": k6, "Orders": k7, "CaseTransferDetails": k8}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            elif res_dct == {}:
                                final_2 = {
                                    "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            else:
                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                    elif op_page_caseHistoryDiv == 'display: none;':
                        # <- if any captcha text miss, then 'Invalid Captcha' ALERT BOX ---
                        # alert_page = soup.find('div', {'id': 'bs_alert'}).get('style')  # ----- get value "true/false"
                        ok_btn = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok = ok_btn.is_enabled()  # value in True n false

                        if ok == True:
                            ok_btn.click()

                        driver.refresh()
                        main_ch = 0
                        delay()

                    elif op_page_caseHistoryDiv == 'display:none;':  # <-- submit empty catcha --- then alert popup
                        ok_btn2 = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok2 = ok_btn2.is_enabled()  # value in True n false

                        if ok2 == True:
                            ok_btn2.click()
                        driver.refresh()
                        main_ch = 0
                        delay()
                        # delay()
                if main_ch == 1:
                    break

            if res_dct != {}:
                yourdata = final

            elif res_dct == {}:
                yourdata = final_2

            data = yourdata
            try:
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")
                cursor = conn.cursor()
                #print('psycopg2 connect execute--')
            except (Exception, psycopg2.DatabaseError) as error:
                #print("Error while creating PostgreSQL table", error)
                pass
            else:
                cursor.execute('SELECT * FROM public."Scrap_App_count4" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_count4" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                #print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------// function 4 End //--------------------------------------------------

        # t = multiprocessing.Process(target=f4).start()
        t = threading.Thread(target=f4).start()

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            wait = Count4.objects.all().order_by('-id')[0].data
            if wait == '':
                w = 0
            else:
                w = 1
            if w == 1:
                break
        # ------// --- //----
        time.sleep(3)
        data = Count4.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        aa_id2 = Count4.objects.all().order_by('-id')[0].id
        aa_id3 = Count4.objects.get(id=aa_id2)
        aa_id3.var = 0  # make 1 to 0 for available for next request.
        aa_id3.save()


# ------------------(cnr_work4e)----------------------

    elif Count5.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()
        Count5(var=1, time=time.time(),
                slug=slug).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        # print('slug_5: ', slug)

        slug_data = Count5.objects.all().order_by('-id')[0].slug

        # ------------------(cnr_work5)----------------------
        def f5():
            global res_dct, final, final_2, re_captcha_txt_final, re_captcha_txt_final
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            # print('f5--')

            # ---/ op / ---
            slug_ = slug_data

            # -------------------------// our code //---------------
            # <--- all repeated Functions ----/////---
            def delay1():
                time.sleep(random.randint(1, 2))

            def delay():
                time.sleep(random.randint(2, 2))

            while True:
                main_ch = 0
                try:
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    div_style_value = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # ----- get value "display: none;"

                    # Find_data----- START --
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.XPATH, "//*[@id='cino']")))  # wait
                    elem = driver.find_element(By.ID, "cino")
                    elem.send_keys(slug_)  ##### <---- Pass CNR no ---->

                    # <----- for maximise windo upto 170% & scroll down --->
                    driver.execute_script("document.body.style.zoom='170%'")
                    driver.execute_script("window.scrollTo(0,400);")
                    delay1()

                    # <--- to find captcha image location ---->
                    element = driver.find_element_by_xpath("//*[@id='captcha_image']")
                    location = {'x': 240, 'y': 260}  # -for without hide chrome {'x': 240, 'y': 280}
                    size = {'height': 61, 'width': 310}  # -for without hide chrome {'height': 59, 'width': 310}

                    driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR5.png')

                    x = location['x']
                    y = location['y']
                    width = location['x'] + size['width']
                    height = location['y'] + size['height']

                    im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR5.png')
                    im = im.crop((int(x), int(y), int(width), int(height)))
                    im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR5.png')
                    delay1()

                    # <-------- for crop save captcha image in propper size ----// START //--
                    im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR5.png')
                    width, height = im2.size
                    #print('w: ', width, 'h: ', height)
                    # Setting the points for cropped image
                    left = 59  # -for without hide chrome "left = 146"
                    top = 0  # done
                    right = 250  # done                      # -for without hide chrome "right = 310"
                    bottom = 57  # done

                    # Cropped image of above dimension
                    im1 = im2.crop((left, top, right, bottom))
                    im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR5.png')
                    #im1.show()  # <-- comment it ---
                    # <-------- for crop save captcha image in propper size ---// END //---

                    # <---- for Captcha image to text convert---------------comment it------------
                    img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR5.png')

                    # <-------- improve image quality ----------->
                    contrast = ImageEnhance.Contrast(img)
                    contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR5.png')
                    # <------------------->

                    # <----- for remove unwanted color from image ---------->
                    image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR5.png')
                    grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                    grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                    lower_range = np.array([0, 0, 0])
                    upper_range = np.array([0, 0, 0])

                    mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR5.png', mask)
                    # <-----Done remove unwanted color from image ---------->

                    # <------ image color change in black n white ------
                    Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR5.png', 0)
                    ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR5.png', thresh_img)
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                    # <----- for expand image text ---------------------
                    Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR5.png', 0)
                    kernel = np.ones((3, 3), np.uint8)
                    erosion = cv2.erode(Load2, kernel, iterations=1)
                    img = erosion.copy()
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR5.png', img)

                    # <----- for Captcha image to text convert---------------------
                    img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR5.png')
                    captcha_txt2 = tess.image_to_string(img2, config='')
                    delay1()

                    # Find_data-------- END ----------------//////-----------

                    # <---- driver back to original position ----
                    driver.execute_script("document.body.style.zoom='100%'")
                    driver.execute_script("window.scrollTo(0,0);")
                    delay1()

                    # --------------------------------
                    # ---------------------------------
                    while True:
                        length_ch = 0
                        if len(captcha_txt2) != 8:
                            # Re_ScreenShot_captcha()
                            # ------------------------------
                            driver.find_element_by_class_name('refresh-btn').click()
                            driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr

                            # <----- for maximise windo upto 170% & scroll down --->
                            driver.execute_script("document.body.style.zoom='170%'")
                            driver.execute_script("window.scrollTo(0,400);")
                            delay1()

                            element = driver.find_element_by_xpath("//*[@id='captcha_image']")

                            location = {'x': 240, 'y': 260}
                            size = {'height': 61, 'width': 310}

                            driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR5.png')

                            x = location['x']
                            y = location['y']
                            width = location['x'] + size['width']
                            height = location['y'] + size['height']

                            im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR5.png')
                            im = im.crop((int(x), int(y), int(width), int(height)))

                            im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR5.png')
                            delay1()
                            # <-------- for crop save captcha image in propper size ----// START //--
                            im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR5.png')
                            width, height = im2.size

                            left = 59
                            top = 0  # done
                            right = 250  # done
                            bottom = 57  # done

                            # Cropped image of above dimension
                            im1 = im2.crop((left, top, right, bottom))
                            im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR5.png')
                            # <-------- for crop save captcha image in propper size ---// END //---

                            # for Captcha image to text convert---------------comment it------------
                            img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR5.png')
                            captcha_txt = tess.image_to_string(img, config='')

                            # <-------- improve image quality ----------->
                            contrast = ImageEnhance.Contrast(img)
                            contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR5.png')
                            # <------------------->

                            # <----- for remove unwanted color from image ---------->
                            image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR5.png')
                            grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                            grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                            lower_range = np.array([0, 0, 0])
                            upper_range = np.array([0, 0, 0])

                            mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR5.png', mask)
                            # <-----Done remove unwanted color from image ---------->

                            # <------ image color change in black n white ------
                            Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR5.png', 0)
                            ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR5.png', thresh_img)
                            cv2.waitKey(0)
                            cv2.destroyAllWindows()

                            # <----- for expand image text ---------------------
                            Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR5.png', 0)
                            kernel = np.ones((3, 3), np.uint8)  # <---- (2, 2)  also work

                            erosion = cv2.erode(Load2, kernel, iterations=1)
                            img = erosion.copy()
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR5.png', img)

                            # <----- for Captcha image to text convert---------------------
                            img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR5.png')
                            # global re_captcha_txt
                            re_captcha_txt = tess.image_to_string(img2, config='')
                            # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)
                            captcha_txt2 = re_captcha_txt

                            # ---------------end---------------------
                            # <---- driver back to original position ----
                            driver.execute_script("document.body.style.zoom='100%'")
                            driver.execute_script("window.scrollTo(0,0);")
                            delay1()

                            if len(captcha_txt2) != 8:
                                length_ch = 0
                                delay1()
                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                #print('re_captcha_txt_final:-', re_captcha_txt_final)

                        else:
                            re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                            #print('re_captcha_txt_final:-', re_captcha_txt_final)
                            length_ch = 1

                        if length_ch == 1:
                            break

                    # <----- for Enter Captcha image text to input field---------------------
                    input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                    input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                    # <----- for Click Search btn---------------------

                    # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                    driver.find_element_by_xpath("//*[@id='searchbtn']").click()  # <--- sertch btn --->
                    # Enter_Captcha_image_text_to_input_field ---- END --------------//////-----------


                except:
                    delay()
                    delay()
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    op_page_caseHistoryDiv = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # -----For Invalid captcha page ("display: block;")

                    if op_page_caseHistoryDiv == 'display: block;':
                        all_divs = soup.find('div', {'id': 'caseHistoryDiv'})
                        all_tables = all_divs.find_all('p')

                        check = str(all_tables)
                        if check == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                            driver.find_elements_by_xpath('//*[@id="bckbtn"]')[0].click()
                            delay()

                            main_ch = 0

                        else:
                            # <-----/// Final page table conversion start ///----->
                            try:
                                # <---1st Table, take single table data, into list ------------>
                                l = []
                                table = soup.find('table', attrs={'class': 'case_details_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l)

                                b = l[:len(
                                    l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                                lst = reduce(lambda x, y: x + y, b)
                                lst.append('cnrnumber')  # < ---- add 'CNR' name
                                lst.append(slug_)  # < ---- add 'CNR number
                                # print(lst)
                                res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in
                                           range(0, len(lst), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('1st Table:-- ', res_dct)
                            except:
                                res_dct = {}

                            try:
                                # <---2nd Table, take single table data, into list ------------>
                                l2 = []
                                table = soup.find('table', attrs={'class': 'table_r'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l2.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l2)

                                lst2 = reduce(lambda x, y: x + y, l2)
                                # print(lst2)
                                res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in
                                            range(0, len(lst2), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('2nd Table: ', res_dct2)
                            except:
                                res_dct2 = {}

                            try:
                                # <---3rd Table, take single table data, into list ----------->
                                l3 = []
                                table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l3.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('Petitioner_Advocate_table: ', l3)

                                lst3 = reduce(lambda x, y: x + y, l3)
                                listToStr = ' '.join(map(str, lst3))
                                # print(lst3)
                                res_dct3 = listToStr.replace('\xa0',
                                                             '')  # <-- make kye 'LOWER case', remove 'space' ---
                                # print('PetitionerAdvocate Table: ', res_dct3)
                            except:
                                res_dct3 = ''

                            try:
                                # <---4th Table, take single table data, into list ---------->
                                l4 = []
                                table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l4.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ', l4)

                                lst4 = reduce(lambda x, y: x + y, l4)
                                listToStr2 = ' '.join(map(str, lst4))
                                # print(lst4)
                                res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                                # print('4th Table: ', res_dct4)
                            except:
                                res_dct4 = ''

                            try:
                                # <---5th Table, take single table data, into list ---------->
                                table_a = soup.find('table', attrs={'class': 'acts_table'})
                                h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                                res_dct5 = [dict(zip(h, i)) for i in d]
                                # print('5th Table: ', res_dct5)

                                # k5 = res_dct5
                                # print(k5)

                                k5 = []
                                for d in res_dct5:
                                    k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                        'UnderSection(s)', 'Section'): v for k, v in d.items()})
                                # print('res_dct5: ', res_dct5)
                            except:
                                k5 = []

                            try:
                                # <---6th Table, take single table data, into list ---------->
                                table_t = soup.find('table', attrs={'class': 'history_table'})
                                h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                                res_dct6 = [dict(zip(h, i)) for i in d]

                                k6 = []
                                for d in res_dct6:
                                    k6.append(
                                        {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                         d.items()})
                                # print('k6---:', k6)
                            except:
                                k6 = []

                            try:
                                # <---7th Table, take single table data, into list ---------->
                                table_order = soup.find('table', attrs={'class': 'order_table'})
                                h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                    [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                                res_dct7 = [dict(zip(h7, x)) for x in q]
                                # print('7th Table: ', res_dct7)

                                k7 = []
                                for d7 in res_dct7:
                                    k7.append(
                                        {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                            except:
                                k7 = []

                            try:
                                # <---8th Table, take single table data, into list ---------->
                                table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                                h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                    [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                                res_dct8 = [dict(zip(h1, x)) for x in q]
                                # print('8th Table: ', res_dct8)

                                k8 = []
                                for d in res_dct8:
                                    k8.append({k.replace(' ', ''): v.replace('\xa0', '') for k, v in d.items()})
                                # # print(k8)
                            except:
                                k8 = []

                            main_ch = 1

                            if res_dct != {}:
                                # print("if res_dct != '{}':---- ", res_dct)
                                final = {"CaseDetails": res_dct, "CaseStatus": res_dct2, "PetitionerAdvocate": res_dct3,
                                         "RespondentAdvocate": res_dct4, "Acts": k5,
                                         "History": k6, "Orders": k7, "CaseTransferDetails": k8}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            elif res_dct == {}:
                                final_2 = {
                                    "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            else:
                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                    elif op_page_caseHistoryDiv == 'display: none;':
                        # <- if any captcha text miss, then 'Invalid Captcha' ALERT BOX ---
                        # alert_page = soup.find('div', {'id': 'bs_alert'}).get('style')  # ----- get value "true/false"
                        ok_btn = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok = ok_btn.is_enabled()  # value in True n false

                        if ok == True:
                            ok_btn.click()

                        driver.refresh()
                        main_ch = 0
                        delay()

                    elif op_page_caseHistoryDiv == 'display:none;':  # <-- submit empty catcha --- then alert popup
                        ok_btn2 = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok2 = ok_btn2.is_enabled()  # value in True n false

                        if ok2 == True:
                            ok_btn2.click()
                        driver.refresh()
                        main_ch = 0
                        delay()
                        # delay()
                if main_ch == 1:
                    break

            if res_dct != {}:
                yourdata = final

            elif res_dct == {}:
                yourdata = final_2

            data = yourdata
            try:
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")
                cursor = conn.cursor()
                #print('psycopg2 connect execute--')
            except (Exception, psycopg2.DatabaseError) as error:
                #print("Error while creating PostgreSQL table", error)
                pass
            else:
                cursor.execute('SELECT * FROM public."Scrap_App_count5" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_count5" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                #print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------// function 5 End //--------------------------------------------------

        # t = multiprocessing.Process(target=f5).start()
        t = threading.Thread(target=f5).start()

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            wait = Count5.objects.all().order_by('-id')[0].data
            if wait == '':
                w = 0
            else:
                w = 1
            if w == 1:
                break
        # ------// --- //----
        time.sleep(3)
        data = Count5.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        aa_id2 = Count5.objects.all().order_by('-id')[0].id
        aa_id3 = Count5.objects.get(id=aa_id2)
        aa_id3.var = 0  # make 1 to 0 for available for next request.
        aa_id3.save()


# ------------------(cnr_work5e)----------------------

    elif Count6.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()
        Count6(var=1, time=time.time(),
                slug=slug).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        # print('slug_6: ', slug)

        slug_data = Count6.objects.all().order_by('-id')[0].slug

        # ------------------(cnr_work6)----------------------
        def f6():
            global res_dct, final, final_2, re_captcha_txt_final, re_captcha_txt_final
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            # print('f6--')

            # ---/ op / ---
            slug_ = slug_data

            # -------------------------// our code //---------------
            # <--- all repeated Functions ----/////---
            def delay1():
                time.sleep(random.randint(1, 2))

            def delay():
                time.sleep(random.randint(2, 2))

            while True:
                main_ch = 0
                try:
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    div_style_value = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # ----- get value "display: none;"

                    # Find_data----- START --
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.XPATH, "//*[@id='cino']")))  # wait
                    elem = driver.find_element(By.ID, "cino")
                    elem.send_keys(slug_)  ##### <---- Pass CNR no ---->

                    # <----- for maximise windo upto 170% & scroll down --->
                    driver.execute_script("document.body.style.zoom='170%'")
                    driver.execute_script("window.scrollTo(0,400);")
                    delay1()

                    # <--- to find captcha image location ---->
                    element = driver.find_element_by_xpath("//*[@id='captcha_image']")
                    location = {'x': 240, 'y': 260}  # -for without hide chrome {'x': 240, 'y': 280}
                    size = {'height': 61, 'width': 310}  # -for without hide chrome {'height': 59, 'width': 310}

                    driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR6.png')

                    x = location['x']
                    y = location['y']
                    width = location['x'] + size['width']
                    height = location['y'] + size['height']

                    im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR6.png')
                    im = im.crop((int(x), int(y), int(width), int(height)))
                    im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR6.png')
                    delay1()

                    # <-------- for crop save captcha image in propper size ----// START //--
                    im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR6.png')
                    width, height = im2.size
                    #print('w: ', width, 'h: ', height)
                    # Setting the points for cropped image
                    left = 59  # -for without hide chrome "left = 146"
                    top = 0  # done
                    right = 250  # done                      # -for without hide chrome "right = 310"
                    bottom = 57  # done

                    # Cropped image of above dimension
                    im1 = im2.crop((left, top, right, bottom))
                    im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR6.png')
                    #im1.show()  # <-- comment it ---
                    # <-------- for crop save captcha image in propper size ---// END //---

                    # <---- for Captcha image to text convert---------------comment it------------
                    img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR6.png')

                    # <-------- improve image quality ----------->
                    contrast = ImageEnhance.Contrast(img)
                    contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR6.png')
                    # <------------------->

                    # <----- for remove unwanted color from image ---------->
                    image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR6.png')
                    grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                    grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                    lower_range = np.array([0, 0, 0])
                    upper_range = np.array([0, 0, 0])

                    mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR6.png', mask)
                    # <-----Done remove unwanted color from image ---------->

                    # <------ image color change in black n white ------
                    Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR6.png', 0)
                    ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR6.png', thresh_img)
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                    # <----- for expand image text ---------------------
                    Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR6.png', 0)
                    kernel = np.ones((3, 3), np.uint8)
                    erosion = cv2.erode(Load2, kernel, iterations=1)
                    img = erosion.copy()
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR6.png', img)

                    # <----- for Captcha image to text convert---------------------
                    img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR6.png')
                    captcha_txt2 = tess.image_to_string(img2, config='')
                    delay1()

                    # Find_data-------- END ----------------//////-----------

                    # <---- driver back to original position ----
                    driver.execute_script("document.body.style.zoom='100%'")
                    driver.execute_script("window.scrollTo(0,0);")
                    delay1()

                    # --------------------------------
                    # ---------------------------------
                    while True:
                        length_ch = 0
                        if len(captcha_txt2) != 8:
                            # Re_ScreenShot_captcha()
                            # ------------------------------
                            driver.find_element_by_class_name('refresh-btn').click()
                            driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr

                            # <----- for maximise windo upto 170% & scroll down --->
                            driver.execute_script("document.body.style.zoom='170%'")
                            driver.execute_script("window.scrollTo(0,400);")
                            delay1()

                            element = driver.find_element_by_xpath("//*[@id='captcha_image']")

                            location = {'x': 240, 'y': 260}
                            size = {'height': 61, 'width': 310}

                            driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR6.png')

                            x = location['x']
                            y = location['y']
                            width = location['x'] + size['width']
                            height = location['y'] + size['height']

                            im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR6.png')
                            im = im.crop((int(x), int(y), int(width), int(height)))

                            im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR6.png')
                            delay1()
                            # <-------- for crop save captcha image in propper size ----// START //--
                            im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR6.png')
                            width, height = im2.size

                            left = 59
                            top = 0  # done
                            right = 250  # done
                            bottom = 57  # done

                            # Cropped image of above dimension
                            im1 = im2.crop((left, top, right, bottom))
                            im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR6.png')
                            # <-------- for crop save captcha image in propper size ---// END //---

                            # for Captcha image to text convert---------------comment it------------
                            img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR6.png')
                            captcha_txt = tess.image_to_string(img, config='')

                            # <-------- improve image quality ----------->
                            contrast = ImageEnhance.Contrast(img)
                            contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR6.png')
                            # <------------------->

                            # <----- for remove unwanted color from image ---------->
                            image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR6.png')
                            grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                            grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                            lower_range = np.array([0, 0, 0])
                            upper_range = np.array([0, 0, 0])

                            mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR6.png', mask)
                            # <-----Done remove unwanted color from image ---------->

                            # <------ image color change in black n white ------
                            Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR6.png', 0)
                            ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR6.png', thresh_img)
                            cv2.waitKey(0)
                            cv2.destroyAllWindows()

                            # <----- for expand image text ---------------------
                            Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR6.png', 0)
                            kernel = np.ones((3, 3), np.uint8)  # <---- (2, 2)  also work

                            erosion = cv2.erode(Load2, kernel, iterations=1)
                            img = erosion.copy()
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR6.png', img)

                            # <----- for Captcha image to text convert---------------------
                            img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR6.png')
                            # global re_captcha_txt
                            re_captcha_txt = tess.image_to_string(img2, config='')
                            # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)
                            captcha_txt2 = re_captcha_txt

                            # ---------------end---------------------
                            # <---- driver back to original position ----
                            driver.execute_script("document.body.style.zoom='100%'")
                            driver.execute_script("window.scrollTo(0,0);")
                            delay1()

                            if len(captcha_txt2) != 8:
                                length_ch = 0
                                delay1()
                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                #print('re_captcha_txt_final:-', re_captcha_txt_final)

                        else:
                            re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                            #print('re_captcha_txt_final:-', re_captcha_txt_final)
                            length_ch = 1

                        if length_ch == 1:
                            break

                    # <----- for Enter Captcha image text to input field---------------------
                    input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                    input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                    # <----- for Click Search btn---------------------

                    # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                    driver.find_element_by_xpath("//*[@id='searchbtn']").click()  # <--- sertch btn --->
                    # Enter_Captcha_image_text_to_input_field ---- END --------------//////-----------


                except:
                    delay()
                    delay()
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    op_page_caseHistoryDiv = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # -----For Invalid captcha page ("display: block;")

                    if op_page_caseHistoryDiv == 'display: block;':
                        all_divs = soup.find('div', {'id': 'caseHistoryDiv'})
                        all_tables = all_divs.find_all('p')

                        check = str(all_tables)
                        if check == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                            driver.find_elements_by_xpath('//*[@id="bckbtn"]')[0].click()
                            delay()

                            main_ch = 0

                        else:
                            # <-----/// Final page table conversion start ///----->
                            try:
                                # <---1st Table, take single table data, into list ------------>
                                l = []
                                table = soup.find('table', attrs={'class': 'case_details_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l)

                                b = l[:len(
                                    l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                                lst = reduce(lambda x, y: x + y, b)
                                lst.append('cnrnumber')  # < ---- add 'CNR' name
                                lst.append(slug_)  # < ---- add 'CNR number
                                # print(lst)
                                res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in
                                           range(0, len(lst), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('1st Table:-- ', res_dct)
                            except:
                                res_dct = {}

                            try:
                                # <---2nd Table, take single table data, into list ------------>
                                l2 = []
                                table = soup.find('table', attrs={'class': 'table_r'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l2.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l2)

                                lst2 = reduce(lambda x, y: x + y, l2)
                                # print(lst2)
                                res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in
                                            range(0, len(lst2), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('2nd Table: ', res_dct2)
                            except:
                                res_dct2 = {}

                            try:
                                # <---3rd Table, take single table data, into list ----------->
                                l3 = []
                                table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l3.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('Petitioner_Advocate_table: ', l3)

                                lst3 = reduce(lambda x, y: x + y, l3)
                                listToStr = ' '.join(map(str, lst3))
                                # print(lst3)
                                res_dct3 = listToStr.replace('\xa0',
                                                             '')  # <-- make kye 'LOWER case', remove 'space' ---
                                # print('PetitionerAdvocate Table: ', res_dct3)
                            except:
                                res_dct3 = ''

                            try:
                                # <---4th Table, take single table data, into list ---------->
                                l4 = []
                                table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l4.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ', l4)

                                lst4 = reduce(lambda x, y: x + y, l4)
                                listToStr2 = ' '.join(map(str, lst4))
                                # print(lst4)
                                res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                                # print('4th Table: ', res_dct4)
                            except:
                                res_dct4 = ''

                            try:
                                # <---5th Table, take single table data, into list ---------->
                                table_a = soup.find('table', attrs={'class': 'acts_table'})
                                h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                                res_dct5 = [dict(zip(h, i)) for i in d]
                                # print('5th Table: ', res_dct5)

                                # k5 = res_dct5
                                # print(k5)

                                k5 = []
                                for d in res_dct5:
                                    k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                        'UnderSection(s)', 'Section'): v for k, v in d.items()})
                                # print('res_dct5: ', res_dct5)
                            except:
                                k5 = []

                            try:
                                # <---6th Table, take single table data, into list ---------->
                                table_t = soup.find('table', attrs={'class': 'history_table'})
                                h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                                res_dct6 = [dict(zip(h, i)) for i in d]

                                k6 = []
                                for d in res_dct6:
                                    k6.append(
                                        {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                         d.items()})
                                # print('k6---:', k6)
                            except:
                                k6 = []

                            try:
                                # <---7th Table, take single table data, into list ---------->
                                table_order = soup.find('table', attrs={'class': 'order_table'})
                                h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                    [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                                res_dct7 = [dict(zip(h7, x)) for x in q]
                                # print('7th Table: ', res_dct7)

                                k7 = []
                                for d7 in res_dct7:
                                    k7.append(
                                        {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                            except:
                                k7 = []

                            try:
                                # <---8th Table, take single table data, into list ---------->
                                table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                                h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                    [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                                res_dct8 = [dict(zip(h1, x)) for x in q]
                                # print('8th Table: ', res_dct8)

                                k8 = []
                                for d in res_dct8:
                                    k8.append({k.replace(' ', ''): v.replace('\xa0', '') for k, v in d.items()})
                                # # print(k8)
                            except:
                                k8 = []

                            main_ch = 1

                            if res_dct != {}:
                                # print("if res_dct != '{}':---- ", res_dct)
                                final = {"CaseDetails": res_dct, "CaseStatus": res_dct2, "PetitionerAdvocate": res_dct3,
                                         "RespondentAdvocate": res_dct4, "Acts": k5,
                                         "History": k6, "Orders": k7, "CaseTransferDetails": k8}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            elif res_dct == {}:
                                final_2 = {
                                    "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            else:
                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                    elif op_page_caseHistoryDiv == 'display: none;':
                        # <- if any captcha text miss, then 'Invalid Captcha' ALERT BOX ---
                        # alert_page = soup.find('div', {'id': 'bs_alert'}).get('style')  # ----- get value "true/false"
                        ok_btn = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok = ok_btn.is_enabled()  # value in True n false

                        if ok == True:
                            ok_btn.click()

                        driver.refresh()
                        main_ch = 0
                        delay()

                    elif op_page_caseHistoryDiv == 'display:none;':  # <-- submit empty catcha --- then alert popup
                        ok_btn2 = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok2 = ok_btn2.is_enabled()  # value in True n false

                        if ok2 == True:
                            ok_btn2.click()
                        driver.refresh()
                        main_ch = 0
                        delay()
                        # delay()
                if main_ch == 1:
                    break

            if res_dct != {}:
                yourdata = final

            elif res_dct == {}:
                yourdata = final_2

            data = yourdata
            try:
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")
                cursor = conn.cursor()
                #print('psycopg2 connect execute--')
            except (Exception, psycopg2.DatabaseError) as error:
                #print("Error while creating PostgreSQL table", error)
                pass
            else:
                cursor.execute('SELECT * FROM public."Scrap_App_count6" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_count6" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                #print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------// function 6 End //--------------------------------------------------

        # t = multiprocessing.Process(target=f6).start()
        t = threading.Thread(target=f6).start()

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            wait = Count6.objects.all().order_by('-id')[0].data
            if wait == '':
                w = 0
            else:
                w = 1
            if w == 1:
                break
        # ------// --- //----
        time.sleep(3)
        data = Count6.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        aa_id2 = Count6.objects.all().order_by('-id')[0].id
        aa_id3 = Count6.objects.get(id=aa_id2)
        aa_id3.var = 0  # make 1 to 0 for available for next request.
        aa_id3.save()


# ------------------(cnr_work6e)----------------------

    elif Count7.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()
        Count7(var=1, time=time.time(),
                slug=slug).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        # print('slug_7: ', slug)

        slug_data = Count7.objects.all().order_by('-id')[0].slug

        # ------------------(cnr_work7)----------------------
        def f7():
            global res_dct, final, final_2, re_captcha_txt_final, re_captcha_txt_final
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            # print('f7--')

            # ---/ op / ---
            slug_ = slug_data

            # -------------------------// our code //---------------
            # <--- all repeated Functions ----/////---
            def delay1():
                time.sleep(random.randint(1, 2))

            def delay():
                time.sleep(random.randint(2, 2))

            while True:
                main_ch = 0
                try:
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    div_style_value = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # ----- get value "display: none;"

                    # Find_data----- START --
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.XPATH, "//*[@id='cino']")))  # wait
                    elem = driver.find_element(By.ID, "cino")
                    elem.send_keys(slug_)  ##### <---- Pass CNR no ---->

                    # <----- for maximise windo upto 170% & scroll down --->
                    driver.execute_script("document.body.style.zoom='170%'")
                    driver.execute_script("window.scrollTo(0,400);")
                    delay1()

                    # <--- to find captcha image location ---->
                    element = driver.find_element_by_xpath("//*[@id='captcha_image']")
                    location = {'x': 240, 'y': 260}  # -for without hide chrome {'x': 240, 'y': 280}
                    size = {'height': 61, 'width': 310}  # -for without hide chrome {'height': 59, 'width': 310}

                    driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR7.png')

                    x = location['x']
                    y = location['y']
                    width = location['x'] + size['width']
                    height = location['y'] + size['height']

                    im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR7.png')
                    im = im.crop((int(x), int(y), int(width), int(height)))
                    im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR7.png')
                    delay1()

                    # <-------- for crop save captcha image in propper size ----// START //--
                    im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR7.png')
                    width, height = im2.size
                    #print('w: ', width, 'h: ', height)
                    # Setting the points for cropped image
                    left = 59  # -for without hide chrome "left = 146"
                    top = 0  # done
                    right = 250  # done                      # -for without hide chrome "right = 310"
                    bottom = 57  # done

                    # Cropped image of above dimension
                    im1 = im2.crop((left, top, right, bottom))
                    im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR7.png')
                    #im1.show()  # <-- comment it ---
                    # <-------- for crop save captcha image in propper size ---// END //---

                    # <---- for Captcha image to text convert---------------comment it------------
                    img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR7.png')

                    # <-------- improve image quality ----------->
                    contrast = ImageEnhance.Contrast(img)
                    contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR7.png')
                    # <------------------->

                    # <----- for remove unwanted color from image ---------->
                    image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR7.png')
                    grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                    grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                    lower_range = np.array([0, 0, 0])
                    upper_range = np.array([0, 0, 0])

                    mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR7.png', mask)
                    # <-----Done remove unwanted color from image ---------->

                    # <------ image color change in black n white ------
                    Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR7.png', 0)
                    ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR7.png', thresh_img)
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                    # <----- for expand image text ---------------------
                    Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR7.png', 0)
                    kernel = np.ones((3, 3), np.uint8)
                    erosion = cv2.erode(Load2, kernel, iterations=1)
                    img = erosion.copy()
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR7.png', img)

                    # <----- for Captcha image to text convert---------------------
                    img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR7.png')
                    captcha_txt2 = tess.image_to_string(img2, config='')
                    delay1()

                    # Find_data-------- END ----------------//////-----------

                    # <---- driver back to original position ----
                    driver.execute_script("document.body.style.zoom='100%'")
                    driver.execute_script("window.scrollTo(0,0);")
                    delay1()

                    # --------------------------------
                    # ---------------------------------
                    while True:
                        length_ch = 0
                        if len(captcha_txt2) != 8:
                            # Re_ScreenShot_captcha()
                            # ------------------------------
                            driver.find_element_by_class_name('refresh-btn').click()
                            driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr

                            # <----- for maximise windo upto 170% & scroll down --->
                            driver.execute_script("document.body.style.zoom='170%'")
                            driver.execute_script("window.scrollTo(0,400);")
                            delay1()

                            element = driver.find_element_by_xpath("//*[@id='captcha_image']")

                            location = {'x': 240, 'y': 260}
                            size = {'height': 61, 'width': 310}

                            driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR7.png')

                            x = location['x']
                            y = location['y']
                            width = location['x'] + size['width']
                            height = location['y'] + size['height']

                            im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR7.png')
                            im = im.crop((int(x), int(y), int(width), int(height)))

                            im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR7.png')
                            delay1()
                            # <-------- for crop save captcha image in propper size ----// START //--
                            im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR7.png')
                            width, height = im2.size

                            left = 59
                            top = 0  # done
                            right = 250  # done
                            bottom = 57  # done

                            # Cropped image of above dimension
                            im1 = im2.crop((left, top, right, bottom))
                            im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR7.png')
                            # <-------- for crop save captcha image in propper size ---// END //---

                            # for Captcha image to text convert---------------comment it------------
                            img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR7.png')
                            captcha_txt = tess.image_to_string(img, config='')

                            # <-------- improve image quality ----------->
                            contrast = ImageEnhance.Contrast(img)
                            contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR7.png')
                            # <------------------->

                            # <----- for remove unwanted color from image ---------->
                            image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR7.png')
                            grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                            grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                            lower_range = np.array([0, 0, 0])
                            upper_range = np.array([0, 0, 0])

                            mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR7.png', mask)
                            # <-----Done remove unwanted color from image ---------->

                            # <------ image color change in black n white ------
                            Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR7.png', 0)
                            ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR7.png', thresh_img)
                            cv2.waitKey(0)
                            cv2.destroyAllWindows()

                            # <----- for expand image text ---------------------
                            Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR7.png', 0)
                            kernel = np.ones((3, 3), np.uint8)  # <---- (2, 2)  also work

                            erosion = cv2.erode(Load2, kernel, iterations=1)
                            img = erosion.copy()
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR7.png', img)

                            # <----- for Captcha image to text convert---------------------
                            img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR7.png')
                            # global re_captcha_txt
                            re_captcha_txt = tess.image_to_string(img2, config='')
                            # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)
                            captcha_txt2 = re_captcha_txt

                            # ---------------end---------------------
                            # <---- driver back to original position ----
                            driver.execute_script("document.body.style.zoom='100%'")
                            driver.execute_script("window.scrollTo(0,0);")
                            delay1()

                            if len(captcha_txt2) != 8:
                                length_ch = 0
                                delay1()
                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                #print('re_captcha_txt_final:-', re_captcha_txt_final)

                        else:
                            re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                            #print('re_captcha_txt_final:-', re_captcha_txt_final)
                            length_ch = 1

                        if length_ch == 1:
                            break

                    # <----- for Enter Captcha image text to input field---------------------
                    input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                    input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                    # <----- for Click Search btn---------------------

                    # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                    driver.find_element_by_xpath("//*[@id='searchbtn']").click()  # <--- sertch btn --->
                    # Enter_Captcha_image_text_to_input_field ---- END --------------//////-----------


                except:
                    delay()
                    delay()
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    op_page_caseHistoryDiv = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # -----For Invalid captcha page ("display: block;")

                    if op_page_caseHistoryDiv == 'display: block;':
                        all_divs = soup.find('div', {'id': 'caseHistoryDiv'})
                        all_tables = all_divs.find_all('p')

                        check = str(all_tables)
                        if check == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                            driver.find_elements_by_xpath('//*[@id="bckbtn"]')[0].click()
                            delay()

                            main_ch = 0

                        else:
                            # <-----/// Final page table conversion start ///----->
                            try:
                                # <---1st Table, take single table data, into list ------------>
                                l = []
                                table = soup.find('table', attrs={'class': 'case_details_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l)

                                b = l[:len(
                                    l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                                lst = reduce(lambda x, y: x + y, b)
                                lst.append('cnrnumber')  # < ---- add 'CNR' name
                                lst.append(slug_)  # < ---- add 'CNR number
                                # print(lst)
                                res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in
                                           range(0, len(lst), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('1st Table:-- ', res_dct)
                            except:
                                res_dct = {}

                            try:
                                # <---2nd Table, take single table data, into list ------------>
                                l2 = []
                                table = soup.find('table', attrs={'class': 'table_r'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l2.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l2)

                                lst2 = reduce(lambda x, y: x + y, l2)
                                # print(lst2)
                                res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in
                                            range(0, len(lst2), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('2nd Table: ', res_dct2)
                            except:
                                res_dct2 = {}

                            try:
                                # <---3rd Table, take single table data, into list ----------->
                                l3 = []
                                table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l3.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('Petitioner_Advocate_table: ', l3)

                                lst3 = reduce(lambda x, y: x + y, l3)
                                listToStr = ' '.join(map(str, lst3))
                                # print(lst3)
                                res_dct3 = listToStr.replace('\xa0',
                                                             '')  # <-- make kye 'LOWER case', remove 'space' ---
                                # print('PetitionerAdvocate Table: ', res_dct3)
                            except:
                                res_dct3 = ''

                            try:
                                # <---4th Table, take single table data, into list ---------->
                                l4 = []
                                table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l4.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ', l4)

                                lst4 = reduce(lambda x, y: x + y, l4)
                                listToStr2 = ' '.join(map(str, lst4))
                                # print(lst4)
                                res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                                # print('4th Table: ', res_dct4)
                            except:
                                res_dct4 = ''

                            try:
                                # <---5th Table, take single table data, into list ---------->
                                table_a = soup.find('table', attrs={'class': 'acts_table'})
                                h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                                res_dct5 = [dict(zip(h, i)) for i in d]
                                # print('5th Table: ', res_dct5)

                                # k5 = res_dct5
                                # print(k5)

                                k5 = []
                                for d in res_dct5:
                                    k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                        'UnderSection(s)', 'Section'): v for k, v in d.items()})
                                # print('res_dct5: ', res_dct5)
                            except:
                                k5 = []

                            try:
                                # <---6th Table, take single table data, into list ---------->
                                table_t = soup.find('table', attrs={'class': 'history_table'})
                                h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                                res_dct6 = [dict(zip(h, i)) for i in d]

                                k6 = []
                                for d in res_dct6:
                                    k6.append(
                                        {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                         d.items()})
                                # print('k6---:', k6)
                            except:
                                k6 = []

                            try:
                                # <---7th Table, take single table data, into list ---------->
                                table_order = soup.find('table', attrs={'class': 'order_table'})
                                h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                    [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                                res_dct7 = [dict(zip(h7, x)) for x in q]
                                # print('7th Table: ', res_dct7)

                                k7 = []
                                for d7 in res_dct7:
                                    k7.append(
                                        {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                            except:
                                k7 = []

                            try:
                                # <---8th Table, take single table data, into list ---------->
                                table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                                h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                    [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                                res_dct8 = [dict(zip(h1, x)) for x in q]
                                # print('8th Table: ', res_dct8)

                                k8 = []
                                for d in res_dct8:
                                    k8.append({k.replace(' ', ''): v.replace('\xa0', '') for k, v in d.items()})
                                # # print(k8)
                            except:
                                k8 = []

                            main_ch = 1

                            if res_dct != {}:
                                # print("if res_dct != '{}':---- ", res_dct)
                                final = {"CaseDetails": res_dct, "CaseStatus": res_dct2, "PetitionerAdvocate": res_dct3,
                                         "RespondentAdvocate": res_dct4, "Acts": k5,
                                         "History": k6, "Orders": k7, "CaseTransferDetails": k8}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            elif res_dct == {}:
                                final_2 = {
                                    "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            else:
                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                    elif op_page_caseHistoryDiv == 'display: none;':
                        # <- if any captcha text miss, then 'Invalid Captcha' ALERT BOX ---
                        # alert_page = soup.find('div', {'id': 'bs_alert'}).get('style')  # ----- get value "true/false"
                        ok_btn = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok = ok_btn.is_enabled()  # value in True n false

                        if ok == True:
                            ok_btn.click()

                        driver.refresh()
                        main_ch = 0
                        delay()

                    elif op_page_caseHistoryDiv == 'display:none;':  # <-- submit empty catcha --- then alert popup
                        ok_btn2 = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok2 = ok_btn2.is_enabled()  # value in True n false

                        if ok2 == True:
                            ok_btn2.click()
                        driver.refresh()
                        main_ch = 0
                        delay()
                        # delay()
                if main_ch == 1:
                    break

            if res_dct != {}:
                yourdata = final

            elif res_dct == {}:
                yourdata = final_2

            data = yourdata
            try:
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")
                cursor = conn.cursor()
                #print('psycopg2 connect execute--')
            except (Exception, psycopg2.DatabaseError) as error:
                #print("Error while creating PostgreSQL table", error)
                pass
            else:
                cursor.execute('SELECT * FROM public."Scrap_App_count7" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_count7" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                #print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------// function 7 End //--------------------------------------------------

        # t = multiprocessing.Process(target=f7).start()
        t = threading.Thread(target=f7).start()

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            wait = Count7.objects.all().order_by('-id')[0].data
            if wait == '':
                w = 0
            else:
                w = 1
            if w == 1:
                break
        # ------// --- //----
        time.sleep(3)
        data = Count7.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        aa_id2 = Count7.objects.all().order_by('-id')[0].id
        aa_id3 = Count7.objects.get(id=aa_id2)
        aa_id3.var = 0  # make 1 to 0 for available for next request.
        aa_id3.save()


# ------------------(cnr_work7e)----------------------

    elif Count8.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()
        Count8(var=1, time=time.time(),
                slug=slug).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        # print('slug_8: ', slug)

        slug_data = Count8.objects.all().order_by('-id')[0].slug

        # ------------------(cnr_work8)----------------------
        def f8():
            global res_dct, final, final_2, re_captcha_txt_final, re_captcha_txt_final
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            # print('f1--')

            # ---/ op / ---
            slug_ = slug_data

            # -------------------------// our code //---------------
            # <--- all repeated Functions ----/////---
            def delay1():
                time.sleep(random.randint(1, 2))

            def delay():
                time.sleep(random.randint(2, 2))

            while True:
                main_ch = 0
                try:
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    div_style_value = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # ----- get value "display: none;"

                    # Find_data----- START --
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.XPATH, "//*[@id='cino']")))  # wait
                    elem = driver.find_element(By.ID, "cino")
                    elem.send_keys(slug_)  ##### <---- Pass CNR no ---->

                    # <----- for maximise windo upto 170% & scroll down --->
                    driver.execute_script("document.body.style.zoom='170%'")
                    driver.execute_script("window.scrollTo(0,400);")
                    delay1()

                    # <--- to find captcha image location ---->
                    element = driver.find_element_by_xpath("//*[@id='captcha_image']")
                    location = {'x': 240, 'y': 260}  # -for without hide chrome {'x': 240, 'y': 280}
                    size = {'height': 61, 'width': 310}  # -for without hide chrome {'height': 59, 'width': 310}

                    driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR8.png')

                    x = location['x']
                    y = location['y']
                    width = location['x'] + size['width']
                    height = location['y'] + size['height']

                    im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR8.png')
                    im = im.crop((int(x), int(y), int(width), int(height)))
                    im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR8.png')
                    delay1()

                    # <-------- for crop save captcha image in propper size ----// START //--
                    im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR8.png')
                    width, height = im2.size
                    #print('w: ', width, 'h: ', height)
                    # Setting the points for cropped image
                    left = 59  # -for without hide chrome "left = 146"
                    top = 0  # done
                    right = 250  # done                      # -for without hide chrome "right = 310"
                    bottom = 57  # done

                    # Cropped image of above dimension
                    im1 = im2.crop((left, top, right, bottom))
                    im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR8.png')
                    #im1.show()  # <-- comment it ---
                    # <-------- for crop save captcha image in propper size ---// END //---

                    # <---- for Captcha image to text convert---------------comment it------------
                    img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR8.png')

                    # <-------- improve image quality ----------->
                    contrast = ImageEnhance.Contrast(img)
                    contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR8.png')
                    # <------------------->

                    # <----- for remove unwanted color from image ---------->
                    image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR8.png')
                    grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                    grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                    lower_range = np.array([0, 0, 0])
                    upper_range = np.array([0, 0, 0])

                    mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR8.png', mask)
                    # <-----Done remove unwanted color from image ---------->

                    # <------ image color change in black n white ------
                    Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR8.png', 0)
                    ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR8.png', thresh_img)
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                    # <----- for expand image text ---------------------
                    Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR8.png', 0)
                    kernel = np.ones((3, 3), np.uint8)
                    erosion = cv2.erode(Load2, kernel, iterations=1)
                    img = erosion.copy()
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR8.png', img)

                    # <----- for Captcha image to text convert---------------------
                    img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR8.png')
                    captcha_txt2 = tess.image_to_string(img2, config='')
                    delay1()

                    # Find_data-------- END ----------------//////-----------

                    # <---- driver back to original position ----
                    driver.execute_script("document.body.style.zoom='100%'")
                    driver.execute_script("window.scrollTo(0,0);")
                    delay1()

                    # --------------------------------
                    # ---------------------------------
                    while True:
                        length_ch = 0
                        if len(captcha_txt2) != 8:
                            # Re_ScreenShot_captcha()
                            # ------------------------------
                            driver.find_element_by_class_name('refresh-btn').click()
                            driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr

                            # <----- for maximise windo upto 170% & scroll down --->
                            driver.execute_script("document.body.style.zoom='170%'")
                            driver.execute_script("window.scrollTo(0,400);")
                            delay1()

                            element = driver.find_element_by_xpath("//*[@id='captcha_image']")

                            location = {'x': 240, 'y': 260}
                            size = {'height': 61, 'width': 310}

                            driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR8.png')

                            x = location['x']
                            y = location['y']
                            width = location['x'] + size['width']
                            height = location['y'] + size['height']

                            im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR8.png')
                            im = im.crop((int(x), int(y), int(width), int(height)))

                            im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR8.png')
                            delay1()
                            # <-------- for crop save captcha image in propper size ----// START //--
                            im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR8.png')
                            width, height = im2.size

                            left = 59
                            top = 0  # done
                            right = 250  # done
                            bottom = 57  # done

                            # Cropped image of above dimension
                            im1 = im2.crop((left, top, right, bottom))
                            im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR8.png')
                            # <-------- for crop save captcha image in propper size ---// END //---

                            # for Captcha image to text convert---------------comment it------------
                            img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR8.png')
                            captcha_txt = tess.image_to_string(img, config='')

                            # <-------- improve image quality ----------->
                            contrast = ImageEnhance.Contrast(img)
                            contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR8.png')
                            # <------------------->

                            # <----- for remove unwanted color from image ---------->
                            image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR8.png')
                            grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                            grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                            lower_range = np.array([0, 0, 0])
                            upper_range = np.array([0, 0, 0])

                            mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR8.png', mask)
                            # <-----Done remove unwanted color from image ---------->

                            # <------ image color change in black n white ------
                            Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR8.png', 0)
                            ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR8.png', thresh_img)
                            cv2.waitKey(0)
                            cv2.destroyAllWindows()

                            # <----- for expand image text ---------------------
                            Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR8.png', 0)
                            kernel = np.ones((3, 3), np.uint8)  # <---- (2, 2)  also work

                            erosion = cv2.erode(Load2, kernel, iterations=1)
                            img = erosion.copy()
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR8.png', img)

                            # <----- for Captcha image to text convert---------------------
                            img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR8.png')
                            # global re_captcha_txt
                            re_captcha_txt = tess.image_to_string(img2, config='')
                            # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)
                            captcha_txt2 = re_captcha_txt

                            # ---------------end---------------------
                            # <---- driver back to original position ----
                            driver.execute_script("document.body.style.zoom='100%'")
                            driver.execute_script("window.scrollTo(0,0);")
                            delay1()

                            if len(captcha_txt2) != 8:
                                length_ch = 0
                                delay1()
                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                #print('re_captcha_txt_final:-', re_captcha_txt_final)

                        else:
                            re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                            #print('re_captcha_txt_final:-', re_captcha_txt_final)
                            length_ch = 1

                        if length_ch == 1:
                            break

                    # <----- for Enter Captcha image text to input field---------------------
                    input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                    input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                    # <----- for Click Search btn---------------------

                    # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                    driver.find_element_by_xpath("//*[@id='searchbtn']").click()  # <--- sertch btn --->
                    # Enter_Captcha_image_text_to_input_field ---- END --------------//////-----------


                except:
                    delay()
                    delay()
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    op_page_caseHistoryDiv = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # -----For Invalid captcha page ("display: block;")

                    if op_page_caseHistoryDiv == 'display: block;':
                        all_divs = soup.find('div', {'id': 'caseHistoryDiv'})
                        all_tables = all_divs.find_all('p')

                        check = str(all_tables)
                        if check == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                            driver.find_elements_by_xpath('//*[@id="bckbtn"]')[0].click()
                            delay()

                            main_ch = 0

                        else:
                            # <-----/// Final page table conversion start ///----->
                            try:
                                # <---1st Table, take single table data, into list ------------>
                                l = []
                                table = soup.find('table', attrs={'class': 'case_details_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l)

                                b = l[:len(
                                    l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                                lst = reduce(lambda x, y: x + y, b)
                                lst.append('cnrnumber')  # < ---- add 'CNR' name
                                lst.append(slug_)  # < ---- add 'CNR number
                                # print(lst)
                                res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in
                                           range(0, len(lst), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('1st Table:-- ', res_dct)
                            except:
                                res_dct = {}

                            try:
                                # <---2nd Table, take single table data, into list ------------>
                                l2 = []
                                table = soup.find('table', attrs={'class': 'table_r'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l2.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l2)

                                lst2 = reduce(lambda x, y: x + y, l2)
                                # print(lst2)
                                res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in
                                            range(0, len(lst2), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('2nd Table: ', res_dct2)
                            except:
                                res_dct2 = {}

                            try:
                                # <---3rd Table, take single table data, into list ----------->
                                l3 = []
                                table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l3.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('Petitioner_Advocate_table: ', l3)

                                lst3 = reduce(lambda x, y: x + y, l3)
                                listToStr = ' '.join(map(str, lst3))
                                # print(lst3)
                                res_dct3 = listToStr.replace('\xa0',
                                                             '')  # <-- make kye 'LOWER case', remove 'space' ---
                                # print('PetitionerAdvocate Table: ', res_dct3)
                            except:
                                res_dct3 = ''

                            try:
                                # <---4th Table, take single table data, into list ---------->
                                l4 = []
                                table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l4.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ', l4)

                                lst4 = reduce(lambda x, y: x + y, l4)
                                listToStr2 = ' '.join(map(str, lst4))
                                # print(lst4)
                                res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                                # print('4th Table: ', res_dct4)
                            except:
                                res_dct4 = ''

                            try:
                                # <---5th Table, take single table data, into list ---------->
                                table_a = soup.find('table', attrs={'class': 'acts_table'})
                                h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                                res_dct5 = [dict(zip(h, i)) for i in d]
                                # print('5th Table: ', res_dct5)

                                # k5 = res_dct5
                                # print(k5)

                                k5 = []
                                for d in res_dct5:
                                    k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                        'UnderSection(s)', 'Section'): v for k, v in d.items()})
                                # print('res_dct5: ', res_dct5)
                            except:
                                k5 = []

                            try:
                                # <---6th Table, take single table data, into list ---------->
                                table_t = soup.find('table', attrs={'class': 'history_table'})
                                h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                                res_dct6 = [dict(zip(h, i)) for i in d]

                                k6 = []
                                for d in res_dct6:
                                    k6.append(
                                        {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                         d.items()})
                                # print('k6---:', k6)
                            except:
                                k6 = []

                            try:
                                # <---7th Table, take single table data, into list ---------->
                                table_order = soup.find('table', attrs={'class': 'order_table'})
                                h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                    [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                                res_dct7 = [dict(zip(h7, x)) for x in q]
                                # print('7th Table: ', res_dct7)

                                k7 = []
                                for d7 in res_dct7:
                                    k7.append(
                                        {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                            except:
                                k7 = []

                            try:
                                # <---8th Table, take single table data, into list ---------->
                                table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                                h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                    [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                                res_dct8 = [dict(zip(h1, x)) for x in q]
                                # print('8th Table: ', res_dct8)

                                k8 = []
                                for d in res_dct8:
                                    k8.append({k.replace(' ', ''): v.replace('\xa0', '') for k, v in d.items()})
                                # # print(k8)
                            except:
                                k8 = []

                            main_ch = 1

                            if res_dct != {}:
                                # print("if res_dct != '{}':---- ", res_dct)
                                final = {"CaseDetails": res_dct, "CaseStatus": res_dct2, "PetitionerAdvocate": res_dct3,
                                         "RespondentAdvocate": res_dct4, "Acts": k5,
                                         "History": k6, "Orders": k7, "CaseTransferDetails": k8}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            elif res_dct == {}:
                                final_2 = {
                                    "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            else:
                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                    elif op_page_caseHistoryDiv == 'display: none;':
                        # <- if any captcha text miss, then 'Invalid Captcha' ALERT BOX ---
                        # alert_page = soup.find('div', {'id': 'bs_alert'}).get('style')  # ----- get value "true/false"
                        ok_btn = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok = ok_btn.is_enabled()  # value in True n false

                        if ok == True:
                            ok_btn.click()

                        driver.refresh()
                        main_ch = 0
                        delay()

                    elif op_page_caseHistoryDiv == 'display:none;':  # <-- submit empty catcha --- then alert popup
                        ok_btn2 = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok2 = ok_btn2.is_enabled()  # value in True n false

                        if ok2 == True:
                            ok_btn2.click()
                        driver.refresh()
                        main_ch = 0
                        delay()
                        # delay()
                if main_ch == 1:
                    break

            if res_dct != {}:
                yourdata = final

            elif res_dct == {}:
                yourdata = final_2

            data = yourdata
            try:
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")
                cursor = conn.cursor()
                #print('psycopg2 connect execute--')
            except (Exception, psycopg2.DatabaseError) as error:
                #print("Error while creating PostgreSQL table", error)
                pass
            else:
                cursor.execute('SELECT * FROM public."Scrap_App_count8" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_count8" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                #print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------// function 8 End //--------------------------------------------------

        # t = multiprocessing.Process(target=f8).start()
        t = threading.Thread(target=f8).start()

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            wait = Count8.objects.all().order_by('-id')[0].data
            if wait == '':
                w = 0
            else:
                w = 1
            if w == 1:
                break
        # ------// --- //----
        time.sleep(3)
        data = Count8.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        aa_id2 = Count8.objects.all().order_by('-id')[0].id
        aa_id3 = Count8.objects.get(id=aa_id2)
        aa_id3.var = 0  # make 1 to 0 for available for next request.
        aa_id3.save()


# ------------------(cnr_work8e)----------------------

    elif Count9.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()
        Count9(var=1, time=time.time(),
                slug=slug).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        # print('slug_9: ', slug)

        slug_data = Count9.objects.all().order_by('-id')[0].slug

        # ------------------(cnr_work9)----------------------
        def f9():
            global res_dct, final, final_2, re_captcha_txt_final, re_captcha_txt_final
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            # print('f9--')

            # ---/ op / ---
            slug_ = slug_data

            # -------------------------// our code //---------------
            # <--- all repeated Functions ----/////---
            def delay1():
                time.sleep(random.randint(1, 2))

            def delay():
                time.sleep(random.randint(2, 2))

            while True:
                main_ch = 0
                try:
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    div_style_value = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # ----- get value "display: none;"

                    # Find_data----- START --
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.XPATH, "//*[@id='cino']")))  # wait
                    elem = driver.find_element(By.ID, "cino")
                    elem.send_keys(slug_)  ##### <---- Pass CNR no ---->

                    # <----- for maximise windo upto 170% & scroll down --->
                    driver.execute_script("document.body.style.zoom='170%'")
                    driver.execute_script("window.scrollTo(0,400);")
                    delay1()

                    # <--- to find captcha image location ---->
                    element = driver.find_element_by_xpath("//*[@id='captcha_image']")
                    location = {'x': 240, 'y': 260}  # -for without hide chrome {'x': 240, 'y': 280}
                    size = {'height': 61, 'width': 310}  # -for without hide chrome {'height': 59, 'width': 310}

                    driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR9.png')

                    x = location['x']
                    y = location['y']
                    width = location['x'] + size['width']
                    height = location['y'] + size['height']

                    im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR9.png')
                    im = im.crop((int(x), int(y), int(width), int(height)))
                    im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR9.png')
                    delay1()

                    # <-------- for crop save captcha image in propper size ----// START //--
                    im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR9.png')
                    width, height = im2.size
                    #print('w: ', width, 'h: ', height)
                    # Setting the points for cropped image
                    left = 59  # -for without hide chrome "left = 146"
                    top = 0  # done
                    right = 250  # done                      # -for without hide chrome "right = 310"
                    bottom = 57  # done

                    # Cropped image of above dimension
                    im1 = im2.crop((left, top, right, bottom))
                    im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR9.png')
                    #im1.show()  # <-- comment it ---
                    # <-------- for crop save captcha image in propper size ---// END //---

                    # <---- for Captcha image to text convert---------------comment it------------
                    img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR9.png')

                    # <-------- improve image quality ----------->
                    contrast = ImageEnhance.Contrast(img)
                    contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR9.png')
                    # <------------------->

                    # <----- for remove unwanted color from image ---------->
                    image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR9.png')
                    grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                    grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                    lower_range = np.array([0, 0, 0])
                    upper_range = np.array([0, 0, 0])

                    mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR9.png', mask)
                    # <-----Done remove unwanted color from image ---------->

                    # <------ image color change in black n white ------
                    Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR9.png', 0)
                    ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR9.png', thresh_img)
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                    # <----- for expand image text ---------------------
                    Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR9.png', 0)
                    kernel = np.ones((3, 3), np.uint8)
                    erosion = cv2.erode(Load2, kernel, iterations=1)
                    img = erosion.copy()
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR9.png', img)

                    # <----- for Captcha image to text convert---------------------
                    img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR9.png')
                    captcha_txt2 = tess.image_to_string(img2, config='')
                    delay1()

                    # Find_data-------- END ----------------//////-----------

                    # <---- driver back to original position ----
                    driver.execute_script("document.body.style.zoom='100%'")
                    driver.execute_script("window.scrollTo(0,0);")
                    delay1()

                    # --------------------------------
                    # ---------------------------------
                    while True:
                        length_ch = 0
                        if len(captcha_txt2) != 8:
                            # Re_ScreenShot_captcha()
                            # ------------------------------
                            driver.find_element_by_class_name('refresh-btn').click()
                            driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr

                            # <----- for maximise windo upto 170% & scroll down --->
                            driver.execute_script("document.body.style.zoom='170%'")
                            driver.execute_script("window.scrollTo(0,400);")
                            delay1()

                            element = driver.find_element_by_xpath("//*[@id='captcha_image']")

                            location = {'x': 240, 'y': 260}
                            size = {'height': 61, 'width': 310}

                            driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR9.png')

                            x = location['x']
                            y = location['y']
                            width = location['x'] + size['width']
                            height = location['y'] + size['height']

                            im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR9.png')
                            im = im.crop((int(x), int(y), int(width), int(height)))

                            im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR9.png')
                            delay1()
                            # <-------- for crop save captcha image in propper size ----// START //--
                            im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR9.png')
                            width, height = im2.size

                            left = 59
                            top = 0  # done
                            right = 250  # done
                            bottom = 57  # done

                            # Cropped image of above dimension
                            im1 = im2.crop((left, top, right, bottom))
                            im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR9.png')
                            # <-------- for crop save captcha image in propper size ---// END //---

                            # for Captcha image to text convert---------------comment it------------
                            img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR9.png')
                            captcha_txt = tess.image_to_string(img, config='')

                            # <-------- improve image quality ----------->
                            contrast = ImageEnhance.Contrast(img)
                            contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR9.png')
                            # <------------------->

                            # <----- for remove unwanted color from image ---------->
                            image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR9.png')
                            grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                            grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                            lower_range = np.array([0, 0, 0])
                            upper_range = np.array([0, 0, 0])

                            mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR9.png', mask)
                            # <-----Done remove unwanted color from image ---------->

                            # <------ image color change in black n white ------
                            Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR9.png', 0)
                            ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR9.png', thresh_img)
                            cv2.waitKey(0)
                            cv2.destroyAllWindows()

                            # <----- for expand image text ---------------------
                            Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR9.png', 0)
                            kernel = np.ones((3, 3), np.uint8)  # <---- (2, 2)  also work

                            erosion = cv2.erode(Load2, kernel, iterations=1)
                            img = erosion.copy()
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR9.png', img)

                            # <----- for Captcha image to text convert---------------------
                            img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR9.png')
                            # global re_captcha_txt
                            re_captcha_txt = tess.image_to_string(img2, config='')
                            # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)
                            captcha_txt2 = re_captcha_txt

                            # ---------------end---------------------
                            # <---- driver back to original position ----
                            driver.execute_script("document.body.style.zoom='100%'")
                            driver.execute_script("window.scrollTo(0,0);")
                            delay1()

                            if len(captcha_txt2) != 8:
                                length_ch = 0
                                delay1()
                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                #print('re_captcha_txt_final:-', re_captcha_txt_final)

                        else:
                            re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                            #print('re_captcha_txt_final:-', re_captcha_txt_final)
                            length_ch = 1

                        if length_ch == 1:
                            break

                    # <----- for Enter Captcha image text to input field---------------------
                    input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                    input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                    # <----- for Click Search btn---------------------

                    # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                    driver.find_element_by_xpath("//*[@id='searchbtn']").click()  # <--- sertch btn --->
                    # Enter_Captcha_image_text_to_input_field ---- END --------------//////-----------


                except:
                    delay()
                    delay()
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    op_page_caseHistoryDiv = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # -----For Invalid captcha page ("display: block;")

                    if op_page_caseHistoryDiv == 'display: block;':
                        all_divs = soup.find('div', {'id': 'caseHistoryDiv'})
                        all_tables = all_divs.find_all('p')

                        check = str(all_tables)
                        if check == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                            driver.find_elements_by_xpath('//*[@id="bckbtn"]')[0].click()
                            delay()

                            main_ch = 0

                        else:
                            # <-----/// Final page table conversion start ///----->
                            try:
                                # <---1st Table, take single table data, into list ------------>
                                l = []
                                table = soup.find('table', attrs={'class': 'case_details_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l)

                                b = l[:len(
                                    l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                                lst = reduce(lambda x, y: x + y, b)
                                lst.append('cnrnumber')  # < ---- add 'CNR' name
                                lst.append(slug_)  # < ---- add 'CNR number
                                # print(lst)
                                res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in
                                           range(0, len(lst), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('1st Table:-- ', res_dct)
                            except:
                                res_dct = {}

                            try:
                                # <---2nd Table, take single table data, into list ------------>
                                l2 = []
                                table = soup.find('table', attrs={'class': 'table_r'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l2.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l2)

                                lst2 = reduce(lambda x, y: x + y, l2)
                                # print(lst2)
                                res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in
                                            range(0, len(lst2), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('2nd Table: ', res_dct2)
                            except:
                                res_dct2 = {}

                            try:
                                # <---3rd Table, take single table data, into list ----------->
                                l3 = []
                                table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l3.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('Petitioner_Advocate_table: ', l3)

                                lst3 = reduce(lambda x, y: x + y, l3)
                                listToStr = ' '.join(map(str, lst3))
                                # print(lst3)
                                res_dct3 = listToStr.replace('\xa0',
                                                             '')  # <-- make kye 'LOWER case', remove 'space' ---
                                # print('PetitionerAdvocate Table: ', res_dct3)
                            except:
                                res_dct3 = ''

                            try:
                                # <---4th Table, take single table data, into list ---------->
                                l4 = []
                                table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l4.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ', l4)

                                lst4 = reduce(lambda x, y: x + y, l4)
                                listToStr2 = ' '.join(map(str, lst4))
                                # print(lst4)
                                res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                                # print('4th Table: ', res_dct4)
                            except:
                                res_dct4 = ''

                            try:
                                # <---5th Table, take single table data, into list ---------->
                                table_a = soup.find('table', attrs={'class': 'acts_table'})
                                h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                                res_dct5 = [dict(zip(h, i)) for i in d]
                                # print('5th Table: ', res_dct5)

                                # k5 = res_dct5
                                # print(k5)

                                k5 = []
                                for d in res_dct5:
                                    k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                        'UnderSection(s)', 'Section'): v for k, v in d.items()})
                                # print('res_dct5: ', res_dct5)
                            except:
                                k5 = []

                            try:
                                # <---6th Table, take single table data, into list ---------->
                                table_t = soup.find('table', attrs={'class': 'history_table'})
                                h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                                res_dct6 = [dict(zip(h, i)) for i in d]

                                k6 = []
                                for d in res_dct6:
                                    k6.append(
                                        {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                         d.items()})
                                # print('k6---:', k6)
                            except:
                                k6 = []

                            try:
                                # <---7th Table, take single table data, into list ---------->
                                table_order = soup.find('table', attrs={'class': 'order_table'})
                                h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                    [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                                res_dct7 = [dict(zip(h7, x)) for x in q]
                                # print('7th Table: ', res_dct7)

                                k7 = []
                                for d7 in res_dct7:
                                    k7.append(
                                        {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                            except:
                                k7 = []

                            try:
                                # <---8th Table, take single table data, into list ---------->
                                table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                                h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                    [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                                res_dct8 = [dict(zip(h1, x)) for x in q]
                                # print('8th Table: ', res_dct8)

                                k8 = []
                                for d in res_dct8:
                                    k8.append({k.replace(' ', ''): v.replace('\xa0', '') for k, v in d.items()})
                                # # print(k8)
                            except:
                                k8 = []

                            main_ch = 1

                            if res_dct != {}:
                                # print("if res_dct != '{}':---- ", res_dct)
                                final = {"CaseDetails": res_dct, "CaseStatus": res_dct2, "PetitionerAdvocate": res_dct3,
                                         "RespondentAdvocate": res_dct4, "Acts": k5,
                                         "History": k6, "Orders": k7, "CaseTransferDetails": k8}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            elif res_dct == {}:
                                final_2 = {
                                    "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            else:
                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                    elif op_page_caseHistoryDiv == 'display: none;':
                        # <- if any captcha text miss, then 'Invalid Captcha' ALERT BOX ---
                        # alert_page = soup.find('div', {'id': 'bs_alert'}).get('style')  # ----- get value "true/false"
                        ok_btn = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok = ok_btn.is_enabled()  # value in True n false

                        if ok == True:
                            ok_btn.click()

                        driver.refresh()
                        main_ch = 0
                        delay()

                    elif op_page_caseHistoryDiv == 'display:none;':  # <-- submit empty catcha --- then alert popup
                        ok_btn2 = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok2 = ok_btn2.is_enabled()  # value in True n false

                        if ok2 == True:
                            ok_btn2.click()
                        driver.refresh()
                        main_ch = 0
                        delay()
                        # delay()
                if main_ch == 1:
                    break

            if res_dct != {}:
                yourdata = final

            elif res_dct == {}:
                yourdata = final_2

            data = yourdata
            try:
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")
                cursor = conn.cursor()
                #print('psycopg2 connect execute--')
            except (Exception, psycopg2.DatabaseError) as error:
                #print("Error while creating PostgreSQL table", error)
                pass
            else:
                cursor.execute('SELECT * FROM public."Scrap_App_count9" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_count9" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                #print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------// function 9 End //--------------------------------------------------

        # t = multiprocessing.Process(target=f9).start()
        t = threading.Thread(target=f9).start()

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            wait = Count9.objects.all().order_by('-id')[0].data
            if wait == '':
                w = 0
            else:
                w = 1
            if w == 1:
                break
        # ------// --- //----
        time.sleep(3)
        data = Count9.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        aa_id2 = Count9.objects.all().order_by('-id')[0].id
        aa_id3 = Count9.objects.get(id=aa_id2)
        aa_id3.var = 0  # make 1 to 0 for available for next request.
        aa_id3.save()


# ------------------(cnr_work9e)----------------------

    elif Count10.objects.all().order_by('-id')[0].var == 0:
        #start = time.perf_counter()
        Count10(var=1, time=time.time(),
                 slug=slug).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        # print('slug_10: ', slug)

        slug_data = Count10.objects.all().order_by('-id')[0].slug

        # ------------------(cnr_work10)----------------------
        def f10():
            global res_dct, final, final_2, re_captcha_txt_final, re_captcha_txt_final
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            # print('f1--')

            # ---/ op / ---
            slug_ = slug_data

            # -------------------------// our code //---------------
            # <--- all repeated Functions ----/////---
            def delay1():
                time.sleep(random.randint(1, 2))

            def delay():
                time.sleep(random.randint(2, 2))

            while True:
                main_ch = 0
                try:
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    div_style_value = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # ----- get value "display: none;"

                    # Find_data----- START --
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.XPATH, "//*[@id='cino']")))  # wait
                    elem = driver.find_element(By.ID, "cino")
                    elem.send_keys(slug_)  ##### <---- Pass CNR no ---->

                    # <----- for maximise windo upto 170% & scroll down --->
                    driver.execute_script("document.body.style.zoom='170%'")
                    driver.execute_script("window.scrollTo(0,400);")
                    delay1()

                    # <--- to find captcha image location ---->
                    element = driver.find_element_by_xpath("//*[@id='captcha_image']")
                    location = {'x': 240, 'y': 260}  # -for without hide chrome {'x': 240, 'y': 280}
                    size = {'height': 61, 'width': 310}  # -for without hide chrome {'height': 59, 'width': 310}

                    driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR10.png')

                    x = location['x']
                    y = location['y']
                    width = location['x'] + size['width']
                    height = location['y'] + size['height']

                    im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR10.png')
                    im = im.crop((int(x), int(y), int(width), int(height)))
                    im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR10.png')
                    delay1()

                    # <-------- for crop save captcha image in propper size ----// START //--
                    im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR10.png')
                    width, height = im2.size
                    #print('w: ', width, 'h: ', height)
                    # Setting the points for cropped image
                    left = 59  # -for without hide chrome "left = 146"
                    top = 0  # done
                    right = 250  # done                      # -for without hide chrome "right = 310"
                    bottom = 57  # done

                    # Cropped image of above dimension
                    im1 = im2.crop((left, top, right, bottom))
                    im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR10.png')
                    #im1.show()  # <-- comment it ---
                    # <-------- for crop save captcha image in propper size ---// END //---

                    # <---- for Captcha image to text convert---------------comment it------------
                    img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR10.png')

                    # <-------- improve image quality ----------->
                    contrast = ImageEnhance.Contrast(img)
                    contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR10.png')
                    # <------------------->

                    # <----- for remove unwanted color from image ---------->
                    image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR10.png')
                    grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                    grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                    lower_range = np.array([0, 0, 0])
                    upper_range = np.array([0, 0, 0])

                    mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR10.png', mask)
                    # <-----Done remove unwanted color from image ---------->

                    # <------ image color change in black n white ------
                    Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR10.png', 0)
                    ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR10.png', thresh_img)
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                    # <----- for expand image text ---------------------
                    Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR10.png', 0)
                    kernel = np.ones((3, 3), np.uint8)
                    erosion = cv2.erode(Load2, kernel, iterations=1)
                    img = erosion.copy()
                    cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR10.png', img)

                    # <----- for Captcha image to text convert---------------------
                    img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR10.png')
                    captcha_txt2 = tess.image_to_string(img2, config='')
                    delay1()

                    # Find_data-------- END ----------------//////-----------

                    # <---- driver back to original position ----
                    driver.execute_script("document.body.style.zoom='100%'")
                    driver.execute_script("window.scrollTo(0,0);")
                    delay1()

                    # --------------------------------
                    # ---------------------------------
                    while True:
                        length_ch = 0
                        if len(captcha_txt2) != 8:
                            # Re_ScreenShot_captcha()
                            # ------------------------------
                            driver.find_element_by_class_name('refresh-btn').click()
                            driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr

                            # <----- for maximise windo upto 170% & scroll down --->
                            driver.execute_script("document.body.style.zoom='170%'")
                            driver.execute_script("window.scrollTo(0,400);")
                            delay1()

                            element = driver.find_element_by_xpath("//*[@id='captcha_image']")

                            location = {'x': 240, 'y': 260}
                            size = {'height': 61, 'width': 310}

                            driver.save_screenshot(settings.MEDIA_ROOT + '/CNRImages/imageCNR10.png')

                            x = location['x']
                            y = location['y']
                            width = location['x'] + size['width']
                            height = location['y'] + size['height']

                            im = Image.open(settings.MEDIA_ROOT + '/CNRImages/imageCNR10.png')
                            im = im.crop((int(x), int(y), int(width), int(height)))

                            im.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR10.png')
                            delay1()
                            # <-------- for crop save captcha image in propper size ----// START //--
                            im2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR10.png')
                            width, height = im2.size

                            left = 59
                            top = 0  # done
                            right = 250  # done
                            bottom = 57  # done

                            # Cropped image of above dimension
                            im1 = im2.crop((left, top, right, bottom))
                            im1.save(settings.MEDIA_ROOT + '/CNRImages/abcCNR10.png')
                            # <-------- for crop save captcha image in propper size ---// END //---

                            # for Captcha image to text convert---------------comment it------------
                            img = Image.open(settings.MEDIA_ROOT + '/CNRImages/abcCNR10.png')
                            captcha_txt = tess.image_to_string(img, config='')

                            # <-------- improve image quality ----------->
                            contrast = ImageEnhance.Contrast(img)
                            contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CNRImages/contrastCNR10.png')
                            # <------------------->

                            # <----- for remove unwanted color from image ---------->
                            image = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/contrastCNR10.png')
                            grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                            grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                            lower_range = np.array([0, 0, 0])
                            upper_range = np.array([0, 0, 0])

                            mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/maskCNR10.png', mask)
                            # <-----Done remove unwanted color from image ---------->

                            # <------ image color change in black n white ------
                            Load = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/maskCNR10.png', 0)
                            ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/imgCNR10.png', thresh_img)
                            cv2.waitKey(0)
                            cv2.destroyAllWindows()

                            # <----- for expand image text ---------------------
                            Load2 = cv2.imread(settings.MEDIA_ROOT + '/CNRImages/imgCNR10.png', 0)
                            kernel = np.ones((3, 3), np.uint8)  # <---- (2, 2)  also work

                            erosion = cv2.erode(Load2, kernel, iterations=1)
                            img = erosion.copy()
                            cv2.imwrite(settings.MEDIA_ROOT + '/CNRImages/resultCNR10.png', img)

                            # <----- for Captcha image to text convert---------------------
                            img2 = Image.open(settings.MEDIA_ROOT + '/CNRImages/resultCNR10.png')
                            # global re_captcha_txt
                            re_captcha_txt = tess.image_to_string(img2, config='')
                            # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)
                            captcha_txt2 = re_captcha_txt

                            # ---------------end---------------------
                            # <---- driver back to original position ----
                            driver.execute_script("document.body.style.zoom='100%'")
                            driver.execute_script("window.scrollTo(0,0);")
                            delay1()

                            if len(captcha_txt2) != 8:
                                length_ch = 0
                                delay1()
                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                #print('re_captcha_txt_final:-', re_captcha_txt_final)

                        else:
                            re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                            #print('re_captcha_txt_final:-', re_captcha_txt_final)
                            length_ch = 1

                        if length_ch == 1:
                            break

                    # <----- for Enter Captcha image text to input field---------------------
                    input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                    input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                    # <----- for Click Search btn---------------------

                    # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='searchbtn']"))).click()
                    driver.find_element_by_xpath("//*[@id='searchbtn']").click()  # <--- sertch btn --->
                    # Enter_Captcha_image_text_to_input_field ---- END --------------//////-----------


                except:
                    delay()
                    delay()
                    WebDriverWait(driver, 300).until(
                        EC.presence_of_element_located((By.ID, "caseHistoryDiv")))  # <---- wait --->
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    op_page_caseHistoryDiv = soup.find('div', {'id': 'caseHistoryDiv'}).get(
                        'style')  # -----For Invalid captcha page ("display: block;")

                    if op_page_caseHistoryDiv == 'display: block;':
                        all_divs = soup.find('div', {'id': 'caseHistoryDiv'})
                        all_tables = all_divs.find_all('p')

                        check = str(all_tables)
                        if check == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                            driver.find_elements_by_xpath('//*[@id="bckbtn"]')[0].click()
                            delay()

                            main_ch = 0

                        else:
                            # <-----/// Final page table conversion start ///----->
                            try:
                                # <---1st Table, take single table data, into list ------------>
                                l = []
                                table = soup.find('table', attrs={'class': 'case_details_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l)

                                b = l[:len(
                                    l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                                lst = reduce(lambda x, y: x + y, b)
                                lst.append('cnrnumber')  # < ---- add 'CNR' name
                                lst.append(slug_)  # < ---- add 'CNR number
                                # print(lst)
                                res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in
                                           range(0, len(lst), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('1st Table:-- ', res_dct)
                            except:
                                res_dct = {}

                            try:
                                # <---2nd Table, take single table data, into list ------------>
                                l2 = []
                                table = soup.find('table', attrs={'class': 'table_r'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l2.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ',l2)

                                lst2 = reduce(lambda x, y: x + y, l2)
                                # print(lst2)
                                res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in
                                            range(0, len(lst2), 2)}  # <-- make kye 'LOWER case', remove 'space' --
                                # print('2nd Table: ', res_dct2)
                            except:
                                res_dct2 = {}

                            try:
                                # <---3rd Table, take single table data, into list ----------->
                                l3 = []
                                table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l3.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('Petitioner_Advocate_table: ', l3)

                                lst3 = reduce(lambda x, y: x + y, l3)
                                listToStr = ' '.join(map(str, lst3))
                                # print(lst3)
                                res_dct3 = listToStr.replace('\xa0',
                                                             '')  # <-- make kye 'LOWER case', remove 'space' ---
                                # print('PetitionerAdvocate Table: ', res_dct3)
                            except:
                                res_dct3 = ''

                            try:
                                # <---4th Table, take single table data, into list ---------->
                                l4 = []
                                table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                                table_body = table.find('tbody')

                                rows = table_body.find_all('tr')
                                for row in rows:
                                    cols = row.find_all('td')
                                    cols = [ele.text.strip() for ele in cols]
                                    l4.append([ele for ele in cols if ele])  # Get rid of empty values
                                # print('data: ', l4)

                                lst4 = reduce(lambda x, y: x + y, l4)
                                listToStr2 = ' '.join(map(str, lst4))
                                # print(lst4)
                                res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                                # print('4th Table: ', res_dct4)
                            except:
                                res_dct4 = ''

                            try:
                                # <---5th Table, take single table data, into list ---------->
                                table_a = soup.find('table', attrs={'class': 'acts_table'})
                                h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                                res_dct5 = [dict(zip(h, i)) for i in d]
                                # print('5th Table: ', res_dct5)

                                # k5 = res_dct5
                                # print(k5)

                                k5 = []
                                for d in res_dct5:
                                    k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                        'UnderSection(s)', 'Section'): v for k, v in d.items()})
                                # print('res_dct5: ', res_dct5)
                            except:
                                k5 = []

                            try:
                                # <---6th Table, take single table data, into list ---------->
                                table_t = soup.find('table', attrs={'class': 'history_table'})
                                h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                    [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                                res_dct6 = [dict(zip(h, i)) for i in d]

                                k6 = []
                                for d in res_dct6:
                                    k6.append(
                                        {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                         d.items()})
                                # print('k6---:', k6)
                            except:
                                k6 = []

                            try:
                                # <---7th Table, take single table data, into list ---------->
                                table_order = soup.find('table', attrs={'class': 'order_table'})
                                h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                    [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                                res_dct7 = [dict(zip(h7, x)) for x in q]
                                # print('7th Table: ', res_dct7)

                                k7 = []
                                for d7 in res_dct7:
                                    k7.append(
                                        {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                            except:
                                k7 = []

                            try:
                                # <---8th Table, take single table data, into list ---------->
                                table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                                h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                    [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                                res_dct8 = [dict(zip(h1, x)) for x in q]
                                # print('8th Table: ', res_dct8)

                                k8 = []
                                for d in res_dct8:
                                    k8.append({k.replace(' ', ''): v.replace('\xa0', '') for k, v in d.items()})
                                # # print(k8)
                            except:
                                k8 = []

                            main_ch = 1

                            if res_dct != {}:
                                # print("if res_dct != '{}':---- ", res_dct)
                                final = {"CaseDetails": res_dct, "CaseStatus": res_dct2, "PetitionerAdvocate": res_dct3,
                                         "RespondentAdvocate": res_dct4, "Acts": k5,
                                         "History": k6, "Orders": k7, "CaseTransferDetails": k8}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            elif res_dct == {}:
                                final_2 = {
                                    "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}

                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                            else:
                                try:
                                    driver.close()
                                    break
                                except:
                                    pass

                    elif op_page_caseHistoryDiv == 'display: none;':
                        # <- if any captcha text miss, then 'Invalid Captcha' ALERT BOX ---
                        # alert_page = soup.find('div', {'id': 'bs_alert'}).get('style')  # ----- get value "true/false"
                        ok_btn = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok = ok_btn.is_enabled()  # value in True n false

                        if ok == True:
                            ok_btn.click()

                        driver.refresh()
                        main_ch = 0
                        delay()

                    elif op_page_caseHistoryDiv == 'display:none;':  # <-- submit empty catcha --- then alert popup
                        ok_btn2 = driver.find_element_by_xpath(
                            "//*[@id='bs_alert']/div/div/div[2]/button")  # <--- if captcha wrong ALERT OK btn
                        ok2 = ok_btn2.is_enabled()  # value in True n false

                        if ok2 == True:
                            ok_btn2.click()
                        driver.refresh()
                        main_ch = 0
                        delay()
                        # delay()
                if main_ch == 1:
                    break

            if res_dct != {}:
                yourdata = final

            elif res_dct == {}:
                yourdata = final_2

            data = yourdata
            try:
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")
                cursor = conn.cursor()
                #print('psycopg2 connect execute--')
            except (Exception, psycopg2.DatabaseError) as error:
                #print("Error while creating PostgreSQL table", error)
                pass
            else:
                cursor.execute('SELECT * FROM public."Scrap_App_count10" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_count10" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                #print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------// function 10 End //--------------------------------------------------

        # t = multiprocessing.Process(target=f10).start()
            t = threading.Thread(target=f10).start()

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            wait = Count10.objects.all().order_by('-id')[0].data
            if wait == '':
                w = 0
            else:
                w = 1
            if w == 1:
                break
        # ------// --- //----
        time.sleep(3)
        data = Count10.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        aa_id2 = Count10.objects.all().order_by('-id')[0].id
        aa_id3 = Count10.objects.get(id=aa_id2)
        aa_id3.var = 0  # make 1 to 0 for available for next request.
        aa_id3.save()


# ------------------(cnr_work10e)----------------------

    return HttpResponse(data)
#<----/*****/-------------/*****/---------/*****/---------




