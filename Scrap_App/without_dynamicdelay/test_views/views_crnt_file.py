import os
import psycopg2
import json
from django.conf import settings

from .models import Count1, Count2, Count3, Count4, Count5, Count6, Count7, Count8, Count9, Count10,\
    Case_1, Case_2, Case_3, Case_4, Case_5, Case_6, Case_7, Case_8, Case_9, Case_10

#-----------------------
import multiprocessing
import threading
from rest_framework import views
from rest_framework.response import Response

from .serializers import YourSerializer, ErrorSerializer
from django.http import HttpResponse
from PIL import Image, ImageEnhance
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ChromeOptions
from selenium.webdriver.support import expected_conditions as EC
import random
import time
import cv2
import pytesseract as tess
from PIL import Image, ImageCms, ImageOps, ImageFilter
import numpy as np
from functools import reduce
import uuid
from asgiref.sync import sync_to_async, async_to_sync
import asyncio
#----------------------------------------------------------------------
def CnrNoView(request, slug):
    pass

@sync_to_async(thread_sensitive=True)
#async def home(request):
def home(request):
    start = time.perf_counter()
    print('st:-')
    time.sleep(5)
    #await asyncio.sleep(10)

    th_id = str(threading.get_native_id())
    print("home:" + str(threading.get_native_id()))

    finish3 = time.perf_counter()
    print(f'{str(threading.get_native_id())} home Finished in {round(finish3 - start, 2)} second(s)')
    return HttpResponse(f'{th_id} Home')
#----------------------------------------------------------------------
#@sync_to_async(thread_sensitive=True)
async def CaseNoView(request, state, dist, complex, casetype, caseno, year):
    print('st:-', {str(threading.get_native_id())})
    start = time.perf_counter()
    print('Case1 input data:--- ', state, dist, complex, casetype, caseno, year)
    await asyncio.sleep(1)
    s = state
    await asyncio.sleep(1)
    d = dist
    await asyncio.sleep(1)
    c = complex
    await asyncio.sleep(1)
    c2 = casetype
    await asyncio.sleep(1)
    c3 = caseno
    await asyncio.sleep(1)
    y = year
    #time.sleep(5)
    

    finish3 = time.perf_counter()
    print(f'{str(threading.get_native_id())} home Finished in {round(finish3 - start, 2)} second(s)')
    return HttpResponse(f'Thread id:- {str(threading.get_native_id())}, inner data:- {s, d, c, c2, c3, y}, Request Finished in:- {round(finish3 - start, 2)} second(s)')

