import os
import psycopg2
import json
from django.conf import settings

# from azcaptchaapi import AZCaptchaApi
# api = AZCaptchaApi('wf9hryjnvvg3clnxh2xcyb7jzgqmkmk8')

from .models import Count1, Count2, Count3, Count4, Count5, Count6, Count7, Count8, Count9, Count10,\
    Case_1, Case_2, Case_3, Case_4, Case_5, Case_6, Case_7, Case_8, Case_9, Case_10

#-----------------------
import multiprocessing
import threading
from rest_framework import views
from rest_framework.response import Response

from .serializers import YourSerializer, ErrorSerializer
from django.http import HttpResponse
from PIL import Image, ImageEnhance
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ChromeOptions
from selenium.webdriver.support import expected_conditions as EC
import random
import time
import cv2
import pytesseract as tess
from PIL import Image, ImageCms, ImageOps, ImageFilter
import numpy as np
from functools import reduce

import uuid
#----------------------------------------------------------------------
#<-----------------/* CNR No */------------------
def CnrNoView(request, slug):
    pass
#----------------------------------------------------------------------
def CaseNoView(request, state, dist, complex, casetype, caseno, year):
    path = '/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/Scrap_App/files/'
    if Case_1.objects.all().order_by('-id')[0].var == 0:
        start = time.perf_counter()

        var_dict = {"var": 1, "data": 0}
        # <-- for write --
        with open(path+'case_1.txt', 'w') as json_file:
            json.dump(var_dict, json_file)

        Case_1(var=1, time=time.time(), state=state, dist=dist, complex=complex, casetype=casetype, caseno=caseno,
               year=year).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        print('Case1 input data:--- ', state, dist, complex, casetype, caseno, year)

        state_data = Case_1.objects.all().order_by('-id')[0].state
        dist_data = Case_1.objects.all().order_by('-id')[0].dist
        complex_data = Case_1.objects.all().order_by('-id')[0].complex
        casetype_data = Case_1.objects.all().order_by('-id')[0].casetype
        caseno_data = Case_1.objects.all().order_by('-id')[0].caseno
        year_data = Case_1.objects.all().order_by('-id')[0].year
        # ------------------(cwork1)----------------------

        def Case_f1():

            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            print('Case_f1--')

            # <--- all repeated Functions ----/////---
            def delay1():
                time.sleep(random.randint(1, 2))
            def delay():
                time.sleep(random.randint(2, 2))

            # ---/ op / ---
            state_ = state_data
            dist_ = dist_data
            complex_ = complex_data
            casetype_ = casetype_data
            caseno_ = caseno_data
            year_ = year_data
            # ----------------// our code //-----------------

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "menuPage")))  # wait
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="menuPage"]/div/div[1]/ul/li[2]')))  # wait
                    driver.find_elements_by_xpath('//*[@id="menuPage"]/div/div[1]/ul/li[2]')[0].click()
                    #delay1()
                    # for alert OK btn
                    WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "bs_alert")))  # wait
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bs_alert"]/div/div/div[2]/button')))  # wait
                    driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                    #delay1()

                    # <--------STATE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "divLangState")))  # <---- wait --->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "sess_state_code")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_state_code'})
                    state_job_profiles = all_divs.find_all('option')

                    k = [str(x).split('"')[1] for x in state_job_profiles]  # <-- output  ['0', '28', '2', '6',..]
                    print('k:-', k)
                    # <---- take input state value from fun() & find index of that input value ----
                    index = k.index(state_)
                    state_option = index + 1    # op-- 20th actual is 21th

                    # <------ Click on 'STATE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_state_code"]')[0].click()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    #delay1()


                    # <------ Select 'STATE' Options ---->
                    state = driver.find_elements_by_xpath('//*[@id="sess_state_code"]/option[{}]'.format(state_option))[
                        0]  # option 1
                    print('selected state:-- ', state.text)
                    state.click()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    #delay1()
                    # <--------STATE---///--End----///--------->

                    # <--------DISTRICT--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "sess_dist_code")))  # <---- wait --->
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_dist_code'})
                    dist_job_profiles = all_divs.find_all('option')

                    # print('dist_job_profiles', dist_job_profiles)

                    k2 = [str(j).split('"')[1] for j in dist_job_profiles]
                    print('k2:-',k2)

                    # <---- take input state value from fun() & find index of that input value ----
                    index2 = k2.index(dist_)
                    dist_option = index2 + 1        # op-- 20th actual is 21th

                    # <------ Click on 'DISTRICT' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]')[0].click()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    #delay1()

                    # <------ Select 'DISTRICT' Options ---->
                    district=driver.find_elements_by_xpath('//*[@id="sess_dist_code"]/option[{}]'.format(dist_option))[0]
                                                                                                # option 2
                    print('selected dist:-- ', district.text)
                    district.click()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    delay1()
                    # <--------DISTRICT---///--End----///--------->

                    # <--------COMPLEX--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "court_complex_code")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    complex_job_profiles = all_divs.find_all('option')
                    # delay()

                    k3 = [str(n).split('"')[1] for n in complex_job_profiles]
                    print('k3:- ', k3, 'len(k3:-)', len(k3), 'len(complex_job_profiles:-)', len(complex_job_profiles))

                    # <---- take input complex_ value from fun()
                    com = complex_.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    #delay1()

                    # <------ Select 'COMPLEX' Options ---->
                    complex=driver.find_elements_by_xpath('//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    print('selected complex: ', complex.text)
                    complex.click()

                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    delay1()
                    # <--------COMPLEX---///--End----///--------->

                    # <--------CLICK CASE NO. BTN--///------>
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CScaseNumber")))  # <---- wait --->
                    driver.find_elements_by_xpath('//*[@id="CScaseNumber"]')[0].click()
                    #delay1()

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="case_type"]')))  #
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CSCaseNumberDiv")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, 'case_type')))  # wait

                    all_divs = soup.find('select', {'id': 'case_type'})
                    case_job_profiles = all_divs.find_all('option')
                    # print('case_job_profiles', case_job_profiles)
                    # delay()

                    k4 = [str(n1).split('"')[1] for n1 in case_job_profiles]
                    print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(case_job_profiles:-)', len(case_job_profiles))

                    case = casetype_.replace('_', '^')
                    index4 = k4.index(str(case))
                    case_option = index4 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))
                    delay1()

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[0]
                    print('selected case: ', case.text)
                    case.click()
                    delay1()
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))

                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(caseno_)  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(year_)  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # Find_data----- START --
                    # ------------------------------
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_class_name('refresh-btn').click()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        # delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        driver.execute_script("document.body.style.zoom='170%'")
                        driver.execute_script("window.scrollTo(0,400);")
                        delay1()
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        start_i = time.perf_counter()
                        driver.find_element_by_xpath("//*[@id='captcha_image']")

                        # --------------(for SHOW chrome)--------
                        '''
                        location = {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        #---------------------------'''

                        # --------------(for hide windo)--------
                        location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        # ----------------------------------------

                        driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE.png')
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')
                        # im.show()  # <--- comment it ----

                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')
                        width, height = im2.size
                        '''
                        # --------------(for SHOW chrome)--------
                        left = 0
                        top = 0  # done
                        right = 175
                        bottom = 57  # done
                        # ----------------------'''

                        # --------------(for hide windo)--------
                        left = 30  # ... run with option
                        top = 0  # done
                        right = 220  # done
                        bottom = 57  # done
                        # ----------------------

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')  # row img
                        # im1.show()
                        # >-----------------------

                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')

                        # <-------- improve image quality ----------->
                        # img.save("quality_abc.png", quality=200)
                        contrast = ImageEnhance.Contrast(img)
                        contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE.png')

                        # <----- for remove unwanted color from image ---------->
                        image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE.png')
                        grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                        lower_range = np.array([0, 0, 0])
                        upper_range = np.array([0, 0, 0])

                        mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE.png', mask)
                        # <-----Done remove unwanted color from image ---------->

                        # <------ image color change in black n white ------
                        Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE.png', 0)
                        ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE.png', thresh_img)
                        cv2.waitKey(0)
                        cv2.destroyAllWindows()

                        # <----- for expand image text ---------------------
                        Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE.png', 0)
                        kernel = np.ones((3, 3), np.uint8)
                        erosion = cv2.erode(Load2, kernel, iterations=1)
                        img = erosion.copy()
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE.png', img)

                        # <----- for Captcha image to text convert---------------------
                        img2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE.png')
                        captcha_txt2 = tess.image_to_string(img2, config='')
                        finish_i = time.perf_counter()
                        print(f'OLD CODE captcha reading Finished in {round(finish_i - start_i, 2)} second(s)')

                        # <------- Find_data--------/// END ///---------------------------

                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 8:
                                # print('!=8 --')

                                # ------------------------------
                                driver.find_element_by_class_name('refresh-btn').click()
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                # delay1()
                                WebDriverWait(driver, 600).until(
                                    EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                driver.execute_script("document.body.style.zoom='170%'")
                                driver.execute_script("window.scrollTo(0,400);")
                                delay1()
                                # ----------------------
                                # <--- to find captcha image location ---->
                                driver.find_element_by_xpath("//*[@id='captcha_image']")
                                start3 = time.perf_counter()
                                '''
                                # --------------(for SHOW chrome)--------
                                location = {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                #-------------------------------------'''

                                # --------------(for hide windo)--------
                                location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                # ----------------------------------------

                                driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE.png')
                                # driver.get_screenshot_as_file(settings.MEDIA_ROOT + '/CaseImages/imageCASE.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')
                                # im.show()  # <--- comment it ----
                                # <--------

                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')
                                width, height = im2.size
                                
                                '''
                                # --------------(for SHOW chrome)--------
                                left = 0
                                top = 0  # done
                                right = 175
                                bottom = 57  # done
                                # ----------------------'''

                                # --------------(for hide windo)--------
                                left = 30  # ... run with option
                                top = 0  # done
                                right = 220  # done
                                bottom = 57  # done
                                # ----------------------

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')
                                im1.show()
                                # >-----------------------

                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                # delay()

                                # for Captcha image to text convert---------------comment it------------
                                img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE.png')

                                # <-------- improve image quality ----------->
                                contrast = ImageEnhance.Contrast(img)
                                contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE.png')

                                # <----- for remove unwanted color from image ---------->
                                image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE.png')
                                grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                                grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                                lower_range = np.array([0, 0, 0])
                                upper_range = np.array([0, 0, 0])

                                mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE.png', mask)
                                # <-----Done remove unwanted color from image ---------->

                                # <------ image color change in black n white ------
                                Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE.png', 0)
                                ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE.png', thresh_img)
                                cv2.waitKey(0)
                                cv2.destroyAllWindows()

                                # <----- for expand image text ---------------------
                                Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE.png', 0)
                                kernel = np.ones((3, 3), np.uint8)
                                erosion = cv2.erode(Load2, kernel, iterations=1)
                                img = erosion.copy()
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE.png', img)

                                # <----- for Captcha image to text convert---------------------
                                img3 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE.png')
                                re_captcha_txt = tess.image_to_string(img3, config='')
                                # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)

                                finish3 = time.perf_counter()
                                print(f'captcha re-reading Finished in {round(finish3 - start3, 2)} second(s)')

                                captcha_txt2 = re_captcha_txt
                                if len(captcha_txt2) != 8:
                                    # print('if len != 8--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                    # print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                # print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                # print('finaly break done in <6 or >6--')
                                break

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                        input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        # <----- for Click Search btn---------------------

                        driver.find_element_by_xpath('//*[@id="goResetDiv"]/input[1]').click()  # <--- sertch btn --->
                        # print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        delay()
                        #delay1()

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display: none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        # html = driver.page_source
                        # soup = BeautifulSoup(html, "html.parser")
                        op_page_bs_alertDiv = soup.find('div', {'id': 'bs_alert'}).get('style')
                        # -----For Invalid captcha page("display: block;")
                        # print('op_page_bs_alertDiv--- ', op_page_bs_alertDiv)
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '':
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                delay()
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                # print('check2--r')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.close()
                                refresh = 1

                        elif op_page_bs_alertDiv == "display: block;":
                            # print('alert 1 ------')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                            refresh = 0

                        elif op_page_bs_alertDiv == "display: block; padding-right: 15px;":
                            # print('alert 2 ---Enter year from 1901 to---')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()

                            res_dct = {}
                            case_final_2 = {'EnterYearFrom1901': 'Enter year from 1901'}
                            Record_not_found = 1
                            driver.close()
                            refresh = 1

                        else:
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            break
                    if Record_not_found == 1:
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        WebDriverWait(driver, 600).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # <---- wait --->
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_showListDiv = soup.find('div', {'id': 'showList'}).get('style')
                        # -----For Invalid captcha page ("display:none;")

                        driver.execute_script("window.scrollTo(0,300);")
                        #time.sleep(1)
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # ----------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "caseHistoryDiv")))
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bckbtn"]')))
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            l = []
                            table_case = soup.find('table', attrs={'class': 'case_details_table'})
                            table_case_body = table_case.find('tbody')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values

                            b = l[:len(l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                            b1 = l[-1][1].replace('\xa0\xa0 (Note the CNR number for future reference)', '')
                            # <-- for remove last "(Note the CNR number for future reference)" text o/p-- "MHPU050000132021"

                            lst = reduce(lambda x, y: x + y, b)
                            lst.append('cnrnumber')  # < ---- add 'CNR' name
                            lst.append(b1)  # < ---- add 'CNR number
                            res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except Exception as e:
                            # print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {"ProgramError": str(e)}

                        try:
                            # <---2nd Table, take single table data, into list ------------>
                            l2 = []
                            table = soup.find('table', attrs={'class': 'table_r'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l2.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst2 = reduce(lambda x, y: x + y, l2)
                            res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in range(0, len(lst2), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct2 = {}

                        try:
                            # <---3rd Table, take single table data, into list ----------->
                            l3 = []
                            table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l3.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst3 = reduce(lambda x, y: x + y, l3)
                            listToStr = ' '.join(map(str, lst3))
                            res_dct3 = listToStr.replace('\xa0', '')  # <-- make kye 'LOWER case', remove 'space' ---
                        except:
                            res_dct3 = ''

                        try:
                            # <---4th Table, take single table data, into list ---------->
                            l4 = []
                            table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l4.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst4 = reduce(lambda x, y: x + y, l4)
                            listToStr2 = ' '.join(map(str, lst4))
                            res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct4 = ''

                        try:
                            # <---5th Table, take single table data, into list ---------->
                            table_a = soup.find('table', attrs={'class': 'acts_table'})
                            h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                            res_dct5 = [dict(zip(h, i)) for i in d]

                            k5 = []
                            for d in res_dct5:
                                k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                    'UnderSection(s)', 'Section'): v for k, v in d.items()})
                        except:
                            k5 = []

                        try:
                            # <---6th Table, take single table data, into list ---------->
                            table_t = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                            res_dct6 = [dict(zip(h, i)) for i in d]

                            k6 = []
                            for d in res_dct6:
                                k6.append(
                                    {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                     d.items()})
                        except:
                            k6 = []

                        try:
                            # <---7th Table, take single table data, into list ---------->
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]

                            k7 = []
                            for d7 in res_dct7:
                                k7.append(
                                    {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                        except:
                            k7 = []

                        try:
                            # <---8th Table, take single table data, into list ---------->
                            table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                            h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                            res_dct8 = [dict(zip(h1, x)) for x in q]

                            k8 = []
                            for d in res_dct8:
                                k8.append({k.replace(' ', ''): v for k, v in d.items()})
                        except:
                            k8 = []

                        main_ch2 = 1

                        if res_dct != {}:
                            case_final = {"CaseDetails": res_dct, "CaseStatus": res_dct2,
                                          "PetitionerAdvocate": res_dct3,
                                          "RespondentAdvocate": res_dct4, "Acts": k5,
                                          "History": k6, "Orders": k7, "CaseTransferDetails": k8}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {
                                "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.close()
                                break
                            except:
                                pass
                except Exception as e:
                    try:
                        driver.close()
                        print('except crser here--', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {"ProgramError": str(e)}
                    except:
                        pass

                if main_ch2 == 1:
                    break

            if res_dct != {}:
                yourdata = case_final

            elif res_dct == {}:
                yourdata = case_final_2

            data = yourdata
            #print('data:--', data)

            try:
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")

                cursor = conn.cursor()
                #print('psycopg2 connect execute--')

                var_dict = {"var": 0, "data": 1}
                # <-- for write --
                with open(path+'case_1.txt', 'w') as json_file:
                    json.dump(var_dict, json_file)

            except (Exception, psycopg2.DatabaseError) as error:
                #print("Error while creating PostgreSQL table", error)
                pass
            else:

                cursor.execute('SELECT * FROM public."Scrap_App_case_1" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_case_1" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                #print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------//Case function 1 End //--------------------------------------------------

        #multiprocessing.Process(target=Case_f1).start()
        threading.Thread(target=Case_f1).start()
        #print('thred--')

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            #wait = Case_1.objects.all().order_by('-id')[0].data

            # <-- for read ---
            with open(path+'case_1.txt', 'r') as f:
                _dict = json.load(f)
                # print(_dict['var'])  # 0
            #print('wait data:- ', _dict['data'])
            time.sleep(5)
            wait = _dict['data']  # 0

            #print('wait ON-')
            #print('wait OFF-')
            if wait == 0:
                w = 0
            else:
                w = 1
            if w == 1:
                # print('out while..')
                break
        # ------// --- //----
        time.sleep(3)

        data = Case_1.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        c_id = Case_1.objects.all().order_by('-id')[0].id
        c_id2 = Case_1.objects.get(id=c_id)
        c_id2.var = 0  # for make 1 to 0
        c_id2.save()

        finish = time.perf_counter()
        print(f'Case_1 Finished in {round(finish - start, 2)} second(s)')
# ------------------(cwork1e)----------------------

    elif Case_2.objects.all().order_by('-id')[0].var == 0:
        start = time.perf_counter()

        var_dict = {"var": 1, "data": 0}
        # <-- for write --
        with open(path+'case_2.txt', 'w') as json_file:
            json.dump(var_dict, json_file)

        Case_2(var=1, time=time.time(), state=state, dist=dist, complex=complex, casetype=casetype, caseno=caseno,
               year=year).save()  # make 0 to 1 / temp., for it is not available if it is running...# SLUG save in data base/ temp.
        print('Case_2 input data:--- ', state, dist, complex, casetype, caseno, year)

        state_data = Case_2.objects.all().order_by('-id')[0].state
        dist_data = Case_2.objects.all().order_by('-id')[0].dist
        complex_data = Case_2.objects.all().order_by('-id')[0].complex
        casetype_data = Case_2.objects.all().order_by('-id')[0].casetype
        caseno_data = Case_2.objects.all().order_by('-id')[0].caseno
        year_data = Case_2.objects.all().order_by('-id')[0].year
        # ------------------(cwork2)----------------------

        def Case_2_f():

            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            global yourdata, case_final_2, case_final, re_captcha_txt_final, Record_not_found

            driver.get('https://services.ecourts.gov.in/ecourtindia_v6/')
            print('Case_2_f--')

            # <--- all repeated Functions ----/////---
            def delay1():
                time.sleep(random.randint(1, 2))
            def delay():
                time.sleep(random.randint(2, 2))

            # ---/ op / ---
            state_ = state_data
            dist_ = dist_data
            complex_ = complex_data
            casetype_ = casetype_data
            caseno_ = caseno_data
            year_ = year_data
            # ----------------// our code //-----------------

            while True:
                main_ch2 = 0
                try:
                    # For Select Case Status
                    WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "menuPage")))  # wait
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="menuPage"]/div/div[1]/ul/li[2]')))  # wait
                    driver.find_elements_by_xpath('//*[@id="menuPage"]/div/div[1]/ul/li[2]')[0].click()
                    #delay1()
                    # for alert OK btn
                    WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "bs_alert")))  # wait
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bs_alert"]/div/div/div[2]/button')))  # wait
                    driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                    #delay1()

                    # <--------STATE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "divLangState")))  # <---- wait --->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "sess_state_code")))  # <---- wait --->
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_state_code'})
                    state_job_profiles = all_divs.find_all('option')

                    k = [str(x).split('"')[1] for x in state_job_profiles]  # <-- output  ['0', '28', '2', '6',..]
                    print('k_c2_:-', k)
                    # <---- take input state value from fun() & find index of that input value ----
                    index = k.index(state_)
                    state_option = index + 1    # op-- 20th actual is 21th

                    # <------ Click on 'STATE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_state_code"]')[0].click()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    #delay1()


                    # <------ Select 'STATE' Options ---->
                    state = driver.find_elements_by_xpath('//*[@id="sess_state_code"]/option[{}]'.format(state_option))[
                        0]  # option 1
                    print('selected state:-- ', state.text)
                    state.click()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    #delay1()
                    # <--------STATE---///--End----///--------->

                    # <--------DISTRICT--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "sess_dist_code")))  # <---- wait --->
                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'sess_dist_code'})
                    dist_job_profiles = all_divs.find_all('option')

                    # print('dist_job_profiles', dist_job_profiles)

                    k2 = [str(j).split('"')[1] for j in dist_job_profiles]
                    print('k_c2_:-', k2)

                    # <---- take input state value from fun() & find index of that input value ----
                    index2 = k2.index(dist_)
                    dist_option = index2 + 1        # op-- 20th actual is 21th

                    # <------ Click on 'DISTRICT' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="sess_dist_code"]')[0].click()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "court_complex_code")))  # wait
                    #delay1()

                    # <------ Select 'DISTRICT' Options ---->
                    district=driver.find_elements_by_xpath('//*[@id="sess_dist_code"]/option[{}]'.format(dist_option))[0]
                                                                                                # option 2
                    print('selected dist:-- ', district.text)
                    district.click()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    delay1()
                    # <--------DISTRICT---///--End----///--------->

                    # <--------COMPLEX--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "court_complex_code")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    all_divs = soup.find('select', {'id': 'court_complex_code'})
                    complex_job_profiles = all_divs.find_all('option')
                    # delay()

                    k3 = [str(n).split('"')[1] for n in complex_job_profiles]
                    print('k3:- ', k3, 'len(k3:-)', len(k3), 'len(complex_job_profiles:-)', len(complex_job_profiles))

                    # <---- take input complex_ value from fun()
                    com = complex_.replace('_', '@').replace('-', ',')
                    index3 = k3.index(com)
                    complex_option = index3 + 1

                    # <------ Click on 'COMPLEX' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="court_complex_code"]')[0].click()
                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    #delay1()

                    # <------ Select 'COMPLEX' Options ---->
                    complex=driver.find_elements_by_xpath('//*[@id="court_complex_code"]/option[{}]'.format(complex_option))[0]
                    print('selected complex: ', complex.text)
                    complex.click()

                    WebDriverWait(driver, 900).until(EC.element_to_be_clickable((By.ID, "CScaseNumber")))  # wait
                    delay1()
                    # <--------COMPLEX---///--End----///--------->

                    # <--------CLICK CASE NO. BTN--///------>
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CScaseNumber")))  # <---- wait --->
                    driver.find_elements_by_xpath('//*[@id="CScaseNumber"]')[0].click()
                    #delay1()

                    # <--------CASE TYPE--///---start---///-->
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="case_type"]')))  #
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, "CSCaseNumberDiv")))  # <---- wait --->

                    html = driver.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.ID, 'case_type')))  # wait

                    all_divs = soup.find('select', {'id': 'case_type'})
                    case_job_profiles = all_divs.find_all('option')
                    # print('case_job_profiles', case_job_profiles)
                    # delay()

                    k4 = [str(n1).split('"')[1] for n1 in case_job_profiles]
                    print('k4:- ', k4, 'len(k4:-)', len(k4), 'len(case_job_profiles:-)', len(case_job_profiles))

                    case = casetype_.replace('_', '^')
                    index4 = k4.index(str(case))
                    case_option = index4 + 1

                    # <------ Click on 'CASE TYPE' DROP DOWN ---->
                    driver.find_elements_by_xpath('//*[@id="case_type"]')[0].click()
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))
                    delay1()

                    # <------ Select 'CASE TYPE' Options ---->
                    case = driver.find_elements_by_xpath('//*[@id="case_type"]/option[{}]'.format(case_option))[0]
                    print('selected case: ', case.text)
                    case.click()
                    delay1()
                    # <--------CASE TYPE---///--End----///--------->

                    # <--------CASE NO input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="search_case_no"]')))  # wait
                    WebDriverWait(driver, 600).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))

                    case_elem = driver.find_element(By.ID, "search_case_no")
                    case_elem.send_keys(caseno_)  ##### <---- Pass case no ---->
                    # <--------CASE No input field---///--End----///--------->

                    # <--------YEAR input field---///--Start----///--------->
                    WebDriverWait(driver, 600).until(
                        EC.presence_of_element_located((By.XPATH, '//*[@id="rgyear"]')))  # wait
                    year_elem = driver.find_element(By.ID, "rgyear")
                    year_elem.send_keys(year_)  ##### <---- Pass year ---->
                    # <--------YEAR input field---///--End----///--------->

                    # <--------------------------------------------/ Captcha code /--------------------------------------------------
                    # Find_data----- START --
                    # ------------------------------
                    # ------------------------------
                    while True:
                        refresh = 0
                        driver.find_element_by_class_name('refresh-btn').click()
                        driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                        # delay1()
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                        # --------------(for windo size 2)--------
                        driver.execute_script("document.body.style.zoom='170%'")
                        driver.execute_script("window.scrollTo(0,400);")
                        delay1()
                        # ------------------------------------------
                        # <--- to find captcha image location ---->
                        start_i = time.perf_counter()
                        driver.find_element_by_xpath("//*[@id='captcha_image']")
                        '''
                        # --------------(for SHOW chrome)--------
                        location = {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        #---------------------------------'''

                        # --------------(for hide windo)--------
                        location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                        size = {'height': 59, 'width': 310}
                        # ----------------------------------------

                        driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_2.png')
                        x = location['x']
                        y = location['y']
                        width = location['x'] + size['width']
                        height = location['y'] + size['height']

                        im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_2.png')
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')
                        # im.show()  # <--- comment it ----

                        # <--------// start //----
                        im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')
                        width, height = im2.size
                        '''
                        # --------------(for SHOW chrome)--------
                        left = 0
                        top = 0  # done
                        right = 175
                        bottom = 57  # done
                        # ----------------------'''

                        # --------------(for hide windo)--------
                        left = 30  # ... run with option
                        top = 0  # done
                        right = 220  # done
                        bottom = 57  # done
                        # ----------------------

                        # Cropped image of above dimension
                        im1 = im2.crop((left, top, right, bottom))
                        im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')  # row img
                        # im1.show()
                        # >-----------------------

                        driver.execute_script("document.body.style.zoom='100%'")
                        driver.execute_script("window.scrollTo(0,0);")
                        # <--------// end //----

                        # <---- for Captcha image to text convert---------------comment it------------
                        img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')

                        # <-------- improve image quality ----------->
                        # img.save("quality_abc_2.png", quality=200)
                        contrast = ImageEnhance.Contrast(img)
                        contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_2.png')

                        # <----- for remove unwanted color from image ---------->
                        image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_2.png')
                        grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)
                        lower_range = np.array([0, 0, 0])
                        upper_range = np.array([0, 0, 0])

                        mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_2.png', mask)
                        # <-----Done remove unwanted color from image ---------->

                        # <------ image color change in black n white ------
                        Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_2.png', 0)
                        ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_2.png', thresh_img)
                        cv2.waitKey(0)
                        cv2.destroyAllWindows()

                        # <----- for expand image text ---------------------
                        Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_2.png', 0)
                        kernel = np.ones((3, 3), np.uint8)
                        erosion = cv2.erode(Load2, kernel, iterations=1)
                        img = erosion.copy()
                        cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_2.png', img)

                        # <----- for Captcha image to text convert---------------------
                        img2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_2.png')
                        captcha_txt2 = tess.image_to_string(img2, config='')
                        
                        finish_i = time.perf_counter()
                        print(f'OLD CODE captcha reading Finished in {round(finish_i - start_i, 2)} second(s)')

                        # <------- Find_data--------/// END ///---------------------------

                        while True:
                            length_ch2 = 0
                            if len(captcha_txt2) != 8:
                                # print('!=8 --')

                                # ------------------------------
                                driver.find_element_by_class_name('refresh-btn').click()
                                driver.find_element_by_xpath('//*[@id="captcha"]').click()  # for reduce black sqr
                                # delay1()
                                WebDriverWait(driver, 600).until(
                                    EC.element_to_be_clickable(
                                        (By.XPATH, '//*[@id="goResetDiv"]/input[1]')))  # <---- wait --->

                                # --------------(for windo size 2)--------
                                driver.execute_script("document.body.style.zoom='170%'")
                                driver.execute_script("window.scrollTo(0,400);")
                                delay1()
                                # ----------------------
                                # <--- to find captcha image location ---->
                                driver.find_element_by_xpath("//*[@id='captcha_image']")
                                start3 = time.perf_counter()

                                '''
                                # --------------(for SHOW chrome)--------
                                location = {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                #----------------------------------'''

                                # --------------(for hide windo)--------
                                location = {'x': 130, 'y': 363}  # ... run without option {'x': 219, 'y': 380}
                                size = {'height': 59, 'width': 310}
                                # ----------------------------------------

                                driver.save_screenshot(settings.MEDIA_ROOT + '/CaseImages/imageCASE_2.png')
                                # driver.get_screenshot_as_file(settings.MEDIA_ROOT + '/CaseImages/imageCASE_2.png')

                                x = location['x']
                                y = location['y']
                                width = location['x'] + size['width']
                                height = location['y'] + size['height']

                                im = Image.open(settings.MEDIA_ROOT + '/CaseImages/imageCASE_2.png')
                                im = im.crop((int(x), int(y), int(width), int(height)))
                                im.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')
                                # im.show()  # <--- comment it ----
                                # <--------

                                im2 = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')
                                width, height = im2.size
                                '''
                                # --------------(for SHOW chrome)--------
                                left = 0
                                top = 0  # done
                                right = 175
                                bottom = 57  # done
                                # ----------------------'''

                                # --------------(for hide windo)--------
                                left = 30  # ... run with option
                                top = 0  # done
                                right = 220  # done
                                bottom = 57  # done
                                # ----------------------

                                # Cropped image of above dimension
                                im1 = im2.crop((left, top, right, bottom))
                                im1.save(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')
                                im1.show()
                                # >-----------------------

                                driver.execute_script("document.body.style.zoom='100%'")
                                driver.execute_script("window.scrollTo(0,0);")
                                # delay()

                                # for Captcha image to text convert---------------comment it------------
                                img = Image.open(settings.MEDIA_ROOT + '/CaseImages/abcCASE_2.png')

                                # <-------- improve image quality ----------->
                                contrast = ImageEnhance.Contrast(img)
                                contrast.enhance(1.5).save(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_2.png')

                                # <----- for remove unwanted color from image ---------->
                                image = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/contrastCASE_2.png')
                                grid_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                                grid_hsv = cv2.cvtColor(grid_rgb, cv2.COLOR_RGB2HSV)

                                lower_range = np.array([0, 0, 0])
                                upper_range = np.array([0, 0, 0])

                                mask = cv2.inRange(grid_hsv, lower_range, upper_range)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/maskCASE_2.png', mask)
                                # <-----Done remove unwanted color from image ---------->

                                # <------ image color change in black n white ------
                                Load = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/maskCASE_2.png', 0)
                                ret, thresh_img = cv2.threshold(Load, 77, 255, cv2.THRESH_BINARY_INV)
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/resultCASE_2.png', thresh_img)
                                cv2.waitKey(0)
                                cv2.destroyAllWindows()

                                # <----- for expand image text ---------------------
                                Load2 = cv2.imread(settings.MEDIA_ROOT + '/CaseImages/resultCASE_2.png', 0)
                                kernel = np.ones((3, 3), np.uint8)
                                erosion = cv2.erode(Load2, kernel, iterations=1)
                                img = erosion.copy()
                                cv2.imwrite(settings.MEDIA_ROOT + '/CaseImages/imgCASE_2.png', img)

                                # <----- for Captcha image to text convert---------------------
                                img3 = Image.open(settings.MEDIA_ROOT + '/CaseImages/imgCASE_2.png')
                                re_captcha_txt = tess.image_to_string(img3, config='')
                                # print('re_captcha_txt FINAL OP ---: ', re_captcha_txt)

                                finish3 = time.perf_counter()
                                print(f'captcha re-reading Finished in {round(finish3 - start3, 2)} second(s)')

                                captcha_txt2 = re_captcha_txt
                                if len(captcha_txt2) != 8:
                                    # print('if len != 8--')
                                    length_ch2 = 0
                                else:
                                    re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                    # print('re_captcha_txt_ //if else--: ', re_captcha_txt_final)
                                    length_ch2 = 1


                            else:
                                re_captcha_txt_final = captcha_txt2.replace('i', 'l').replace('I', 'l')
                                # print('re_captcha_txt_final-to input-: ', re_captcha_txt_final)
                                length_ch2 = 1

                            if length_ch2 == 1:
                                # print('finaly break done in <6 or >6--')
                                break

                        # <----- for Enter Captcha image text to // input field //---------------------
                        input_element = driver.find_element_by_xpath("//*[@id='captcha']")
                        input_element.send_keys(re_captcha_txt_final)  ##### <---- Pass text ---->
                        # print('send_keys done------')
                        # <----- for Click Search btn---------------------

                        driver.find_element_by_xpath('//*[@id="goResetDiv"]/input[1]').click()  # <--- sertch btn --->
                        # print('seartch btn clicked ------')
                        # <--------------------------------------------------------------------------------------------------------
                        delay()
                        #delay1()

                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_errSpanDiv = soup.find('div', {'id': 'errSpan'}).get('style')
                        # -----For Invalid captcha page ("display: none;")/("display: block;")

                        # print('-------------------------------')
                        # print('op_page_errSpanDiv: ', op_page_errSpanDiv)
                        # print('-------------------------------')
                        # -------------------------------------------------------------------------------

                        # html = driver.page_source
                        # soup = BeautifulSoup(html, "html.parser")
                        op_page_bs_alertDiv = soup.find('div', {'id': 'bs_alert'}).get('style')
                        # -----For Invalid captcha page("display: block;")
                        # print('op_page_bs_alertDiv--- ', op_page_bs_alertDiv)
                        # -------------------------------------------------------------------------------

                        if op_page_errSpanDiv == "display: block;" or op_page_errSpanDiv == '':
                            all_divs = soup.find('div', {'id': 'errSpan'})
                            all_tables2 = all_divs.find_all('p')
                            # print('all_tables2:--', all_tables2)

                            check2 = str(all_tables2)
                            if check2 == '[<p align="center" style="color:red;">Invalid Captcha</p>]':
                                delay()
                                refresh = 0

                            elif check2 == '[<p align="center" style="color:red;">Record not found</p>]':
                                # print('check2--r')
                                res_dct = {}
                                case_final_2 = {'RecordNotFound': 'Record not found'}
                                Record_not_found = 1
                                driver.close()
                                refresh = 1

                        elif op_page_bs_alertDiv == "display: block;":
                            # print('alert 1 ------')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()
                            refresh = 0

                        elif op_page_bs_alertDiv == "display: block; padding-right: 15px;":
                            # print('alert 2 ---Enter year from 1901 to---')
                            driver.find_elements_by_xpath('//*[@id="bs_alert"]/div/div/div[2]/button')[0].click()

                            res_dct = {}
                            case_final_2 = {'EnterYearFrom1901': 'Enter year from 1901'}
                            Record_not_found = 1
                            driver.close()
                            refresh = 1

                        else:
                            refresh = 1
                            Record_not_found = 0

                        if refresh == 1:
                            break
                    if Record_not_found == 1:
                        main_ch2 = 1
                    elif Record_not_found == 0:
                        WebDriverWait(driver, 600).until(
                            EC.presence_of_element_located((By.ID, "showList")))  # <---- wait --->
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_showListDiv = soup.find('div', {'id': 'showList'}).get('style')
                        # -----For Invalid captcha page ("display:none;")

                        driver.execute_script("window.scrollTo(0,300);")
                        #time.sleep(1)
                        WebDriverWait(driver, 600).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')))
                        driver.find_elements_by_xpath('//*[@id="dispTable"]/tbody/tr[2]/td[4]/a')[0].click()
                        time.sleep(1)
                        # ----------------------------------------------------------------------------------------------------

                        # <-----/// Final page table conversion start ///----->
                        WebDriverWait(driver, 600).until(EC.presence_of_element_located((By.ID, "caseHistoryDiv")))
                        WebDriverWait(driver, 600).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="bckbtn"]')))
                        html = driver.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        try:
                            # <---1st Table, take single table data, into list ------------>
                            # print('try_1:-')
                            l = []
                            table_case = soup.find('table', attrs={'class': 'case_details_table'})
                            table_case_body = table_case.find('tbody')

                            rows = table_case_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l.append([ele for ele in cols if ele])  # Get rid of empty values

                            b = l[:len(l) - 1]  # <-- for remove last "['CNR Number', 'MHPU050000132021\xa0\xa0 (No..]"

                            b1 = l[-1][1].replace('\xa0\xa0 (Note the CNR number for future reference)', '')
                            # <-- for remove last "(Note the CNR number for future reference)" text o/p-- "MHPU050000132021"

                            lst = reduce(lambda x, y: x + y, b)
                            lst.append('cnrnumber')  # < ---- add 'CNR' name
                            lst.append(b1)  # < ---- add 'CNR number
                            res_dct = {lst[i].lower().replace(' ', ''): lst[i + 1] for i in range(0, len(lst), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except Exception as e:
                            # print('Exception:-', e)
                            res_dct = {}
                            case_final_2 = {"ProgramError": str(e)}

                        try:
                            # <---2nd Table, take single table data, into list ------------>
                            l2 = []
                            table = soup.find('table', attrs={'class': 'table_r'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l2.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst2 = reduce(lambda x, y: x + y, l2)
                            res_dct2 = {lst2[i].lower().replace(' ', ''): lst2[i + 1] for i in range(0, len(lst2), 2)}
                            # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct2 = {}

                        try:
                            # <---3rd Table, take single table data, into list ----------->
                            l3 = []
                            table = soup.find('table', attrs={'class': 'Petitioner_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l3.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst3 = reduce(lambda x, y: x + y, l3)
                            listToStr = ' '.join(map(str, lst3))
                            res_dct3 = listToStr.replace('\xa0', '')  # <-- make kye 'LOWER case', remove 'space' ---
                        except:
                            res_dct3 = ''

                        try:
                            # <---4th Table, take single table data, into list ---------->
                            l4 = []
                            table = soup.find('table', attrs={'class': 'Respondent_Advocate_table'})
                            table_body = table.find('tbody')

                            rows = table_body.find_all('tr')
                            for row in rows:
                                cols = row.find_all('td')
                                cols = [ele.text.strip() for ele in cols]
                                l4.append([ele for ele in cols if ele])  # Get rid of empty values

                            lst4 = reduce(lambda x, y: x + y, l4)
                            listToStr2 = ' '.join(map(str, lst4))
                            res_dct4 = listToStr2  # <-- make kye 'LOWER case', remove 'space' --
                        except:
                            res_dct4 = ''

                        try:
                            # <---5th Table, take single table data, into list ---------->
                            table_a = soup.find('table', attrs={'class': 'acts_table'})
                            h, [_, *d] = [i.text for i in table_a.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_a.find_all('tr')]
                            res_dct5 = [dict(zip(h, i)) for i in d]

                            k5 = []
                            for d in res_dct5:
                                k5.append({k.replace(' ', '').replace('UnderAct(s)', 'Act').replace(
                                    'UnderSection(s)', 'Section'): v for k, v in d.items()})
                        except:
                            k5 = []

                        try:
                            # <---6th Table, take single table data, into list ---------->
                            table_t = soup.find('table', attrs={'class': 'history_table'})
                            h, [_, *d] = [i.text for i in table_t.tr.find_all('th')], [
                                [i.text for i in b.find_all('td')] for b in table_t.find_all('tr')]
                            res_dct6 = [dict(zip(h, i)) for i in d]

                            k6 = []
                            for d in res_dct6:
                                k6.append(
                                    {k.replace(' ', '').replace('PurposeofHearing', 'Purpose'): v for k, v in
                                     d.items()})
                        except:
                            k6 = []

                        try:
                            # <---7th Table, take single table data, into list ---------->
                            table_order = soup.find('table', attrs={'class': 'order_table'})
                            h7, [_, *q] = [x.text for x in table_order.tr.find_all('td')], [
                                [x.text for x in a.find_all('td')] for a in table_order.find_all('tr')]
                            res_dct7 = [dict(zip(h7, x)) for x in q]

                            k7 = []
                            for d7 in res_dct7:
                                k7.append(
                                    {k.replace('  ', '').replace(' ', ''): v for k, v in d7.items()})

                        except:
                            k7 = []

                        try:
                            # <---8th Table, take single table data, into list ---------->
                            table_tran = soup.find('table', attrs={'class': 'transfer_table'})
                            h1, [_, *q] = [x.text for x in table_tran.tr.find_all('th')], [
                                [x.text for x in a.find_all('td')] for a in table_tran.find_all('tr')]
                            res_dct8 = [dict(zip(h1, x)) for x in q]

                            k8 = []
                            for d in res_dct8:
                                k8.append({k.replace(' ', ''): v for k, v in d.items()})
                        except:
                            k8 = []

                        main_ch2 = 1

                        if res_dct != {}:
                            case_final = {"CaseDetails": res_dct, "CaseStatus": res_dct2,
                                          "PetitionerAdvocate": res_dct3,
                                          "RespondentAdvocate": res_dct4, "Acts": k5,
                                          "History": k6, "Orders": k7, "CaseTransferDetails": k8}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        elif res_dct == {}:
                            case_final_2 = {
                                "CaseDetailsNotFound": 'This Case Code does not exists/Data not available, please check CNR number'}
                            try:
                                driver.close()
                                break
                            except:
                                pass

                        else:
                            try:
                                driver.close()
                                break
                            except:
                                pass
                except Exception as e:
                    try:
                        driver.close()
                        print('except crser here--', e)
                        main_ch2 = 1
                        res_dct = {}
                        case_final_2 = {"ProgramError": str(e)}
                    except:
                        pass

                if main_ch2 == 1:
                    break

            if res_dct != {}:
                yourdata = case_final

            elif res_dct == {}:
                yourdata = case_final_2

            data = yourdata
            #print('data:--', data)

            try:
                conn = psycopg2.connect(database="defaultdb",user="doadmin",password="cknc7dhz9w20p6a2",
                                        host="db-postgresql-blr1-04861-do-user-7104723-0.b.db.ondigitalocean.com",
                                        port="25060")
                # conn = psycopg2.connect(database="ScrapCaseCnrDB", user="postgres", password="root", host="localhost",
                #                         port="5432")

                cursor = conn.cursor()
                #print('psycopg2 connect execute--')

                var_dict = {"var": 0, "data": 1}
                # <-- for write --
                with open(path+'case_2.txt', 'w') as json_file:
                    json.dump(var_dict, json_file)

            except (Exception, psycopg2.DatabaseError) as error:
                #print("Error while creating PostgreSQL table", error)
                pass
            else:

                cursor.execute('SELECT * FROM public."Scrap_App_case_2" ORDER BY id DESC')
                id = cursor.fetchone()[0]

                sql_update_query = """Update public."Scrap_App_case_2" set data = %s where id = %s"""
                cursor.executemany(sql_update_query, [(str(data), id)])
                conn.commit()

                #print("Table updated..", id)
                conn.commit()
                conn.close()

        # -----------------------------//Case function 1 End //--------------------------------------------------

        #multiprocessing.Process(target=Case_2_f).start()
        threading.Thread(target=Case_2_f).start()
        print('thred--')

        # ------// wait antil final data is coming //----
        while True:
            w = 0
            #wait = Case_2.objects.all().order_by('-id')[0].data

            # <-- for read ---
            with open(path+'case_1.txt', 'r') as f:
                _dict = json.load(f)
                # print(_dict['var'])  # 0
            #print('wait data:- ', _dict['data'])
            time.sleep(5)
            wait = _dict['data']  # 0

            #print('wait ON-')
            #print('wait OFF-')
            if wait == 0:
                w = 0
            else:
                w = 1
            if w == 1:
                # print('out while..')
                break
        # ------// --- //----
        time.sleep(3)

        data = Case_2.objects.all().order_by('-id')[0].data

        # -----------current id make it 0 -------------
        c_id = Case_2.objects.all().order_by('-id')[0].id
        c_id2 = Case_2.objects.get(id=c_id)
        c_id2.var = 0  # for make 1 to 0
        c_id2.save()

        finish = time.perf_counter()
        print(f'Case_2 Finished in {round(finish - start, 2)} second(s)')
    return HttpResponse(str(data).replace("'", '"'))
