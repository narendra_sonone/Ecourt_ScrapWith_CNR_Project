from django.contrib import admin
from .models import WCFinder_main_table, WCFinder_flag_urls

#------------------------------------------------------------------------------

class WCFinder_MainAdmin(admin.ModelAdmin):
    list_display = ['id', 'Website_list', 'watcher_var', 'xpath', 'ict', 'Table_id', 'Hash_kye', 'Hash_kye_2']
admin.site.register(WCFinder_main_table, WCFinder_MainAdmin)


class WCFinder_showAdmin(admin.ModelAdmin):
    list_display = ['id', 'date', 'url', 'Status']
admin.site.register(WCFinder_flag_urls, WCFinder_showAdmin)
