from django.apps import AppConfig


class WebsitechangefinderAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'WebsiteChangeFinder_App'
