from django.db import models

#---------------------------------------------------------------------
        # Change website finder
#---------------------------------------------------------------------
from django.utils.html import format_html


class WCFinder_main_table(models.Model):
    Website_list = models.URLField(max_length=5000)
    xpath = models.CharField(max_length=200, default='', null=True, blank=True, editable=False)
    ict = models.CharField(max_length=10, null=True, blank=True, editable=False)
    Table_id = models.CharField(max_length=5000, null=True, blank=True, editable=False)
    Hash_kye = models.CharField(max_length=5000, editable=False)
    Hash_kye_2 = models.CharField(max_length=5000, default='', blank=True, editable=False)
    watcher_var = models.CharField(max_length=10, default=0, null=False, blank=False, editable=False)

class WCFinder_flag_urls(models.Model):
    date = models.DateField(auto_now_add=True, null=True, blank=True, editable=False)
    Website_list = models.URLField(max_length=5000)


    def url(self):
        return format_html("<a href='{url}' target='_blank'>{url}</a>", url=self.Website_list)

    CHOICES = (
        ('', '----'),
        ('ua', 'Done'),
        ('na', 'Update Not Available'),
        ('dc', 'Only Date Change'),
    )
    Status = models.CharField(max_length=250, blank=True, default='', null=True, choices=CHOICES)
