import os

from bs4 import BeautifulSoup

from Ecourt_ScrapWith_CNR_Project.config import LEGTECHTASKSTATUS_TELEGRAM_CREDENTIAL
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ChromeOptions
from selenium.webdriver.support import expected_conditions as EC
import random
import time
import hashlib
from WebsiteChangeFinder_App.models import WCFinder_main_table, WCFinder_flag_urls
from celery import shared_task
from telegraph import Telegraph
import requests
import datetime
from datetime import datetime, timedelta
#<-------- Legtech task status ------------------->
l_bot_id = LEGTECHTASKSTATUS_TELEGRAM_CREDENTIAL['BOT_ID']
l_chat_id = LEGTECHTASKSTATUS_TELEGRAM_CREDENTIAL['CHAT_ID']
l_base_url = "https://api.telegram.org/bot{l_bot_id}/sendMessage".format(l_bot_id=l_bot_id)    # bot


#--------------------------------------------/ Main Task /-----------------
@shared_task
def Change_website_finder_task():
    global op_page_html, i, driverR_, watcher_var_after
    try:
        os.remove("/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/page_source/task_page_source.txt")
    except:
        pass
    #global op_page_html, driverR_, i
    date_time = datetime.now().isoformat(" ", "seconds")
    parameters = {
        "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
        "text": "Change_website_finder Task:-- Start\nDate:-- {date_time}".format(date_time=date_time)

    }
    resp = requests.get(l_base_url, data=parameters)
    # ----------------------------------------------------------------------
    # ----------------------------------------------------------------------
    try:
        while True:
            Q = 0
            try:
                qs = WCFinder_main_table.objects.filter(watcher_var__lte='0')
                if qs:
                    for i in qs:
                        # -----------Code start---Chrome--
                        options = ChromeOptions()
                        options.add_argument('--no-sandbox')
                        options.add_argument('--disable-dev-shm-usage')
                        options.headless = True
                        driverR_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
                        print('driverR:-')
                        driverR_.maximize_window()
                        driverR_.get(str(i.Website_list))
                        driverR_.refresh()
                        print('url:--', i.Website_list)
                        time.sleep(4)

                        if i.ict:
                            # -----------------------
                            if i.ict == 'd_i':
                                #print('div:-i--')
                                html = driverR_.page_source
                                soup = BeautifulSoup(html, "html.parser")
                                op_page_html = soup.find('div', {'id': f'{i.Table_id}'})
                                # ------------------------------------
                            elif i.ict == 'd_c':
                                # ------------------------------------
                                #print('div:-c--')
                                html = driverR_.page_source
                                soup = BeautifulSoup(html, "html.parser")
                                op_page_html = soup.find('div', {'class': f'{i.Table_id}'})
                                # ------------------------------------
                            elif i.ict == 't_i':
                                # ------------------------------------
                                #print('table:-i--')
                                html = driverR_.page_source
                                soup = BeautifulSoup(html, "html.parser")
                                op_page_html = soup.find('table', {'id': f'{i.Table_id}'})
                                # ------------------------------------
                            elif i.ict == 't_c':
                                # ------------------------------------
                                #print('table:-c--')
                                html = driverR_.page_source
                                soup = BeautifulSoup(html, "html.parser")
                                op_page_html = soup.find('table', {'class': f'{i.Table_id}'})
                                # ------------------------------------
                            elif i.ict == 'sec_c':
                                # ------------------------------------
                                #print('table:-sec_c--')
                                html = driverR_.page_source
                                soup = BeautifulSoup(html, "html.parser")
                                op_page_html = soup.find('section', {'class': f'{i.Table_id}'})
                                # ------------------------------------
                            elif i.ict == 'sec_i':
                                # ------------------------------------
                                #print('table:-sec_i--')
                                html = driverR_.page_source
                                soup = BeautifulSoup(html, "html.parser")
                                op_page_html = soup.find('section', {'id': f'{i.Table_id}'})
                                # ------------------------------------
                            elif i.ict == 'ul_i':
                                # ------------------------------------
                                #print('table:-ul_i--')
                                html = driverR_.page_source
                                soup = BeautifulSoup(html, "html.parser")
                                op_page_html = soup.find('ul', {'id': f'{i.Table_id}'})
                                # ------------------------------------
                            elif i.ict == 'ul_c':
                                # ------------------------------------
                                #print('table:-c--')
                                html = driverR_.page_source
                                soup = BeautifulSoup(html, "html.parser")
                                op_page_html = soup.find('ul', {'class': f'{i.Table_id}'})
                                # ------------------------------------
                        # -----------------------
                        elif i.xpath:
                            # --------- get html of page ------------------
                            op_page_html = driverR_.find_element_by_xpath(f'{i.xpath}').get_attribute('outerHTML')
                            time.sleep(1)
                        # ---------------------------------------------
                        pageSource = str(op_page_html)
                        fileToWrite = open("/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/page_source/task_page_source.txt",
                                           "w")
                        fileToWrite.write(pageSource)
                        time.sleep(1)
                        fileToWrite.close()
                        # --------------------------sample.html
                        time.sleep(1)
                        file = "/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/page_source/task_page_source.txt"  # Location of the file (can be set a different way)
                        BLOCK_SIZE = 65536  # The size of each read from the file

                        file_hash = hashlib.md5()  # Create the hash object, can use something other than `.sha256()` if you wish
                        with open(file, 'rb') as f:  # Open the file to read it's bytes
                            fb = f.read(BLOCK_SIZE)  # Read from the file. Take in the amount declared above
                            while len(fb) > 0:  # While there is still data being read from the file
                                file_hash.update(fb)  # Update the hash
                                fb = f.read(BLOCK_SIZE)  # Read the next block from the file

                        print('hash kye:--', file_hash.hexdigest())  # Get the hexadecimal digest of the hash
                        out_hash = file_hash.hexdigest()
                        # --------------------------
                        # time.sleep(2)
                        driverR_.quit()
                        # print('driver:- close')

                        if str(out_hash) == str(i.Hash_kye_2):
                            # --------- update watcher_var 0-1 ----------
                            WCFinder_main_table.objects.filter(id=i.id).update(watcher_var=1)

                        elif str(out_hash) != str(i.Hash_kye_2):
                            if str(i.Hash_kye_2) == '':
                                # -----------------------------------/ save data /--------------------------
                                WCFinder_main_table.objects.filter(id=i.id).update(Hash_kye_2=out_hash)
                                # -------- update first kye -----
                                WCFinder_main_table.objects.filter(id=i.id).update(Hash_kye=out_hash)
                                # --------- update watcher_var 0-1 ----------
                                WCFinder_main_table.objects.filter(id=i.id).update(watcher_var=1)

                            elif str(out_hash) != str(i.Hash_kye_2):
                                # -----------------------------------/ save data /--------------------------
                                WCFinder_main_table.objects.filter(id=i.id).update(Hash_kye_2=out_hash)
                                # -------- update first kye -----
                                WCFinder_main_table.objects.filter(id=i.id).update(Hash_kye=out_hash)
                                # --------- show on FLAG TABLE ---
                                WCFinder_flag_urls(Website_list=i.Website_list).save()
                                # --------- update watcher_var 0-1 ----------
                                WCFinder_main_table.objects.filter(id=i.id).update(watcher_var=1)
                    Q = 1
                    '''
                    # ----------------/ telegram message /-----------------------
                    date_time = datetime.now().isoformat(" ", "seconds")
                    parameters = {
                        "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
                        "text": "Change_website_finder Task:-- Done\nDate:-- {date_time}\nTotal Link's found:- {found}".format(
                            date_time=date_time, found=count)

                    }
                    resp = requests.get(l_base_url, data=parameters)'''
                else:
                    Q = 1
            except Exception as e:
                try:
                    driverR_.quit()
                except:
                    pass

                watcher_var_before = i.watcher_var
                # ------ make error object as pass ---
                WCFinder_main_table.objects.filter(id=i.id).update(watcher_var='1')
                s = WCFinder_main_table.objects.filter(id=i.id)
                for w in s:
                    watcher_var_after = w.watcher_var

                # ----------------/ telegram message /-----------------------
                date_time = datetime.now().isoformat(" ", "seconds")
                parameters = {
                    "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
                    "text": "Date:--{date_time}\nID:- {id}\nLINK:- {link}\nWATCHER VAR BEFORE:- {watcher_var_before}\nWATCHER VAR AFTER:- {watcher_var_after}\n\nERROR in Change_website_finder Task [Handler TRY]:-\n\nError:--\n{error}".format(
                        id=i.id, link=i.Website_list, error=e, date_time=date_time, watcher_var_before=watcher_var_before, watcher_var_after=watcher_var_after)
                }
                resp = requests.get(l_base_url, data=parameters)
                # ----------------------------------------------------------------------
                #---- for run again ---
                Q = 0

            if Q == 1:
                Date = datetime.now().date()
                #---------/ for get count flag urls /-------------
                qs2 = WCFinder_flag_urls.objects.filter(date=str(Date))

                # ----------------/ telegram message /-----------------------
                date_time = datetime.now().isoformat(" ", "seconds")
                parameters = {
                    "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
                    "text": "Change_website_finder Task:-- Done\nDate:-- {date_time}\nTotal Link's found:- {found}".format(
                        date_time=date_time, found=qs2.count())

                }
                resp = requests.get(l_base_url, data=parameters)
                #----- break while loop ------
                break
    except Exception as e:
        try:
            driverR_.quit()
        except:
            pass
        # ----------------/ telegram message /-----------------------
        date_time = datetime.now().isoformat(" ", "seconds")
        parameters = {
            "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
            "text": "Date:--{date_time}\nID:- {id}\nLINK:- {link}\n\nERROR in Change_website_finder Task [MAIN TRY]:-\n\nError:--\n{error}".format(
                id=i.id, link=i.Website_list, error=e, date_time=date_time)

        }
        resp = requests.get(l_base_url, data=parameters)
        # ----------------------------------------------------------------------
    #------------------------------------------------------------------------
    '''
    try:
        qs = WCFinder_main_table.objects.filter(watcher_var__lte='0')
        if qs:
            for i in qs:
                # -----------Code start---Chrome--
                options = ChromeOptions()
                options.add_argument('--no-sandbox')
                options.add_argument('--disable-dev-shm-usage')
                options.headless = True
                driverR_ = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
                print('driverR:-')
                driverR_.maximize_window()
                driverR_.get(str(i.Website_list))
                driverR_.refresh()
                print('url:--', i.Website_list)
                time.sleep(4)

                if i.ict:
                    # -----------------------
                    if i.ict == 'd_i':
                        print('div:-i--')
                        html = driverR_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_html = soup.find('div', {'id': f'{i.Table_id}'})
                        # ------------------------------------
                    elif i.ict == 'd_c':
                        # ------------------------------------
                        print('div:-c--')
                        html = driverR_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_html = soup.find('div', {'class': f'{i.Table_id}'})
                        # ------------------------------------
                    elif i.ict == 't_i':
                        # ------------------------------------
                        print('table:-i--')
                        html = driverR_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_html = soup.find('table', {'id': f'{i.Table_id}'})
                        # ------------------------------------
                    elif i.ict == 't_c':
                        # ------------------------------------
                        print('table:-c--')
                        html = driverR_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_html = soup.find('table', {'class': f'{i.Table_id}'})
                        # ------------------------------------
                    elif i.ict == 'sec_c':
                        # ------------------------------------
                        print('table:-sec_c--')
                        html = driverR_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_html = soup.find('section', {'class': f'{i.Table_id}'})
                        # ------------------------------------
                    elif i.ict == 'sec_i':
                        # ------------------------------------
                        print('table:-sec_i--')
                        html = driverR_.page_source
                        soup = BeautifulSoup(html, "html.parser")
                        op_page_html = soup.find('section', {'id': f'{i.Table_id}'})
                        # ------------------------------------
                # -----------------------
                elif i.xpath:
                    #--------- get html of page ------------------
                    op_page_html = driverR_.find_element_by_xpath(f'{i.xpath}').get_attribute('outerHTML')
                    time.sleep(1)
                #---------------------------------------------
                pageSource = str(op_page_html)
                fileToWrite = open("/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/page_source/task_page_source.txt",
                                   "w")
                fileToWrite.write(pageSource)
                time.sleep(1)
                fileToWrite.close()
                # --------------------------sample.html
                time.sleep(1)
                file = "/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/page_source/task_page_source.txt"  # Location of the file (can be set a different way)
                BLOCK_SIZE = 65536  # The size of each read from the file

                file_hash = hashlib.md5()  # Create the hash object, can use something other than `.sha256()` if you wish
                with open(file, 'rb') as f:  # Open the file to read it's bytes
                    fb = f.read(BLOCK_SIZE)  # Read from the file. Take in the amount declared above
                    while len(fb) > 0:  # While there is still data being read from the file
                        file_hash.update(fb)  # Update the hash
                        fb = f.read(BLOCK_SIZE)  # Read the next block from the file

                print('hash kye:--', file_hash.hexdigest())  # Get the hexadecimal digest of the hash
                out_hash = file_hash.hexdigest()
                # --------------------------
                # time.sleep(2)
                driverR_.quit()
                # print('driver:- close')

                if str(out_hash) == str(i.Hash_kye_2):
                    # --------- update watcher_var 0-1 ----------
                    WCFinder_main_table.objects.filter(id=i.id).update(watcher_var=1)

                elif str(out_hash) != str(i.Hash_kye_2):
                    if str(i.Hash_kye_2) == '':
                        # -----------------------------------/ save data /--------------------------
                        WCFinder_main_table.objects.filter(id=i.id).update(Hash_kye_2=out_hash)
                        # -------- update first kye -----
                        WCFinder_main_table.objects.filter(id=i.id).update(Hash_kye=out_hash)
                        # --------- update watcher_var 0-1 ----------
                        WCFinder_main_table.objects.filter(id=i.id).update(watcher_var=1)

                    elif str(out_hash) != str(i.Hash_kye_2):
                        count = count + 1
                        # -----------------------------------/ save data /--------------------------
                        WCFinder_main_table.objects.filter(id=i.id).update(Hash_kye_2=out_hash)
                        # -------- update first kye -----
                        WCFinder_main_table.objects.filter(id=i.id).update(Hash_kye=out_hash)
                        # --------- show on FLAG TABLE ---
                        WCFinder_flag_urls(Website_list=i.Website_list).save()
                        # --------- update watcher_var 0-1 ----------
                        WCFinder_main_table.objects.filter(id=i.id).update(watcher_var=1)


            # ----------------/ telegram message /-----------------------
            date_time = datetime.now().isoformat(" ", "seconds")
            parameters = {
                "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
                "text": "Change_website_finder Task:-- Done\nDate:-- {date_time}\nTotal Link's found:- {found}".format(
                    date_time=date_time, found=count)

            }
            resp = requests.get(l_base_url, data=parameters)
        else:
            pass
    except Exception as e:
        # ----------------/ telegram message /-----------------------
        date_time = datetime.now().isoformat(" ", "seconds")
        parameters = {
            "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
            "text": "Date:--{date_time}\nID:- {id}\nLINK:- {link}\n\nERROR in Change_website_finder Task\n\nError:--\n{error}".format(
                id=i.id, link=i.Website_list, error=e, date_time=date_time)

        }
        resp = requests.get(l_base_url, data=parameters)
        # ----------------------------------------------------------------------
        '''



#===================================================================================

#------------/ delete image from scrapApp, make 1 to 0 watcher var of WCF_App Task /-----------------
@shared_task
def DeleteImage_1to0_var_WCFApp_task():
    date_time = datetime.now().isoformat(" ", "seconds")
    parameters = {
        "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
        "text": "DeleteImage_1to0_var_WCFApp Task:-- Start\nDate:-- {date_time}".format(date_time=date_time)

    }
    resp = requests.get(l_base_url, data=parameters)

    #----------/ for delete Scrap App (Case) images /-----------------------------------------
    try:
        path = '/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/Images/CaseImages'
        for file in os.listdir(path):
            print(file)
            if file.endswith('.png'):
                os.remove(path + '/' + file)
                #print('done:-')
    except Exception as e:
        date_time = datetime.now().isoformat(" ", "seconds")
        parameters = {
            "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
            "text": "Date:--{date_time}\nERROR in DeleteImage_1to0_var_WCFApp Task (CaseImages DeleteImage)\n\nError:--\n{error}".format(error=e, date_time=date_time)

        }
        resp = requests.get(l_base_url, data=parameters)

    #----------/ for delete Scrap App (Advocate) images /-----------------------------------------
    try:
        path = '/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/Images/AdvocateImages'
        for file in os.listdir(path):
            print(file)
            if file.endswith('.png'):
                os.remove(path + '/' + file)
                #print('done:-')
    except Exception as e:
        date_time = datetime.now().isoformat(" ", "seconds")
        parameters = {
            "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
            "text": "Date:--{date_time}\nERROR in DeleteImage_1to0_var_WCFApp Task (Advocate DeleteImage)\n\nError:--\n{error}".format(error=e, date_time=date_time)

        }
        resp = requests.get(l_base_url, data=parameters)
    #-----------/ for make 1 to 0 'watcher_var' of WebsiteChangeFinder_App--------------
    try:
        qs_ = WCFinder_main_table.objects.filter(watcher_var__gte='1')
        for i in qs_:
            #print(i.watcher_var)
            WCFinder_main_table.objects.filter(id=i.id).update(watcher_var='0')

    except Exception as e:
        date_time = datetime.now().isoformat(" ", "seconds")
        parameters = {
            "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
            "text": "Date:--{date_time}\nERROR in DeleteImage_1to0_var_WCFApp Task (1 to 0)\n\nError:--\n{error}".format(error=e, date_time=date_time)

        }
        resp = requests.get(l_base_url, data=parameters)
#===================================================================================
