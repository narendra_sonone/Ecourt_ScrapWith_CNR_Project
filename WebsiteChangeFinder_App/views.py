import os
from asgiref.sync import sync_to_async
from bs4 import BeautifulSoup
from django.http import HttpResponse
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ChromeOptions
from selenium.webdriver.support import expected_conditions as EC
import random
import time
import hashlib
from WebsiteChangeFinder_App.models import WCFinder_main_table, WCFinder_flag_urls
import asyncio


#----------/ by url query /----------------
#import asyncio
def Change_website_finder(request):
    global op_page_html, url, ict, table_id, xpath
    try:
        try:
            os.remove("/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/page_source/page_source.txt")
        except:
            pass
        #global op_page_html, itc
        try:
            xpath = request.GET.get('xpath')
        except:
            xpath = None
        if xpath:
            url = request.GET.get('url')
            xpath = request.GET.get('xpath')
            #print('xpath:-', xpath)
            # -----------Code start---Chrome--
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driverR = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            #print('driverR:-')
            driverR.maximize_window()
            driverR.get(f'{url}')
            driverR.refresh()
            time.sleep(3)
            # --------- get html of page ------------------
            # op_page_html = driverR.find_element_by_xpath(f'{xpath}').get_attribute('outerHTML')
            op_page_html = driverR.find_element_by_xpath(f'{xpath}').text
            time.sleep(1)

        else:
            url = request.GET.get('url')
            table_id = request.GET.get('html_kye')
            ict = request.GET.get('type')
            print(url, ',', table_id, ',', ict)
            # -----------Code start---Chrome--
            options = ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.headless = True
            driverR = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver", options=options)
            # print('driverR:-')
            driverR.maximize_window()
            driverR.get(f'{url}')
            driverR.refresh()
            time.sleep(3)
            if ict == 'd_i':
                # ------------------------------------
                print('div:-i--')
                ict = 'd_i'
                html = driverR.page_source
                soup = BeautifulSoup(html, "html.parser")
                op_page_html = soup.find('div', {'id': f'{table_id}'}).text
                # ------------------------------------
            elif ict == 'd_c':
                # ------------------------------------
                print('div:-c--')
                itc = 'd_c'
                html = driverR.page_source
                soup = BeautifulSoup(html, "html.parser")
                op_page_html = soup.find('div', {'class': f'{table_id}'}).text
                # ------------------------------------
            elif ict == 't_i':
                # ------------------------------------
                print('table:-i--')
                itc = 't_i'
                html = driverR.page_source
                soup = BeautifulSoup(html, "html.parser")
                op_page_html = soup.find('table', {'id': f'{table_id}'}).text
                # ------------------------------------
            elif ict == 't_c':
                # ------------------------------------
                print('table:-c--')
                itc = 't_c'
                html = driverR.page_source
                soup = BeautifulSoup(html, "html.parser")
                op_page_html = soup.find('table', {'class': f'{table_id}'}).text
                # ------------------------------------
            elif ict == 'sec_c':
                # ------------------------------------
                print('table:-sec_c--')
                itc = 'sec_c'
                html = driverR.page_source
                soup = BeautifulSoup(html, "html.parser")
                op_page_html = soup.find('section', {'class': f'{table_id}'}).text
                # ------------------------------------
            elif ict == 'sec_i':
                # ------------------------------------
                print('table:-sec_i--')
                itc = 'sec_i'
                html = driverR.page_source
                soup = BeautifulSoup(html, "html.parser")
                op_page_html = soup.find('section', {'id': f'{table_id}'}).text
                # ------------------------------------
            elif ict == 'ul_i':
                # ------------------------------------
                # print('table:-ul_i--')
                ict = 'ul_i'
                html = driverR.page_source
                soup = BeautifulSoup(html, "html.parser")
                op_page_html = soup.find('ul', {'id': f'{table_id}'}).text
                # ------------------------------------
            elif ict == 'ul_c':
                # ------------------------------------
                # print('table:-c--')
                ict = 'ul_c'
                html = driverR.page_source
                soup = BeautifulSoup(html, "html.parser")
                op_page_html = soup.find('ul', {'class': f'{table_id}'}).text
                # ------------------------------------
            
        
        pageSource = str(op_page_html)
        fileToWrite = open("/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/page_source/page_source.txt", "w")
        # fileToWrite = open("page_source.txt", "w")
        fileToWrite.write(pageSource)
        time.sleep(1)
        fileToWrite.close()
        # --------------------------sample.html
        # time.sleep(1)
        file = "/home/legtech_deploy/Ecourt_ScrapWith_CNR_Project/page_source/page_source.txt"  # Location of the file (can be set a different way)
        # file = "page_source.txt"  # Location of the file (can be set a different way)
        BLOCK_SIZE = 65536  # The size of each read from the file

        #-------------------------------------
        with open(file, 'rb') as f:  # Open the file to read it's bytes
            FB_ = f.read()
            #print('FB_:-', FB_)
            if FB_ != b'None':
                #print('nnnnnnnnnn')
                file_hash = hashlib.md5()  # Create the hash object, can use something other than `.sha256()` if you wish
                with open(file, 'rb') as f:  # Open the file to read it's bytes
                    fb = f.read(BLOCK_SIZE)  # Read from the file. Take in the amount declared above
                    while len(fb) > 0:  # While there is still data being read from the file
                        file_hash.update(fb)  # Update the hash
                        fb = f.read(BLOCK_SIZE)  # Read the next block from the file

                #print('hash kye:--', file_hash.hexdigest())  # Get the hexadecimal digest of the hash
                out_hash = file_hash.hexdigest()
                # --------------------------
                # time.sleep(2)
                driverR.quit()
                #print('driver:- close')

                if xpath:
                    with open(file, 'rb') as f:  # Open the file to read it's bytes
                        fb_ = f.read()
                    # -----------------------------------/ save data /--------------------------
                    #WCFinder_main_table(Website_list=str(url), xpath=str(xpath), Hash_kye=out_hash, data=str(fb_)).save()
                    WCFinder_main_table(Website_list=str(url), xpath=str(xpath), Hash_kye=out_hash).save()
                    #---------------------------------------------------------------------------
                    return HttpResponse(f'URL:- {url}, XPATH:- {xpath}, {fb_}')
                else:
                    with open(file, 'rb') as f:  # Open the file to read it's bytes
                        fb_ = f.read()
                    # -----------------------------------/ save data /--------------------------
                    #WCFinder_main_table(Website_list=str(url), ict=str(ict), Table_id=str(table_id), Hash_kye=out_hash, data=str(fb_)).save()
                    WCFinder_main_table(Website_list=str(url), ict=str(ict), Table_id=str(table_id), Hash_kye=out_hash).save()
                    # ---------------------------------------------------------------------------
                    return HttpResponse(f'URL:- {url}, HTML KYE:- {table_id}, TYPE:- {ict}, {fb_}')
            else:
                try:
                    driverR.quit()
                except:
                    pass

                if xpath:
                    return HttpResponse('Data not save:--[None], URL:- {url}, XPATH:- {xpath}, FILE CONTENT:- {FB_}'.format(url=url, xpath=xpath, FB_=FB_))
                else:
                    return HttpResponse('Data not save:--[None], URL:- {url}, HTML KYE:- {table_id}, TYPE:- {ict}, FILE CONTENT:- {FB_}'.format(url=url, table_id=table_id, ict=ict, FB_=FB_))

        #=========================
    except Exception as e:
        #print('e:-', e)
        return HttpResponse(f'{e}')


#===========================================
    




