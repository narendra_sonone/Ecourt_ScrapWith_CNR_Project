from django.contrib import admin
from .models import JudgementDetail

class JudgementDetailAdmin(admin.ModelAdmin):

    list_display = ['id', 'Judgement_date', 'watcher_var', 'title', 'image', 'keywords', 'document', 'date', 's3_link']
admin.site.register(JudgementDetail, JudgementDetailAdmin)
