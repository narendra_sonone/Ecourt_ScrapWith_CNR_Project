import random
from celery import shared_task
from Ecourt_ScrapWith_CNR_Project.config import JUDGEMENT_TELEGRAM_CREDENTIAL, LEGTECHTASKSTATUS_TELEGRAM_CREDENTIAL, \
    AMAZON_CREDENTIAL
from .models import JudgementDetail
from telegraph import Telegraph
from django.template.defaultfilters import striptags
import requests
import datetime
import time
import re
from datetime import datetime, timedelta

# <-------- Legtech task status ------------------->
l_bot_id = LEGTECHTASKSTATUS_TELEGRAM_CREDENTIAL['BOT_ID']
l_chat_id = LEGTECHTASKSTATUS_TELEGRAM_CREDENTIAL['CHAT_ID']
l_base_url = "https://api.telegram.org/bot{l_bot_id}/sendMessage".format(l_bot_id=l_bot_id)  # bot

# <--------- Judgement Telegram ----------------->
bot_id = JUDGEMENT_TELEGRAM_CREDENTIAL['JUDGEMENT_BOT_ID']
chat_id = JUDGEMENT_TELEGRAM_CREDENTIAL['JUDGEMENT_CHAT_ID']
base_url = "https://api.telegram.org/bot{bot_id}/sendMessage".format(bot_id=bot_id)  # bot

telegraph = Telegraph()
telegraph.create_account(short_name='1337')
# ---------------------------------

#------------- AWS Credential's ------------------
# AWS Configurations S3
from boto3.session import Session
AWS_ACCESS_KEY_ID = AMAZON_CREDENTIAL['ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = AMAZON_CREDENTIAL['SECRET_ACCESS_KEY']
AWS_STORAGE_BUCKET_NAME = AMAZON_CREDENTIAL['STORAGE_BUCKET_NAME']

session = Session(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
s3 = session.resource('s3')

# <--------- Judgement Telegram ----------------->
# ------------------------------------------------

@shared_task
def Judgement_telegram_bot():
    try:
        try:
            none_b = JudgementDetail.objects.filter(link=None)
            for j in none_b:
                j.link = ''
                j.save()
        except:
            pass

        data = JudgementDetail.objects.filter(watcher_var__lte='0')
        # -------------/ show s3 link /------------------
        for i in data:
            if i.link:
                try:
                    # <-------/ search file form s3 /-----------------
                    my_bucket = s3.Bucket('sitena')
                    filename = str(i.link).replace(' ', '').replace('.pdf', '').replace('.PDF', '').replace('.',
                                                                                                            '_').replace(
                        'https://', '').replace('http://', '').replace('www', '').replace('/', '-')
                    for obj in my_bucket.objects.all():
                        if re.search(filename, obj.key):
                            #print(obj.key)  # o/p=> link_pdf/httpsrbidocsrbiorginrdocsnotificationPDFsNOT175E28A758924C047D29651D40A0E90F952PDF.pdf
                            s3_link_data = obj.key
                            time.sleep(1)
                            # <---/ save s3 link in database /---
                            link_data = JudgementDetail.objects.filter(pk=i.id)[0]
                            link_data.link = f'https://sitena.s3.amazonaws.com/{s3_link_data}'
                            link_data.save()
                except:
                    pass
            else:
                pass
        # -------------/ telegram updates /------------------
        for i in data:
            content = striptags(i.description)
            if i.image:
                def delay():
                    time.sleep(random.randint(3, 3))

                response = telegraph.create_page(i.title,
                                                 html_content='<img src="https://sitena.s3.amazonaws.com/{image}" alt="loading image" /><p>{content}</p><a href="https://legtech.in/{y}/{z}/Judgements/">Click here for more details</a><br/>'.format(
                                                     content=content, image=i.image, y=i.id, z=i.slug))
                # print('https://telegra.ph/{}'.format(response['path']))
                delay()
                delay()

                parameters = {
                    "chat_id": "{chat_id}".format(chat_id=chat_id),  # bot
                    # "text": "{x}\n\nhttps://legtech.in/{y}/{z}/LegalUpdates/\n\nINSTANT VIEW\nhttps://telegra.ph/{p}".format(x=i.title, y=i.id, z=i.slug, p=response['path'])
                    "text": "{x}\n\nINSTANT VIEW\nhttps://telegra.ph/{p}".format(x=i.title, p=response['path'])

                }

                resp = requests.get(base_url, data=parameters)
                # print(resp.text)
                delay()
                JudgementDetail.objects.filter(id=i.id).update(watcher_var=1)
                delay()

            else:
                def delay7():
                    time.sleep(random.randint(3, 3))

                response = telegraph.create_page(i.title,
                                                 html_content='<img src="https://sitena.s3.amazonaws.com/detailpage_images/For_Update.gif" alt="loading image" /><p>{content}</p><a href="https://legtech.in/{y}/{z}/LegalUpdates/">Click here for more details</a><br/>'.format(
                                                     content=content, y=i.id, z=i.slug))
                # print('https://telegra.ph/{}'.format(response['path']))
                delay7()
                delay7()

                parameters = {
                    "chat_id": "{chat_id}".format(chat_id=chat_id),  # bot
                    # "text": "{x}\n\nhttps://legtech.in/{y}/{z}/LegalUpdates/\n\nINSTANT VIEW\nhttps://telegra.ph/{p}".format(x=i.title, y=i.id, z=i.slug, p=response['path'])
                    "text": "{x}\n\nINSTANT VIEW\nhttps://telegra.ph/{p}".format(x=i.title, p=response['path'])

                }

                resp = requests.get(base_url, data=parameters)
                # print(resp.text)
                delay7()
                JudgementDetail.objects.filter(id=i.id).update(watcher_var=1)
                delay7()

    except Exception as e:
        x = datetime.strftime(datetime.now() - timedelta(0), '%Y-%m-%d')
        parameters = {
            "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
            "text": "Date:--{d}\nERROR in Judgement Telegram Task\n\nException as e:--{a}".format(a=e, d=x)

        }
        resp = requests.get(l_base_url, data=parameters)
#========================================================================


