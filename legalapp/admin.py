from django.contrib import admin
from .models import LegalDetail

class LegalDetailAdmin(admin.ModelAdmin):

    list_display = ['id', 'legalupdate_date', 'watcher_var', 'title', 'image', 'keywords', 'document', 'date', 's3_link']
admin.site.register(LegalDetail, LegalDetailAdmin)



