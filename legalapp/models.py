from datetime import date
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.html import format_html
from requests.exceptions import InvalidURL
from tinymce import models as tinymce_models
from django.db.models.signals import pre_save

from Ecourt_ScrapWith_CNR_Project.config import AMAZON_CREDENTIAL
from Ecourt_ScrapWith_CNR_Project.utils import unique_slug_generator
from django.urls import reverse

import re
import os
import urllib.request
from boto3.session import Session
import boto3

# AWS Configurations S3
AWS_ACCESS_KEY_ID = AMAZON_CREDENTIAL['ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = AMAZON_CREDENTIAL['SECRET_ACCESS_KEY']
AWS_STORAGE_BUCKET_NAME = AMAZON_CREDENTIAL['STORAGE_BUCKET_NAME']

session = Session(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
s3 = session.resource('s3')

import urllib.request

def check_url(url):
    if url.endswith(".html"):
        raise ValidationError("submitted link end's with '.html', please download first '.pdf' from these link, then upload 'pdf' file.")

    elif url.endswith(".pdf"):
        try:
            response = urllib.request.urlopen(url)
            if response.getcode() == 200:
                filename = str(url).replace(' ', '').replace('.pdf', '').replace('.PDF', '').replace('.', '_').replace('https://', '').replace('http://', '').replace('www', '').replace('/', '-')
                file = open('Link_document/' + filename + ".pdf", 'wb')
                file.write(response.read())
                file.close()
                # <---/ save file in s3 /---
                s3.meta.client.upload_file(f'Link_document/{filename}.pdf', 'sitena', f'link_pdf/{filename}.pdf', ExtraArgs={'ContentType': "application/pdf", 'ACL': "public-read"})
                # ---------------------------------------
                os.remove(f"Link_document/{filename}.pdf")
            elif response.getcode() != 200:
                raise ValidationError('submitted link not valid/working, please check and try again.')
        except Exception as e:
            raise ValidationError(f"InvalidURL: {e}")
    else:
        pass

def no_future(value):
    today = date.today()
    if value > today:
        raise ValidationError('Date cannot be in the future.')

class LegalDetail(models.Model):
    legalupdate_date = models.DateField(auto_now_add=True)
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, null=True, blank=True, editable=False)
    image = models.ImageField(null=True, blank=True, upload_to='detailpage_images/')
    keywords = models.CharField(blank=True, max_length=200)
    description = tinymce_models.HTMLField(null=True)
    document = models.FileField(null=True, blank=True, upload_to='pdfs/', max_length=500)
    date = models.DateField(null=True, validators=[no_future])
    link = models.CharField(max_length=10000, default='', null=True, blank=True, validators=[check_url])
    watcher_var = models.CharField(max_length=10, default=0, null=False, blank=False, editable=False)


    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("Legal-detail", kwargs={"id": str(self.id), "slug": str(self.slug)})

    def s3_link(self):
        return format_html("<a href='{url_data}' target='_blank'>{url_data}</a>", url_data=self.link)

def rl_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(rl_pre_save_receiver, sender=LegalDetail)


