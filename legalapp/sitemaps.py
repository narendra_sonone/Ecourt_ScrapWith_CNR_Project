from django.contrib.sitemaps import Sitemap
from .models import LegalDetail
from django.shortcuts import reverse

class LegalDetailSitemap(Sitemap):
    changefreq = "weekly"
    priority = 1.00

    def items(self):
        return LegalDetail.objects.all() #.order_by('-id')

    # def lastmod(self, obj):
    #     return obj.updated_on