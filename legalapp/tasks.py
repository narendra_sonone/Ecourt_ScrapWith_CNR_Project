import random
from celery import shared_task
from Ecourt_ScrapWith_CNR_Project.config import LEGAL_TELEGRAM_CREDENTIAL, LEGTECHTASKSTATUS_TELEGRAM_CREDENTIAL, \
    P_TELEGRAM_CREDENTIAL, AMAZON_CREDENTIAL
from .models import LegalDetail
from telegraph import Telegraph

from django.template.defaultfilters import striptags
import datetime
from datetime import datetime, timedelta
import requests
import time
import re

#---personal---
p_bot_id = P_TELEGRAM_CREDENTIAL['P_BOT_ID']
p_chat_id = P_TELEGRAM_CREDENTIAL['P_CHAT_ID']
p_base_url = "https://api.telegram.org/bot{p_bot_id}/sendMessage".format(p_bot_id=p_bot_id)    # bot

#<-------- Legtech task status ------------------->
l_bot_id = LEGTECHTASKSTATUS_TELEGRAM_CREDENTIAL['BOT_ID']
l_chat_id = LEGTECHTASKSTATUS_TELEGRAM_CREDENTIAL['CHAT_ID']
l_base_url = "https://api.telegram.org/bot{l_bot_id}/sendMessage".format(l_bot_id=l_bot_id)    # bot

#-----------------
bot_id = LEGAL_TELEGRAM_CREDENTIAL['BOT_ID']
chat_id = LEGAL_TELEGRAM_CREDENTIAL['CHAT_ID']
base_url = "https://api.telegram.org/bot{bot_id}/sendMessage".format(bot_id=bot_id)    # bot


telegraph = Telegraph()
telegraph.create_account(short_name='13377')

#------------- AWS Credential's ------------------
# AWS Configurations S3
from boto3.session import Session
AWS_ACCESS_KEY_ID = AMAZON_CREDENTIAL['ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = AMAZON_CREDENTIAL['SECRET_ACCESS_KEY']
AWS_STORAGE_BUCKET_NAME = AMAZON_CREDENTIAL['STORAGE_BUCKET_NAME']

session = Session(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
s3 = session.resource('s3')


@shared_task
def Legal_updates_telegram_bot():
    try:
        try:
            none_bl = LegalDetail.objects.filter(link=None)
            for l in none_bl:
                l.link = ''
                l.save()
        except:
            pass

        data = LegalDetail.objects.filter(watcher_var__lte='0')
        #-----------/s3 link save /-------------------
        for i in data:
            if i.link:
                try:
                    # <-------/ search file form s3 /-----------------
                    my_bucket = s3.Bucket('sitena')
                    filename = str(i.link).replace(' ', '').replace('.pdf', '').replace('.PDF', '').replace('.',
                                                                                                            '_').replace(
                        'https://', '').replace('http://', '').replace('www', '').replace('/', '-')
                    for obj in my_bucket.objects.all():
                        if re.search(filename, obj.key):
                            #print(obj.key)  # o/p=> link_pdf/httpsrbidocsrbiorginrdocsnotificationPDFsNOT175E28A758924C047D29651D40A0E90F952PDF.pdf
                            s3_link_data = obj.key
                            time.sleep(1)
                            # <---/ save s3 link in database /---
                            link_data = LegalDetail.objects.filter(pk=i.id)[0]
                            link_data.link = f'https://sitena.s3.amazonaws.com/{s3_link_data}'
                            link_data.save()
                except:
                    pass
            else:
                pass
        #------------/ telegram updates /------------------
        for i in data:
            content = striptags(i.description)
            if i.image:
                def delay():
                    time.sleep(random.randint(3, 3))

                response = telegraph.create_page(i.title,
                                                 html_content='<img src="https://sitena.s3.amazonaws.com/{image}" alt="loading image" /><p>{content}</p><a href="https://legtech.in/{y}/{z}/LegalUpdates/">Click here for more details</a><br/>'.format(
                                                     content=content, image=i.image, y=i.id, z=i.slug))
                #print('https://telegra.ph/{}'.format(response['path']))
                delay()
                delay()

                parameters = {
                    "chat_id": "{chat_id}".format(chat_id=chat_id),  # bot
                    # "text": "{x}\n\nhttps://legtech.in/{y}/{z}/LegalUpdates/\n\nINSTANT VIEW\nhttps://telegra.ph/{p}".format(x=i.title, y=i.id, z=i.slug, p=response['path'])
                    "text": "{x}\n\nINSTANT VIEW\nhttps://telegra.ph/{p}".format(x=i.title, p=response['path'])

                }

                resp = requests.get(base_url, data=parameters)
                # print(resp.text)
                delay()
                LegalDetail.objects.filter(id=i.id).update(watcher_var=1)
                delay()

            else:
                def delay7():
                    time.sleep(random.randint(3, 3))

                response = telegraph.create_page(i.title,
                                                 html_content='<img src="https://sitena.s3.amazonaws.com/detailpage_images/For_Update.gif" alt="loading image" /><p>{content}</p><a href="https://legtech.in/{y}/{z}/LegalUpdates/">Click here for more details</a><br/>'.format(
                                                     content=content, y=i.id, z=i.slug))
                #print('https://telegra.ph/{}'.format(response['path']))
                delay7()
                delay7()

                parameters = {
                    "chat_id": "{chat_id}".format(chat_id=chat_id),  # bot
                    # "text": "{x}\n\nhttps://legtech.in/{y}/{z}/LegalUpdates/\n\nINSTANT VIEW\nhttps://telegra.ph/{p}".format(x=i.title, y=i.id, z=i.slug, p=response['path'])
                    "text": "{x}\n\nINSTANT VIEW\nhttps://telegra.ph/{p}".format(x=i.title, p=response['path'])

                }

                resp = requests.get(base_url, data=parameters)
                # print(resp.text)
                delay7()
                LegalDetail.objects.filter(id=i.id).update(watcher_var=1)
                delay7()

    except Exception as e:
        x = datetime.strftime(datetime.now() - timedelta(0), '%Y-%m-%d')
        parameters = {
            "chat_id": "{l_chat_id}".format(l_chat_id=l_chat_id),  # bot
            "text": "Date:--{d}\nERROR in Legal update Telegram Task\n\nException as e:--{a}".format(a=e, d=x)

        }
        resp = requests.get(l_base_url, data=parameters)

#========================================================================
