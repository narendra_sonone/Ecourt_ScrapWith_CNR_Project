from django.urls import path
from django.contrib.sitemaps.views import sitemap
from .sitemaps import LegalDetailSitemap

sitemaps = {'static': LegalDetailSitemap,}


urlpatterns = [
    path('', sitemap, {'sitemaps': sitemaps}),
]
