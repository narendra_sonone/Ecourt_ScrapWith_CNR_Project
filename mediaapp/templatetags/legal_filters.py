from django import template
register = template.Library()

@register.filter(name='characterlimit')
def character_limit(value):
    result = value[:80]
    return result

@register.filter(name='titlecharacterlimit')
def character_limit(value):
    result = value[:90]
    return result

# @register.filter(name='replaces')
# def character_limit(value):
#     value="&Nbsp;"
#     result = value.replace("&Nbsp;", "apples")
#
#     return result
