from django.urls import path
from . import views
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static




urlpatterns = [

    path('', views.IndexView),
    path('aboutus/',views.AboutView),
    path('LitigationManagement/', views.LitigationManagementView),
    path('ComplianceManagement/', views.ComplianceManagementView),
    path('LicenseRegistrationandTesting/', views.LicenseRegistrationandTestingView),
    path('Insidertrading/', views.InsidertradingView),
    path('LegalComplianceAudit/', views.LegalComplianceAuditView),
    path('ComplianceTrainingandSeminars/', views.ComplianceTrainingandSeminarsView),
    path('ComplianceUpdates/', views.ComplianceUpdatesView),
    path('LegalComplianceSystemAudit/', views.LegalComplianceSystemAuditView),
    path('Contactus/', views.ContactusView),
    # path('LegalDetailsPageView/', views.LegalDetailsPageView),

    # path('<slug:slug>/', views.LegalDetailsPageView.as_view()),
    # path('<slug:titles>/<slug:slug>/<int:pk>/',views.LegalDetailsPageView.as_view(),name='detail'),

    path('legalupdates/', views.LegalUpdatesView,name="Legal updates"),
    path('<int:id>/<slug:slug>/LegalUpdates/', views.LegalDetailsPageView, name='Legal-detail'), #main detail page url


    path('facebook/', views.facebookView),
    path('robots.txt/', views.robotsView),

    path('Privacy_Policy/', views.PrivacyPolicyView, name='Privacy_Policy'),
    path('Terms_of_service/', views.Terms_of_serviceView, name='Terms_of_service'),
    path('Refund_Cancellation_Policy/', views.RefundCancellationPolicyView, name='Refund_Cancellation_Policy'),

    # -------------------< Judgement application url's start >--------------------
    path('judgements/', views.JudgementsView, name="judgements"),
    path('<int:id>/<slug:slug>/Judgements/', views.JudgementsDetailsPageView, name='Judgements-detail'),



]+static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)

