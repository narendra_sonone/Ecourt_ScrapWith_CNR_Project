from django.shortcuts import render

from judgementsAPP.models import JudgementDetail
from legalapp.models import LegalDetail



# from legalapp import telegram_bot

def LegalDetailsPageView(request, id, slug):
    pg = LegalDetail.objects.get(id=id)

    context = {'title': pg.title,
               'image': pg.image,

               'data': LegalDetail.objects.filter(id=id, slug=slug),
               }
    return render(request, 'legalapp/legaldetail_detail.html', context)


def IndexView(request):
    data = LegalDetail.objects.all().order_by('-legalupdate_date', '-date')[0:3]  # & LegalDetail.objects.all().order_by('-date') , LegalDetail.objects.all().order_by('-id')&
    return render(request, 'mediaapp/index.html', {'data': data})


def LegalUpdatesView(request):
    data = LegalDetail.objects.all().order_by('-legalupdate_date', '-date')
    return render(request, 'mediaapp/legalupdates.html', {'data': data})


def AboutView(request):
    return render(request, 'mediaapp/aboutus.html')


def LitigationManagementView(request):
    return render(request, 'mediaapp/LitigationManagement.html')


def ComplianceManagementView(request):
    return render(request, 'mediaapp/ComplianceManagement.html')


def LicenseRegistrationandTestingView(request):
    return render(request, 'mediaapp/LicenseRegistrationandTesting.html')


def InsidertradingView(request):
    return render(request, 'mediaapp/Insidertrading.html')


def LegalComplianceAuditView(request):
    return render(request, 'mediaapp/LegalComplianceAudit.html')


def ComplianceUpdatesView(request):
    return render(request, 'mediaapp/ComplianceUpdates.html')


def LegalComplianceSystemAuditView(request):
    return render(request, 'mediaapp/LegalComplianceSystemAudit.html')


def ComplianceTrainingandSeminarsView(request):
    return render(request, 'mediaapp/ComplianceTrainingandSeminars.html')


def ContactusView(request):
    return render(request, 'mediaapp/Contactus.html')


def facebookView(request):
    return render(request, 'facebook/post_to_facebook.html')


def robotsView(request):
    return render(request, 'mediaapp/robots.txt')

def PrivacyPolicyView(request):
    return render(request, 'mediaapp/Privacy_Policys/Privacy_Policy.html')

def Terms_of_serviceView(request):
    return render(request, 'mediaapp/Privacy_Policys/Terms_of_service.html')

def RefundCancellationPolicyView(request):
    return render(request, 'mediaapp/Privacy_Policys/Refund_Cancellation_Policy.html')


# -------------------< Judgement application start >--------------------
# ----------------------------------------------------------------------
def JudgementsView(request):
    data = JudgementDetail.objects.all().order_by('-Judgement_date', '-date')
    return render(request, 'judgementsAPP/judgements.html', {'data': data})


def JudgementsDetailsPageView(request, id, slug):
    pg = JudgementDetail.objects.get(id=id)
    context = {'title': pg.title,
               'image': pg.image,
               'keywords': pg.keywords,

               'data': JudgementDetail.objects.filter(id=id, slug=slug),
               }
    return render(request, 'judgementsAPP/judgements_detail.html', context)